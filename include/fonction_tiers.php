﻿<?php

/**
 * fonction_tiers.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include ("include/fonction_general.php");


function liste_tiers()
{
	connectsql();
	if (isset($_POST['societe']))
	{
		if (!isset($critere))
		{
			$critere = "'SOCIETE'";}
		else
		{$critere .=",'SOCIETE'";}
	}
	if (isset($_POST['perso']))
	{
		if (!isset($critere))
		{$critere = "'PARTICULIER'";}
		else
		{$critere .=",'PARTICULIER'";}
	}

	?>
	<form id="formplaning" action="" method="post">
	<p>
		<?php
		if (isset($_POST['societe']))
		{
			?>Société : <input style="margin-right:20px;" type="checkbox" id="societe" name="societe" checked><?php
		}
		else
		{
			?>Société : <input style="margin-right:20px;" type="checkbox" id="societe" name="societe"><?php
		}
		if (isset($_POST['perso']))
		{
			?>Particulier : <input style="margin-right:20px;" type="checkbox" id="perso" name="perso" checked><?php
		}
		else
		{
			?>Particulier : <input style="margin-right:20px;" type="checkbox" id="perso" name="perso"><?php
		}
		if (isset($_POST['ferme']))
		{
			?>Fermé : <input style="margin-right:20px;" type="checkbox" id="ferme" name="ferme" checked><?php
		}
		else
		{
			?>Fermé : <input style="margin-right:20px;" type="checkbox" id="ferme" name="ferme"><?php
		}
		?>
		<input type="submit" class="bouton2" value="Rafraichir" />
		<input style="margin-left:20px;" type=button value="Nouveau Adhérant" class="bouton2" onclick="window.open('nouveau_tiers.php?phase=1', 'exemple', 'height=600, width=800, top=100, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
	</p>
	</form>

	<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td align="center" style="width:10%"> Société</td>
		<td align="center" style="width:10%"> Adresse 1</td>
		<td align="center" style="width:10%"> Adresse 2</td>
		<td align="center" style="width:5%"> Code Postal</td>
		<td align="center" style="width:10%"> Ville</td>
		<td align="center" style="width:5%"> Pays</td>
		<td align="center" style="width:10%"> téléphone</td>
		<td align="center" style="width:15%"> Email</td>
		<td align="center" style="width:15%"> </td>
	</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-color:white">
	<?php
	if (isset($critere))
	{
		$sql = "SELECT * FROM TIERS
			LEFT JOIN CHOIXCODE ON CC_TYPE = 'TYPE_TIERS' AND CC_CODE = T_TYPE
			WHERE T_TYPE in (" .$critere .");";

	}
	else
	{

		$sql = "SELECT * FROM TIERS
			LEFT JOIN CHOIXCODE ON CC_TYPE = 'TYPE_TIERS' AND CC_CODE = T_TYPE;";
	}

	$req = mysql_query($sql) or die("Requete pas comprise");

	while ($data = mysql_fetch_array($req))
		{
		?>
		<tr style="border-top:1px 1px solid #000">
		<td align="center" style="width:10%" color="red"><b><?php echo $data['T_NUMERO']; ?></b></td>
		<td align="center" style="width:10%"><?php echo $data['T_NOM']; ?></td>
		<td align="center" style="width:10%"><?php echo $data['T_CIVILITE']; ?></td>
		<td align="center" style="width:5%"><?php echo $data['T_EMAIL1']; ?></td>
		<td align="center" style="width:10%"><?php echo $data['T_VILLE']; ?></td>
		<td align="center" style="width:5%"><?php echo $data['T_PAYS']; ?></td>
		<td align="center" style="width:10%"><?php echo $data['T_TELEPHONE']; ?></td>
		<td align="center" style="width:15%"><?php echo $data['T_EMAIL1']; ?></td>
		<td align="center" style="width:15%"> </td>
		</tr>
		<?php
		}
	mysql_close;
	?>
	</table></center>

	<?php
}

function new_tiers()
{

echo $_POST['newloginstatut'];
	connectsql();
	if (isset($_POST['newusertype']))
	{
		?>
		<form id="formplaning" action="" method="post"  style="padding-top:0;">

		<p align="center">Création d'un nouvel adhérant</p>
		<p>Saisir le Nom : <input type="text" id="newusernom" name="newusernom" value="<?php echo $_POST['newusernom']; ?>" size="50" maxlength="50" required></p>
		<input type="hidden" name="newusertype" id="newusertype" value="<?php echo $_POST['newusertype']; ?>">
		<?php
		if ($_POST['newusertype'] == 'Particulier')
		{
			?>
			<p>Saisir le Prénom : <input type="text" id="newuserprenom" name="newuserprenom" size="50" maxlength="50" required></p>
			<label>Civilité : </label>
			<select name="newusercivilite">
				<option value="Monsieur">Monsieur</option>
				<option value="Madame">Madame</option>
			</select>
			<?php
		}
		else
		{
			?>
			<label>Type de société : </label>
			<select name="newusercivilite">
				<option value="SA">SA</option>
				<option value="SAS">SAS</option>
				<option value="SARL">SARL</option>
				<option value="ASSOCIATION">Association</option>
			</select>
			<?php
		}
		?>
		<p>Saisir l'adresse : <input type="text" id="newuseradresse1" name="newuseradresse1" size="50" maxlength="50" required></p>
		<p style="margin-top:2px;">Saisir le complément d'adresse : <input type="text" id="newuseradresse2" name="newuseradresse2" size="50" maxlength="50"></p>
		<p>Saisir la ville : <input type="text" id="newuserville" name="newuserville" size="50" maxlength="50" required></p>
		<p>Saisir le code postal : <input type="text" id="newusercodepostal" name="newusercodepostal" size="5" maxlength="5" required></p>
		<p>Saisir le téléphone : <input type="text" id="newuserphone" name="newuserphone" size="15" maxlength="15" required></p>
		<p>Saisir l'email : <input type="text" id="newuseremail" name="newuseremail" size="50" maxlength="250" required></p>
		<label>Pays : </label>
			<select name="newuserpays">
				<option value="FRANCE">FRANCE</option>
				<option value="BELGIQUE">BELGIQUE</option>
				<option value="ANGLETERRE">ANGLETERRE</option>
				<option value="LUXEMBOURG">LUXEMBOURG</option>
			</select>
		<p><label>Statut : </label>
			<select name="newuserstatut">
				<option value="COWORKER">CoWorker</option>
				<option value="PERMANANT">Permanant</option>
				<option value="PROSPECTION">Prospection</option>

			</select>
		<p>Payeur : <input style="margin-right:20px;" type="checkbox" id="newuserpayeur" name="newuserpayeur"></p>
		<p align="left">Date d'adhésion : <input name="DateDebut" id="datepicker" type="text" size="28" size="12" required/></p>


		<br /><br />
		<input align="center" type="submit" class="bouton2" value="Valider" />
		</form>
		<?php
	}
	else
	{
		?>
		<form id="formplaning" action="" method="post" width="30%">

		<p align="center">Création d'un nouvel adhérant</p>
		<p>Saisir le Nom : <input type="text" id="newusernom" name="newusernom"  size="50" maxlength="50" required></p>
		<label>Type d'adhérant : </label>
		<select name="newusertype">
			<option value="societe">Société</option>
			<option value="Particulier">Particulier</option>
		</select>
		<input type="hidden" name="idvalid" value="<?php uniqid('', true); ?>">
		<br /><br />
		<input align="center" type="submit" class="bouton2" value="Valider" />
		</form>
		<?php
	}


}

function liste_modepaie()
{
	connectsql();
	if ($_POST['user'] != "Tous CoWorker")
	{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="1" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td align="center" bgcolor="#1B9E6F" style="width:15%"> Code</td>
		<td align="center" bgcolor="#1B9E6F" style="width:20%"> Libéllé</td>
	</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%">
	<?php
	$sql = "SELECT * FROM MODEPAIE;";

	$req = mysql_query($sql) or die("Requete pas comprise");

	while ($data = mysql_fetch_array($req))
		{
		?>
		<tr>
		<td align="left" style="width:15%"><?php echo $data['MO_MODEPAIE']; ?></td>
		<td align="left" style="width:20%"><b><?php echo $data['MO_LIBELLE']; ?></b></a></td>
		</tr>
		<?php
		}
	mysql_close;
	?>
	</table></center>

	<?php
}

function saisie_payment()
{
	if(isset($_GET["RefFacture"]))
	{
		connectsql();
		$sql = "SELECT * FROM PIECE WHERE GP_REFFACTURE = '" .$_GET["RefFacture"] ."';";
		$req = mysql_query($sql) or die("Requete pas comprise");
		?>
		<center><div id="support"><table border="1" cellpadding="2" cellspacing="0" width="100%">
		<tr>
		<?php
			while ($data = mysql_fetch_array($req))
			{
			?>
			<td align="left" bgcolor="#1B9E6F" style="border: none">Référence COWORKER : <b><?php echo $data['GP_USER']; ?></b></td>
			<td align="left" bgcolor="#1B9E6F" style="border: none" colspan=2>Nom COWORKER : <b><?php echo $data['GP_USERNOM'] .' ' .$data['GP_USERPRENOM']; ?></b></td></tr>
			<tr><td align="left" bgcolor="#1B9E6F" style="border: none"colspan=3>Référence Facture : <b><?php echo $data['GP_REFFACTURE']; ?></b></td></tr>
			<td align="left" bgcolor="#1B9E6F" style="border: none" colspan=3>Montant HT de la facture : <b><?php echo $data['GP_TOTALHT'] . ' €'; ?><b></td></tr>


		<?php
			}
		?>
		</table></center>
		<br>
		<div id="formplaning">
		<form class="formplaning" action="" method="post">
		<input type="hidden" value="<?php echo $_GET['RefFacture']; ?>" name="RefFacture">

		<?php
		// Affichage de la catégorie d'appel
		$sql = "SELECT * FROM MODEPAIE;";
		$req = mysql_query($sql) or die("Requete pas comprise");
		?>
		<br />

		<label>Mode de paiement  </label>
		<div id='CategorieCall' style='display:inline'>
		<select id="test" onChange="JAVASCRIPT:menuToSpan(this.options[this.selectedIndex]);">
		<option>Selectionner un mode de paiement</option>
		<?php
			while ($data = mysql_fetch_array($req)) {
		?>
			<option value="<?php echo $data['MO_MODEPAIE'];  ?>"><?php echo $data['MO_LIBELLE'];  ?></option>
		<?php
			} // fin while
		// liberation ressource
			mysql_free_result ($req);
		?>
		</select>
		</div>
		<
		<input type="text" name="Payment"  size="35" id="targetOption" required></p>
		<p>Date: <input name="DatePayment" id="datepicker" type="text" size="10" value="<?php echo date("d/m/Y"); ?>" size="12" required/></p>
		<p>Référence du paiement : <input type="text" name="RefPayment" required size="50"></p>
		<br />
		<center><input class="bouton2" type="submit" value="Valider" /></center>
		</form>
		<?php
		// fin connexion
		mysql_close();

	}
}

function valid_payment()
{
	connectsql();
	echo $_POST['Payment'];
	$sql = "SELECT * FROM MODEPAIE WHERE MO_LIBELLE = '" .$_POST['Payment'] ."';";
	$req = mysql_query($sql) or die("Requete pas comprise");
	while ($data = mysql_fetch_array($req))
	{
		$Modepaie = $data['MO_MODEPAIE'];
	}
	mysql_free_result ($req);

	$sql = "UPDATE PIECE SET GP_MODEPAIE = '" .$Modepaie ."', GP_REFPAYMENT = '".addslashes($_POST['RefPayment']) ."',
			GP_DATEPAYEMENT = STR_TO_DATE('" .$_POST['DatePayment']. "', '%d/%m/%Y %H:%i'), GP_PAYEMENT = 'OUI'
			WHERE GP_REFFACTURE = '" . $_POST['RefFacture'] ."';";
	$req = mysql_query($sql) or die("Requete pas comprise");
	echo $_POST['test'];
}

function popup_rapport_facture()
{
	$date = new DateTime();
	$date->modify('-1 month');
	?>
	<form action="" method="post">
	<div id="facture" style="width:40%;">
	<input name="action" type="hidden" value="RAPPORTFACTURE">
	<p align="center">sélecetion de la recherche</p>
	<p align="left">Date début: <input name="DateDebut" id="datepicker" type="text" size="10" value="<?php echo $date->format('d/m/Y'); ?>" size="12" required/></p>
	<p align="left">Date de fin: <input name="DateFin" id="datepicker1" type="text" size="10" value="<?php echo date("d/m/Y"); ?>" size="12" required/></p>
	<br>
	<p align="left">Facture payé    <input type="checkbox" id="FacturePayer" name="FacturePayer" checked></p>
	<p align="left">Facture non payé    <input type="checkbox" id="FactureNoPayer" name="FactureNoPayer"></p>
	<center><input type="submit" value="Valider" /></center>
	<br>
	</form>
	</div>

	<?php
	// fin connexion
}

function rapport_facture()
{
	require('../include/fpdf.php');
	define('EURO', chr(128));


	echo "1";

}
