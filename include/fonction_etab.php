﻿<?php
/**
 * function_etab.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include ("include/fonction_general.php");
//require ("scripts/constantes.php");
//require ("scripts/fonctions.php");




function insert_new_place()
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = " select * from ZONE inner join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT
	where EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN'] ."' AND EM_EMPLACEMENT = '" .$_POST['ZONE'] ."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while ($data = mysqli_fetch_array($req))
	{
		$ET_LIBELLE = $data['ET_LIBELLE'];
		$ET_ADRESSE1 = $data['ET_ADRESSE1'];
		$ET_ADRESSE2 = $data['ET_ADRESSE2'];
		$ET_ADRESSE3 = $data['ET_ADRESSE3'];
		$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];
		$ET_VILLE = $data['ET_VILLE'];
		$EM_EMPLACEMENT = $data['EM_EMPLACEMENT'];
		$EM_LIBELLE = $data['EM_LIBELLE'];
	}

	$sql = "insert into DISPONIBLE (DI_LIBELLE, DI_COMMENTAIRE, DI_NOMBREPLACE, DI_PRIX, DI_PRIXPLACE) VALUES ";
	$sql = $sql ."('".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE'] ;
	$sql = $sql ."," .$_POST['PRIX'] ."," .$_POST['PRIXPARPERSONNE'] .")";

	$sql = "INSERT INTO `DISPONIBLE`(`DI_ETABLISSEMENT`, `DI_ETADRESSE1`, `DI_ETADRESSE2`, `DI_ETADRESSE3`,
			`DI_ETCODEPOSTAL`, `DI_ETVILLE`, `DI_ZONE`, `DI_LIBELLEZONE`, `DI_LIBELLE`, `DI_COMMENTAIRE`, `DI_NOMBREPLACE`, `DI_PRIX`, `DI_PRIXPLACE`,
			`Di_TYPEJOUR`) VALUES
			('".$_SESSION['ETABADMIN'] ."','".$ET_ADRESSE1."','".$ET_ADRESSE2."','".$ET_ADRESSE3."','".$ET_CODEPOSTAL."','".$ET_VILLE."','".$_POST['ZONE']."',
			'".$EM_LIBELLE."','".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE']."," .$_POST['PRIX'] .","
			.$_POST['PRIXPARPERSONNE'] .",".$_POST['TEMPSLOCATION'].")";

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	echo "Création de l'emplacement " .$_POST['LIBELLE'] ."terminé";

	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function insert_new_zone()
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "INSERT INTO `zone`(`EM_ETABLISSEMENT`, `EM_LIBELLE`, `EM_NBRRESAMAX`, `EM_TYPEZONE`, `EM_NBRPLACESMAX`, `EM_PRIXPARZONE`, `EM_PRIXPARPLACE`, EM_HEUREDEBUT, EM_HEUREFIN) VALUES (
			'".$_SESSION['ETABADMIN'] ."','".$_POST['LIBELLE'] ."',".$_POST['NOMBREZONE'] .",'" .$_POST['TYPEZONE'] ."',".$_POST['NOMBREPERSONNE'].",'" .$_POST['PRIX'] ."',
			'" .$_POST['PRIXPARPERSONNE'] ."','".$_POST['heuredebut']."','".$_POST['heurefin']."') ";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	echo "Création de la zone " .$_POST['LIBELLE'] ." terminée!";

	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function new_zone()
{
?>
	<br />
<form  action="" method="post">
<table style='text-align: left; width: 474px; height: 281px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Création d'un emplacement</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Nom de l'emplacement</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" required></td>
			</tr>
				<td style="width: 304px; height: 28px;"><label>Type de zone : </label></td>
			<td>
				<select name="TYPEZONE">
					<option value="COWORKING">Coworking</option>
					<option value="SALLE REUNION">Salle de réunion</option>
				</select>
			</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Nombre d'emplacement (maximum)</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="4" name="NOMBREZONE" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nombre de personne par emplacement (maximum)</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="4" name="NOMBREPERSONNE" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix par emplacement</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="3" name="PRIX" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix par personne (uniquement si plus de 1 personne)</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" name="PRIXPARPERSONNE" value=0></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Heure de début</td>
			  <td style="height: 28px; width: 212px;"><input type="time" id="heuredebut" name="heuredebut" value='09:00'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Heure de fin</td>
			  <td style="height: 28px; width: 212px;"><input type="time" id="heurefin" name="heurefin" value='18:00'></td>
			</tr>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
<input type="hidden" value="AJOUT" name="action">
</form>
<?php
}

function delete_zone()
{

	if(!isset($_POST["action"]))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from ZONE WHERE EM_EMPLACEMENT ='" .$_GET['numero'] ."' AND EM_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']. "'" ;

		$req = $conn->query($sql) or die('Erreur SQL !<br>');

		while($data = mysqli_fetch_array($req))
		{
?>
		<br />
		<form  action="" method="post">
		<table style="text-align: left; width: 474px; height: 281px;" border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Suppression d'une zone</td>
			<tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nom de la zone</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['EM_EMPLACEMENT']; ?></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de la zone</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['EM_LIBELLE']; ?></td>
			</tr>

			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Supprimer</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="DELETE" name="action">
		</form>

<?php
		mysqli_close;
		}
	}
	else
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "DELETE from ZONE WHERE EM_EMPLACEMENT = '" .$_GET['numero']. "'";

		$req = $conn->query($sql) or die('Erreur SQL !<br>');


		echo "Suppression de la zone " .$_POST['LIBELLE'] ." terminée !";

		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}



function modif_etablissement_old($etablissement)
{
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{

?>
		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 100%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="6" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification d'un établissement</td>
			<tr>
				<td style="width: 304px; height: 28px;">Code de l'établissement</td>
				<td style="height: 28px; width: 212px;"><input disabled maxlength="30" size="30" tabindex="1" name="" value='<?php echo $data['ET_ETABLISSEMENT']; ?>'></td>
				<td style="width: 304px; height: 28px;">Nom de l'établissement</td>
				<td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" value='<?php echo $data['ET_LIBELLE']; ?>'></td>
			</tr>
			
			<tr>
			  <td style="width: 304px; height: 28px;">Adresse : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="2" name="ADRESSE1" value='<?php echo $data['ET_ADRESSE1']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Adresse (suite) : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="3" name="ADRESSE2" value='<?php echo $data['ET_ADRESSE2']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Code Postal : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="CPOSTAL" value='<?php echo $data['ET_CODEPOSTAL']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Ville : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="5" name="VILLE" value='<?php echo $data['ET_VILLE']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Email : </td>
			  <td colspan="1" style="height: 28px; "><input maxlength="250" size="250" tabindex="6" name="EMAIL1" value='<?php echo $data['ET_EMAIL']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Site WEB : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="250" size="250" tabindex="7" name="SITEWEB" value='<?php echo $data['ET_SITEWEB']; ?>'></td>
			</tr>
			<tr><td colspan="4"><u>Information Banquaire</u></td>   </tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nom de la banque: </td>
			  <td colspan="3" style="height: 28px; "><input maxlength="250" size="250" tabindex="8" name="BNQNOM" value='<?php echo $data['ET_BANQUENOM']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">IBAN : </td>
			  <td style="height: 28px; "><input maxlength="250" size="250" tabindex="9" name="IBAN" value='<?php echo $data['ET_IBAN']; ?>'></td>
			  <td style="width: 304px; height: 28px;">BIC : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="10" size="10" tabindex="10" name="BNQBIC" value='<?php echo $data['ET_BANQUEBIC']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Code Banque : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="12" name="BNQCODE" value='<?php echo $data['ET_BANQUECODE']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Guichet Banque : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="13" name="BNQGUICHET" value='<?php echo $data['ET_BANQUEGUICHET']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Compte Banquaire : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="10" size="5" tabindex="14" name="BNQCPT" value='<?php echo $data['ET_BANQUECPT']; ?>'></td>
			</tr>
			<tr>
			  
			  <td style="width: 304px; height: 28px;">Clé Banqque : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="15" name="BNQCLEF" value='<?php echo $data['ET_BANQUECLEF']; ?>'></td>
			</tr>
			<tr><td colspan="4"><u>Siret</u></td>   </tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Siret : </td>
			  <td style="height: 28px; "><input maxlength="20" size="20" tabindex="16" name="SIRET" value='<?php echo $data['ET_SIRET']; ?>'></td>
			  <td style="width: 304px; height: 28px;">NAF-APE : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="17" name="NAP" value='<?php echo $data['ET_NAP']; ?>'></td>
			</tr>
			<tr><td colspan="4"><u>Paramétrage facturation	</u></td>   </tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nombre de jour échéance : </td>
			  <td style="height: 28px; "><input size="2" maxlength="2" size="2" tabindex="18" name="JOURECHEANCE" value='<?php echo $data['ET_ECHEANCEFACTURE']; ?>'></td>
			  <!--<td style="width: 304px; height: 28px;">NAF-APE : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="17" name="NAP" value='<?php echo $data['ET_NAP']; ?>'></td> -->
			</tr>
			<tr><td colspan="4"><u>Paramétrage adhésion	</u></td>   </tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Type d'adhésion : </td>
			  <td style="height: 28px; "><select align="left" name="TYPEADHESION" id="TYPEADHESION">
										<option value="GLISSANT">Année glissante</option>
										<option value="ANNEEENCOUR">Année en court</option>
			  </td>
			  <td style="width: 304px; height: 28px;">Code article adhésion : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="10" size="10" tabindex="19" name="ARTADHESION" value='<?php echo $data['ET_ARTADHESION']; ?>'></td>
			</tr>
			<tr><td colspan="4"><u>Paramétrage client	</u></td>   </tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Porte monnaie électronique : </td>
			  <td style="height: 28px; width: 212px;"><SELECT name="MONNAIEART">
					<?php
					echo '<OPTION value="">Vide ...</option>';
					$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTFI' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
					
						if ($data1['AR_CODEARTICLE'] == $data['ET_ARTMONNAIE'])
						{	
							echo '<OPTION selected="selected" value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION'].'</option>';
						}
						else
						{
							echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION'].'</option>';
						}
					}
					?>
					</SELECT>
				</td>
			  
			  <!--<td style="width: 304px; height: 28px;">NAF-APE : </td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="17" name="NAP" value='<?php echo $data['ET_NAP']; ?>'></td> -->
			</tr>
			<tr><td colspan="4"><u>Logo	</u></td>   </tr>
			<tr>
				<td style="width: 304px; height: 28px;">Grande image de l'établissement</td>
				<td style="height: 28px; width: 212px;">
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input type="file" name="fic" size=50 />
				</td>
				<td align="left" colspan="2" style="width: 304px; height: 28px;">
					<img src="img/<?php echo $data['ET_IMAGENOM']; ?>">
				</td>
				<td style="width: 304px; height: 28px;">Petite image de l'établissement</td>
				<td style="height: 28px; width: 212px;">
					
					<input type="file" name="petitlogo" size=50 />
				</td>
				<td align="left" colspan="1" style="width: 304px; height: 28px;">
					<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>">
				</td>
				
			</tr>
			<tr align="center">
			  <td colspan="6" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" tabindex="20" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				$img_nom = $data['ET_IMAGENOM'];
				$smallimagenom = $data['ET_PETITEIMAGENOM'];
			}
			$ret        = false;
			$img_blob   = '';
			$img_taille = 0;
			
			$taille_max = 250000;
			
			if(!empty($_FILES['fic']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['fic']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['fic']['name'];
					if (rename($_FILES['fic']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					
					$img_type = $_FILES['fic']['type'];
					$img_nom  = $_FILES['fic']['name'];
				}
			}
			
			if(!empty($_FILES['petitlogo']['name']))
			{
				
				$ret = false;
				$ret        = is_uploaded_file($_FILES['petitlogo']['tmp_name']);
				echo $ret1;
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['petitlogo']['name'];
					

					if (rename($_FILES['petitlogo']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['petitlogo']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
									
					$smallimagenom  = $nomDestination;
				}
			}
			//$img_blob = file_get_contents ($_FILES['fic']['tmp_name']);
			$sql = "UPDATE ETABLISSEMENT SET ET_LIBELLE = '" .$_POST['LIBELLE'] ."', ET_ADRESSE1 = '" .$_POST['ADRESSE1'] ."',ET_ADRESSE2 = '" .$_POST['ADRESSE2'] ."',
					ET_CODEPOSTAL = " .$_POST['CPOSTAL'] .", ET_VILLE = '" .$_POST['VILLE'] ."', ET_EMAIL = '" .$_POST['EMAIL1'] ."', ET_IBAN = '" .$_POST['IBAN'] ."',
					ET_BANQUECODE = '" .$_POST['BNQCODE'] ."', ET_BANQUEGUICHET = '" .$_POST['BNQGUICHET'] ."', ET_BANQUECPT = '" .$_POST['BNQCPT'] ."', ET_BANQUECLEF = '" .$_POST['BNQCLEF'] ."',
					ET_BANQUENOM = '".$_POST['BNQNOM']."', ET_BANQUEBIC = '" .$_POST['BNQBIC'] ."', ET_SIRET = '" .$_POST['SIRET'] ."', ET_NAP = '" .$_POST['NAP'] ."', ET_ECHEANCEFACTURE = '" .$_POST['JOURECHEANCE'] ."'
					, ET_SITEWEB = '" .$_POST['SITEWEB'] ."', ET_TYPEADHESION = '".$_POST['TYPEADHESION']."', ET_ARTMONNAIE = '".$_POST['MONNAIEART']."', ET_IMAGENOM = '" . $img_nom . "', 
					ET_PETITEIMAGENOM = '".$smallimagenom."', ET_ARTADHESION = '".$_POST['ARTADHESION']."'
					WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
			
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
		}
		?>
		<form  action="" method="post">
		<input type="hidden" value="AFFICHE" name="action">
		<button value="Valid" name="Valid">Valider</button>
		</form> 
		<?php
	}
}

function liste_etablissement()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
		?>

		<body>
		<center><div id="support1">
		<table  style=""width: 80%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">

		<tbody>
			<tr>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 150px;">Etablissement</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 250px;">Nom</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 250px;">Ville</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 250px;">Bloqué</td>
				
				<td colspan="2" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);">
				<span style="font-family: Calibri;"><br></span>
				</td>
			</tr>
		
		<?php
		//$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "SELECT * FROM ETABLISSEMENT ORDER BY ET_ETABLISSEMENT;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			?>
			<tr>
			<td style="text-align: left; width: 150px;"><b><?php echo $data['ET_ETABLISSEMENT']; ?></b></td>
			<td style="text-align: left; width: 250px;"><?php echo $data['ET_LIBELLE']; ?></td>
			<td style="text-align: left; width: 250px;"><?php echo $data['ET_VILLE']; ?></td>
			<td style="text-align: center; width: 250px;"><?php echo $data['ET_BLOQUE']; ?></td>
			<?php 
			if ($data['ET_BLOQUE'] == 'OUI')
			{
				?>
				<td style="width: 30px;"><img border="0" src="img/yes.png" width="25" height="25" title="Débloquer l'établissement !" onclick="window.open('manageetab.php?ACTION=DEBLOQUER&ETABLISSEMENT=<?php echo $data['ET_ETABLISSEMENT']; ?>', 'exemple', 'height=100, width=400, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
				<?php
			}
			else
			{
				?>
				<td style="width: 30px;"><img border="0" src="img/stop.png" width="25" height="25" title="Bloquer l'établissement !" onclick="window.open('manageetab.php?ACTION=BLOQUER&ETABLISSEMENT=<?php echo $data['ET_ETABLISSEMENT']; ?>', 'exemple', 'height=100, width=400, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
				<?php
			}
			?>
			</tr>

		<?php
		}

		?>
		<tr align="center" ><td width="800" colspan="9" rowspan="1">
			<table border = "0" cellpadding="15">
				<tr><td align="center" ><a href="adminscription.php"><input type=button value="Ajouter un établissement" class="bouton1" </td></tr>
			</table>
			</td></tr>
		</table></center>
	<?php
	}

}

function bloque_etablissement($etablissement)
{
	$sql = "UPDATE ETABLISSEMENT SET ET_BLOQUE = 'OUI' WHERE ET_ETABLISSEMENT = '".$etablissement."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);
	
	echo "L'établissement " .$etablissement." a été bloqué";
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function debloque_etablissement($etablissement)
{
	$sql = "UPDATE ETABLISSEMENT SET ET_BLOQUE = 'NON' WHERE ET_ETABLISSEMENT = '".$etablissement."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);
	
	echo "L'établissement " .$etablissement." a été débloqué";
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function modif_etablissement($etablissement)
{
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{

?>
		
	

		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 90%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif; padding-left: 20px; padding-right: 20px;' border="0" cellpadding="0" cellspacing="0">
		  <tbody>
			<tr>
			  <td colspan="6" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification d'un établissement</td>
			</tr>
			<tr><td colspan="6" style="height: 10px;"/></tr>	
			<tr>
				<td style="width: 200px; height: 20px;">Code de l'établissement</td>
				<td style="height: 20px; width: 212px;"><input disabled maxlength="30" size="30" tabindex="1" name="" value='<?php echo $data['ET_ETABLISSEMENT']; ?>'></td>
				<td style="width: 150px; height: 20px;">Nom de l'établissement</td>
				<td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" value='<?php echo $data['ET_LIBELLE']; ?>' required></td>
			</tr>
			
			<tr>
			  <td style="width: 200px; height: 20px;">Adresse : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="2" name="ADRESSE1" value='<?php echo $data['ET_ADRESSE1']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Adresse (suite) : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="3" name="ADRESSE2" value='<?php echo $data['ET_ADRESSE2']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 200px; height: 20px;">Code Postal : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="CPOSTAL" value='<?php echo $data['ET_CODEPOSTAL']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Ville : </td>
			  <td style="height: 20px; width: 20px;"><input maxlength="30" size="30" tabindex="5" name="VILLE" value='<?php echo $data['ET_VILLE']; ?>' required></td>
			</tr>
			<tr>
			  <td style="width: 200px; height: 20px;">Email : </td>
			  <td colspan="1" style="height: 20px; "><input maxlength="250" size="250" tabindex="6" name="EMAIL1" value='<?php echo $data['ET_EMAIL']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Site WEB : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="250" size="250" tabindex="7" name="SITEWEB" value='<?php echo $data['ET_SITEWEB']; ?>' required></td>
			</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>
			
			
			
			<tr>
			  <td style="width: 200px; height: 20px;">Gestion de la facturation : </td>
			  <td style="height: 20px; width: 212px;"><SELECT name="GESTION_FACTURATION" id="GESTION_FACTURATION" style="width: 60px;">
			  <?php
			  if ($data['ET_FACTURATION'] == 'OUI')
			  {
				  echo '<OPTION VALUE="OUI" selected="selected">OUI</OPTION>';
				  echo '<OPTION VALUE="NON">NON</OPTION>';
			  }
			  else
			  {
				  echo '<OPTION VALUE="NON" selected="selected">NON</OPTION>';
				  echo '<OPTION VALUE="OUI">OUI</OPTION>';
			  }
			  ?>
			  </SELECT></TD>
			  
			  <!--<td style="align:right;width: 30px;"><img id="bouton1" border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('manageetab.php?ACTION=MODIFBANQUE&ETABLISSEMENT=<?php echo $data['ET_ETABLISSEMENT']; ?>', 'exemple', 'height=600, width=1000, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>-->
			  <td colspan="2" align="left" style="align: center;width: 30px;"><span class="bouton1" id="btn_parambnq" onclick="javascript:cacher('parambnq');">Afficher le paramétrage facturation</span></td>
			  
			</tr>
			
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_1" style="width: 200px; height: 20px;">Nom de la banque: </td>
			  <td id="tdparambnq_2" style="height: 20px; width: 212px;"><input id="parambnq_1" maxlength="50" size="50" tabindex="8" name="BNQNOM" value='<?php echo $data['ET_BANQUENOM']; ?>' required></td>
			  <td colspan="2"/>
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_3" style="width: 200px; height: 20px;">IBAN : </td>
			  <td id="tdparambnq_4" style="height: 20px; "><input id="parambnq_2" maxlength="30" size="30" tabindex="9" name="IBAN" value='<?php echo $data['ET_IBAN']; ?>' required></td>
			  <td id="tdparambnq_5" style="width: 240px; height: 20px;">BIC : </td>
			  <td id="tdparambnq_6"><input id="parambnq_3" maxlength="10" size="10" tabindex="10" name="BNQBIC" value='<?php echo $data['ET_BANQUEBIC']; ?>' required></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_7" style="width: 200px; height: 20px;">Code Banque : </td>
			  <td id="tdparambnq_8"><input id="parambnq_4" maxlength="10" size="10" tabindex="12" name="BNQCODE" value='<?php echo $data['ET_BANQUECODE']; ?>' required></td>
			  <td id="tdparambnq_9" style="width: 240px; height: 20px;">Guichet Banque : </td>
			  <td id="tdparambnq_10"><input id="parambnq_5" maxlength="10" size="10" tabindex="13" name="BNQGUICHET" value='<?php echo $data['ET_BANQUEGUICHET']; ?>' required></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_11" style="width: 200px; height: 20px;">Compte Banquaire : </td>
			  <td id="tdparambnq_12"><input id="parambnq_6" maxlength="15" size="15" tabindex="14" name="BNQCPT" value='<?php echo $data['ET_BANQUECPT']; ?>' required></td> 
			  <td id="tdparambnq_13" style="width: 240px; height: 20px;">Clé Banque : </td>
			  <td id="tdparambnq_14"><input id="parambnq_7" maxlength="2" size="2" tabindex="15" name="BNQCLEF" value='<?php echo $data['ET_BANQUECLEF']; ?>' required></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparambnq_15" colspan="4"><u>Siret</u></td>   
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_16" style="width: 200px; height: 20px;">Siret : </td>
			  <td id="tdparambnq_17"><input id="parambnq_8" maxlength="20" size="20" tabindex="16" name="SIRET" value='<?php echo $data['ET_SIRET']; ?>' required></td>
			  <td id="tdparambnq_18" style="width: 240px; height: 20px;">NAF-APE : </td>
			  <td id="tdparambnq_19"><input id="parambnq_9" maxlength="5" size="5" tabindex="17" name="NAP" value='<?php echo $data['ET_NAP']; ?>' required></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparambnq_20" colspan="4"><u>Paramétrage facturation	</u></td>   
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_21" colspan="2" style="width: 200px; height: 20px;">Nombre de jour échéance : </td>
			  <td id="tdparambnq_22" colspan="2"><input id="parambnq_10" size="2" maxlength="2" size="2" tabindex="18" name="JOURECHEANCE" value='<?php echo $data['ET_ECHEANCEFACTURE']; ?>' required></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparambnq_23" colspan="4"><u>Paramétrage édition facture	</u></td>   
			</tr>
			<tr style="background-color: #edf1f6; ">
			  <td id="tdparambnq_24" style="width: 200px; height: 20px;">Texte pied de facture : </td>
			  <td id="tdparambnq_25" colspan="3" style="align: center;"><TEXTAREA style="width: 600px;" id="parambnq_11" name="PIEDFACTURE" rows="6" cols="200"><?php echo $data['ET_PIEDFACTURE']; ?> </TEXTAREA></td>
			</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>
			<tr >
			  <td style="width: 200px; height: 20px;">Gestion des adhésions : </td>
			  <td style="height: 20px; width: 212px;"><SELECT name="GESTION_ADHESION" id="GESTION_ADHESION" style="width: 60px;">
			  <?php
			  if ($data['ET_GESTIONADHESION'] == 'OUI')
			  {
				  echo '<OPTION VALUE="OUI" selected="selected">OUI</OPTION>';
				  echo '<OPTION VALUE="NON">NON</OPTION>';
			  }
			  else
			  {
				  echo '<OPTION VALUE="NON" selected="selected">NON</OPTION>';
				  echo '<OPTION VALUE="OUI">OUI</OPTION>';
			  }
			  ?>
			  </SELECT></TD>
			 
			  <td colspan="2" align="left" style="width: 30px;"><span class="bouton1" id="btn_paramadh" onclick="javascript:cacher('paramadh');">Afficher le paramétrage des adhésion</span></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_1" style="width: 304px; height: 28px;">Type d'adhésion : </td>
					<td id="tdparamadh_2" colspan="3"style="height: 28px; "><select id="paramadh_1" align="left" name="TYPEADHESION" id="TYPEADHESION" required>
											<option value="GLISSANT">Année glissante</option>
											<option value="ANNEEENCOUR">Année en court</option>
					</td>
					
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_3" style="width: 304px; height: 28px;">Article d'adhésion : </td>
					<td id="tdparamadh_4" colspan="3" style="height: 28px; "><select id="paramadh_2" align="left" name="ARTADHESION" id="ARTADHESION" style="width: 300px;" required>
					<?php
					$sql = "SELECT 'SELECTED' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
							FROM ARTICLE
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
							WHERE AR_ETABLISSEMENT = '".$etablissement."'
								AND AR_TYPE = 'ARTAD' AND AR_CODEARTICLE = ET_ARTADHESION
							UNION ALL
							SELECT '' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
							FROM ARTICLE
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
							WHERE AR_ETABLISSEMENT = '".$etablissement."'
							AND AR_TYPE = 'ARTAD' AND AR_CODEARTICLE <> ET_ARTADHESION
							ORDER BY SELECTED DESC;";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						echo '<option value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION']. ' - ' .$data1['AR_TARIF'].' €</option>';
					}
					?>
					
				</tr>
				<tr style="background-color: #edf1f6; "><td id="tdparamadh_5" colspan="4"></td></tr>
				<tr style="background-color: #edf1f6; "><td id="tdparamadh_6" colspan="4"><u>Paramétrage gestion adhésion	</u></td>   </tr>
				<tr style="background-color: #edf1f6; ">
				  <td id="tdparamadh_7" style="width: 240px; height: 28px;">Texte coche 1 : </td>
				  <td  id="tdparamadh_8" colspan="3" style="align: center;"><TEXTAREA id="paramadh_3" name="TEXTCOCHE1" rows="6" cols="85" style="width: 600px;" required disabled><?php echo $data['ET_ADHESIONCOCHE1']; ?> </TEXTAREA></td>
				</tr>
				<tr style="background-color: #edf1f6; ">
				  <td id="tdparamadh_9" style="width: 240px; height: 28px;">Texte coche 2 : </td>
				  <td id="tdparamadh_10" colspan="3" style="align: center;"><TEXTAREA id="paramadh_4" name="TEXTCOCHE2" rows="6" cols="85" style="width: 600px;" required disabled><?php echo $data['ET_ADHESIONCOCHE2']; ?> </TEXTAREA></td>
				</tr>
				<tr style="background-color: #edf1f6; ">
				  <td id="tdparamadh_11" style="width: 240px; height: 28px;">Texte RGPD: </td>
				  <td id="tdparamadh_12" colspan="3" style="align: center;"><TEXTAREA id="paramadh_5" name="TEXTRGPD1" rows="6" cols="85" style="width: 600px;" required disabled><?php echo $data['ET_ADHESIONRGPD1']; ?> </TEXTAREA></td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_13" style="width: 304px; height: 28px;">Condition générale d'utilisation</td>
					<td id="tdparamadh_14" style="height: 28px; width: 212px;">
						
						<input type="file" name="ficcgu" size=50 />
					</td id="tdparamadh_15">
					<?php
					if (!empty($data['ET_FICHIERCGU']))
					{
						?>
					<td id="tdparamadh_16"  align="left" colspan="2" style="width: 304px; height: 28px;">
						<a title="" href="document/<?php echo $etablissement .'/' .$data['ET_FICHIERCGU']; ?>">Condition générale d'utilisation</a>
					</td>
					<?php
					}
					?>
				</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>	
			<tr >
			  <td style="width: 200px; height: 20px;">Gestion crédit client : </td>
			  <td style="height: 20px; width: 212px;"><SELECT name="GESTION_CREDITCLI" id="GESTION_CREDITCLI" style="width: 60px;">
			  <?php
			  if ($data['ET_GESTIONCREDITCLIENT'] == 'OUI')
			  {
				  echo '<OPTION VALUE="OUI" selected="selected">OUI</OPTION>';
				  echo '<OPTION VALUE="NON">NON</OPTION>';
			  }
			  else
			  {
				  echo '<OPTION VALUE="NON" selected="selected">NON</OPTION>';
				  echo '<OPTION VALUE="OUI">OUI</OPTION>';
			  }
			  ?>
			  </SELECT></TD>
			 
			  <td colspan="2" align="left" style="width: 30px;"><span class="bouton1" id="btn_paramccli" onclick="javascript:cacher('paramccli');">Afficher le paramétrage du crédit client</span></td>
			</tr>
			<tr style="background-color: #edf1f6; ">
					<td id="tdparamccli_1" style="width: 304px; height: 28px;">Article financier Porte monnaie : </td>
					<td id="tdparamccli_2" colspan="3" style="height: 28px; "><select id="paramadh_2" align="left" name="MONNAIEART" id="MONNAIEART" style="width: 300px;" required disabled>
					<?php
					$sql = "SELECT 'SELECTED' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
							FROM ARTICLE
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
							WHERE AR_ETABLISSEMENT = '".$etablissement."'
								AND AR_TYPE = 'ARTFI' AND AR_CODEARTICLE = ET_ARTMONNAIE
							UNION ALL
							SELECT '' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
							FROM ARTICLE
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
							WHERE AR_ETABLISSEMENT = '".$etablissement."'
							AND AR_TYPE = 'ARTFI' AND AR_CODEARTICLE <> ET_ARTMONNAIE
							ORDER BY SELECTED DESC;";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						echo '<option value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION']. '</option>';
					}
					?>
					
				</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>	
			<tr><td colspan="4"><u>Logo	</u></td>   </tr>
			<tr>
				<td >Grande image de l'établissement</td>
				<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input type="file" name="fic" size=50 />
				</td>
				<td colspan="2" align="center" >
					<img src="img/<?php echo $data['ET_IMAGENOM']; ?>"  width="200" height="55">
				</td>
			</tr>
			<tr>
				<td style="width: 304px; height: 20px;">Petite image de l'établissement</td>
				<td >
					
					<input type="file" name="petitlogo" size=50 />
				</td>
				<td colspan="2" align="center" >
					<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>"  width="200" height="55">
				</td>
				
			</tr>
			<tr align="center">
			  <td colspan="6" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" tabindex="20" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				$img_nom = $data['ET_IMAGENOM'];
				$smallimagenom = $data['ET_PETITEIMAGENOM'];
			}
			$ret        = false;
			$img_blob   = '';
			$img_taille = 0;
			
			$taille_max = 250000;
			
			if(!empty($_FILES['fic']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['fic']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['fic']['name'];
					if (rename($_FILES['fic']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					
					$img_type = $_FILES['fic']['type'];
					$img_nom  = $_FILES['fic']['name'];
				}
			}
			
			if(!empty($_FILES['petitlogo']['name']))
			{
				
				$ret = false;
				$ret        = is_uploaded_file($_FILES['petitlogo']['tmp_name']);
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['petitlogo']['name'];
					

					if (rename($_FILES['petitlogo']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['petitlogo']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
									
					$smallimagenom  = $nomDestination;
				}
			}
			//$img_blob = file_get_contents ($_FILES['fic']['tmp_name']);
			$sql = "UPDATE ETABLISSEMENT SET ET_LIBELLE = '" .$_POST['LIBELLE'] ."', ET_ADRESSE1 = '" .$_POST['ADRESSE1'] ."',ET_ADRESSE2 = '" .$_POST['ADRESSE2'] ."',
					ET_CODEPOSTAL = " .$_POST['CPOSTAL'] .", ET_VILLE = '" .$_POST['VILLE'] ."', ET_EMAIL = '" .$_POST['EMAIL1'] ."', ET_SITEWEB = '" .$_POST['SITEWEB'] ."', 
					ET_IMAGENOM = '" . $img_nom . "', 
					ET_PETITEIMAGENOM = '".$smallimagenom."',
					ET_GESTIONADHESION = '".$_POST['GESTION_ADHESION']."', ET_FACTURATION = '".$_POST['GESTION_FACTURATION']."', ET_GESTIONCREDITCLIENT = '".$_POST['GESTION_CREDITCLI']."' 
					WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
			if ($_POST['GESTION_FACTURATION'] == 'OUI')
			{
				$sql = "UPDATE ETABLISSEMENT SET ET_IBAN = '" .$_POST['IBAN'] ."',
						ET_BANQUECODE = '" .$_POST['BNQCODE'] ."', ET_BANQUEGUICHET = '" .$_POST['BNQGUICHET'] ."', ET_BANQUECPT = '" .$_POST['BNQCPT'] ."', ET_BANQUECLEF = '" .$_POST['BNQCLEF'] ."',
						ET_BANQUENOM = '".$_POST['BNQNOM']."', ET_BANQUEBIC = '" .$_POST['BNQBIC'] ."', ET_SIRET = '" .$_POST['SIRET'] ."', ET_NAP = '" .$_POST['NAP'] ."', ET_ECHEANCEFACTURE = '" .$_POST['JOURECHEANCE'] ."',
						ET_PIEDFACTURE = '".$_POST['PIEDFACTURE']."' 
						WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
				
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}

			if ($_POST['GESTION_ADHESION'] == 'OUI')
			{
				if(!empty($_FILES['ficcgu']['name']))
					{
						
					$ret        = is_uploaded_file($_FILES['ficcgu']['tmp_name']);
					
					if (!$ret) {
						echo "Problème de transfert";
						return false;
					} 
					else 
					{
						$repertoireDestination = dirname(__FILE__)."/";
						$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
						$repertoireDestination = $repertoireDestination  . 'document/' .$etablissement .'/';
						$nomDestination        = $_FILES['ficcgu']['name'];
						if (rename($_FILES['ficcgu']['tmp_name'], $repertoireDestination.$nomDestination))
						{
							//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
						}
						else 
						{
							echo "Le déplacement du fichier temporaire a échoué".
									" vérifiez l'existence du répertoire ".$repertoireDestination;
						}          
						
						$img_type = $_FILES['ficcgu']['type'];
						$fic_cgu  = $_FILES['ficcgu']['name'];
					}
				}
				//echo $_POST['PIEDFACTURE'];
				if(!empty($_FILES['ficcgu']['name']))
				{
					$sql = 'UPDATE ETABLISSEMENT SET ET_TYPEADHESION = "' .$_POST['TYPEADHESION'] .'", ET_ARTADHESION = "' .$_POST['ARTADHESION'] .'", ET_ADHESIONCOCHE1 = "' .$_POST['TEXTCOCHE1'] .'", 
							ET_ADHESIONCOCHE2 = "' .$_POST['TEXTCOCHE2'] .'", ET_ADHESIONRGPD1 = "' .$_POST['TEXTRGPD1'] .'", ET_FICHIERCGU = "' . $fic_cgu . '"
							
							WHERE ET_ETABLISSEMENT = "' .$etablissement.'";';
				}
				else
				{
					$sql = 'UPDATE ETABLISSEMENT SET ET_TYPEADHESION = "' .$_POST['TYPEADHESION'] .'", ET_ARTADHESION = "' .$_POST['ARTADHESION'] .'", ET_ADHESIONCOCHE1 = "' .$_POST['TEXTCOCHE1'] .'", 
							ET_ADHESIONCOCHE2 = "' .$_POST['TEXTCOCHE2'] .'", ET_ADHESIONRGPD1 = "' .$_POST['TEXTRGPD1'] .'"
							
							WHERE ET_ETABLISSEMENT = "' .$etablissement.'";';
				}
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}
		}
	}
}

function modif_etablissementV2($etablissement)
{
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{

?>
		
	

		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 90%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif; padding-left: 20px; padding-right: 20px;' border="0" cellpadding="0" cellspacing="0">
		  <tbody>
			<tr>
			  <td colspan="6" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification d'un établissement</td>
			</tr>
			<tr><td colspan="6" style="height: 10px;"/></tr>	
			<tr>
				<td style="width: 200px; height: 20px;">Code de l'établissement</td>
				<td style="height: 20px; width: 212px;"><input disabled maxlength="30" size="30" tabindex="1" name="" value='<?php echo $data['ET_ETABLISSEMENT']; ?>'></td>
				<td style="width: 150px; height: 20px;">Nom de l'établissement</td>
				<td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" value='<?php echo $data['ET_LIBELLE']; ?>' required></td>
			</tr>
			
			<tr>
			  <td style="width: 200px; height: 20px;">Adresse : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="2" name="ADRESSE1" value='<?php echo $data['ET_ADRESSE1']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Adresse (suite) : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="30" size="30" tabindex="3" name="ADRESSE2" value='<?php echo $data['ET_ADRESSE2']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 200px; height: 20px;">Code Postal : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="CPOSTAL" value='<?php echo $data['ET_CODEPOSTAL']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Ville : </td>
			  <td style="height: 20px; width: 20px;"><input maxlength="30" size="30" tabindex="5" name="VILLE" value='<?php echo $data['ET_VILLE']; ?>' required></td>
			</tr>
			<tr>
			  <td style="width: 200px; height: 20px;">Email : </td>
			  <td colspan="1" style="height: 20px; "><input maxlength="250" size="250" tabindex="6" name="EMAIL1" value='<?php echo $data['ET_EMAIL']; ?>' required></td>
			  <td style="width: 150px; height: 20px;">Site WEB : </td>
			  <td style="height: 20px; width: 212px;"><input maxlength="250" size="250" tabindex="7" name="SITEWEB" value='<?php echo $data['ET_SITEWEB']; ?>' required></td>
			</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>
			
			
			
			<tr>
			  <td style="width: 200px; height: 20px;">Gestion du panier lors de la réservation : </td>
			  <td style="height: 20px; width: 212px;"><SELECT name="GESTION_PANIER" id="GESTION_PANIER" style="width: 60px;">
			  <?php
			  if ($data['ET_GESTIONPANIER'] == 'OUI')
			  {
				  echo '<OPTION VALUE="OUI" selected="selected">OUI</OPTION>';
				  echo '<OPTION VALUE="NON">NON</OPTION>';
			  }
			  else
			  {
				  echo '<OPTION VALUE="NON" selected="selected">NON</OPTION>';
				  echo '<OPTION VALUE="OUI">OUI</OPTION>';
			  }
			  ?>
			  </SELECT></TD>
			  
			  <!--<td style="align:right;width: 30px;"><img id="bouton1" border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('manageetab.php?ACTION=MODIFBANQUE&ETABLISSEMENT=<?php echo $data['ET_ETABLISSEMENT']; ?>', 'exemple', 'height=600, width=1000, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>-->
			  <td colspan="2" align="left" style="align: center;width: 30px;"><span class="bouton1" id="btn_parambnq" onclick="javascript:cacher('parambnq');">Afficher le paramétrage facturation</span></td>
			  
			</tr>
			<tr><td colspan="6" style="height: 10px;"</TD></tr>
			
			<tr >
			  <td style="width: 200px; height: 20px;">Crédit client autorisé : </td>
			  <td style="height: 20px; width: 212px;"><SELECT name="GESTION_CREDITCLI" id="GESTION_CREDITCLI" style="width: 60px;">
			  <?php
			  if ($data['ET_GESTIONCREDITCLIENT'] == 'OUI')
			  {
				  echo '<OPTION VALUE="OUI" selected="selected">OUI</OPTION>';
				  echo '<OPTION VALUE="NON">NON</OPTION>';
			  }
			  else
			  {
				  echo '<OPTION VALUE="NON" selected="selected">NON</OPTION>';
				  echo '<OPTION VALUE="OUI">OUI</OPTION>';
			  }
			  ?>
			  </SELECT></TD>
			 
			  
			</tr>
			
			
			<tr><td colspan="6" style="height: 10px;"</TD></tr>	
			<tr><td colspan="4"><u>Logo	</u></td>   </tr>
			<tr>
				<td >Grande image de l'établissement</td>
				<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input type="file" name="fic" size=50 />
				</td>
				<td colspan="2" align="center" >
					<img src="img/<?php echo $data['ET_IMAGENOM']; ?>"  width="200" height="55">
				</td>
			</tr>
			<tr>
				<td style="width: 304px; height: 20px;">Petite image de l'établissement</td>
				<td >
					
					<input type="file" name="petitlogo" size=50 />
				</td>
				<td colspan="2" align="center" >
					<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>"  width="200" height="55">
				</td>
				
			</tr>
			
			<tr><td colspan="6" style="height: 10px;"</TD></tr>	
			<tr><td colspan="4"><u>Documents	</u></td>   </tr>
			<tr>
				<td>Mentions légales</td>
				<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input type="file" name="MENTIONLEG" size=50 />
				</td>
				<td colspan="2" align="center" >
					<a href="#" class="modif" onclick="window.open('document/<?php echo $_SESSION['ETABADMIN'] .'_' .$data['ET_MENTIONLEG']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
					<span class="value_info"><?php echo addslashes($data['ET_MENTIONLEG']);?></span>
					
				</td>
			</tr>
			<tr>
				<td>Conditions générales d'utilisations</td>
				<td>
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input type="file" name="CGU" size=50 />
				</td>
				<td colspan="2" align="center" >
					<a href="#" class="modif" onclick="window.open('document/<?php echo $_SESSION['ETABADMIN'] .'_' .$data['ET_CGU']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
					<span class="value_info"><?php echo addslashes($data['ET_CGU']);?></span>
					
				</td>
			</tr>
			<tr>
				<td style="width: 304px; height: 20px;">Conditions générales de ventes</td>
				<td >
					
					<input type="file" name="CGV" size=50 />
				</td>
				<td colspan="2" align="center" >
					<a href="#" class="modif" onclick="window.open('document/<?php echo $_SESSION['ETABADMIN'] .'_' .$data['ET_CGV']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
					<span class="value_info"><?php echo addslashes($data['ET_CGV']);?></span>
					
				</td>
				
			</tr>
			
			<tr align="center">
			  <td colspan="6" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" tabindex="20" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				$img_nom = $data['ET_IMAGENOM'];
				$smallimagenom = $data['ET_PETITEIMAGENOM'];
				$mentionleg = $data['ET_MENTIONLEG'];
				$cgu = $data['ET_CGU'];
				$cgv = $data['ET_CGV'];
			}
			$ret        = false;
			$img_blob   = '';
			$img_taille = 0;
			
			$taille_max = 250000;
			
			if(!empty($_FILES['fic']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['fic']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['fic']['name'];
					if (rename($_FILES['fic']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					
					$img_type = $_FILES['fic']['type'];
					$img_nom  = $_FILES['fic']['name'];
				}
			}
			
			if(!empty($_FILES['petitlogo']['name']))
			{
				
				$ret = false;
				$ret        = is_uploaded_file($_FILES['petitlogo']['tmp_name']);
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination        = $_FILES['petitlogo']['name'];
					

					if (rename($_FILES['petitlogo']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['petitlogo']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
									
					$smallimagenom  = $nomDestination;
				}
			}
			
			if(!empty($_FILES['MENTIONLEG']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['MENTIONLEG']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'document/' .$_SESSION['ETABADMIN'] .'_';
					$nomDestination        = strtoupper($_FILES['MENTIONLEG']['name']);
					if (rename($_FILES['MENTIONLEG']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					$mentionleg  = strtoupper($_FILES['MENTIONLEG']['name']);
					
				}
			}
			
			if(!empty($_FILES['CGU']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['CGU']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'document/' .$_SESSION['ETABADMIN'] .'_';
					$nomDestination        = strtoupper($_FILES['CGU']['name']);
					if (rename($_FILES['CGU']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					$cgu  = strtoupper($_FILES['CGU']['name']);
					
				}
			}
			
			if(!empty($_FILES['CGV']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['CGV']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'document/' .$_SESSION['ETABADMIN'] .'_';
					$nomDestination        = strtoupper($_FILES['CGV']['name']);
					if (rename($_FILES['CGV']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					$cgv  = strtoupper($_FILES['CGV']['name']);
					
				}
			}
			
			//$img_blob = file_get_contents ($_FILES['fic']['tmp_name']);
			$sql = 'UPDATE ETABLISSEMENT SET ET_LIBELLE = "' .$_POST['LIBELLE'] .'", ET_ADRESSE1 = "' .$_POST['ADRESSE1'] .'",ET_ADRESSE2 = "' .$_POST['ADRESSE2'] .'",
					ET_CODEPOSTAL = ' .$_POST['CPOSTAL'] .', ET_VILLE = "' .$_POST['VILLE'] .'", ET_EMAIL = "' .$_POST['EMAIL1'] .'", ET_SITEWEB = "' .$_POST['SITEWEB'] .'", 
					ET_IMAGENOM = "' . $img_nom . '", ET_GESTIONPANIER = "'.$_POST['GESTION_PANIER'].'",
					ET_PETITEIMAGENOM = "'.$smallimagenom.'",
					ET_GESTIONCREDITCLIENT = "'.$_POST['GESTION_CREDITCLI'].'", ET_MENTIONLEG = "'.$mentionleg.'", ET_CGU = "'.$cgu.'", ET_CGV = "'.$cgv.'" 
					WHERE ET_ETABLISSEMENT = "' .$_SESSION['ETABADMIN'].'"';

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
			

			
		}
	}
}


function modif_banque_etablissement($etablissement)
{
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{

?>
		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 95%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Information bancaires de l'établissement</td>
			<tr>
			<tr><td colspan="2"><u>Information Banquaire</u></td>   </tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Nom de la banque: </td>
			  <td colspan="1" style="height: 28px; "><input maxlength="50" size="50" tabindex="8" name="BNQNOM" value='<?php echo $data['ET_BANQUENOM']; ?>' required disabled></td>
			</tr>
			<tr>
			  <td style="width: 240px; height: 28px;">IBAN : </td>
			  <td style="height: 28px; "><input maxlength="30" size="30" tabindex="9" name="IBAN" value='<?php echo $data['ET_IBAN']; ?>' required disabled></td>
			  <td style="width: 240px; height: 28px;">BIC : </td>
			  <td><input maxlength="10" size="10" tabindex="10" name="BNQBIC" value='<?php echo $data['ET_BANQUEBIC']; ?>' required disabled></td>
			</tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Code Banque : </td>
			  <td><input maxlength="10" size="10" tabindex="12" name="BNQCODE" value='<?php echo $data['ET_BANQUECODE']; ?>' required disabled></td>
			  <td style="width: 240px; height: 28px;">Guichet Banque : </td>
			  <td><input maxlength="10" size="10" tabindex="13" name="BNQGUICHET" value='<?php echo $data['ET_BANQUEGUICHET']; ?>' required disabled></td>
			</tr>
			<tr>  
			  <td style="width: 240px; height: 28px;">Compte Banquaire : </td>
			  <td><input maxlength="15" size="15" tabindex="14" name="BNQCPT" value='<?php echo $data['ET_BANQUECPT']; ?>' required disabled></td> 
			  <td style="width: 240px; height: 28px;">Clé Banque : </td>
			  <td><input maxlength="2" size="2" tabindex="15" name="BNQCLEF" value='<?php echo $data['ET_BANQUECLEF']; ?>' required disabled></td>
			</tr>
			<tr><td colspan="4"><u>Siret</u></td>   </tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Siret : </td>
			  <td><input maxlength="20" size="20" tabindex="16" name="SIRET" value='<?php echo $data['ET_SIRET']; ?>' required disabled></td>
			  <td style="width: 240px; height: 28px;">NAF-APE : </td>
			  <td><input maxlength="5" size="5" tabindex="17" name="NAP" value='<?php echo $data['ET_NAP']; ?>' required disabled></td>
			</tr>
			<tr><td colspan="2"><u>Paramétrage facturation	</u></td>   </tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Nombre de jour échéance : </td>
			  <td><input size="2" maxlength="2" size="2" tabindex="18" name="JOURECHEANCE" value='<?php echo $data['ET_ECHEANCEFACTURE']; ?>' required disabled></td>
			</tr>
			<tr><td colspan="2"><u>Paramétrage édition facture	</u></td>   </tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Texte pied de facture : </td>
			  <td colspan="3" style="align: center;"><TEXTAREA id="PIEDFACTURE" name="PIEDFACTURE" rows="6" cols="85"><?php echo $data['ET_PIEDFACTURE']; ?> </TEXTAREA></td>
			</tr>
			
			<tr align="center">
			  <td colspan="4" rowspan="1" ><button value="Valid" tabindex="20" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			//echo $_POST['PIEDFACTURE'];
			
			$sql = "UPDATE ETABLISSEMENT SET ET_IBAN = '" .$_POST['IBAN'] ."',
					ET_BANQUECODE = '" .$_POST['BNQCODE'] ."', ET_BANQUEGUICHET = '" .$_POST['BNQGUICHET'] ."', ET_BANQUECPT = '" .$_POST['BNQCPT'] ."', ET_BANQUECLEF = '" .$_POST['BNQCLEF'] ."',
					ET_BANQUENOM = '".$_POST['BNQNOM']."', ET_BANQUEBIC = '" .$_POST['BNQBIC'] ."', ET_SIRET = '" .$_POST['SIRET'] ."', ET_NAP = '" .$_POST['NAP'] ."', ET_ECHEANCEFACTURE = '" .$_POST['JOURECHEANCE'] ."',
					ET_PIEDFACTURE = '".$_POST['PIEDFACTURE']."' 
					WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
			
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}

function modif_adhésion_etablissement($etablissement)
{
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from ETABLISSEMENT WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{

?>
		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 95%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Gestion des adhésions</td>
			<tr>
				<td style="width: 304px; height: 28px;">Type d'adhésion : </td>
				<td style="height: 28px; "><select align="left" name="TYPEADHESION" id="TYPEADHESION" required disabled>
										<option value="GLISSANT">Année glissante</option>
										<option value="ANNEEENCOUR">Année en court</option>
				</td>
				
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;">Article d'adhésion : </td>
				<td colspan="2" style="height: 28px; "><select align="left" name="ARTADHESION" id="ARTADHESION" required disabled>
				<?php
				$sql = "SELECT 'SELECTED' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
						FROM ARTICLE
						LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
						WHERE AR_ETABLISSEMENT = '".$etablissement."'
							AND AR_TYPE = 'ARTAD' AND AR_CODEARTICLE = ET_ARTADHESION
						UNION ALL
						SELECT '' AS SELECTED, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF 
						FROM ARTICLE
						LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = AR_ETABLISSEMENT
						WHERE AR_ETABLISSEMENT = '".$etablissement."'
						AND AR_TYPE = 'ARTAD' AND AR_CODEARTICLE <> ET_ARTADHESION
						ORDER BY SELECTED DESC;";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					echo '<option value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION']. ' - ' .$data1['AR_TARIF'].' €</option>';
				}
				?>
				
			<tr>
			<tr><td colspan="4"></td></tr>
			<tr><td colspan="2"><u>Paramétrage question adhésion	</u></td>   </tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Texte coche 1 : </td>
			  <td colspan="3" style="align: center;"><TEXTAREA id="TEXTCOCHE1" name="TEXTCOCHE1" rows="6" cols="85"><?php echo $data['ET_ADHESIONCOCHE1']; ?> </TEXTAREA></td>
			</tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Texte coche 2 : </td>
			  <td colspan="3" style="align: center;"><TEXTAREA id="TEXTCOCHE2" name="TEXTCOCHE2" rows="6" cols="85"><?php echo $data['ET_ADHESIONCOCHE2']; ?> </TEXTAREA></td>
			</tr>
			<tr>
			  <td style="width: 240px; height: 28px;">Texte RGPD: </td>
			  <td colspan="3" style="align: center;"><TEXTAREA id="TEXTRGPD1" name="TEXTRGPD1" rows="6" cols="85"><?php echo $data['ET_ADHESIONRGPD1']; ?> </TEXTAREA></td>
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;">Condition générale d'utilisation</td>
				<td style="height: 28px; width: 212px;">
					
					<input type="file" name="fic" size=50 />
				</td>
				<?php
				if (!empty($data['ET_FICHIERCGU']))
				{
					?>
				<td align="left" colspan="2" style="width: 304px; height: 28px;">
					<a title="" href="document/<?php echo $etablissement .'/' .$data['ET_FICHIERCGU']; ?>">Condition générale d'utilisation</a>
				</td>
				<?php
				}
				?>
			</tr>
			<tr align="center">
			  <td colspan="4" rowspan="1" ><button value="Valid" tabindex="20" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			if(!empty($_FILES['fic']['name']))
				{
					
				$ret        = is_uploaded_file($_FILES['fic']['tmp_name']);
				
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'document/' .$etablissement .'/';
					$nomDestination        = $_FILES['fic']['name'];
					if (rename($_FILES['fic']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						//echo "Le fichier temporaire ".$_FILES['fic']['tmp_name']. " a été déplacé vers ".$repertoireDestination.$nomDestination;
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					
					$img_type = $_FILES['fic']['type'];
					$fic_cgu  = $_FILES['fic']['name'];
				}
			}
			//echo $_POST['PIEDFACTURE'];
			if(!empty($_FILES['fic']['name']))
			{
				$sql = 'UPDATE ETABLISSEMENT SET ET_TYPEADHESION = "' .$_POST['TYPEADHESION'] .'", ET_ARTADHESION = "' .$_POST['ARTADHESION'] .'", ET_ADHESIONCOCHE1 = "' .$_POST['TEXTCOCHE1'] .'", 
						ET_ADHESIONCOCHE2 = "' .$_POST['TEXTCOCHE2'] .'", ET_ADHESIONRGPD1 = "' .$_POST['TEXTRGPD1'] .'", ET_FICHIERCGU = "' . $fic_cgu . '"
						
						WHERE ET_ETABLISSEMENT = "' .$etablissement.'";';
			}
			else
			{
				$sql = 'UPDATE ETABLISSEMENT SET ET_TYPEADHESION = "' .$_POST['TYPEADHESION'] .'", ET_ARTADHESION = "' .$_POST['ARTADHESION'] .'", ET_ADHESIONCOCHE1 = "' .$_POST['TEXTCOCHE1'] .'", 
						ET_ADHESIONCOCHE2 = "' .$_POST['TEXTCOCHE2'] .'", ET_ADHESIONRGPD1 = "' .$_POST['TEXTRGPD1'] .'"
						
						WHERE ET_ETABLISSEMENT = "' .$etablissement.'";';
			}
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}
