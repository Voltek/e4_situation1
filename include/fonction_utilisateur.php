<?php

/**
 * fonction_utilisateur.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
include ("include/fonction_general.php");
//require ("scripts/constantes.php");
//require ("scripts/fonctions.php");


function popup_valide_utilisateur()
{
	?>
	<form id="formplaning" action="" method="post">
		<br />
		<p>Saisir le mot de passe : <input type="text" id="newloginpassword1" name="newloginpassword1"  size="50" maxlength="50" required></p>
		<p>Saisir le mot de passe : <input type="text" id="newloginpassword2" name="newloginpassword2"  size="50" maxlength="50" required></p>
		<input type="hidden" id="validmdp" name="validmdp" value="validmdp">

		<br /><br />
		<input type="submit" value="Valider" />
	</form>
	<?php

}

function modif_utilisateur($statut)
{
	?>

	<div class="head">
		<div class="head_container account">
			<a href="mon_compte.php" class="account_tab active">Vos informations</a>
			<a href="mes_factures.php" class="account_tab active">Vos factures</a>
			<a href="mes_documents.php" class="account_tab active">Mes documents</a>
		</div>
	</div>

	<?php
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	if ($statut == 'admin')
	{
		$sql= "SELECT * from UTILISATEUR WHERE UT_LOGIN = '" .$_GET['numero']. "';";
		?>
		<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_GET['numero']; ?>">
		<?php
	}
	else
	{
		$sql= "SELECT * from UTILISATEUR WHERE UT_LOGIN = '" .$_SESSION['login']. "';";
		?>
		<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_SESSION['login']; ?>">
		<?php
	}

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		?>

		<div class="account_container ">
			<section>
				<span class="section_head">
					<h3>Identification</h3>
					<a href="#" class="modif" onclick="openmodif($(this))">Modifier</a>
				</span>
				<span class="section_body">
					<div class="block_info">
						<span class="name_info" data-field="login">Identifiant</span>
						<span class="value_info"><?php echo addslashes($data['UT_LOGIN']);?></span>
					</div>
					<div class="block_info">
						<span class="name_info" data-field="pwd">Mot de passe</span>
						<span class="value_info">*********</span>
					</div>
				</span>
			</section>
			<section>
				<span class="section_head">
					<h3>Civilité</h3>
					<a href="#" class="modif" onclick="openmodif($(this))">Modifier</a>
				</span>
				<span class="section_body">
					<div class="block_info">
						<span class="name_info" data-field="prenom">Prénom</span>
						<span class="value_info"><?php echo addslashes(decrypt($data['UT_PRENOM'],$_SESSION['ID']));?></span>
					</div>
					<div class="block_info">
						<span class="name_info" data-field="nom">Nom</span>
						<span class="value_info"><?php echo addslashes(decrypt($data['UT_NOM'],$_SESSION['ID']));?></span>
					</div>
				</span>
			</section>
			<section>
				<span class="section_head">
					<h3>Contact</h3>
					<a href="#" class="modif" onclick="openmodif($(this))">Modifier</a>
				</span>
				<span class="section_body">
					<div class="block_info">
						<span class="name_info" data-field="email">Email</span>
						<span class="value_info"><?php echo addslashes(decrypt($data['UT_EMAIL'],$_SESSION['ID']));?></span>
					</div>
				</span>
			</section>
			<section>
				<span class="section_head">
					<h3>Adresse</h3>
					<a href="#" class="modif" onclick="openmodif($(this))">Modifier</a>
				</span>
				<span class="section_body">
					<div class="block_info">
						<span class="name_info" data-field="Adresse">Adresse</span>
						<?php
						if (!empty($data['UT_ADRESSE1']))
						{
							?>
							<span class="value_info"><?php echo addslashes(decrypt($data['UT_ADRESSE1'],$_SESSION['ID']));?></span>
							<?php
						}
						?>
					</div>
					<div class="block_info">
						<span class="name_info" data-field="ComplAdresse">Complément d'adresse</span>
						<?php
						if (!empty($data['UT_ADRESSE1']))
						{
							?>
							<span class="value_info"><?php echo addslashes(decrypt($data['UT_ADRESSE2'],$_SESSION['ID']));?></span>
							<?php
						}
						?>
					</div>
					<div class="block_info">
						<span class="name_info" data-field="CodePostal">Code postal</span>
						<?php
						if (!empty($data['UT_ADRESSE1']))
						{
							?>
							<span class="value_info"><?php echo addslashes(decrypt($data['UT_CODEPOSTAL'],$_SESSION['ID']));?></span>
						<?php
						}
						?>
					</div>
					<div class="block_info">
						<span class="name_info" data-field="Ville">Ville</span>
						<?php
						if (!empty($data['UT_ADRESSE1']))
						{
							?>
							<span class="value_info"><?php echo addslashes(decrypt($data['UT_VILLE'],$_SESSION['ID']));?></span>
							<?php
						}
						?>
					</div>
				</span>
			</section>
		</div>





		<div class="resa_popin">
			<div class="popin_head">
				<div class="close_btn" onclick="closemodif();"></div>
				<h3>Modification</h3>
			</div>
			<form class="popin_body" method="POST">
				<div class="infos_popin">
					<h3></h3>
					<label class="nom" for="nom">Nom</label>
					<input type="text" id="nom" name="nom" value="<?php echo addslashes(decrypt($data['UT_NOM'],$_SESSION['ID']));?>">
					<label class="prenom" for="prenom">Prénom</label>
					<input type="text" id="prenom" name="prenom" value="<?php echo addslashes(decrypt($data['UT_PRENOM'],$_SESSION['ID']));?>">
					<label class="email" for="email">Email</label>
					<input type="text" id="email" name="email" value="<?php echo addslashes(decrypt($data['UT_EMAIL'],$_SESSION['ID']));?>">
					<label class="Adresse" for="Adresse">Adresse</label>
					<input type="text" id="Adresse" name="Adresse" value="<?php echo addslashes(decrypt($data['UT_ADRESSE1'],$_SESSION['ID']));?>">
					<label class="ComplAdresse" for="ComplAdresse">Complément d'adresse</label>
					<input type="text" id="ComplAdresse" name="ComplAdresse" value="<?php echo addslashes(decrypt($data['UT_ADRESSE2'],$_SESSION['ID']));?>">
					
					<label class="CodePostal" for="CodePostal">Code Postal</label>
					<input type="text" id="CodePostal" name="CodePostal" value="<?php echo addslashes(decrypt($data['UT_CODEPOSTAL'],$_SESSION['ID']));?>">
					<label class="Ville" for="Ville">Code Postal</label>
					<input type="text" id="Ville" name="Ville" value="<?php echo addslashes(decrypt($data['UT_VILLE'],$_SESSION['ID']));?>">
					<label class="pwd" for="pwd">Mot de passe</label>
					<!--<input type="password" id="pwd" name="pwd" value="<?php echo addslashes($data['UT_PASSWORD']);?>">-->
					<input type="hidden" id="token" name="token" value="<?php echo $_SESSION['token']; ?>">
					<input type="hidden" id="login" name="login" value="<?php echo $_SESSION['login']; ?>">
				</div>
				<div class="resa_valid">
					<a href="#" class="cancel" onclick="closemodif();">Annuler</a>
					<input type="submit" id="submit" name="submit" class="valider" value="Valider">
				</div>
			</form>
		</div>
		<div class="mask_popin" onclick="closemodif();"></div>

		<script type="text/javascript">

			function openmodif(e) {
			// on créée les variables :

			var champs = $(e).parent().parent().find('.section_body').find('.block_info').length,
			champ = new Array(),
			head = $(e).parent().parent().find('.section_head').find('h3').html(),
			u = 1;
			inputs = '';

			for (var i = 1; i <= champs; i++) {
				champ[i] = $(e).parent().parent().find('.section_body').find('.block_info:nth-child('+i+')').find('.name_info').data('field');
			};
			$('.resa_popin').find('.popin_body').find('.infos_popin').find('h3').empty().append(head);
			$('.resa_popin').find('.popin_body').find('.infos_popin').find('input').hide();
			$('.resa_popin').find('.popin_body').find('.infos_popin').find('label').hide();
			while(champ[u]) {
				$('input#'+champ[u]).show();
				$('label.'+champ[u]).show();
				u++;
			}
			$('.resa_popin').find('.popin_body').find('.infos_popin').append(inputs);

			// On renseigne les inputs hidden de base :


			// On reseigne le nombre de places libre


			// On affiche la popin
			$('.mask_popin').addClass('view');
			$('.resa_popin').addClass('view');
		}

		function closemodif() {
			$('.mask_popin').removeClass('view');
			$('.resa_popin').removeClass('view');
		}
	</script>

			<?php
		}

	}
	
function update_account() {
	
	$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes(encrypt($_POST['nom'],$_SESSION['ID']))."', UT_PRENOM = '".addslashes(encrypt($_POST['prenom'],$_SESSION['ID']))."', UT_EMAIL = '".addslashes(encrypt($_POST['email'],$_SESSION['ID']))."'
	WHERE UT_LOGIN = '" .$_SESSION['login']. "';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);	
	$_SESSION['NOM'] = addslashes($_POST['nom']);
	$_SESSION['PRENOM'] = addslashes($_POST['prenom']);

	}

function update_utilsateur()
{

	$sql= "SELECT count(*) as CPT from UTILISATEUR where UT_LOGIN = '".$_POST['newlogin']."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$NbUser = $data['CPT'];
	}
	
	if ($NbUser == 0)
	{
		echo "utisitateur " .$_POST['newlogin'] ." n existe pas";
	}
	else
	{
		
		if ($_POST['statut'] == 'admin')
		{
			$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes($_POST['newloginnom'])."', UT_PRENOM = '".addslashes($_POST['newloginprenom'])."', UT_ADRESSE1 = '".addslashes($_POST['newloginadresse1'])."',
			UT_ADRESSE2 = '".addslashes($_POST['newloginadresse2'])."', UT_VILLE = '".addslashes($_POST['newloginville'])."',
			UT_CODEPOSTAL = '".addslashes($_POST['newlogincodepostal'])."', UT_VALIDE = '".addslashes($_POST['newloginvalide'])."',
			UT_TIERSGESTION = '".addslashes($_POST['newlogintiers'])."', UT_STATUT = '".addslashes($_POST['newloginstatut'])."',
			UT_EMAIL = '".addslashes($_POST['newloginemail'])."'
			WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
		}
		else
		{
			$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes($_POST['newloginnom'])."', UT_PRENOM = '".addslashes($_POST['newloginprenom'])."', UT_ADRESSE1 = '".addslashes($_POST['newloginadresse1'])."',
			UT_ADRESSE2 = '".addslashes($_POST['newloginadresse2'])."', UT_VILLE = '".addslashes($_POST['newloginville'])."',
			UT_CODEPOSTAL = '".addslashes($_POST['newlogincodepostal'])."',	UT_EMAIL = '".addslashes($_POST['newloginemail'])."'
			WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
		}
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);	
		echo "Utisitateur " .$_POST['newlogin']." a été modifié";
		if ($_POST['statut'] == 'admin')
		{
			?>
			<a href="javascript:myclosewindow();">Fermer</a>
			<?php
		}
		else
		{
			?>
			<a href="mon_compte.php">Fermer</a>
			<?php
		}
	}
}


		

function liste_facture()
{
	if ($_POST['user'] != "Tous CoWorker")
		{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr style="background-color:#46B593">
			<td align="left" style="width:20%"> Référence Facture</td>
			<td align="left" style="width:20%"> Date Facture</td>
			<td align="left" style="width:25%"> Montant</td>
			<td align="left" style="width:20%"> </td>
		</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-color:white">
		<?php
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		if ($_SESSION['ETABADMIN'] = $_GET['etablissement'])
		{
			$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m %Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE order by GP_DATEPIECE, GP_USER";
		}
		else
		{
			$sql = "SELECT GP_DATEPIECE as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE where GP_USER = '" .$_SESSION['login']. "' order by GP_DATEPIECE, GP_USER";
		}
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			?>
			<tr>

				<td align="left" style="width:20%"><b><?php echo $data['GP_REFFACTURE']; ?></b></a></td>
				<td align="left" style="width:20%"><?php echo $data['JOUR']; ?></td>
				<td align="left" style="width:25%"><?php echo number_format($data['GP_TOTALHT'],2,',',''); ?></td>
				<td align="center" style="width:10%"><img border="0" src="img/logo_print.jpg" width="25" height="25" onclick="window.open('print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
				<td align="center" style="width:10%"><img border="0" src="img/logo-mail.jpg" width="25" height="25" onclick="window.open('email_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr>
			<?php
		}
		mysqli_close();
		?>
	</table></center>

	<?php
}

function newpassword()
{
	?>
	<form action="" method="post">
		<br />
		<p>Saisir le login (email) : <input type="text" id="newloginemail" name="newloginemail"  size="50" maxlength="250" required></p>
		<input type="hidden" id="TypeAction" name="TypeAction" value="demande">

		<br /><br />
		<input type="submit" value="Valider" />
	</form>
	<?php

}

function liste_adherant()
{
	
	if ($_POST['user'] != "Tous CoWorker")
		{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr style="background-color:#46B593">
			<td align="left" style="width:20%"> Référence Facture</td>
			<td align="left" style="width:20%"> Date Facture</td>
			<td align="left" style="width:25%"> Montant</td>
			<td align="left" style="width:20%"> </td>
		</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-color:white">
		<?php
		if (($_SESSION['STATUT']) == "ADMIN")
		{
			$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m %Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE order by GP_DATEPIECE, GP_USER";
		}
		else
		{
			$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m/%Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE where GP_USER = '" .$_SESSION['login']. "' order by GP_DATEPIECE, GP_USER";
		}
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			?>
			<tr>

				<td align="left" style="width:20%"><b><?php echo $data['GP_REFFACTURE']; ?></b></a></td>
				<td align="left" style="width:20%"><?php echo $data['JOUR']; ?></td>
				<td align="left" style="width:25%"><?php echo number_format($data['GP_TOTALHT'],2,',',''); ?></td>
				<td align="center" style="width:10%"><img border="0" src="img/logo_print.jpg" width="25" height="25" onclick="window.open('print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
				<td align="center" style="width:10%"><img border="0" src="img/logo-mail.jpg" width="25" height="25" onclick="window.open('email_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr>
			<?php
		}
		
		?>
	</table></center>

	<?php
}

						
function visu_facture($statut)
{
	?>

	<div class="head">
		<div class="head_container account">
			<a href="mon_compte.php" class="account_tab active">Vos informations</a>
			<a href="mes_factures.php" class="account_tab active">Vos factures</a>
			<a href="mes_documents.php" class="account_tab active">Mes documents</a>
		</div>
	</div>
	<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_SESSION['login']; ?>">
	

		<div class="account_container ">
			<section>
				<span class="section_head">
					<h3>Factures</h3>
					<!-- <a href="#" class="modif" onclick="openmodif($(this))">Modifier</a> -->
				</span>
				<?php
				$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m/%Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE WHERE GP_USER = '" .$_SESSION['login']. "' order by GP_DATEPIECE, GP_REFFACTURE";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data)
				{
				?>
				
				<span class="section_body">
					<div class="block_info">
						<!--<span class="name_info" data-field="login">Identifiant</span> -->
						<a href="#" class="modif" onclick="window.open('pdf/print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
						<span class="value_info">Facture <?php echo addslashes($data['GP_REFFACTURE']);?> du <?php echo $data['JOUR'];?></span>
						<!--<span class="name_info" data-field="pwd">facture</span>-->
					</div>
					
				</span>
				
			



				<?php
				}
				?>
				</section>
			
			</div>
			<?php

}

function visu_document($statut)
{
	?>

	<script type="text/javascript">
	$(document).ready(function() {
	$('.book_choice span').on('click', function(){
		var $this = $(this).parent();

		$this.toggleClass('open');
	});

	$('.book_choice li').on('click', function(){
		var $this = $(this),
			txt = $this.html(),
			etablissement = $this.data('etablissement');

		$this.parent().find('span').empty().append(txt);
		$this.parent('ul').removeClass('open');
		$('#choix').attr('href',"mes_documents.php?typeplace=" + etablissement);
	})
	})

	</script>
	<div class="head">
		<div class="head_container account">
			<a href="mon_compte.php" class="account_tab active">Vos informations</a>
			<a href="mes_factures.php" class="account_tab active">Vos factures</a>
			<a href="mes_documents.php" class="account_tab active">Mes documents</a>
		</div>
	</div>
	<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_SESSION['login']; ?>">
	<?php
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql="select * from ETABLISSEMENT";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while ($data = mysqli_fetch_array($req))
	{
		//echo $data['ET_ETABLISSEMENT'];
		$zone_li[$data['ET_ETABLISSEMENT']] = $data['ET_LIBELLE'];
	}
	mysqli_close($conn);
	?>

	<div class="logged_in_choice">
		<div class="logged_in_box col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">

			<h3>Je choisi ...</h3>
			<ul class="book_choice"><span class="selected">Un lieu ...</span>
				<?php foreach($zone_li as $id => $li) :?>
					<li data-etablissement="<?php echo $id; ?>"><?php echo $li; ?></li>
				<?php endforeach; ?>
			</ul>
			<a id="choix" href="#" class="valider col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">Valider</a>
		</div>
	</div>
	

		
			<?php

}
														