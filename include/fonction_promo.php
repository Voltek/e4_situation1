<?php
/**
 * fonction_promo.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include ("include/fonction_general.php");


function liste_promo()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
	?>

	<body>
		<!-- TABLE 1 DEBUT -->

		<style>
			#customers {
			    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			    border-collapse: collapse;
			    width: 100%;
			}

			#customers td, #customers th {
			    border: 1px solid #ddd;
			    padding: 8px;
			}

			#customers tr:nth-child(even){background-color: #f2f2f2;}

			#customers tr:hover {background-color: #ddd;}

			#customers th {
			    padding-top: 12px;
			    padding-bottom: 12px;
			    text-align: left;
			    background-color: #4CAF50;
			    color: white;
			}
		</style>
		
<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto; font-size : 14px;"cellpadding="2" cellspacing="2" id="customers">
	<form  action="" method="post">
  <tbody>
    <tr>
      <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 100px; font-weight: bold; background-color: rgb(70, 181, 147);">Code Promotion</td>
	  <td style="width: 400px ;font-family: Calibri; color: rgb(0, 1, 0);font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Désignation</td>
	  <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 100px; font-weight: bold; background-color: rgb(70, 181, 147);">Active
	  <br>
	  <select name="PromoActive" id="PromoActive" >
      <?php
		$listevar = array("Tous","OUI", "NON");
		if (!isset($_POST['PromoActive']))
		{
			for ($i = 0; $i <= 2; $i++)
			{
				if ($i==0)
				{
					?>
					<option value="<?php echo $listevar[$i];?>" selected="selected"><?php echo $listevar[$i];?></option>
					<?php
				}
				else
				{
					?>
					<option value="<?php echo $listevar[$i];?>"><?php echo $listevar[$i];?></option>
					<?php
				}
			}
		}
		else
		{
			for ($i = 0; $i <= 2; $i++)
			{
				if ($_POST['PromoActive'] == $listevar[$i])
				{
					?>
					<option value="<?php echo $listevar[$i];?>" selected="selected"><?php echo $listevar[$i];?></option>
					<?php
				}
				else
				{
					?>
					<option value="<?php echo $listevar[$i];?>"><?php echo $listevar[$i];?></option>
					<?php
				}
			}
		}
		?>
	  

      </select>
	  </td>
      <td style="width: 100px;font-family: Calibri; color: rgb(0, 1, 0);font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Valeur remise</td>
      
	  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);"><button value="Valid" name="Valid">Valider</button></td>
      </tr>
	  </form>

<!-- DEBUT -->


<?php
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$critere = 0;

if ((isset($_POST['PromoActive'])) && ($_POST['PromoActive'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND PR_ACTIVE = '".$_POST['PromoActive']."' ";
	}
	else
	{
		$sqlcritere = " AND PR_ACTIVE = '".$_POST['PromoActive']."' ";
	}
	$critere = 1;

}

if ($critere == 1)
{
	$sql = "SELECT * FROM PROMOTION WHERE PR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' " .$sqlcritere ." ORDER BY PR_NUMERO";
}
else
{
	$sql = "SELECT * FROM PROMOTION WHERE PR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ORDER BY PR_NUMERO";
}

$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
if (count($tab_r) != 0)
{	
	foreach ($tab_r as $data)
	{
		?>
		<tr>
			<td><?php echo $data['PR_NUMERO']; ?></td>
			<td><?php echo $data['PR_LIBELLE']; ?></td>
			<td><?php echo $data['PR_ACTIVE']; ?></td>
			<td colspan="1"><?php echo $data['PR_VALEUR']; ?></td>
			<td style="width: 30px;"><img border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('modif_promo.php?ACTION=MODIF&numero=<?php echo $data['PR_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			<td style="width: 30px;"><img border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('delete_article.php?numero=<?php echo $data['PR_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
		</tr>
	<?php
	}
	
}
?>
	<tr><td colspan="6" align="center" ><input type=button value="Ajouter une promotion" class="bouton1" onclick="window.open('ajout_promo.php', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td></tr>
			
	<?php
?>


<!-- FIN -->




	<!-- </tbody></table> -->
	<!-- TABLE 1 FIN -->

		<!-- TABLE 2 -->

	<!-- TABLE 2 FIN -->
	<?php
	}

}


function new_promo()
{
?>
	<br />
<form  action="" method="post">
<table style='text-align: left; width: 474px; height: 200px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center;  color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Création d'une promotion</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de la promotion</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="50" size="30" tabindex="2" name="LIBELLEPROMO" required></td>
			</tr>
			<tr><td style="width: 304px; height: 28px;"><label>Article déclencheur : </label></td>
			<td>
				<select name="DECLENCHEUR">
				<?php
				$sql = "SELECT * FROM ARTICLE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['AR_CODEARTICLE']; ?>" selected="selected"><?php echo $r['AR_DESIGNATION']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr><td style="width: 304px; height: 28px;"><label>Quantité déclencheur : </label></td>
			<td>
				<select name="QTEDECLENCHEUR">
				<option value="1" selected="selected">1</option>
				<?php
				
				for ($i = 2; $i <= 9; $i++)
				{
					?>
					<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
					<?php
				}
				?>
				
				</select>
			</td><tr>
			<tr><td style="width: 304px; height: 28px;"><label>Type de remise : </label></td>
			<td>
				<select name="TYPEREMISE">
				<option value="0" selected="selected">Valeur</option>
				<option value="1" >Pourcentage</option>
				</select>
			</td><tr>
			<tr><td style="width: 304px; height: 28px;"><label>Affectation de la remise : </label></td>
			<td>
				<select name="REMISEDIRECT">
				<option value="0" selected="selected">Création d'un bon achat</option>
				<option value="1" >Remise direct</option>
				</select>
			</td><tr>
			  <td style="width: 304px; height: 28px;">Valeur de la remise</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="VALEURPROMO" required></td>
			</tr>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
<input type="hidden" value="AJOUT" name="action">
</form>
<?php
}

function insert_new_promo()
{
	$sql = "SELECT * FROM PROMOTION WHERE PR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND PR_DECLENCHEUR = '".$_POST['DECLENCHEUR']."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	$count = count($tab_r);
	if (count($tab_r) == 0)
	{
		$sql = 'INSERT INTO `PROMOTION`(`PR_LIBELLE`, `PR_ETABLISSEMENT`, `PR_DECLENCHEUR`, `PR_QTEDECLENCHEUR`, `PR_TYPEREMISE`, `PR_VALEUR`, `PR_REMISEDIRECT`, `PR_ACTIVE`) VALUES
				("'.addslashes($_POST['LIBELLEPROMO']).'","'.$_SESSION['ETABADMIN'].'","'.$_POST['DECLENCHEUR'].'",'.$_POST['QTEDECLENCHEUR'].','.$_POST['TYPEREMISE'].',"'.$_POST['VALEURPROMO'].'",'.$_POST['REMISEDIRECT'].',"OUI");';
		
		
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);	
		echo "Création de la promo " .$_POST['LIBELLEPROMO'] ."  terminée!";
	}
	else
	{
		foreach ($tab_r as $r)
		{
			echo "L'article déclencheur " .$r['PR_DECLENCHEUR'] ." est déja affecté à une promotion !";
		}
	}
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function modif_promo()
{
	
	if(!isset($_POST["action"]))
	{
		$sql = "select * from PROMOTION WHERE PR_NUMERO =" .$_GET['numero'];
	
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
?>
		<form  action="" method="post">
		<table style='text-align: left; width: 474px;  font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; height: 28px;  color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification d'une promotion</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de la promotion</td>
			  <td style="height: 28px; width: 300px;"><input maxlength="70" size="70" tabindex="2" name="LIBELLEPROMO" value='<?php echo $data['PR_LIBELLE']; ?>'></td>
			</tr>
				<td style="width: 304px; height: 28px;"><label>Article déclencheur : </label></td>
			<td>
				<select name="DECLENCHEUR">
				<?php
				$sql = "select * FROM ARTICLE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ORDER BY AR_CODEARTICLE ";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					if ($data['PR_DECLENCHEUR'] == $data1['AR_CODEARTICLE'])
					{
						?>
						<option value="<?php echo $data1['AR_CODEARTICLE']; ?>" selected="selected"><?php echo $data1['AR_DESIGNATION']; ?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $data1['AR_CODEARTICLE']; ?>"><?php echo $data1['AR_DESIGNATION']; ?></option>
						<?php
					}

				}
				?>
				</select>
			</td></tr>
			<tr><td style="width: 304px; height: 28px;"><label>Quantité déclencheur : </label></td>
				<td>
				<select name="QTEDECLENCHEUR">
				<option value="1" selected="selected">1</option>
				<?php
				
				for ($i = 1; $i <= 9; $i++)
				{
					if ($i == $data['PR_QTEDECLENCHEUR'])
					{
						?>
						<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
						<?php
					}
				}
				?>
				
				</select>
			</td><tr>
			<tr><td style="width: 304px; height: 28px;"><label>Type de remise : </label></td>
			<td>
				<select name="TYPEREMISE">
				<?php
				$listevar = array("Valeur","Pourcentage");
				for ($i = 0; $i <= 1; $i++)
				{
					if ($i == $data['PR_TYPEREMISE'])
					{
						?>
						<option value="<?php echo $i;?>" selected="selected"><?php echo $listevar[$i];?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $i;?>" ><?php echo $listevar[$i];?></option>
						<?php
					}
				}
				?>
				</select>
			</td><tr>
			<tr><td style="width: 304px; height: 28px;"><label>Affectation de la remise : </label></td>
			<td>
				<select name="REMISEDIRECT">
				<?php
				$listevar = array("Création d'un bon achat","Remise direct");
				for ($i = 0; $i <= 1; $i++)
				{
					if ($i == $data['PR_REMISEDIRECT'])
					{
						?>
						<option value="<?php echo $i;?>" selected="selected"><?php echo $listevar[$i];?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $i;?>" ><?php echo $listevar[$i];?></option>
						<?php
					}
				}
				?>
				</select>
			</td><tr>
			<tr><td style="width: 304px; height: 28px;"><label>Promotion activée : </label></td>
			<td>
				<select name="PROMOACTIVE">
				<?php
				$listevar = array("OUI","NON");
				for ($i = 0; $i <= 1; $i++)
				{
					if ($listevar[$i] == $data['PR_ACTIVE'])
					{
						?>
						<option value="<?php echo $listevar[$i];?>" selected="selected"><?php echo $listevar[$i];?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $listevar[$i];?>" ><?php echo $listevar[$i];?></option>
						<?php
					}
				}
				?>
				</select>
			</td><tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Valeur de la promotion :</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="VALEURPROMO" value='<?php echo $data['PR_VALEUR']; ?>'></td>
			</tr>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			
			$sql = 'UPDATE PROMOTION SET PR_LIBELLE = "'.$_POST['LIBELLEPROMO'].'", PR_DECLENCHEUR = "'.$_POST['DECLENCHEUR'].'",PR_QTEDECLENCHEUR = "'.$_POST['QTEDECLENCHEUR'].'",
					PR_TYPEREMISE = "'.$_POST['TYPEREMISE'].'", PR_VALEUR = "'.$_POST['VALEURPROMO'].'", PR_REMISEDIRECT = "'.$_POST['REMISEDIRECT'].'", PR_ACTIVE = "'.$_POST['PROMOACTIVE'].'"
					WHERE PR_NUMERO = "'.$_GET['numero'].'";';
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			echo "Mise à jour de promotion " .$_POST['LIBELLEPROMO'] ."terminé";
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}