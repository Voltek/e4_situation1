<?php
/**
 * rapport_reservation.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
require('fpdf.php');

$db = mysql_connect('127.0.0.1', 'LAVERRIERE', '!LaVerrire59530');

	// on sélectionne la base
mysql_select_db('LAVERRIERE',$db);
$sql = "SELECT * FROM ETABLISSEMENT WHERE ET_ETABLISSEMENT = 'ET0001'";
$req = mysql_query($sql) or die('Erreur SQL !<br>'.mysql_error());
while($data = mysql_fetch_array($req))
	{
		$nometab = $data['ET_LIBELLE'];
		$villeetab = $data['ET_VILLE'];
	}

$valtest = "tovilleetabto";
$toto = "TOTO11";

class PDF extends FPDF
{

function Header()
{
	global $nometab;
	global $villeetab;
    $this->SetFont('Arial','B',15);
    $this->Cell(0,10,'Liste des réservation',1,1,C);
	$this->Cell(0,10,$nometab,0,1,L);
	$this->SetFont('Arial','B',10);
	$this->Cell(0,10,$villeetab,0,1,L);
    // Saut de ligne
    $this->Ln(5);
}


}


$pdf = new PDF('L','mm','A4');

$pdf->AddPage();
$pdf->SetFont('Times','',12);
for($i=1;$i<=40;$i++)
    $pdf->Cell(0,10,'Impression de la ligne numéro '.$i,0,1);
$pdf->Output();
?>
