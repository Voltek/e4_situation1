﻿<?php

/**
 * fonction_planning.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


include ("include/fonction_general.php");

function modifplanning()
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from PLANNING where PL_USER = '" . $_POST['user'] ."' and PL_MOIS = " .$_POST['mois'] ." AND PL_ANNEE = ".$_POST['year']. " order by PL_JOUR;";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
	while($data = mysqli_fetch_array($req))
	{
		$JourOk[$data['PL_JOUR']] = "1";
	}
	mysqli_close;
	$nbrinsert = 0;
	$sqlinsert = "insert into PLANNING (PL_USER, PL_DATE, PL_JOUR, PL_MOIS, PL_ANNEE) VALUES ";
	$sqldelete = "delete from PLANNING WHERE";
	$nbrdelete = 0;

	for ($i=1; $i<32; $i++)
	{
		if ($_POST['cbox'.$i] == "on" and $JourOk[$i] == "0")
		{
			if ($nbrinsert != 0)
			{
				$sqlinsert = $sqlinsert .",";
			}
			$sqlinsert = $sqlinsert ."('" .$_POST['user'] ."','" .date('Y-m-d', strtotime($_POST['mois'] ."/" .$i ."/" .$_POST['year'])) ."'," .$i .","
				.$_POST['mois'] ."," .$_POST['year'] .")";
			$nbrinsert = $nbrinsert + 1;
		}
		if ($_POST['cbox'.$i] != "on" and $JourOk[$i] == "1")
		{
			if ($nbrdelete != 0)
			{
				$sqldelete = $sqldelete ." OR";
			}
			$sqldelete = $sqldelete ." (PL_FACTURE = 'NON' AND PL_USER = '" .$_SESSION['login'] ."' AND PL_JOUR = " .$i ." AND PL_MOIS = " .$_POST['mois'] ." AND PL_ANNEE = ".$_POST['year'] .")";
			$nbrdelete = $nbrdelete + 1;
		}
	}

	if ($nbrinsert != 0)
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$req = $conn->query($sqlinsert) or die('Erreur SQL !<br>');
		mysqli_close();
	}



	echo "Planning mis à jour !";
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php

}

function modifresa()
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from RESERVATION where RE_USER = '" . $_SESSION['login'] ."' and RE_MOIS = " .$_POST['mois'] ." AND RE_ANNEE = ".$_POST['year']. "
		AND RE_EMPLACEMENT = 'EM0001' AND RE_ETABLISSEMENT = 'ET0001' order by RE_JOUR;";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
	while($data = mysql_fetch_array($req))
	{
		$JourOk[$data['RE_JOUR']] = "1";
	}

	$nbrinsert = 0;
	$sqlinsert = "insert into RESERVATION (RE_USER, RE_DATE, RE_JOUR, RE_MOIS, RE_ANNEE, RE_ETABLISSEMENT, RE_EMPLACEMENT) VALUES ";
	$sqldelete = "delete from RESERVATION WHERE";
	$nbrdelete = 0;

	for ($i=1; $i<32; $i++)
	{
		if ($_POST['cbox'.$i] == "on" and $JourOk[$i] == "0")
		{
			if ($nbrinsert != 0)
			{
				$sqlinsert = $sqlinsert .",";
			}
			$sqlinsert = $sqlinsert ."('" .$_SESSION['login'] ."','" .date('Y-m-d', strtotime($_POST['mois'] ."/" .$i ."/" .$_POST['year'])) ."'," .$i .","
				.$_POST['mois'] ."," .$_POST['year'] .", 'ET0001', 'EM0001')";
			$nbrinsert = $nbrinsert + 1;
		}
		if ($_POST['cbox'.$i] != "on" and $JourOk[$i] == "1")
		{
			if ($nbrdelete != 0)
			{
				$sqldelete = $sqldelete ." OR";
			}
			$sqldelete = $sqldelete ." (RE_USER = '" .$_SESSION['login'] ."' AND RE_JOUR = " .$i ." AND RE_MOIS = " .$_POST['mois'] ." AND RE_ANNEE = ".$_POST['year'] ."
				AND RE_ETABLISSEMENT = 'ET0001' AND RE_EMPLACEMENT = 'EM0001')";
			$nbrdelete = $nbrdelete + 1;
		}
	}
	if ($nbrinsert != 0)
	{
		$req = $conn->query($sqlinsert) or die('Erreur SQL !<br>');
	}
	if ($nbrdelete != 0)
	{
		$req = $conn->query($sqldelete) or die('Erreur SQL !<br>');
	}


	mysqli_close();
	affiche_resa();
}


function affiche_planning()
{
$jour_actuel = date("j", time());
$mois_actuel = date("m", time());
$an_actuel = date("Y", time());
$jour = $jour_actuel;

// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante
if(!isset($_GET["mois"]))
{
	$mois = $mois_actuel;
	$an = $an_actuel;
}
else
{
	$mois = $_GET['mois'];
	$an = $_GET['an'];
}
if (!isset($_GET['user']))
{
	$user = $_SESSION['login'];
}
else
{
	$user = $_GET['user'];
}

//defini le mois suivant
$mois_suivant = $mois + 1;
$an_suivant = $an;
if ($mois_suivant == 13)
{
	$mois_suivant = 1;
	$an_suivant = $an + 1;
}

//defini le mois précédent
$mois_prec = $mois - 1;
$an_prec = $an;
if ($mois_prec == 0)
{
	$mois_prec = 12;
	$an_prec = $an - 1;
}

//affichage du mois et de l'année en french
$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
$mois_en_clair = $mois_de_annee[$mois - 1];
	?>
	<br />

<table align="center" width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_cal">
	<tr>
		<td height="51" colspan="7">
			<table width="381" border="0" cellpadding="0" cellspacing="0">
				<tr>
				  <td width="290" class="date"><div><?php echo $mois_en_clair," ", $an; ?></div></td>
				</tr>
		  </table>
	  </td>
	</tr>
	<tr align="center" class="jours">
		<td width="60">L</td>
		<td width="60">M</td>
		<td width="60">M</td>
		<td width="60">J</td>
		<td width="60">V</td>
		<td width="60">S</td>
		<td width="60">D</td>
	</tr>
</table>

<form  action="" method="post">
<table align="center"  width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_numero">
	<tr align="center">
<?php
//Détection du 1er et dernier jour du moiS
$nombre_date = mktime(0,0,0, $mois, 1, $an);
$premier_jour = date('w', $nombre_date);
$dernier_jour = 28;

$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select * from PLANNING where PL_USER = '" . $user ."' and PL_MOIS = " .$mois ." AND PL_ANNEE = ".$an. " order by PL_JOUR;";

$req = $conn->query($sql) or die('Erreur SQL !<br>');
$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
$JourFacture = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
while($data = mysqli_fetch_array($req))
{
	$JourOk[$data['PL_JOUR']] = "1";
	if ($data['PL_FACTURE'] == "OUI")
	{
		$JourFacture[$data['PL_JOUR']] = "1";
	}
}
mysqli_close;

while (checkdate($mois, $dernier_jour + 1, $an))
	{ $dernier_jour++;}

//Affichage de 7 jours du calendrier

for ($i = 1; $i < 8; $i++){
	if ($i < $premier_jour)
	{
		echo '<td width="60"></td>';
	}
	else
	{
		$ce_jour = ($i+1) - $premier_jour;
		// si c'est un jour reserve on applique le style reserve
		if($tab_jours[$ce_jour]){
			echo '<td width="60" class="reserve">';
		// sinon on ne met pas de style
		}else{
			echo '<td width="60">';
		}
		echo $ce_jour;
		if ($JourOk[$ce_jour]==1)
		{

				echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'" checked disabled="disabled">';

		}
		else
		{
			echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'">';
		}

		echo '</td>';
	}
}
//affichage du reste du calendrier
$jour_suiv = ($i+1) - $premier_jour;
for ($rangee = 0; $rangee <= 4; $rangee++){
		echo '</tr>';
		echo '<tr align="center" class="numero">';
		for ($i = 0; $i < 7; $i++){
			if($jour_suiv > $dernier_jour){
				echo '<td width="60">';
				echo '</td>';
			}else{
				// si c'est un jour reserve on applique le style reserve
				if($tab_jours[$jour_suiv]){
					echo '<td width="60" class="reserve">';
				// sinon on ne met pas de style
				}else{
					echo '<td width="60">';
				}
				echo $jour_suiv;
				if ($JourOk[$jour_suiv]==1)
				{

						echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'" checked disabled="disabled">';

				}
				else
				{
					echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'">';
				}


				echo '</td>';
			}
			$jour_suiv++;
		}
}
?>
</tr>
</table>
</br>

<input type="hidden" value="<?php echo $mois; ?>" name="mois">
<input type="hidden" value="<?php echo $an; ?>" name="year">
<input type="hidden" value="<?php echo $user; ?>" name="user">
<input type="hidden" value="oui" name="modifplanning">
<center><input class=bouton1 type="submit" value="Valider" /></center>
</form>
<?php
}

function affiche_resa()
{
$jour_actuel = date("j", time());
$mois_actuel = date("m", time());
$an_actuel = date("Y", time());
$jour = $jour_actuel;

// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante
if(!isset($_GET["mois"]))
{
	$mois = $mois_actuel;
	$an = $an_actuel;
}
else
{
	$mois = $_GET['mois'];
	$an = $_GET['an'];
}

//defini le mois suivant
$mois_suivant = $mois + 1;
$an_suivant = $an;
if ($mois_suivant == 13)
{
	$mois_suivant = 1;
	$an_suivant = $an + 1;
}

//defini le mois précédent
$mois_prec = $mois - 1;
$an_prec = $an;
if ($mois_prec == 0)
{
	$mois_prec = 12;
	$an_prec = $an - 1;
}

//affichage du mois et de l'année en french
$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
$mois_en_clair = $mois_de_annee[$mois - 1];
	?>
	<br />

<table align="center" width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_cal">
	<tr>
		<td height="51" colspan="7">
			<table width="381" border="0" cellpadding="0" cellspacing="0">
				<tr>
				  <td width="290" class="date"><div><?php echo $mois_en_clair," ", $an; ?></div></td>
					<td width="50">
						<a href="reservation.php?mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>">
					  <div align="left"><img border="0" src="img/prec.png" /></div></a>
				  </td>
					<td width="41">
						<a href="reservation.php?mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>">
					  <div><img border="0" src="img/suiv.png" /></div>
					  </a>
				  </td>
				</tr>
		  </table>
	  </td>
	</tr>
	<tr align="center" class="jours">
		<td width="60">D</td>
		<td width="60">L</td>
		<td width="60">M</td>
		<td width="60">M</td>
		<td width="60">J</td>
		<td width="60">V</td>
		<td width="60">S</td>
	</tr>
</table>
<form action="" method="post">
<table align="center"  width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_numero">
	<tr align="center">
<?php
//Détection du 1er et dernier jour du moiS
$nombre_date = mktime(0,0,0, $mois, 1, $an);
$premier_jour = date('w', $nombre_date);
$dernier_jour = 28;

$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select trim(R1.RE_USER) as RE_USER, ET_LIBELLE, R1.RE_DATE, R1.RE_JOUR, R1.RE_MOIS, R1.RE_ANNEE, (select count(*) from RESERVATION R2 WHERE R2.RE_DATE = R1.RE_DATE) as RESAACTIVE,
EM_NBRRESAMAX - (select count(*) from RESERVATION R2 WHERE R2.RE_DATE = R1.RE_DATE) as RESARESTANTE, EM_NBRRESAMAX,
case when R1.RE_DATE < CURDATE() then 'N' else 'O' end as MODIFIABLE, DATE_FORMAT(CURDATE(),'%d') as DATEMIN
from RESERVATION R1
left join ETABLISSEMENT on R1.RE_ETABLISSEMENT = ET_ETABLISSEMENT
left join EMPLACEMENT on EM_ETABLISSEMENT = R1.RE_ETABLISSEMENT and EM_EMPLACEMENT = R1.RE_EMPLACEMENT
where R1.RE_MOIS = " .$mois ." AND R1.RE_ANNEE = ".$an. " order by R1.RE_JOUR;";

$req = $conn->query($sql) or die('Erreur SQL !<br>');
$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
$JourNonModifiable = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
$MaxResa = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
while($data = mysql_fetch_array($req))
{
	if ($_SESSION['login'] == $data['RE_USER'])
	{
		$JourOk[$data['RE_JOUR']] = "1";
	}

	if ($data['RESARESTANTE'] == "0")
	{
		$MaxResa[$data['RE_JOUR']] = "1";
	}
	if ($data['MODIFIABLE'] == "N")
	{
		$JourNonModifiable[$data['RE_JOUR']] = "1";
	}
	for ($i=1; $i< $data['DATEMIN']; $i++)
	{
		$JourNonModifiable[$i] = "1";
	}
}
mysqli_close;
if ($an > date("Y", time()))
{
	for ($i=1; $i< 32; $i++)
	{
		$JourNonModifiable[$i] = "0";
	}
}
if ($an == date("Y", time()))
{
	if ($mois > date("m", time()))
	{
		for ($i=1; $i< 32; $i++)
		{
			$JourNonModifiable[$i] = "0";
		}
	}
	elseif ($mois < date("m", time()))
	{
		for ($i=1; $i< 32; $i++)
		{
			$JourNonModifiable[$i] = "1";
		}
	}
}
if ($an < date("Y", time()))
{
	echo "5";
	for ($i=1; $i< 32; $i++)
	{
		$JourNonModifiable[$i] = "1";
	}
}




while (checkdate($mois, $dernier_jour + 1, $an))
	{ $dernier_jour++;}

//Affichage de 7 jours du calendrier

for ($i = 0; $i < 7; $i++){
	if ($i < $premier_jour)
	{
		echo '<td width="60"></td>';
	}
	else
	{
		$ce_jour = ($i+1) - $premier_jour;
		// si c'est un jour reserve on applique le style reserve
		if($MaxResa[$ce_jour]==1)
		{
			echo '<td width="60" class="reserve">rr';
		// sinon on ne met pas de style
		}
		else
		{
			if ($JourNonModifiable[$ce_jour]==0)
			{
				echo '<td width="60" class="nonreserve">';
			}
			else
			{
				echo '<td width="60">';
			}

		}
		echo $ce_jour;
		if ($JourOk[$ce_jour]==1)
		{
			if ($JourNonModifiable[$ce_jour]==1)
			{
				echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'" checked disabled="disabled">';
			}
			else
			{
				echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'" checked>';
			}
		}
		else
		{
			if ($JourNonModifiable[$ce_jour]==1)
			{
				echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'" disabled="disabled">';
			}
			else
			{
				echo '<input type="checkbox" id="cbox'.$ce_jour.'" name="cbox'.$ce_jour.'">';
			}

		}

		echo '</td>';
	}
}
//affichage du reste du calendrier
$jour_suiv = ($i+1) - $premier_jour;
for ($rangee = 0; $rangee <= 4; $rangee++){
		echo '</tr>';
		echo '<tr align="center" class="numero">';
		for ($i = 0; $i < 7; $i++){
			if($jour_suiv > $dernier_jour){
				echo '<td width="60">';
				echo '</td>';
			}else{
				// si c'est un jour reserve on applique le style reserve
				if($MaxResa[$jour_suiv]){
					echo '<td width="60" class="reserve">';
				// sinon on ne met pas de style
				}else
				{
					if ($JourNonModifiable[$jour_suiv]==0)
					{
						echo '<td width="60" class="nonreserve">';
					}
					else
					{
						echo '<td width="60">';
					}
				}
				echo $jour_suiv;
				if ($JourOk[$jour_suiv]==1)
				{
					if ($JourNonModifiable[$jour_suiv]==1)
					{
						echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'" checked disabled="disabled">';
					}
					else
					{
						echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'" checked>';
					}
				}
				else
				{
					if ($JourNonModifiable[$jour_suiv]==1)
					{
						echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'" disabled="disabled">';
					}
					else
					{
						if($MaxResa[$jour_suiv] == 1)
						{
							echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'" disabled="disabled">';
						}
						else
						{
							echo '<input type="checkbox" id="cbox'.$jour_suiv.'" name="cbox'.$jour_suiv.'">';
						}

					}
				}

				echo '</td>';
			}
			$jour_suiv++;
		}
}
?>
</tr>
</table>
</br>

<input type="hidden" value="<?php echo $mois; ?>" name="mois">
<input type="hidden" value="<?php echo $an; ?>" name="year">
<input type="hidden" value="oui" name="modifresa">
<center><input type="submit" value="Valider" /></center>
</form>
<?php
}

function affiche_planning_alluser()
{

$jour_actuel = date("j", time());
$mois_actuel = date("m", time());
$an_actuel = date("Y", time());
$jour = $jour_actuel;

// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante
if(!isset($_GET["mois"]))
{
	$mois = $mois_actuel;
	$an = $an_actuel;
}
else
{
	$mois = $_GET['mois'];
	$an = $_GET['an'];
}

if (!isset($_GET['user']))
{
	$user = $_SESSION['login'];
}
else
{
	$user = $_GET['user'];
}

//defini le mois suivant
$mois_suivant = $mois + 1;
$an_suivant = $an;
if ($mois_suivant == 13)
{
	$mois_suivant = 1;
	$an_suivant = $an + 1;
}

//defini le mois précédent
$mois_prec = $mois - 1;
$an_prec = $an;
if ($mois_prec == 0)
{
	$mois_prec = 12;
	$an_prec = $an - 1;
}

//affichage du mois et de l'année en french
$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
$mois_en_clair = $mois_de_annee[$mois - 1];
	?>
	<br />

<table align="center" width="720" border="0" cellpadding="2" cellspacing="0" >
<tr><td align="left">

<select name="listuser" id="listuser" onchange="window.location='visuplanning.php?user='+this.value;">
<OPTION>Choisir un autre utilisateur  </OPTION>
<?php
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select PL_USER, UT_NOM, UT_PRENOM from PLANNING left join UTILISATEUR on UT_LOGIN = PL_USER where PL_USER <> '" .$user . "' GROUP BY PL_USER;";
$req = $conn->query($sql) or die('Erreur SQL !<br>');
while ($data = mysqli_fetch_array($req))
	{
	echo '<option>' .$data['PL_USER'] .'</option>';
	}
mysqli_close;
?>
</SELECT></TD><tr>

<?php
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select UT_LOGIN, UT_NOM, UT_PRENOM, (select count(*) from PLANNING where PL_USER = UT_LOGIN AND PL_FACTURE = 'OUI') as NBJOURSFACTURE,
	   (select count(*) from PLANNING where PL_USER = UT_LOGIN AND PL_FACTURE = 'NON') as NBJOURSNONFACTURE from UTILISATEUR where UT_LOGIN = '" .$user . "';";
$req = $conn->query($sql) or die('Erreur SQL !<br>');
while ($data = mysqli_fetch_array($req))
	{
	echo '<td align="left" colspan=2>Login : ' .$data['UT_LOGIN'] . ' </td>';
	echo '<td align="left" colspan=2>Nom : ' .$data['UT_NOM'] . ' </td>';
	echo '<td align="left" colspan=2>Prénom : ' .$data['UT_PRENOM'] . ' </td></tr>';
	echo '<tr><td align="left" colspan=5>Nombre de jours facturé : ' .$data['NBJOURSFACTURE'] . '</td></tr>';
	echo '<tr><td align="left" colspan=5>Nombre de jours à facturer : ' .$data['NBJOURSNONFACTURE'] . '</td></tr>';
	echo '<tr></tr>';
	}

mysqli_close;
?>
</table>
<br>
<table align="center" width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_cal">
	<tr>
		<td height="51" colspan="7">
			<table width="381" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="290" class="date"><div><?php echo $mois_en_clair," ", $an; ?></div></td>
					<td width="50">
						<a href="visuplanning.php?mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>&user=<?php echo $user; ?>">
					  <div align="left"><img border="0" src="img/prec.png" /></div></a>
				  </td>
					<td width="41">
						<a href="visuplanning.php?mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>&user=<?php echo $user; ?>">
					  <div><img border="0" src="img/suiv.png" /></div>
					  </a>
				  </td>
				</tr>
		  </table>
	  </td>
	</tr>
	<tr align="center" class="jours">
		<td width="60">L</td>
		<td width="60">M</td>
		<td width="60">M</td>
		<td width="60">J</td>
		<td width="60">V</td>
		<td width="60">S</td>
		<td width="60">D</td>
	</tr>
</table>

<table align="center"  width="420" border="0" cellpadding="9" cellspacing="0"  class="tab_numero">
	<tr align="center">
<?php
//Détection du 1er et dernier jour du moiS
$nombre_date = mktime(0,0,0, $mois, 1, $an);
$premier_jour = date('w', $nombre_date);
$dernier_jour = 28;

$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select * from PLANNING where PL_USER = '" . $user ."' and PL_MOIS = " .$mois ." AND PL_ANNEE = ".$an. " order by PL_JOUR;";
$req = $conn->query($sql) or die('Erreur SQL !<br>');
$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
$JourFacture = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
while($data = mysqli_fetch_array($req))
{
	$JourOk[$data['PL_JOUR']] = "1";
	if ($data['PL_FACTURE'] == "OUI")
	{
		$JourFacture[$data['PL_JOUR']] = "1";
	}
}
mysqli_close;

while (checkdate($mois, $dernier_jour + 1, $an))
	{ $dernier_jour++;}

//Affichage de 7 jours du calendrier

for ($i = 1; $i < 8; $i++){
	if ($i < $premier_jour)
	{
		echo '<td width="60" border="1px"></td>';
	}
	else
	{
		$ce_jour = ($i+1) - $premier_jour;
		// si c'est un jour reserve on applique le style reserve
		if($JourOk[$ce_jour]==1){
			echo '<td width="60" class="nonreserve">' .$ce_jour. '</td>';
		// sinon on ne met pas de style
		}else{
			echo '<td width="60" >' .$ce_jour. '</td>';
		}
	}
}
//affichage du reste du calendrier
$jour_suiv = ($i+1) - $premier_jour;
for ($rangee = 0; $rangee <= 4; $rangee++){
		echo '</tr>';
		echo '<tr align="center" class="numero">';
		for ($i = 0; $i < 7; $i++){
			if($jour_suiv > $dernier_jour){
				echo '<td width="60">';
				echo '</td>';
			}else{
				// si c'est un jour reserve on applique le style reserve
				if($JourOk[$jour_suiv]==1){
					echo '<td width="60" class="nonreserve">' .$jour_suiv. '</td>';
				// sinon on ne met pas de style
				}else{
					echo '<td width="60" >' .$jour_suiv. '</td>';
				}
			}
			$jour_suiv++;
		}
}
?>
</tr>
</table>
</br>

<?php
}

function affiche_planning_user()
{

$jour_actuel = date("j", time());
$mois_actuel = date("m", time());
$an_actuel = date("Y", time());
$jour = $jour_actuel;

// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante
if(!isset($_GET["mois"]))
{
	$mois = $mois_actuel;
	$an = $an_actuel;
}
else
{
	$mois = $_GET['mois'];
	$an = $_GET['an'];
}

if (!isset($_GET['user']))
{
	$user = $_SESSION['login'];
}
else
{
	$user = $_GET['user'];
}

//defini le mois suivant
$mois_suivant = $mois + 1;
$an_suivant = $an;
if ($mois_suivant == 13)
{
	$mois_suivant = 1;
	$an_suivant = $an + 1;
}

//defini le mois précédent
$mois_prec = $mois - 1;
$an_prec = $an;
if ($mois_prec == 0)
{
	$mois_prec = 12;
	$an_prec = $an - 1;
}

//affichage du mois et de l'année en french
$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
$mois_en_clair = $mois_de_annee[$mois - 1];
	?>
	<br />


<br>
<table align="center" width="800" border="0" cellpadding="0" cellspacing="0" >

<tr>
<td width="300">
<table border = "0" cellpadding="5" >
<tr><td>
<?php
if ($_SESSION['STATUT']=='ADMIN')
{
	?>
	<select name="listuser" id="listuser" onchange="window.location='planning.php?user='+this.value;">
	<OPTION>Choisir un autre utilisateur  </OPTION>
	<?php
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select PL_USER, UT_NOM, UT_PRENOM from PLANNING left join UTILISATEUR on UT_LOGIN = PL_USER where PL_USER <> '" .$user . "' GROUP BY PL_USER;";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while ($data = mysqli_fetch_array($req))
		{
		echo '<option>' .$data['PL_USER'] .'</option>';
		}
	mysqli_close;
	?>
	</SELECT>
	<?php
}
?>
</td></tr></table>
</td>
<td width="50"> <table width="50" border=0 cellpadding="2"></table>   </td>
<td width="420">
<table align="center" width="420" border="0" cellpadding="5" cellspacing="0"  class="tab_cal">
	<tr>
		<td height="51" colspan="7">
			<table width="381" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="290" class="date"><div><?php echo $mois_en_clair," ", $an; ?></div></td>
					<td width="50">
						<a href="planning.php?mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>&user=<?php echo $user; ?>">
					  <div align="left"><img border="0" src="img/prec.png" /></div></a>
				  </td>
					<td width="41">
						<a href="planning.php?mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>&user=<?php echo $user; ?>">
					  <div><img border="0" src="img/suiv.png" /></div>
					  </a>
				  </td>
				</tr>
		  </table>
	  </td>
	</tr>
	<tr align="center" class="jours">
		<td width="60">L</td>
		<td width="60">M</td>
		<td width="60">M</td>
		<td width="60">J</td>
		<td width="60">V</td>
		<td width="60">S</td>
		<td width="60">D</td>
	</tr>
</table>
</td><td width="200"></td></tr>
<tr>
</td><td width="300">
		<table border = "0" cellpadding="5" >

			<?php

			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "select UT_LOGIN, UT_NOM, UT_PRENOM, (select count(*) from PLANNING where PL_USER = UT_LOGIN AND PL_FACTURE = 'OUI') as NBJOURSFACTURE,
				   (select count(*) from PLANNING where PL_USER = UT_LOGIN AND PL_FACTURE = 'NON') as NBJOURSNONFACTURE from UTILISATEUR where UT_LOGIN = '" .$user . "';";
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			while ($data = mysqli_fetch_array($req))
				{
				echo '<tr><td align="left" colspan=2>' .$data['UT_LOGIN'] . ' </td></tr>';
				echo '<tr><td align="left" colspan=2>' .$data['UT_NOM'] . ' </td></tr>';
				echo '<tr><td align="left" colspan=2>' .$data['UT_PRENOM'] . ' </td></tr>';
				echo '<tr><td align="left" colspan=2>' .$data['NBJOURSNONFACTURE'] . ' jours à facturer</td></tr>';
				echo '<tr></tr>';
				}

			mysqli_close;
			?>

		</table>
	</td>
<td width="50">

</td>
<td width="420">
<table align="center"  width="420" border="0" cellpadding="9" cellspacing="0"  class="tab_numero">
	<tr align="center">
<?php
//Détection du 1er et dernier jour du moiS
$nombre_date = mktime(0,0,0, $mois, 1, $an);
$premier_jour = date('w', $nombre_date);
$dernier_jour = 28;

$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "select * from PLANNING where PL_USER = '" . $user ."' and PL_MOIS = " .$mois ." AND PL_ANNEE = ".$an. " order by PL_JOUR;";

$req = $conn->query($sql) or die('Erreur SQL !<br>');
$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
$JourFacture = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");
while($data = mysqli_fetch_array($req))
{
	$JourOk[$data['PL_JOUR']] = "1";
	if ($data['PL_FACTURE'] == "OUI")
	{
		$JourFacture[$data['PL_JOUR']] = "1";
	}
}
mysqli_close;

while (checkdate($mois, $dernier_jour + 1, $an))
	{ $dernier_jour++;}

//Affichage de 7 jours du calendrier

for ($i = 1; $i < 8; $i++){
	if ($i < $premier_jour)
	{
		echo '<td width="60" border="1px"></td>';
	}
	else
	{
		$ce_jour = ($i+1) - $premier_jour;
		// si c'est un jour reserve on applique le style reserve
		if($JourOk[$ce_jour]==1){
			echo '<td width="60" class="nonreserve">' .$ce_jour. '</td>';
		// sinon on ne met pas de style
		}else{
			echo '<td width="60" >' .$ce_jour. '</td>';
		}
	}
}
//affichage du reste du calendrier
$jour_suiv = ($i+1) - $premier_jour;
for ($rangee = 0; $rangee <= 4; $rangee++){
		echo '</tr>';
		echo '<tr align="center" class="numero">';
		for ($i = 0; $i < 7; $i++){
			if($jour_suiv > $dernier_jour){
				echo '<td width="60">';
				echo '</td>';
			}else{
				// si c'est un jour reserve on applique le style reserve
				if($JourOk[$jour_suiv]==1){
					echo '<td width="60" class="nonreserve">' .$jour_suiv. '</td>';
				// sinon on ne met pas de style
				}else{
					echo '<td width="60" >' .$jour_suiv. '</td>';
				}
			}
			$jour_suiv++;
		}
}

?>
	</tr>
	</table>
	</td><td width="200">
		<table border = "0" cellpadding="15">
			<tr><td><input type=button value="Ajouter une date" class="bouton1" onclick="window.open('modif_planning.php?mois=<?php echo $mois; ?>&an=<?php echo $an; ?>&user=<?php echo $user; ?>', 'exemple', 'height=400, width=500, top=100, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td></tr>
			<tr><td><input type=button value="Retirer une date" class="bouton1"></td></tr>
		</table>
	</td></tr>
</table>
</br>

<?php
}
