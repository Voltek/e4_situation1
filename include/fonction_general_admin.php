﻿<?php

/**
 * fonction_general_admin.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

function header_page()
{
	?>
	<link rel="icon" href="img/laverriere.ico" />
	<title>Gestion Tiers Lieux Haut de France</title>
	<?php
}

function old_entete_page($titre_footer)
{

?>

<table border="0" width="100%" height="20%">
<tr>
	<td width="20%"><img src="img/logo-la-verriere.png" border="0" width="400" height="115" alt=""></td>

	<td>
	<table border="0" width="100%">
	<tr><td><div id="titre">Gestion Tiers Lieux Haut de France</div></td></tr>
	<tr align="center"><td align="center"><div id="text1"><?php echo $titre_footer ?></div></td></tr>
	<tr><td><?php echo barre_menu(); ?></td></tr>
	</table>
	</td>
	<td>
	<table>
	<?php
	echo '<tr><td width="20%"><div id="text1">'.$_SESSION['PRENOM']. ' '.$_SESSION['NOM']. '</div></td></tr>';

	if (isset($_SESSION['login']))
	{
		?>
		<tr><td><a style="margin-left:30px; color:#F69730" href="deconnection.php" ><B>Déconnexion</b></a> </td></tr>
		<?php
	}

	?>

	</table>
</tr>
</table>
<hr><br>
<div>
<?php
}

function entete_page($titre_footer)
{

?>

<table style="text-align: left; width: 100%; height: 20%;" border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="width: 20%; height: 100px;"><img style="width: 400px; height: 111px;" alt="" src="img/logo-la-verriere.png"></td>
      <td style="width: 60%; height: 100px;">
      <table style="width: 75%; height: 100%; text-align: center; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
        <tbody>
          <tr><td><div id="titre">Gestion Tiers Lieux Haut de France</div></td></tr>
          <tr align="center"><td align="center"><div id="text1"><?php echo $titre_footer ?></div></td></tr>
		  <?php
		  if (isset($_SESSION['login']))
		  {
			if ($_SESSION['ETABADMIN'] = $_GET['etablissement'])
			{
				 ?>
				<tr><td><?php echo barre_menu(); ?></td></tr>
				<?php
			}

		  }
		  ?>

        </tbody>
      </table>
      </td>
      <td style="width: 20%; height: 100px;">
	  <table>
	<?php
	echo '<tr><td width="20%" align="center"><div id="text1">'.$_SESSION['PRENOM']. ' '.$_SESSION['NOM']. '</div></td></tr>';

	if (isset($_SESSION['login']))
	{
		?>
		<tr><td align="center"><a style="color:#F69730" href="deconnection.php" ><B>Déconnexion</b></a> </td></tr>
		<tr><td align="center"></td></tr>
		<tr><td align="center"><a style="color:#F69730" href="mon_compte.php" ><input type=button value="Mon compte" class="bouton1" ></a> </td></tr>
		<tr><td align="center"></td></tr>
		<?php


	}

	?>


	</table>
	  </td>
    </tr>
  </tbody>
</table>


<hr><br>
<div>
<?php
}

function petite_entete_page($titre_footer)
{

?>

<table border="0" width="100%" height="20%">
<tr>
	<td width="20%"><img src="img/logo-la-verriere.png" border="0" width="200" height="55" alt=""></td>

	<td>
	<table border="0" width="100%">
	<tr><td><div id="titre">Gestion Tiers Lieux Haut de France</div></td></tr>
	<tr><td><div id="text1"><?php echo $titre_footer ?></div></td></td>
	</table>
	</td>
	</tr>
</table>
<hr><br>
<div>
<?php
}

function barre_menu()
{
	?>
	<div id="menu">
	<ul>
	<li><a href="#">Utilisateur</a>
	<ul>
	  <li><a href="manageuser.php">Gestion Utilisateur</a></li>

	 </ul>
	</li>
	<?php
	if ($_SESSION['ETABADMIN'] = $_GET['etablissement'])
	{
		echo '<li><a href="#">Comptabilité</a>';
		echo '<UL>';
		echo '<li><a href="payment_facture.php">Paiement de facture</a></li>';
		echo '<li><a href="rapport_facture.php">Rapport des factures</a></li>';
		echo '<li><a href="list_facture.php?etablissement='.$_GET['etablissement'].'">Rapport des factures</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>

	<li><a href="#">Réservation et Saisie</a>
	<ul>
		<?php
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql="SELECT * FROM ZONE WHERE EM_ETABLISSEMENT = '".$_SESSION['ETABLISSEMENT']."'";
		$req = $conn->query($sql) or die('Erreur SQL !<br>');
		while($data = mysqli_fetch_array($req))
		{
			?>
			<li><a href="manageresa.php?typeplace=<?php echo $data['EM_EMPLACEMENT']; ?>"><?php echo $data['EM_LIBELLE']; ?></a></li>
			<?php
		}
		?>
	 </ul>
	</li>
	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Paramétrage</a>';
		echo '<UL>';
		echo '<li><a href="manageplace.php">Emplacement</a></li>';
		echo '<li><a href="managezone.php">Zone</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>
	</ul>
	</div>
<?php
}


function barre_menu_new()
{
	?>
	<div id="menu">
	<ul>

	<?php
	echo $_SESSION['STATUT'];
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Comptabilité</a>';
		echo '<UL>';
		echo '<li><a href="payment_facture.php">Paiement de facture</a></li>';
		echo '<li><a href="create_facture.php">Creation de facture</a></li>';
		echo '</UL>';
		echo '</li>';
	}

	if (($_SESSION['STATUT']) == "ADMIN")
	{
	?>
		<li><a href="#">Gestion du planning CoWorking</a>
		<ul>
		<li><a href="visuplanning.php">Visualisation Planning des CoWorker</a></li>
		</ul>
		</li>
	<?php
	}
	else
	{
		?>
		<li><a href="#">Gestion de mon planning</a>
		<ul>
		<li><a href="planning.php">Planning</a></li>
		<li><a href="reservation.php">Réservation</a></li>
		</ul>
		</li>
		<?php
	}

	if (($_SESSION['STATUT']) == "ADMIN")
	{
	?>
		<li><a href="#">Gestion des adhérants</a>
		<ul>
		<li><a href="list_tiers.php">Liste des clients</a></li>
		</ul>
		</li>
	<?php
	}

	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Paramétrage</a>';
		echo '<UL>';
		echo '<li><a href="modepaie.php">Mode de paiement</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>
	</ul>
	</div>
<?php
}


function footer()
{
?>
<div style="text-align:center;height:100px;color:#F69730">
<br>
<hr />
<p><a style="color:#097855" href="acceuil.php">Acceuil</a>&nbsp;»</p>
</div>
<?php
}
