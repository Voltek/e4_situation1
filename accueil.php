
<?php
/**
 * accueil.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


session_start ();




// On vérifie si l'utilisateur a envoyé des informations de connexion

if(isset($_SESSION['login']))

{


    ?>

      <!-- Insérez ici le contenu à protéger -->

       <!DOCTYPE html>

			<html lang="fr">

			<head>

			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

				<meta name="viewport" content="width=device-width">



			<link rel="icon" href="img/laverriere.ico" />

			<title>Gestion Tiers Lieux Haut de France</title>

			<link rel="stylesheet" href="lib/bootstrap.min.css">

			<link rel="stylesheet" href="lib/style.css">



			<!-- SCRIPTS -->

	<script

	  src="https://code.jquery.com/jquery-3.2.1.min.js"

	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="

	  crossorigin="anonymous"></script>

			</head>

			<?php
			include ("include/fonction_general.php");
			?>


			<body>

			<?php


			entete_page("");

			$sql = "SELECT COUNT(ET_ETABLISSEMENT) as CPT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
			
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
					if ($data1['CPT'] == 1)
					{
						$sql = "SELECT ET_ETABLISSEMENT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data2)
						{
							$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
							$sql="select * from ZONE WHERE EM_ETABLISSEMENT = '".$data2['ET_ETABLISSEMENT']."' AND EM_BLOQUE = 'NON'";
							$req = $conn->query($sql) or die('Erreur SQL !<br>');
							while ($data = mysqli_fetch_array($req))
							{
								$zone_li[$data['EM_EMPLACEMENT']] = $data['EM_LIBELLE'];
								
							}
							mysqli_close($conn);
							?>

								<div class="logged_in_choice">
									<div class="logged_in_box col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">

										<h3>Je choisi ...</h3>
										<ul class="book_choice"><span class="selected">Une zone ...</span>
											<?php foreach($zone_li as $id => $li) :?>
												<li data-etablissement="<?php echo $id; ?>"><?php echo $li; ?></li>
											<?php endforeach; ?>
										</ul>
										<a id="choix" href="#" class="valider col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">Valider</a>
									</div>
								</div>
							</body>
							<script type="text/javascript">
							$(document).ready(function() {
							$('.book_choice span').on('click', function(){
								var $this = $(this).parent();

								$this.toggleClass('open');
							});

							$('.book_choice li').on('click', function(){
								var $this = $(this),
									txt = $this.html(),
									etablissement = $this.data('etablissement');

								$this.parent().find('span').empty().append(txt);
								$this.parent('ul').removeClass('open');
								$('#choix').attr('href',"manageresa.php?etablissement=<?php echo $data2['ET_ETABLISSEMENT'];?>&typeplace=" + etablissement);
							})
							})

							</script>
							<?php
						}
					}
					else
					{
						
						$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
						$sql="select * from ETABLISSEMENT";
						
						$req = $conn->query($sql) or die('Erreur SQL !<br>');
						while ($data = mysqli_fetch_array($req))
						{
							$zone_li[$data['ET_ETABLISSEMENT']] = $data['ET_LIBELLE'];
						
						}
						mysqli_close($conn);
						?>

							<div class="logged_in_choice">
								<div class="logged_in_box col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">

									<h3>Je choisi ...</h3>
									<ul class="book_choice"><span class="selected">Un lieu ...</span>
										<?php foreach($zone_li as $id => $li) :?>
											<li data-etablissement="<?php echo $id; ?>"><?php echo $li; ?></li>
										<?php endforeach; ?>
									</ul>
									<a id="choix" href="#" class="valider col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">Valider</a>
								</div>
							</div>
						</body>
						<script type="text/javascript">
						$(document).ready(function() {
						$('.book_choice span').on('click', function(){
							var $this = $(this).parent();

							$this.toggleClass('open');
						});

						$('.book_choice li').on('click', function(){
							var $this = $(this),
								txt = $this.html(),
								etablissement = $this.data('etablissement');

							$this.parent().find('span').empty().append(txt);
							$this.parent('ul').removeClass('open');
							$('#choix').attr('href',"manageresa.php?etablissement=" + etablissement);
						})
						})

						</script>
						<?php
					}
			}
			?>



			</html>

		  <!-- Fin du contenu à protéger -->

    <?php

  }

  else

  {
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur

    header('Location: index.php');


}


