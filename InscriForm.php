<?php
/**
 * InscriForm.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */
//info BDD

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width">
    <link rel="icon" href="img/laverriere.ico" />
    <title>Inscription</title>
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="lib/style.css">
</head>
<body>
	<?php
    include ("include/fonction_general.php");
    entete_page_login("Formulaire");
  ?>

  <?php
  if (isset($_SERVER['HTTP_USER_AGENT'])) {
      $agent = $_SERVER['HTTP_USER_AGENT'];
  }

  if (strlen(strstr($agent, 'Firefox')) > 0) {
      $browser = 'firefox';
      echo "<p style=\"color : transparent; font-size : 2px\">.</p> <!-- mozilla -->";
  }


  	$cnx_bdd = ConnexionBDD();
      $req = "SELECT UT_VERIF FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."' ;";
      $result_req = $cnx_bdd->query($req);
      $tab_r = $result_req->fetchAll();
      //si count vaut 0 c'est que la requête de ne renvoie rien donc pas d'UT_VERIF qui correspond à l'id, renvoie automatiquement
      //l'user sur l'index.php
      $count = count($tab_r);
      if ($count == 0) {
        ?><script type="text/javascript"> window.location = "index.php"
          </script>';
        <?php
      }
      foreach ($tab_r as $r) {
        //recup l'url actuel
        $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $goodURL = url_site."InscriForm.php?id=".$r['UT_VERIF'];
		echo $goodURL;
		
        //si les url ne correspondent pas renvoie sur index
        if($url != $goodURL){
          ?><script type="text/javascript"> window.location = "index.php"
            </script>';
          <?php
          //si l'id est vide
        }elseif ($_GET['id']=="") {
          ?><script type="text/javascript"> window.location = "index.php"
            </script>';
          <?php
        }else{
          //si la page est trouvé
        }
      }

      $req = "SELECT UT_VERIF FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."'";
      $result_req = $cnx_bdd->query($req);
      $tab_r = $result_req->fetchALL();
      foreach ($tab_r as $r) {
        //si id vaut NULL
        if($_GET['id'] == "NULL"){
          ?><script type="text/javascript"> window.location = "index.php"
            </script>';
          <?php
        }elseif($_GET['id'] == $r['UT_VERIF']){
          include ("include/formInscription.php");
        }
      }
  ?>


</body>
</html>
