﻿<?php
/**
 * admin.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Association La Verrière
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */

// Paramètres du programme

include ("include/connect.php");


if( isset($_POST['login'])) { $UserPost = $_POST['login'];};
if(isset($_POST['password'])) {$mdpPost = $_POST['password'];};



// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_POST['login'], $_POST['password']) || isset($_SESSION['login']))
{
  connectsql();		checkadmin($_POST['login'], $_POST['password']);
  if (isset($_SESSION['login']))
  {
    if($_SESSION['STATUT'] == 'ADMIN')
	{

		// Les informations de connexion sont bonnes, on affiche le contenu protégé
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

			<?php
			include ("include/fonction_general.php");
			admheader_page();
			?>

			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body>
			<?php
			admentete_page("");




			//footer();
			?>

			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	}
	else
	{
		echo $_SESSION['login'];
		//header('Location: accueil.php');
	}
  // On quitte le programme
  exit;
}
}
else
{
?>

	<!DOCTYPE html>
	<html lang="fr">
	<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
		<meta name="viewport" content="width=device-width">

	<link rel="icon" href="img/laverriere.ico" />
	<title>Gestion Tiers Lieux Haut de France</title>
	<link rel="stylesheet" href="lib/bootstrap.min.css">
	<link rel="stylesheet" href="lib/style.css">

	<!-- SCRIPTS -->
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>


	</head>
	<body class="login">
	<?php
	include ("include/fonction_general.php");
	entete_page_login("Connexion");
	?>


  <div class="login_form">
	  <form action="" method="post" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
	    <label for="login" class="col-xs-12"> Nom d'utilisateur : </label>
	    <input type="text" name="login" id="login" maxlength="50" class="col-xs-12">
	    <label for="password" class="col-xs-12"> Mot de passe : </label>
	    <input type="password" name="password" id="password" maxlength="100" class="col-xs-12">
	    <input type="submit" value=" Valider " class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
	    <a style="color:#F69730" href="inscription.php" class="forgot_passwd col-xs-12" >Inscription</a>
		  <a style="color:#F69730" href="newmdp.php" class="forgot_passwd col-xs-12" >Mot de passe oublié ?</a>
	  </form>
	  <br>
  </div>

<?php
}
?>
</body>
</html>
