﻿<?php
/**
 * create_adhesion.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   3.0
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
	//echo $_POST['login'];
    // Les informations de connexion sont bonnes, on affiche le contenu protégé
	
	if(isset($_POST['USERLOGIN']))
	{
		?>
		<!DOCTYPE html>
		<html lang="fr">
		<head>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
		
		<link rel="icon" href="img/laverriere.ico" />
		<title>Gestion Tiers Lieux Haut de France</title>
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />

			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>

			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>
		<script type="text/javascript">
				jQuery(function($){
			   $.datepicker.regional['fr'] = {
				  closeText: 'Fermer',
				  prevText: '&#x3c;Préc',
				  nextText: 'Suiv&#x3e;',
				  currentText: 'Courant',
				  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
				  'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				  monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
				  'Jul','Aoû','Sep','Oct','Nov','Déc'],
				  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
				  dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
				  dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
				  weekHeader: 'Sm',
				  //dateFormat: 'dd/mm/yy',
							dateFormat: 'dd/mm/yy',
				  firstDay: 1,
				  isRTL: false,
				  showMonthAfterYear: false,
				  yearSuffix: ''};
			   $.datepicker.setDefaults($.datepicker.regional['fr']);
			});
			
			$(function() {
				$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
				$( "#datepicker" ).datepicker( $.datepicker.regional[ "fr" ] );
				$( "#datepicker1" ).datepicker( $.datepicker.regional[ "fr" ] );
				$( "#datepicker2" ).datepicker( $.datepicker.regional[ "fr" ] );
			});
			
			function cachertout()
			{
				alert('0');
				alert($('#nbruseraffich').val());
				var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = (i * 100) + j;
						$('#tdparamuser_' + index).hide();
					}
				}
			};
		</script> 
		<link rel="stylesheet" href="lib/file.css">
		</head>
		<body>
		<?php
		
		include ("include/fonction_admutilisateur.php"); 
		////$_SESSION['token'] = random(25); // clé aléatoire de 25 caractères créée a partir de la fonction
		admentete_page("Validation d'une adhésion");
		
		newvalideadhesion();
		if ($_POST['action']=='CREATEADHESION')
		{
			creation_adhesion();
		}
		if ($_POST['action']=='VALIDEADHESION')
		{
			valide_adhesion();
		}
	
		?>
		</body>
		</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	
	//include ("includes/fonction_support.php"); 
	//	insert_appel($_POST['Nom_Client'], $_POST['DateCall'], $_POST['HeureCall'], $_SESSION['login'], $_POST['TitreCall'], $_POST['commentaires']);
	}
	else
	{
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="stylesheet" href="style.css" type="text/css" media="all" />
			<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />

			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>

			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>
			<script type="text/javascript">
				jQuery(function($){
			   $.datepicker.regional['fr'] = {
				  closeText: 'Fermer',
				  prevText: '&#x3c;Préc',
				  nextText: 'Suiv&#x3e;',
				  currentText: 'Courant',
				  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
				  'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				  monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
				  'Jul','Aoû','Sep','Oct','Nov','Déc'],
				  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
				  dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
				  dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
				  weekHeader: 'Sm',
				  //dateFormat: 'dd/mm/yy',
							dateFormat: 'dd/mm/yy',
				  firstDay: 1,
				  isRTL: false,
				  showMonthAfterYear: false,
				  yearSuffix: ''};
			   $.datepicker.setDefaults($.datepicker.regional['fr']);
			});
			
			var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{
					alert(i);
					// affichage du calendrier
					$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
					$( "#datepicker_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker1_" + i).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker2_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
					
				
				}
			
			
			function cachertout()
			{
				var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{
					alert(i);
					// affichage du calendrier
					$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
					$( "#datepicker_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker1_" + i).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker2_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
					
				
				}
				
			};
			
			function cacher (id)
			{

				// Cacher tout
				var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{
					$('#ADHVALID_' + i).attr('disabled', true);
					$('#CHECKADHESION_' + i).attr('disabled', true);
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = (i * 100) + j;
						$('#tdparamadh_' + index).hide();
					}
					//$('#CHECKADHESION_' + (i * 100)).attr('disabled', true);
				}

				//Afficher seulement le choix
				var test = 'tdparamadh_'+ (id+1);
				//$('#CHECKADHESION_' + id).attr('disabled', false);
				$('#CHECKADHESION_' + (id / 100)).attr('disabled', false);
				$('#ADHVALID_' + (id / 100)).attr('disabled', false);
				if(document.getElementById(test).style.display=="none")
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = id + j;
						$('#tdparamadh_' + index).show();
						
					}
				}
				else
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = id + j;
						$('#tdparamuser_' + index).hide();
					}
				}

			};
			</script>
			
			</script>
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
			
			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body  onload="javascript:cachertout();">
			
			
			
			<?php
			include ("include/fonction_admutilisateur.php"); 
			
			if(isset($_POST['nbruseraffich']))
			{
				newvalideadhesion();
			}
			admentete_page("Validation d'une adhésion");
			
		
			liste_adhesion();

			?>
			
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur
    
    header('Location: index.php');


}
?>