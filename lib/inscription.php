<?php
//info BDD
require ("scripts/constantes.php");
require ("scripts/fonctions.php");
//Envoie mail
require ("include/fonction_email.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="img/laverriere.ico" />
        <title>Inscription</title>
        <link rel="stylesheet" href="lib/bootstrap.min.css">
        <link rel="stylesheet" href="lib/style.css">
    </head>
    <body>
        <?php
        include ("include/fonction_general.php");
        entete_page_login("Inscription");
        ?>
        <div class="login_form">
            <form action="" method="post" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                <label for="Username" class="col-xs-12"> Nom d'utilisateur :</label>
                <input type="text" name="Username" class="col-xs-12" id="Username" required="Requis">

                <label for="email" class="col-xs-12"> Email :</label>
                <input type="email" name="Email" class="col-xs-12" id="Email" required="Requis">

                <p>Le nom d'utilisateur et le même que l'adresse mail : <input type="checkbox" name="UserEmail" id="UserEmail"></p>
                <!-- Quand l'user click sur la checkbox complete automatiquement l'input texte de Email -->
                <script>
                    var UserEmail = document.getElementById('UserEmail');
                    var Email = document.getElementById('Email');
                    var Username = document.getElementById('Username');
                    UserEmail.onchange = function () {
                        if (this.checked) {
                            Email.value = Username.value;
                        } else {
                            Email.value = "";
                        }
                        ;
                    };
                </script>

                <input type="submit" name="Envoie" value="Envoie" class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                <a style="color:#F69730" href="index.php" class="forgot_passwd col-xs-12" >retour</a>
            </form>
        </div>

        <?php
        $cnx_bdd = ConnexionBDD();
        // Vérifie si la connection à la bdd est bonne
        if ($cnx_bdd != false) {
            // Quand l'user clique sur le bouton d'envoie
            if (isset($_POST['Envoie'])) {
                // Declaration des variable
                $Username = $_POST['Username'];
                $Email = $_POST['Email'];
                $date = date("Y-m-d");
                // si le champs "Nom d'utilisateur n'est pas vide"
                if ($Username != "" || $Email != "") {
                    // Lance la requete pour verifier si le nom d'utilisateur est pris
                    $req = "SELECT UT_LOGIN FROM utilisateur WHERE UT_LOGIN='$Username';";
                    $result_req = $cnx_bdd->query($req);
                    // Recupere tout les nom d'utilisateur dans la bdd
                    $tab_r = $result_req->fetchAll();

                    //si $tab_r n'est pas vide c'est que le nom d'utilisateur est deja pris
                    if (!empty($tab_r)) {
                        ?><script>alert('Nom d\'utilisateur deja utilisé');</script><?php
                        //sinon on l'insere dans la bdd

                        //si checkbox coche
                    } elseif (isset($_POST['UserEmail'])) {
                        $Email = $Username;
                        $Rand = random(20);
                        $req = "INSERT INTO utilisateur (UT_LOGIN, UT_VERIF, UT_PASSWORD, UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_ADRESSE2, UT_VILLE, UT_CODEPOSTAL, UT_PAYS, UT_EMAIL, UT_STATUT, UT_ID1, UT_ID2, UT_ETABLISSEMENT, UT_VALIDE, UT_INITIALE, UT_IDMODIF, UT_CIVILITE, UT_IDDATEDEMANDE, UT_TIERSGESTION, UT_ADMINSITE) VALUES ('$Username', '$Rand','','','','','','','','','$Email','','','','','','','','','$date','','')";
                        $result_req = $cnx_bdd->exec($req);
                        emailInscri($Email, $Rand);
                        ?> <script> alert('Un mail vous a été envoyé')</script> <?php
                        ?><script type="text/javascript"> window.location = "index.php"
                          </script>';
                        <?php
                    } else {
                        $Rand = random(20);
                        $req = "INSERT INTO utilisateur (UT_LOGIN, UT_VERIF, UT_PASSWORD, UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_ADRESSE2, UT_VILLE, UT_CODEPOSTAL, UT_PAYS, UT_EMAIL, UT_STATUT, UT_ID1, UT_ID2, UT_ETABLISSEMENT, UT_VALIDE, UT_INITIALE, UT_IDMODIF, UT_CIVILITE, UT_IDDATEDEMANDE, UT_TIERSGESTION, UT_ADMINSITE) VALUES ('$Username', '$Rand','','','','','','','','','$Email','','','','','','','','','$date','','')";
                        $result_req = $cnx_bdd->exec($req);
                        emailInscri($Email, $Rand);
                        ?> <script> alert('Un mail vous a été envoyé')</script> <?php
                        ?><script type="text/javascript"> window.location = "index.php"
                          </script>';
                        <?php
                    }
                }
            }
        } else {
            echo 'erreur bdd';
        }
        ?>
    </body>
</html>
