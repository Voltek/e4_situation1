-- ----------------------
-- dump de la base adminlv_reservation au 14-Jan-2019
-- ----------------------


-- -----------------------------
-- Structure de la table article
-- -----------------------------
CREATE TABLE `article` (
  `AR_ARTICLENO` int(5) NOT NULL AUTO_INCREMENT,
  `AR_CODEARTICLE` varchar(20) NOT NULL,
  `AR_DESIGNATION` varchar(50) NOT NULL,
  `AR_TYPE` varchar(20) NOT NULL,
  `AR_ETABLISSEMENT` varchar(10) NOT NULL,
  `AR_FAMILLE1` varchar(50) DEFAULT NULL,
  `AR_FAMILLE2` varchar(50) DEFAULT NULL,
  `AR_TARIF` decimal(12,2) NOT NULL,
  `AR_RESAHEUREDEBUT` time DEFAULT NULL,
  `AR_RESAHEUREFIN` time DEFAULT NULL,
  `AR_IMAGE` varchar(250) DEFAULT NULL,
  UNIQUE KEY `AR_ARTICLENO` (`AR_ARTICLENO`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

ADHESION;

ARTCOWORK;

SALLEAPM;

SALLEMATIN;

SALLESOIR;

PLACEREUNION;

-- -----------------------------
-- Structure de la table choixcode
-- -----------------------------
CREATE TABLE `choixcode` (
  `CC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CC_TYPE` varchar(20) NOT NULL,
  `CC_CODE` varchar(15) NOT NULL,
  `CC_LIBELLE` varchar(50) NOT NULL,
  `CC_ABREGE` varchar(30) NOT NULL,
  PRIMARY KEY (`CC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

MOIS;

TYPEART;

TYPEART;

TYPEART;

TYPEART;

TYPEZONE;

TYPEZONE;

TYPE_MORALE;

TYPE_MORALE;

TYPE_MORALE;

TYPE_PHYSIQUE;

TYPE_PHYSIQUE;

FAMRESANIV1;

FAMRESANIV1;

FAMRESANIV2;

-- -----------------------------
-- Structure de la table compteur
-- -----------------------------
CREATE TABLE `compteur` (
  `CT_NUMERO` int(11) NOT NULL AUTO_INCREMENT,
  `CT_ETABLISSEMENT` varchar(6) NOT NULL,
  `CT_DOCUMENT` varchar(15) NOT NULL,
  `CT_COMPTEUR` int(6) NOT NULL,
  `CT_CODEDOC` varchar(3) NOT NULL,
  PRIMARY KEY (`CT_NUMERO`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

ET0001;

-- -----------------------------
-- Structure de la table config
-- -----------------------------
CREATE TABLE `config` (
  `CO_MULTIETABLISSEMENT` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table document
-- -----------------------------
CREATE TABLE `document` (
  `DO_NUMERO` int(3) NOT NULL AUTO_INCREMENT,
  `DO_CODE` varchar(3) NOT NULL,
  `DO_LIBELLE` varchar(30) NOT NULL,
  `DO_NOMFICHIER` varchar(50) NOT NULL,
  PRIMARY KEY (`DO_NUMERO`,`DO_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table emplacement
-- -----------------------------
CREATE TABLE `emplacement` (
  `EM_ETABLISSEMENT` varchar(6) NOT NULL,
  `EM_EMPLACEMENT` varchar(7) NOT NULL,
  `EM_LIBELLE` varchar(50) NOT NULL,
  `EM_NBRRESAMAX` int(2) NOT NULL,
  PRIMARY KEY (`EM_ETABLISSEMENT`,`EM_EMPLACEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table etablissement
-- -----------------------------
CREATE TABLE `etablissement` (
  `ET_ETABLISSEMENT` varchar(6) NOT NULL,
  `ET_LIBELLE` varchar(50) NOT NULL,
  `ET_BLOQUE` varchar(3) NOT NULL,
  `ET_ADRESSE1` varchar(100) NOT NULL,
  `ET_ADRESSE2` varchar(100) NOT NULL,
  `ET_ADRESSE3` varchar(100) NOT NULL,
  `ET_CODEPOSTAL` varchar(5) NOT NULL,
  `ET_VILLE` varchar(50) NOT NULL,
  `ET_NBRRESAMAX` int(2) NOT NULL,
  `ET_LOGO` varchar(50) NOT NULL,
  `ET_EMAIL` varchar(250) NOT NULL,
  `ET_SITEWEB` varchar(250) DEFAULT NULL,
  `ET_IBAN` varchar(100) DEFAULT NULL,
  `ET_BANQUECODE` varchar(10) DEFAULT NULL,
  `ET_BANQUEGUICHET` varchar(10) DEFAULT NULL,
  `ET_BANQUECPT` varchar(10) DEFAULT NULL,
  `ET_BANQUECLEF` varchar(2) DEFAULT NULL,
  `ET_BANQUENOM` varchar(30) DEFAULT NULL,
  `ET_BANQUEBIC` varchar(10) DEFAULT NULL,
  `ET_SIRET` varchar(20) DEFAULT NULL,
  `ET_NAP` varchar(5) DEFAULT NULL,
  `ET_ECHEANCEFACTURE` int(2) DEFAULT NULL,
  `ET_TYPEADHESION` varchar(20) DEFAULT NULL,
  `ET_ARTADHESION` varchar(20) DEFAULT NULL,
  `ET_ARTMONNAIE` varchar(20) DEFAULT NULL,
  `ET_IMAGETYPE` varchar(25) DEFAULT NULL,
  `ET_IMAGENOM` varchar(50) DEFAULT NULL,
  `ET_PETITEIMAGENOM` varchar(50) DEFAULT NULL,
  `ET_FACTURATION` varchar(3) DEFAULT NULL,
  `ET_PIEDFACTURE` longtext,
  `ET_GESTIONADHESION` varchar(3) DEFAULT NULL,
  `ET_GESTIONCREDITCLIENT` varchar(3) DEFAULT NULL,
  `ET_ADHESIONCOCHE1` longtext,
  `ET_ADHESIONCOCHE2` longtext,
  `ET_ADHESIONRGPD1` longtext,
  `ET_FICHIERCGU` varchar(100) DEFAULT NULL,
  `ET_MODELEADHESION` varchar(3) DEFAULT NULL,
  `ET_IMAGE` varchar(10) DEFAULT NULL,
  `ET_ADHESIONBYERP` varchar(3) NOT NULL,
  `ET_GESTIONPANIER` varchar(3) NOT NULL,
  `ET_CGU` varchar(250) NOT NULL,
  `ET_CGV` varchar(250) NOT NULL,
  `ET_MENTIONLEG` varchar(250) NOT NULL,
  PRIMARY KEY (`ET_ETABLISSEMENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ASSOCIATION LA VERRIERE;

JE TRAVAILLE AU VERT;

-- -----------------------------
-- Structure de la table gestionemail
-- -----------------------------
CREATE TABLE `gestionemail` (
  `GE_NUMERO` int(11) NOT NULL AUTO_INCREMENT,
  `GE_ETABLISSEMENT` varchar(8) NOT NULL,
  `GE_TYPE` varchar(30) DEFAULT NULL,
  `GE_LIBELLE` varchar(250) NOT NULL,
  `GE_HEADER` text,
  `GE_MESSAGE` text,
  `GE_SIGNATURE` text,
  `GE_NBCONTACT` int(2) DEFAULT NULL,
  `GE_CONTACT` varchar(250) DEFAULT NULL,
  `GE_TYPEENVOI` int(1) DEFAULT NULL COMMENT '1 = envoi simple',
  PRIMARY KEY (`GE_NUMERO`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

ET0001;

ET0001;

-- -----------------------------
-- Structure de la table ligne
-- -----------------------------
CREATE TABLE `ligne` (
  `GL_IDLIGNE` int(11) NOT NULL AUTO_INCREMENT,
  `GL_NUMLIGNE` int(11) NOT NULL,
  `GL_LIBELLE` varchar(60) NOT NULL,
  `GL_NUMERO` int(11) NOT NULL,
  `GL_ETABLISSEMENT` varchar(6) NOT NULL,
  `GL_USER` varchar(250) NOT NULL,
  `GL_DATEPIECE` date NOT NULL,
  `GL_ARTICLENO` varchar(20) NOT NULL,
  `GL_ARTLIBELLE` varchar(50) NOT NULL,
  `GL_ARTFAMILLE1` varchar(50) DEFAULT NULL,
  `GL_ARTFAMILLE2` varchar(50) DEFAULT NULL,
  `GL_ARTTARIF` decimal(12,2) NOT NULL,
  `GL_QTEFACT` int(5) NOT NULL,
  `GL_TOTALHT` decimal(12,2) NOT NULL,
  `GL_REFFACTURE` varchar(30) NOT NULL,
  `GL_COMMENTAIRE` text NOT NULL,
  `GL_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GL_PRIXPARPLACE` decimal(12,1) NOT NULL,
  PRIMARY KEY (`GL_IDLIGNE`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

1;

2;

3;

4;

1;

1;

1;

1;

2;

1;

2;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

2;

1;

2;

3;

4;

5;

6;

7;

1;

1;

1;

1;

2;

3;

1;

2;

1;

2;

1;

2;

1;

1;

2;

3;

1;

2;

3;

4;

5;

6;

1;

2;

3;

4;

5;

6;

7;

8;

1;

1;

1;

2;

3;

4;

1;

2;

3;

-- -----------------------------
-- Structure de la table ligneattente
-- -----------------------------
CREATE TABLE `ligneattente` (
  `GLA_IDLIGNE` int(11) NOT NULL AUTO_INCREMENT,
  `GLA_DATECREATE` datetime NOT NULL,
  `GLA_VALIDE` varchar(3) NOT NULL,
  `GLA_NUMLIGNE` int(11) NOT NULL,
  `GLA_NUMERO` int(11) NOT NULL,
  `GLA_LIBELLE` varchar(50) NOT NULL,
  `GLA_ETABLISSEMENT` varchar(6) NOT NULL,
  `GLA_USER` varchar(250) NOT NULL,
  `GLA_DATEPIECE` date NOT NULL,
  `GLA_ARTICLENO` varchar(15) NOT NULL,
  `GLA_ARTLIBELLE` varchar(50) NOT NULL,
  `GLA_ARTFAMILLE1` varchar(50) DEFAULT NULL,
  `GLA_ARTFAMILLE2` varchar(50) DEFAULT NULL,
  `GLA_ARTTARIF` decimal(12,2) NOT NULL,
  `GLA_QTEFACT` int(5) NOT NULL,
  `GLA_TOTALHT` decimal(12,2) NOT NULL,
  `GLA_REFFACTURE` varchar(30) NOT NULL,
  `GLA_COMMENTAIRE` text NOT NULL,
  `GLA_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GLA_PRIXPARPLACE` decimal(12,1) NOT NULL,
  `GLA_SESSIONTOKEN` varchar(250) NOT NULL,
  PRIMARY KEY (`GLA_IDLIGNE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table lignecompl
-- -----------------------------
CREATE TABLE `lignecompl` (
  `GL2_NUMID` int(11) NOT NULL AUTO_INCREMENT,
  `GL2_NUMLIGNE` int(11) NOT NULL,
  `GL2_INDEXLIGNE` int(11) NOT NULL,
  `GL2_LIBELLE1` varchar(250) NOT NULL,
  `GL2_LIBELLE2` varchar(250) NOT NULL,
  `GL2_REFFACTURE` varchar(50) NOT NULL,
  `GL2_DATEPIECE` date NOT NULL,
  `GL2_USER` varchar(50) NOT NULL,
  `GL2_ETABLISSEMENT` varchar(50) NOT NULL,
  `GL2_PRIXPLACE` decimal(12,2) NOT NULL,
  `GL2_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GL2_QTEPLACE` int(11) NOT NULL,
  `GL2_QTEPERSONNE` int(11) NOT NULL,
  `GL2_PRIXTOTALHT` decimal(12,2) NOT NULL,
  PRIMARY KEY (`GL2_NUMID`)
) ENGINE=MyISAM AUTO_INCREMENT=234 DEFAULT CHARSET=latin1;

1;

1;

1;

1;

1;

1;

1;

2;

2;

2;

2;

2;

2;

2;

3;

3;

4;

4;

4;

1;

1;

1;

1;

1;

1;

1;

1;

2;

2;

1;

1;

2;

2;

2;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

2;

2;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

2;

2;

2;

2;

2;

2;

2;

2;

2;

2;

2;

2;

2;

2;

3;

3;

3;

3;

3;

3;

3;

4;

4;

4;

4;

4;

4;

4;

5;

5;

5;

5;

5;

5;

6;

6;

6;

6;

6;

6;

7;

7;

7;

7;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

1;

2;

2;

3;

3;

1;

1;

1;

2;

2;

2;

1;

1;

1;

2;

2;

2;

1;

1;

2;

2;

1;

1;

1;

1;

2;

2;

3;

3;

1;

1;

2;

2;

3;

3;

4;

4;

5;

5;

6;

6;

1;

1;

2;

2;

3;

3;

4;

4;

5;

5;

6;

6;

7;

7;

8;

8;

1;

1;

1;

1;

-- -----------------------------
-- Structure de la table modepaie
-- -----------------------------
CREATE TABLE `modepaie` (
  `MO_NUMERO` int(11) NOT NULL AUTO_INCREMENT,
  `MO_MODEPAIE` varchar(3) NOT NULL,
  `MO_LIBELLE` varchar(20) NOT NULL,
  PRIMARY KEY (`MO_NUMERO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CHQ;

VIR;

ESP;

-- -----------------------------
-- Structure de la table panier
-- -----------------------------
CREATE TABLE `panier` (
  `RE_NUMRESA` int(6) NOT NULL AUTO_INCREMENT,
  `RE_USER` varchar(250) NOT NULL,
  `RE_DATE` date NOT NULL,
  `RE_JOUR` int(11) NOT NULL,
  `RE_MOIS` int(11) NOT NULL,
  `RE_ANNEE` int(11) NOT NULL,
  `RE_VALIDEE` varchar(3) NOT NULL DEFAULT 'OUI',
  `RE_FACTURE` varchar(30) NOT NULL DEFAULT 'NON',
  `RE_REFFACTURE` varchar(15) NOT NULL,
  `RE_ETABLISSEMENT` varchar(6) NOT NULL,
  `RE_ETLIBELLE` varchar(50) NOT NULL,
  `RE_ETADRESSE1` varchar(50) NOT NULL,
  `RE_ETADRESSE2` varchar(50) NOT NULL,
  `RE_CODEPOSTAL` varchar(5) NOT NULL,
  `RE_VILLE` varchar(50) NOT NULL,
  `RE_EMPLACEMENT` int(5) NOT NULL,
  `RE_LIBELLEEMPLACEMENT` varchar(50) NOT NULL,
  `RE_NBRPLACE` int(2) NOT NULL,
  `RE_RESA` varchar(3) NOT NULL DEFAULT 'NON',
  `RE_DATERESA` datetime NOT NULL,
  `RE_USERANNUL` varchar(250) NOT NULL,
  `RE_DATEANNUL` datetime NOT NULL,
  `RE_ZONE` int(7) NOT NULL,
  `RE_ZONELIBELLE` varchar(50) NOT NULL,
  `RE_TYPEZONE` varchar(30) NOT NULL,
  `RE_USERMODIF` varchar(250) NOT NULL,
  `RE_SESSIONTOKEN` varchar(250) NOT NULL,
  `RE_PRIXPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXPARPERSONNE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXTOTALRESA` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_NBRRESAMAX` int(99) NOT NULL DEFAULT '0',
  `RE_NBRPLACESMAX` int(99) NOT NULL DEFAULT '0',
  `RE_HEUREDEBUT` time NOT NULL,
  `RE_HEUREFIN` time NOT NULL,
  `RE_CODEART1` varchar(20) NOT NULL,
  `RE_LIBART1` varchar(50) NOT NULL,
  `RE_QTEART1` int(2) NOT NULL,
  `RE_PRIXART1` decimal(5,2) NOT NULL,
  `RE_CODEART2` varchar(20) NOT NULL,
  `RE_LIBART2` varchar(50) NOT NULL,
  `RE_QTEART2` int(2) NOT NULL,
  `RE_PRIXART2` decimal(5,2) NOT NULL,
  `RE_CREDITCLI` varchar(3) NOT NULL,
  `RE_JOURTYPE` varchar(15) NOT NULL,
  `RE_ARTJOURCODE` varchar(30) NOT NULL,
  `RE_ARTJOURLIB` varchar(50) NOT NULL,
  `RE_ARTJOURPRIX` decimal(5,2) NOT NULL,
  `RE_ARTMATINCODE` varchar(30) NOT NULL,
  `RE_ARTMATINLIB` varchar(50) NOT NULL,
  `RE_ARTMATINPRIX` decimal(5,2) NOT NULL,
  `RE_ARTAPMCODE` varchar(30) NOT NULL,
  `RE_ARTAPMLIB` varchar(50) NOT NULL,
  `RE_ARTAPMPRIX` decimal(5,2) NOT NULL,
  `RE_PANIER` varchar(3) DEFAULT NULL,
  `RE_TOKENPANIER` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`RE_NUMRESA`),
  KEY `PL_USER` (`RE_USER`),
  KEY `PL_USER_2` (`RE_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table piece
-- -----------------------------
CREATE TABLE `piece` (
  `GP_NUMERO` int(11) NOT NULL AUTO_INCREMENT,
  `GP_TYPE` varchar(20) DEFAULT NULL,
  `GP_USER` varchar(250) NOT NULL,
  `GP_USERNOM` varchar(250) NOT NULL,
  `GP_DATEPIECE` date NOT NULL,
  `GP_ETABLISSEMENT` varchar(6) NOT NULL,
  `GP_USERPRENOM` varchar(250) NOT NULL,
  `GP_USERADRESSE1` varchar(250) NOT NULL,
  `GP_USERADRESSE2` varchar(250) NOT NULL,
  `GP_USERADRESSE3` varchar(250) DEFAULT NULL,
  `GP_USERVILLE` varchar(250) NOT NULL,
  `GP_USERCODEPOSTAL` varchar(250) NOT NULL,
  `GP_USERINITIALE` varchar(5) DEFAULT NULL,
  `GP_USERID` int(10) NOT NULL,
  `GP_ETLIBELLE` varchar(50) NOT NULL,
  `GP_ETADRESSE1` varchar(100) NOT NULL,
  `GP_ETADRESSE2` varchar(100) NOT NULL,
  `GP_ETADRESSE3` varchar(100) NOT NULL,
  `GP_ETCODEPOSTAL` varchar(5) NOT NULL,
  `GP_ETVILLE` varchar(50) NOT NULL,
  `GP_USEREMAIL` varchar(250) NOT NULL,
  `GP_TOTALHT` decimal(12,2) NOT NULL,
  `GP_TOTALQTEFACT` int(5) NOT NULL,
  `GP_REFFACTURE` varchar(50) DEFAULT NULL,
  `GP_TRANSMISE` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATETRANSMISSION` date NOT NULL,
  `GP_PAYEMENT` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATEPAYEMENT` date NOT NULL,
  `GP_RELANCE` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATERELANCE` date NOT NULL,
  `GP_NOMBRERELANCE` int(2) NOT NULL DEFAULT '0',
  `GP_REFPAYMENT` varchar(30) DEFAULT NULL,
  `GP_MODEPAIE` varchar(3) DEFAULT NULL,
  `GP_SESSIONTOKEN` varchar(250) NOT NULL,
  `GP_DATEECHEANCE` date NOT NULL,
  `GP_CREATEUR` varchar(250) NOT NULL,
  PRIMARY KEY (`GP_NUMERO`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

;

FACTURE;

FACTURE;

FACTURE;

FACTURE;

FACTURE;

FACTURE;

ADHESION;

ADHESION;

ADHESION;

ADHESION;

CLOTURE;

CLOTURE;

-- -----------------------------
-- Structure de la table promotion
-- -----------------------------
CREATE TABLE `promotion` (
  `PR_NUMERO` int(6) NOT NULL AUTO_INCREMENT,
  `PR_LIBELLE` varchar(30) NOT NULL,
  `PR_ETABLISSEMENT` varchar(6) NOT NULL,
  `PR_DECLENCHEUR` varchar(10) NOT NULL,
  `PR_QTEDECLENCHEUR` int(2) NOT NULL,
  `PR_TYPEREMISE` int(1) NOT NULL COMMENT '1 - REMISE VALEUR 0 = REMISE EN POURCENTAGE',
  `PR_VALEUR` decimal(5,0) NOT NULL,
  `PR_REMISEDIRECT` int(1) NOT NULL COMMENT '0 : CR�ATION D''UN VOUCHER | 1 : REMISE DIRECT',
  `PR_ACTIVE` varchar(3) NOT NULL COMMENT 'NON OU OUI',
  PRIMARY KEY (`PR_NUMERO`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

Promo adhésion;

-- -----------------------------
-- Structure de la table reservation
-- -----------------------------
CREATE TABLE `reservation` (
  `RE_NUMRESA` int(6) NOT NULL AUTO_INCREMENT,
  `RE_USER` varchar(250) NOT NULL,
  `RE_DATE` date NOT NULL,
  `RE_JOUR` int(11) NOT NULL,
  `RE_MOIS` int(11) NOT NULL,
  `RE_ANNEE` int(11) NOT NULL,
  `RE_VALIDEE` varchar(3) NOT NULL DEFAULT 'OUI',
  `RE_FACTURE` varchar(50) NOT NULL DEFAULT 'NON',
  `RE_REFFACTURE` varchar(30) NOT NULL,
  `RE_ETABLISSEMENT` varchar(6) NOT NULL,
  `RE_ETLIBELLE` varchar(50) NOT NULL,
  `RE_ETADRESSE1` varchar(50) NOT NULL,
  `RE_ETADRESSE2` varchar(50) NOT NULL,
  `RE_CODEPOSTAL` varchar(5) NOT NULL,
  `RE_VILLE` varchar(50) NOT NULL,
  `RE_EMPLACEMENT` int(5) NOT NULL,
  `RE_LIBELLEEMPLACEMENT` varchar(50) NOT NULL,
  `RE_NBRPLACE` int(2) NOT NULL,
  `RE_RESA` varchar(3) NOT NULL DEFAULT 'NON',
  `RE_DATERESA` datetime NOT NULL,
  `RE_USERANNUL` varchar(250) NOT NULL,
  `RE_DATEANNUL` datetime NOT NULL,
  `RE_ZONE` int(7) NOT NULL,
  `RE_ZONELIBELLE` varchar(50) NOT NULL,
  `RE_TYPEZONE` varchar(30) NOT NULL,
  `RE_USERMODIF` varchar(250) NOT NULL,
  `RE_SESSIONTOKEN` varchar(250) NOT NULL,
  `RE_PRIXPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXPARPERSONNE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXTOTALRESA` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_NBRRESAMAX` int(99) NOT NULL DEFAULT '0',
  `RE_NBRPLACESMAX` int(99) NOT NULL DEFAULT '0',
  `RE_HEUREDEBUT` time NOT NULL,
  `RE_HEUREFIN` time NOT NULL,
  `RE_CODEART1` varchar(20) NOT NULL,
  `RE_LIBART1` varchar(50) NOT NULL,
  `RE_QTEART1` int(2) NOT NULL,
  `RE_PRIXART1` decimal(5,2) NOT NULL,
  `RE_CODEART2` varchar(20) NOT NULL,
  `RE_LIBART2` varchar(50) NOT NULL,
  `RE_QTEART2` int(2) NOT NULL,
  `RE_PRIXART2` decimal(5,2) NOT NULL,
  `RE_CREDITCLI` varchar(3) NOT NULL,
  `RE_JOURTYPE` varchar(15) NOT NULL,
  `RE_ARTJOURCODE` varchar(30) NOT NULL,
  `RE_ARTJOURLIB` varchar(50) NOT NULL,
  `RE_ARTJOURPRIX` decimal(5,2) NOT NULL,
  `RE_ARTMATINCODE` varchar(30) NOT NULL,
  `RE_ARTMATINLIB` varchar(50) NOT NULL,
  `RE_ARTMATINPRIX` decimal(5,2) NOT NULL,
  `RE_ARTAPMCODE` varchar(30) NOT NULL,
  `RE_ARTAPMLIB` varchar(50) NOT NULL,
  `RE_ARTAPMPRIX` decimal(5,2) NOT NULL,
  `RE_PANIER` varchar(3) DEFAULT NULL,
  `RE_TOKENPANIER` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`RE_NUMRESA`),
  KEY `PL_USER` (`RE_USER`),
  KEY `PL_USER_2` (`RE_USER`)
) ENGINE=InnoDB AUTO_INCREMENT=600 DEFAULT CHARSET=latin1;

jr.menu@gmail.com;

jr.menu@gmail.com;

jr.menu@gmail.com;

jr.menu@gmail.com;

jr.menu@gmail.com;

test1;

jr.menu@gmail.com;

commeunebrouette@gmail.com;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

brohette.nicolas@gmail.com;

jr.menu@gmail.com;

bjolibert@gmail.com;

louis.veyer@ateliers-art-strong.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

test1;

test1;

test1;

brohette.nicolas@gmail.com;

test1;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

brohette.nicolas@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

mathilde@transversaldesign.fr;

mathilde@transversaldesign.fr;

mathilde@transversaldesign.fr;

nbernier@centre-britannique.org;

test1;

bjolibert@gmail.com;

test1;

test1;

test1;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

commeunebrouette@gmail.com;

bjolibert@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

bjolibert@gmail.com;

test1;

mathilde@transversaldesign.fr;

test1;

bjolibert@gmail.com;

hugo@hdwebmarketing.fr;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

Test1;

test1;

test1;

test1;

test1;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Test1;

Test1;

Test1;

Test1;

Test1;

test1;

test1;

bjolibert@gmail.com;

bjolibert@gmail.com;

test1;

test1;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

test1;

test1;

louis.veyer@ateliers-art-strong.fr;

j.cappel@pnr-scarpe-escaut.fr;

bjolibert@gmail.com;

test1;

test1;

test1;

antoine.demailly@lessensdugout.fr;

antoine.demailly@lessensdugout.fr;

antoine.demailly@lessensdugout.fr;

antoine.demailly@lessensdugout.fr;

antoine.demailly@lessensdugout.fr;

test1;

test1;

test1;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

annie.tacquet@yahoo.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

louis.veyer@ateliers-art-strong.fr;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

test1;

test1;

test1;

martial.dubois28@outlook.fr;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

annie.tacquet@yahoo.fr;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

bjolibert@gmail.com;

j.cappel@pnr-scarpe-escaut.fr;

j.cappel@pnr-scarpe-escaut.fr;

j.cappel@pnr-scarpe-escaut.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

nbernier@centre-britannique.org;

bjolibert@gmail.com;

Test1;

nbernier@centre-britannique.org;

nbernier@centre-britannique.org;

nbernier@centre-britannique.org;

coeur d etoile;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

nbernier@centre-britannique.org;

martial.dubois28@outlook.fr;

coeur d etoile;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

martial.dubois28@outlook.fr;

bjolibert@gmail.com;

Jr.menu@gmail.com ;

perm1;

perm1;

perm1;

perm1;

perm1;

perm1;

perm1;

nbernier@centre-britannique.org;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

bjolibert@gmail.com;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

commeunebrouette@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

bjolibert@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

nbernier@centre-britannique.org;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

test1;

test1;

test1;

test1;

test1;

test1;

Jr.menu@gmail.com ;

test1;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

test1;

test1;

test1;

test1;

test1;

test1;

bjolibert@gmail.com;

bjolibert@gmail.com;

test1;

commeunebrouette@gmail.com;

commeunebrouette@gmail.com;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

-1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

jr.menu@gmail.com;

28-LV;

28-LV;

28-LV;

28-LV;

commeunebrouette@gmail.com;

test1;

test1;

test1;

test1;

test1;

test1;

test1;

coeur d etoile;

test1;

coeur d etoile;

28-LV;

jr.menu@gmail.com;

louis.veyer@ateliers-art-strong.fr;

louis.veyer@ateliers-art-strong.fr;

bjolibert@gmail.com;

bjolibert@gmail.com;

bjolibert@gmail.com;

test1;

test1;

test1;

test1;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

bjolibert@gmail.com;

28-LV;

bjolibert@gmail.com;

Jr.menu@gmail.com ;

bjolibert@gmail.com;

bjolibert@gmail.com;

28-LV;

28-LV;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

C00000045;

C00000045;

C00000045;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

Jr.menu@gmail.com ;

-- -----------------------------
-- Structure de la table tiers
-- -----------------------------
CREATE TABLE `tiers` (
  `T_IDUSER` int(10) NOT NULL,
  `T_NUMERO` int(11) NOT NULL AUTO_INCREMENT,
  `T_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `T_PASSWORD` varchar(250) NOT NULL,
  `T_NOM` varchar(250) NOT NULL,
  `T_PRENOM` varchar(250) NOT NULL,
  `T_ADRESSE1` varchar(250) NOT NULL,
  `T_ADRESSE2` varchar(250) NOT NULL,
  `T_VILLE` varchar(250) NOT NULL,
  `T_CODEPOSTAL` varchar(250) NOT NULL,
  `T_PAYS` varchar(250) NOT NULL,
  `T_EMAIL` varchar(250) NOT NULL,
  `T_STATUT` varchar(20) NOT NULL,
  `T_ID1` varchar(20) NOT NULL,
  `T_ID2` varchar(20) NOT NULL,
  `T_ETABLISSEMENT` varchar(6) NOT NULL,
  `T_VALIDE` varchar(1) NOT NULL DEFAULT 'N',
  `T_INITIALE` varchar(5) NOT NULL,
  `T_IDMODIF` varchar(50) NOT NULL,
  `T_CIVILITE` varchar(5) NOT NULL,
  `T_IDDATEDEMANDE` date NOT NULL,
  `T_TIERSGESTION` varchar(50) NOT NULL,
  `T_ADMINSITE` varchar(6) NOT NULL,
  `T_VERIF` varchar(20) NOT NULL,
  `T_TEL` varchar(250) NOT NULL,
  PRIMARY KEY (`T_NUMERO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table tiersetab
-- -----------------------------
CREATE TABLE `tiersetab` (
  `TE_NUMERO` int(6) NOT NULL AUTO_INCREMENT,
  `TE_LOGIN` varchar(50) DEFAULT NULL,
  `TE_ETABLISSEMENT` varchar(6) DEFAULT NULL,
  `TE_DATEVALIDATION` date DEFAULT NULL,
  `TE_DEBUTADHESION` date NOT NULL,
  `TE_DATEFINADHESION` date NOT NULL,
  `TE_COCHE1` varchar(3) DEFAULT NULL,
  `TE_TEXTCOCHE1` longtext,
  `TE_COCHE2` varchar(3) DEFAULT NULL,
  `TE_TEXTCOCHE2` longtext,
  `TE_TEXTRGPD` longtext,
  `TE_STATUT` varchar(15) NOT NULL,
  `TE_CREDITOK` varchar(3) DEFAULT NULL,
  `TE_CREDITVAL` decimal(5,0) DEFAULT NULL,
  `TE_ARTADHESIONCODE` varchar(20) DEFAULT NULL,
  `TE_ARTADHESIONLIB` varchar(50) DEFAULT NULL,
  `TE_ARTADHESIONPRIX` decimal(5,2) DEFAULT NULL,
  `TE_ADHESIONPAYE` varchar(3) DEFAULT NULL,
  `TE_VALIDEUR` varchar(250) DEFAULT NULL,
  `TE_ID` varchar(20) DEFAULT NULL,
  `TE_COMPTATIERS` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`TE_NUMERO`),
  KEY `TE_NUMERO` (`TE_NUMERO`),
  KEY `TE_LOGIN` (`TE_LOGIN`),
  KEY `TE_ETABLISSEMENT` (`TE_ETABLISSEMENT`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

jr.menu@gmail.com;

test1;

bjolibert@gmail.com;

commeunebrouette@gmail.com;

nbernier@centre-britannique.org;

28-LV;

louis.veyer@ateliers-art-strong.fr;

jr.menu@gmail.com;

C00000038;

C00000039;

C00000040;

C0043;

C0044;

31-LV;

C00000045;

-- -----------------------------
-- Structure de la table tmpresa
-- -----------------------------
CREATE TABLE `tmpresa` (
  `USER` varchar(250) NOT NULL,
  `TOKEN` varchar(250) NOT NULL,
  `JOUR` int(11) NOT NULL,
  `MOIS` int(11) NOT NULL,
  `ANNEE` int(11) NOT NULL,
  `NBJOURUSED` int(11) NOT NULL,
  `NBMATINUSED` int(11) NOT NULL,
  `NBAPMUSED` int(11) NOT NULL,
  `NBJOURFREE` int(11) NOT NULL,
  `NBMATINFREE` int(11) NOT NULL,
  `NBAPMFREE` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table utilisateur
-- -----------------------------
CREATE TABLE `utilisateur` (
  `UT_IDUSER` int(10) NOT NULL AUTO_INCREMENT,
  `UT_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `UT_PASSWORD` varchar(250) NOT NULL,
  `UT_NOM` varchar(250) NOT NULL,
  `UT_PRENOM` varchar(250) NOT NULL,
  `UT_CONTACTNOM` varchar(250) DEFAULT NULL,
  `UT_CONTACTPRENOM` varchar(250) DEFAULT NULL,
  `UT_ADRESSE1` varchar(250) DEFAULT NULL,
  `UT_ADRESSE2` varchar(250) DEFAULT NULL,
  `UT_VILLE` varchar(250) DEFAULT NULL,
  `UT_CODEPOSTAL` varchar(250) DEFAULT NULL,
  `UT_PAYS` varchar(250) DEFAULT NULL,
  `UT_EMAIL` varchar(250) NOT NULL,
  `UT_STATUT` varchar(20) NOT NULL,
  `UT_SUPERADMIN` varchar(3) DEFAULT 'NON',
  `UT_ID1` varchar(20) NOT NULL,
  `UT_ID2` varchar(20) NOT NULL,
  `UT_ETABLISSEMENT` varchar(6) DEFAULT NULL,
  `UT_VALIDE` varchar(1) DEFAULT 'N',
  `UT_INITIALE` varchar(5) DEFAULT NULL,
  `UT_IDMODIF` varchar(50) DEFAULT NULL,
  `UT_CIVILITE` varchar(250) DEFAULT NULL,
  `UT_IDDATEDEMANDE` date DEFAULT NULL,
  `UT_TIERSGESTION` varchar(50) DEFAULT NULL,
  `UT_ADMINSITE` varchar(6) DEFAULT NULL,
  `UT_VERIF` varchar(20) DEFAULT NULL,
  `UT_TEL` varchar(250) DEFAULT NULL,
  `UT_LASTCONNECT` datetime NOT NULL,
  `UT_LASTMODIF` datetime NOT NULL,
  `UT_TOKEN` varchar(250) DEFAULT NULL,
  `UT_TYPE` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`UT_LOGIN`),
  KEY `UT_IDUSER` (`UT_IDUSER`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

28-LV;

31-LV;

32-LV;

33-LV;

annie.tacquet@yahoo.fr;

antoine.demailly@lessensdugout.fr;

bjolibert@gmail.com;

brohette.nicolas@gmail.com;

C00000038;

C00000039;

C00000040;

C00000045;

C0041;

C0042;

C0043;

C0044;

coeur d etoile;

commeunebrouette@gmail.com;

elisabeth.gruson@gmail.com;

herve.hazard.lessensdugout@gmail.com;

hugo@hdwebmarketing.fr;

j.cappel@pnr-scarpe-escaut.fr;

jr.menu@gmail.com;

louis.veyer@ateliers-art-strong.fr;

martial.dubois28@outlook.fr;

mathilde@transversaldesign.fr;

nbernier@centre-britannique.org;

perm1;

test1;

-- -----------------------------
-- Structure de la table utilisateursav
-- -----------------------------
CREATE TABLE `utilisateursav` (
  `UT_IDUSER` int(10) NOT NULL,
  `UT_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `UT_PASSWORD` varchar(250) NOT NULL,
  `UT_NOM` varchar(250) NOT NULL,
  `UT_PRENOM` varchar(250) NOT NULL,
  `UT_ADRESSE1` varchar(250) NOT NULL,
  `UT_ADRESSE2` varchar(250) NOT NULL,
  `UT_VILLE` varchar(250) NOT NULL,
  `UT_CODEPOSTAL` varchar(250) NOT NULL,
  `UT_PAYS` varchar(250) NOT NULL,
  `UT_EMAIL` varchar(250) NOT NULL,
  `UT_STATUT` varchar(20) NOT NULL,
  `UT_ID1` varchar(20) NOT NULL,
  `UT_ID2` varchar(20) NOT NULL,
  `UT_ETABLISSEMENT` varchar(6) NOT NULL,
  `UT_VALIDE` varchar(1) NOT NULL DEFAULT 'N',
  `UT_INITIALE` varchar(5) NOT NULL,
  `UT_IDMODIF` varchar(50) NOT NULL,
  `UT_CIVILITE` varchar(5) NOT NULL,
  `UT_IDDATEDEMANDE` date NOT NULL,
  `UT_TIERSGESTION` varchar(50) NOT NULL,
  `UT_ADMINSITE` varchar(6) NOT NULL,
  `UT_VERIF` varchar(20) NOT NULL,
  `UT_TEL` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Structure de la table voucher
-- -----------------------------
CREATE TABLE `voucher` (
  `BA_NUMERO` int(6) NOT NULL AUTO_INCREMENT,
  `BA_LOGIN` varchar(20) NOT NULL,
  `BA_ETABLISSEMENT` varchar(10) NOT NULL,
  `BA_LIBELLE` varchar(30) NOT NULL,
  `BA_VALEUR` decimal(5,0) NOT NULL,
  `BA_DATECREATION` date NOT NULL,
  `BA_DATEREPRISE` date NOT NULL,
  `BA_VALIDE` varchar(3) NOT NULL,
  `BA_REFREPRISE` varchar(30) NOT NULL,
  PRIMARY KEY (`BA_NUMERO`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

28-LV;

jr.menu@gmail.com;

-- -----------------------------
-- Structure de la table zone
-- -----------------------------
CREATE TABLE `zone` (
  `EM_EMPLACEMENT` int(7) NOT NULL AUTO_INCREMENT,
  `EM_ETABLISSEMENT` varchar(6) NOT NULL,
  `EM_LIBELLE` varchar(50) NOT NULL,
  `EM_NBRRESAMAX` int(2) NOT NULL,
  `EM_TYPEZONE` varchar(50) NOT NULL,
  `EM_NBRPLACESMAX` int(2) NOT NULL,
  `EM_PRIXPARZONE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `EM_PRIXPARPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `EM_HEUREDEBUT` time NOT NULL,
  `EM_HEUREFIN` time NOT NULL,
  `EM_ARTZONE` varchar(20) NOT NULL COMMENT 'ARTICLE DE L''EMPLACEMENT',
  `EM_ARTPLACE` varchar(20) NOT NULL COMMENT 'ARTICLE DE LA PLACE - NULL SI UNE SEULE PLACE',
  `EM_DEMIJOURNEE` varchar(3) DEFAULT NULL,
  `EM_ARTJOURNEE` varchar(30) DEFAULT NULL,
  `EM_ARTMATIN` varchar(30) DEFAULT NULL,
  `EM_ARTAPM` varchar(30) DEFAULT NULL,
  `EM_ARTPERSONNE` varchar(30) DEFAULT NULL,
  `EM_BLOQUE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`EM_EMPLACEMENT`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

ET0001;

ET0001;

ET0001;

ET0001;

ET0002;



, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , 