<?php
/**************************************************************************************/
/*                          __          ____                                          */
/*                    ___  / /  ___    / __/__  __ _____________ ___                  */
/*                   / _ \/ _ \/ _ \  _\ \/ _ \/ // / __/ __/ -_|_-<                  */
/*                  / .__/_//_/ .__/  \___/\___/\_,_/_/  \__/\__/___/                 */
/*                 /_/       /_/                                                      */
/*                                                                                    */
/**************************************************************************************/
/*                                                                                    */
/*  Titre          : Dump (sauvegarde) avec PHP d'une base de donnée MySQL            */
/*                                                                                    */
/*  URL            : http://phpsources.org/scripts612-PHP.htm                         */
/*  Auteur         : miragoo                                                          */
/*  Date edition   : 28 Oct 2010                                                      */
/*                                                                                    */
/**************************************************************************************/


require_once ("../scripts/constantes.php");

require ("../scripts/fonctions.php");

function dump_MySQL($serveur, $login, $password, $base, $mode)
{
	echo $serveur .'<br>'. $login;
    //$connexion = mysqli_connect($serveur, $login, $password);
    //mysql_select_db($base, $connexion);
    $sql = 'show tables';
    $entete  = "-- ----------------------\n";
    $entete .= "-- dump de la base ".$base." au ".date("d-M-Y")."\n";
    $entete .= "-- ----------------------\n\n\n";
    $creations = "";
    $insertions = "\n\n";
    $cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $table)
	{
		$creations .= "-- -----------------------------\n";
		$creations .= "-- Structure de la table ".$table[0]."\n";
		$creations .= "-- -----------------------------\n";
		
		$sql = 'show create table '.$table[0];
		echo $sql;
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $creationTable)
		{
			$creations .= $creationTable[1].";\n\n";
		}
		
		$sql = 'SELECT * FROM '.$table[0];
		$cnx_bdd = ConnexionBDD();
		
		$result_req = $cnx_bdd->query($sql);
		$nbr_colonne = $result_req->columnCount();
		echo $nbr_colonne;
		$type = $result_req->getColumnMeta(2);
		var_dump($type);
		echo $type(1);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $donnees)
		{
			for ($i=0; $i < $nbr_colonne; $i++)
			{
				if($i != 0)
                     $insertions .=  ", ";
				 
			}
			$creations .= $donnees[1].";\n\n";
		}
	}
	
	$fichierDump = fopen("sauvegarde.sql", "wb");
    fwrite($fichierDump, $entete);
    fwrite($fichierDump, $creations);
    fwrite($fichierDump, $insertions);
    fclose($fichierDump);
	
	exit;
    $listeTables = mysqli_query("show tables", $connexion);
    while($table = mysqli_fetch_array($listeTables))
    {
        // structure ou la totalité de la BDD
        if($mode == 1 || $mode == 2)
        {
            $creations .= "-- -----------------------------\n";
            $creations .= "-- Structure de la table ".$table[0]."\n";
            $creations .= "-- -----------------------------\n";
            $listeCreationsTables = mysqli_query("show create table ".($table[0]), $connexion);
            while($creationTable = mysqli_fetch_array($listeCreationsTables))
            {
              $creations .= strtoupper($creationTable[1]).";\n\n";
            }
        }
        // données ou la totalité
        if($mode > 1)
        {
            $donnees = mysqli_query("SELECT * FROM ".$table[0]);
            $insertions .= "-- -----------------------------\n";
            $insertions .= "-- Contenu de la table ".$table[0]."\n";
            $insertions .= "-- -----------------------------\n";
            while($nuplet = mysqli_fetch_array($donnees))
            {
                $insertions .= "INSERT INTO ".$table[0]." VALUES(";
                for($i=0; $i < mysqli_num_fields($donnees); $i++)
                {
                  if($i != 0)
                     $insertions .=  ", ";
                  if(mysqli_field_type($donnees, $i) == "string" || mysqli_field_type($donnees, $i) == "blob")
                     $insertions .=  "'";
                  $insertions .= addslashes($nuplet[$i]);
                  if(mysqli_field_type($donnees, $i) == "string" || mysqli_field_type($donnees, $i) == "blob")
                    $insertions .=  "'";
                }
                $insertions .=  ");\n";
            }
            $insertions .= "\n";
        }
    }
 
    mysqli_close($connexion);
 
    $fichierDump = fopen("sauvegarde.sql", "wb");
    fwrite($fichierDump, $entete);
    fwrite($fichierDump, $creations);
    fwrite($fichierDump, $insertions);
    fclose($fichierDump);

    echo "Sauvegarde terminée";
}

dump_MySQL("127.0.0.1", "root", "", "adminlv_reservation", 2);

?>
