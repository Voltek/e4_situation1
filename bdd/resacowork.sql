-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 28 mai 2019 à 11:54
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `resacowork`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `AR_ARTICLENO` int(5) NOT NULL,
  `AR_CODEARTICLE` varchar(20) NOT NULL,
  `AR_DESIGNATION` varchar(50) NOT NULL,
  `AR_TYPE` varchar(20) NOT NULL,
  `AR_ETABLISSEMENT` varchar(10) NOT NULL,
  `AR_FAMILLE1` varchar(50) DEFAULT NULL,
  `AR_FAMILLE2` varchar(50) DEFAULT NULL,
  `AR_TARIF` decimal(12,2) NOT NULL,
  `AR_RESAHEUREDEBUT` time DEFAULT NULL,
  `AR_RESAHEUREFIN` time DEFAULT NULL,
  `AR_IMAGE` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`AR_ARTICLENO`, `AR_CODEARTICLE`, `AR_DESIGNATION`, `AR_TYPE`, `AR_ETABLISSEMENT`, `AR_FAMILLE1`, `AR_FAMILLE2`, `AR_TARIF`, `AR_RESAHEUREDEBUT`, `AR_RESAHEUREFIN`, `AR_IMAGE`) VALUES
(1, 'ADHESION', 'ADHESION A LA VERRIERE', 'ARTAD', 'ET0001', '', '', '5.00', '00:00:00', '00:00:00', ''),
(3, 'ARTCOWORK', 'RÃ©servation Coworking', 'ARTRE', 'ET0001', '', '', '5.00', '00:00:00', '19:00:00', NULL),
(4, 'SALLEAPM', 'RÃ©servation Salle AprÃ¨s-midi', 'ARTRE', 'ET0001', '', '', '5.00', '00:00:00', '19:00:00', NULL),
(5, 'SALLEMATIN', 'RÃ©servation Salle Matin', 'ARTRE', 'ET0001', '', '', '5.00', '00:00:00', '19:00:00', NULL),
(6, 'SALLESOIR', 'RÃ©servation Salle Soir', 'ARTRE', 'ET0001', '', '', '5.00', '00:00:00', '19:00:00', NULL),
(7, 'PLACEREUNION', 'Place salle de rÃ©union', 'ARTRE', 'ET0001', '', '', '1.00', '00:00:00', '19:00:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `choixcode`
--

CREATE TABLE `choixcode` (
  `CC_ID` int(11) NOT NULL,
  `CC_TYPE` varchar(20) NOT NULL,
  `CC_CODE` varchar(15) NOT NULL,
  `CC_LIBELLE` varchar(50) NOT NULL,
  `CC_ABREGE` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `choixcode`
--

INSERT INTO `choixcode` (`CC_ID`, `CC_TYPE`, `CC_CODE`, `CC_LIBELLE`, `CC_ABREGE`) VALUES
(1, 'MOIS', '01', 'janvier', 'janvier'),
(2, 'MOIS', '02', 'février', 'février'),
(3, 'MOIS', '03', 'mars', 'mars'),
(4, 'MOIS', '04', 'avril', 'avril'),
(5, 'MOIS', '05', 'mai', 'mai'),
(6, 'MOIS', '06', 'juin', 'juin'),
(7, 'MOIS', '07', 'juillet', 'juillet'),
(8, 'MOIS', '08', 'aout', 'aout'),
(9, 'MOIS', '09', 'septembre', 'septembre'),
(10, 'MOIS', '10', 'octobre', 'octobre'),
(11, 'MOIS', '11', 'novembre', 'novembre'),
(12, 'MOIS', '12', 'décembre', 'décembre'),
(13, 'TYPEART', 'ARTAD', 'ARTICLE ADHESION', 'ARTICLE ADHESION'),
(14, 'TYPEART', 'ARTVE', 'ARTICLE VENTE PRODUIT', 'ARTICLE VENTE PRODUIT'),
(15, 'TYPEART', 'ARTFI', 'ARTICLE FINANCIER', 'ARTICLE FINANCIER'),
(16, 'TYPEART', 'ARTRE', 'ARTICLE RESERVATION', 'ARTICLE RESERVATION'),
(17, 'TYPEZONE', 'COW', 'COWORKING', 'COWORKING'),
(18, 'TYPEZONE', 'REU', 'SALLE REUNION', 'SALLE REUNION'),
(19, 'TYPE_MORALE', 'ASS', 'Association', 'Association'),
(20, 'TYPE_MORALE', 'SARL', 'SARL', 'SARL'),
(21, 'TYPE_MORALE', 'SAS', 'SAS', 'SAS'),
(22, 'TYPE_PHYSIQUE', 'M', 'Monsieur', 'M'),
(23, 'TYPE_PHYSIQUE', 'MME', 'Madame', 'Mme'),
(24, 'FAMRESANIV1', 'COWORKING', 'Espace de Coworking', 'Coworking'),
(25, 'FAMRESANIV1', 'SALLE REUNION', 'Salle de Réunion', 'Réunion'),
(26, 'FAMRESANIV2', 'RESAPLACE', 'Place de Réunion', 'Place de Réunion');

-- --------------------------------------------------------

--
-- Structure de la table `compteur`
--

CREATE TABLE `compteur` (
  `CT_NUMERO` int(11) NOT NULL,
  `CT_ETABLISSEMENT` varchar(6) NOT NULL,
  `CT_DOCUMENT` varchar(15) NOT NULL,
  `CT_COMPTEUR` int(6) NOT NULL,
  `CT_CODEDOC` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compteur`
--

INSERT INTO `compteur` (`CT_NUMERO`, `CT_ETABLISSEMENT`, `CT_DOCUMENT`, `CT_COMPTEUR`, `CT_CODEDOC`) VALUES
(1, 'ET0001', 'COTISATION', 2, 'CLO');

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

CREATE TABLE `config` (
  `CO_MULTIETABLISSEMENT` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `DO_NUMERO` int(3) NOT NULL,
  `DO_CODE` varchar(3) NOT NULL,
  `DO_LIBELLE` varchar(30) NOT NULL,
  `DO_NOMFICHIER` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

CREATE TABLE `emplacement` (
  `EM_ETABLISSEMENT` varchar(6) NOT NULL,
  `EM_EMPLACEMENT` varchar(7) NOT NULL,
  `EM_LIBELLE` varchar(50) NOT NULL,
  `EM_NBRRESAMAX` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `ET_ETABLISSEMENT` varchar(6) NOT NULL,
  `ET_LIBELLE` varchar(50) NOT NULL,
  `ET_BLOQUE` varchar(3) NOT NULL,
  `ET_ADRESSE1` varchar(100) NOT NULL,
  `ET_ADRESSE2` varchar(100) NOT NULL,
  `ET_ADRESSE3` varchar(100) NOT NULL,
  `ET_CODEPOSTAL` varchar(5) NOT NULL,
  `ET_VILLE` varchar(50) NOT NULL,
  `ET_NBRRESAMAX` int(2) NOT NULL,
  `ET_LOGO` varchar(50) NOT NULL,
  `ET_EMAIL` varchar(250) NOT NULL,
  `ET_SITEWEB` varchar(250) DEFAULT NULL,
  `ET_IBAN` varchar(100) DEFAULT NULL,
  `ET_BANQUECODE` varchar(10) DEFAULT NULL,
  `ET_BANQUEGUICHET` varchar(10) DEFAULT NULL,
  `ET_BANQUECPT` varchar(10) DEFAULT NULL,
  `ET_BANQUECLEF` varchar(2) DEFAULT NULL,
  `ET_BANQUENOM` varchar(30) DEFAULT NULL,
  `ET_BANQUEBIC` varchar(10) DEFAULT NULL,
  `ET_SIRET` varchar(20) DEFAULT NULL,
  `ET_NAP` varchar(5) DEFAULT NULL,
  `ET_ECHEANCEFACTURE` int(2) DEFAULT NULL,
  `ET_TYPEADHESION` varchar(20) DEFAULT NULL,
  `ET_ARTADHESION` varchar(20) DEFAULT NULL,
  `ET_ARTMONNAIE` varchar(20) DEFAULT NULL,
  `ET_IMAGETYPE` varchar(25) DEFAULT NULL,
  `ET_IMAGENOM` varchar(50) DEFAULT NULL,
  `ET_PETITEIMAGENOM` varchar(50) DEFAULT NULL,
  `ET_FACTURATION` varchar(3) DEFAULT NULL,
  `ET_PIEDFACTURE` longtext,
  `ET_GESTIONADHESION` varchar(3) DEFAULT NULL,
  `ET_GESTIONCREDITCLIENT` varchar(3) DEFAULT NULL,
  `ET_ADHESIONCOCHE1` longtext,
  `ET_ADHESIONCOCHE2` longtext,
  `ET_ADHESIONRGPD1` longtext,
  `ET_FICHIERCGU` varchar(100) DEFAULT NULL,
  `ET_MODELEADHESION` varchar(3) DEFAULT NULL,
  `ET_IMAGE` varchar(10) DEFAULT NULL,
  `ET_ADHESIONBYERP` varchar(3) NOT NULL,
  `ET_GESTIONPANIER` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etablissement`
--

INSERT INTO `etablissement` (`ET_ETABLISSEMENT`, `ET_LIBELLE`, `ET_BLOQUE`, `ET_ADRESSE1`, `ET_ADRESSE2`, `ET_ADRESSE3`, `ET_CODEPOSTAL`, `ET_VILLE`, `ET_NBRRESAMAX`, `ET_LOGO`, `ET_EMAIL`, `ET_SITEWEB`, `ET_IBAN`, `ET_BANQUECODE`, `ET_BANQUEGUICHET`, `ET_BANQUECPT`, `ET_BANQUECLEF`, `ET_BANQUENOM`, `ET_BANQUEBIC`, `ET_SIRET`, `ET_NAP`, `ET_ECHEANCEFACTURE`, `ET_TYPEADHESION`, `ET_ARTADHESION`, `ET_ARTMONNAIE`, `ET_IMAGETYPE`, `ET_IMAGENOM`, `ET_PETITEIMAGENOM`, `ET_FACTURATION`, `ET_PIEDFACTURE`, `ET_GESTIONADHESION`, `ET_GESTIONCREDITCLIENT`, `ET_ADHESIONCOCHE1`, `ET_ADHESIONCOCHE2`, `ET_ADHESIONRGPD1`, `ET_FICHIERCGU`, `ET_MODELEADHESION`, `ET_IMAGE`, `ET_ADHESIONBYERP`, `ET_GESTIONPANIER`) VALUES
('ET0001', 'ASSOCIATION LA VERRIERE', 'NON', '7 rue George V', '', '', '59530', 'LE QUESNOY', 3, 'logo-la-verriere.png', 'association.laverriere@gmail.com', 'www.coworking-laverriere.fr', 'FR7615629027250005452530108', '15629', '02725', '0005452530', '08', 'CrÃ©dit Mutuel', 'CMCIFR2A', '82328504400017', '9499Z', 30, 'GLISSANT', 'ADHESION', '', 'image/png', 'logo-la-verriere.png', 'logo-la-verriere-mini.svg', '', '', '', 'OUI', '', '', '', '', '', '', 'OUI', 'NON'),
('ET0002', 'JE TRAVAILLE AU VERT', 'OUI', '350 Rue du Moulin', '', '', '59246', ' Mons-en-Pévèle', 3, 'logo-jetravailleauvert.png', 'association.laverriere@gmail.com', NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'OUI', '');

-- --------------------------------------------------------

--
-- Structure de la table `gestionemail`
--

CREATE TABLE `gestionemail` (
  `GE_NUMERO` int(11) NOT NULL,
  `GE_ETABLISSEMENT` varchar(8) NOT NULL,
  `GE_TYPE` varchar(30) DEFAULT NULL,
  `GE_LIBELLE` varchar(250) NOT NULL,
  `GE_HEADER` text,
  `GE_MESSAGE` text,
  `GE_SIGNATURE` text,
  `GE_NBCONTACT` int(2) DEFAULT NULL,
  `GE_CONTACT` varchar(250) DEFAULT NULL,
  `GE_TYPEENVOI` int(1) DEFAULT NULL COMMENT '1 = envoi simple'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `gestionemail`
--

INSERT INTO `gestionemail` (`GE_NUMERO`, `GE_ETABLISSEMENT`, `GE_TYPE`, `GE_LIBELLE`, `GE_HEADER`, `GE_MESSAGE`, `GE_SIGNATURE`, `GE_NBCONTACT`, `GE_CONTACT`, `GE_TYPEENVOI`) VALUES
(1, 'ET0001', 'CONFIRMRESERVATION', 'Confirmation de réservation', NULL, NULL, NULL, 2, 'jr.menu@gmail.com;association.laverriere@gmail.com', 1),
(2, 'ET0001', 'ANNULRESERVATION', 'Annulation Réservation', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ligne`
--

CREATE TABLE `ligne` (
  `GL_IDLIGNE` int(11) NOT NULL,
  `GL_NUMLIGNE` int(11) NOT NULL,
  `GL_LIBELLE` varchar(60) NOT NULL,
  `GL_NUMERO` int(11) NOT NULL,
  `GL_ETABLISSEMENT` varchar(6) NOT NULL,
  `GL_USER` varchar(250) NOT NULL,
  `GL_DATEPIECE` date NOT NULL,
  `GL_ARTICLENO` varchar(20) NOT NULL,
  `GL_ARTLIBELLE` varchar(50) NOT NULL,
  `GL_ARTFAMILLE1` varchar(50) DEFAULT NULL,
  `GL_ARTFAMILLE2` varchar(50) DEFAULT NULL,
  `GL_ARTTARIF` decimal(12,2) NOT NULL,
  `GL_QTEFACT` int(5) NOT NULL,
  `GL_TOTALHT` decimal(12,2) NOT NULL,
  `GL_REFFACTURE` varchar(30) NOT NULL,
  `GL_COMMENTAIRE` text NOT NULL,
  `GL_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GL_PRIXPARPLACE` decimal(12,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ligne`
--

INSERT INTO `ligne` (`GL_IDLIGNE`, `GL_NUMLIGNE`, `GL_LIBELLE`, `GL_NUMERO`, `GL_ETABLISSEMENT`, `GL_USER`, `GL_DATEPIECE`, `GL_ARTICLENO`, `GL_ARTLIBELLE`, `GL_ARTFAMILLE1`, `GL_ARTFAMILLE2`, `GL_ARTTARIF`, `GL_QTEFACT`, `GL_TOTALHT`, `GL_REFFACTURE`, `GL_COMMENTAIRE`, `GL_PRIXPARPERSONNE`, `GL_PRIXPARPLACE`) VALUES
(51, 1, '', 52, 'ET0001', 'test1', '2018-06-21', '1', 'Zone Coworking', NULL, NULL, '0.00', 5, '0.00', 'FACT1806_00052', '', '0.00', '0.0'),
(52, 2, '', 52, 'ET0001', 'test1', '2018-06-21', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00052', '', '5.00', '0.0'),
(53, 3, '', 52, 'ET0001', 'test1', '2018-06-21', '2', 'Salle r&eacute;union matin', NULL, NULL, '0.00', 1, '0.00', 'FACT1806_00052', '', '0.00', '0.0'),
(54, 4, '', 52, 'ET0001', 'test1', '2018-06-21', '3', 'Salle r&eacute;union apr&egrave;s-midi', NULL, NULL, '0.00', 2, '0.00', 'FACT1806_00052', '', '0.00', '0.0'),
(55, 1, '', 53, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00053', '', '5.00', '0.0'),
(56, 1, '', 54, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00054', '', '5.00', '0.0'),
(57, 1, '', 55, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00055', '', '5.00', '0.0'),
(58, 1, '', 56, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00056', '', '5.00', '0.0'),
(59, 2, '', 56, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '12.00', 'FACT1806_00056', '', '1.00', '5.0'),
(60, 1, '', 57, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00057', '', '5.00', '0.0'),
(61, 2, '', 57, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 2, '29.00', 'FACT1806_00057', '', '1.00', '5.0'),
(62, 1, '', 58, 'ET0001', 'test1', '2018-06-25', '3', 'Salle rÃ©union aprÃ¨s-midi', NULL, NULL, '0.00', 1, '11.00', 'FACT1806_00058', '', '1.00', '5.0'),
(63, 1, '', 59, 'ET0001', 'test1', '2018-06-25', '4', 'Salle rÃ©union soir', NULL, NULL, '0.00', 1, '9.00', 'FACT1806_00059', '', '1.00', '5.0'),
(64, 1, '', 60, 'ET0001', 'test1', '2018-06-25', '4', 'Salle rÃ©union soir', NULL, NULL, '0.00', 1, '9.00', 'FACT1806_00060', '', '1.00', '5.0'),
(65, 1, '', 61, 'ET0001', 'test1', '2018-06-25', '3', 'Salle rÃ©union aprÃ¨s-midi', NULL, NULL, '0.00', 1, '8.00', 'FACT1806_00061', '', '1.00', '5.0'),
(66, 1, '', 62, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '12.00', 'FACT1806_00062', '', '1.00', '5.0'),
(67, 1, '', 63, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '14.00', 'FACT1806_00063', '', '1.00', '5.0'),
(68, 1, '', 64, 'ET0001', 'test1', '2018-06-25', '3', 'Salle rÃ©union aprÃ¨s-midi', NULL, NULL, '0.00', 1, '13.00', 'FACT1806_00064', '', '1.00', '5.0'),
(69, 1, '', 65, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '14.00', 'FACT1806_00065', '', '1.00', '5.0'),
(70, 1, '', 66, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '11.00', 'FACT1806_00066', '', '1.00', '5.0'),
(71, 1, '', 67, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '11.00', 'FACT1806_00067', '', '1.00', '5.0'),
(72, 1, '', 68, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00068', '', '5.00', '0.0'),
(73, 1, '', 69, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00069', '', '5.00', '0.0'),
(74, 2, '', 69, 'ET0001', 'test1', '2018-06-25', '4', 'Salle rÃ©union soir', NULL, NULL, '0.00', 1, '11.00', 'FACT1806_00069', '', '1.00', '5.0'),
(75, 1, '', 70, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 5, '0.00', 'FACT1806_00070', '', '0.00', '0.0'),
(76, 2, '', 70, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 8, '40.00', 'FACT1806_00070', '', '5.00', '0.0'),
(77, 3, '', 70, 'ET0001', 'test1', '2018-06-25', '2', 'Salle r&eacute;union matin', NULL, NULL, '0.00', 1, '0.00', 'FACT1806_00070', '', '0.00', '0.0'),
(78, 4, '', 70, 'ET0001', 'test1', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 5, '67.00', 'FACT1806_00070', '', '1.00', '5.0'),
(79, 5, '', 70, 'ET0001', 'test1', '2018-06-25', '3', 'Salle r&eacute;union apr&egrave;s-midi', NULL, NULL, '0.00', 2, '0.00', 'FACT1806_00070', '', '0.00', '0.0'),
(80, 6, '', 70, 'ET0001', 'test1', '2018-06-25', '3', 'Salle rÃ©union aprÃ¨s-midi', NULL, NULL, '0.00', 3, '32.00', 'FACT1806_00070', '', '1.00', '5.0'),
(81, 7, '', 70, 'ET0001', 'test1', '2018-06-25', '4', 'Salle rÃ©union soir', NULL, NULL, '0.00', 3, '29.00', 'FACT1806_00070', '', '1.00', '5.0'),
(82, 1, '', 71, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00071', '', '5.00', '0.0'),
(83, 1, '', 72, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00072', '', '5.00', '0.0'),
(84, 1, '', 73, 'ET0001', 'test1', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00073', '', '5.00', '0.0'),
(85, 1, '', 74, 'ET0001', 'Jr.menu@gmail.com ', '2018-06-25', '1', 'Zone Coworking', NULL, NULL, '0.00', 5, '25.00', 'FACT1806_00074', '', '5.00', '0.0'),
(86, 2, '', 74, 'ET0001', 'Jr.menu@gmail.com ', '2018-06-25', '2', 'Salle rÃ©union matin', NULL, NULL, '0.00', 1, '7.00', 'FACT1806_00074', '', '1.00', '5.0'),
(87, 3, '', 74, 'ET0001', 'Jr.menu@gmail.com ', '2018-06-25', '3', 'Salle r&eacute;union apr&egrave;s-midi', NULL, NULL, '0.00', 1, '0.00', 'FACT1806_00074', '', '0.00', '0.0'),
(88, 1, '', 75, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00075', '', '5.00', '0.0'),
(89, 2, '', 75, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00075', '', '5.00', '0.0'),
(90, 1, '', 76, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00076', '', '5.00', '0.0'),
(91, 2, '', 76, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00076', '', '5.00', '0.0'),
(92, 1, '', 77, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00077', '', '5.00', '0.0'),
(93, 2, '', 77, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00077', '', '5.00', '0.0'),
(94, 1, '', 78, 'ET0001', 'test1', '2018-06-27', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1806_00078', '5.00', '5.00', '0.0'),
(95, 1, '', 79, 'ET0001', '28-LV', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00079', '5.00', '5.00', '0.0'),
(96, 2, '', 79, 'ET0001', '28-LV', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00079', '5.00', '5.00', '0.0'),
(97, 3, '', 79, 'ET0001', '28-LV', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00079', '5.00', '5.00', '0.0'),
(98, 1, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(99, 2, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(100, 3, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(101, 4, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(102, 5, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(103, 6, '', 80, 'ET0001', 'bjolibert@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00080', '5.00', '5.00', '0.0'),
(104, 1, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(105, 2, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(106, 3, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(107, 4, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(108, 5, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(109, 6, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(110, 7, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(111, 8, '', 81, 'ET0001', 'commeunebrouette@gmail.com', '2018-07-02', '1', 'Zone Coworking', NULL, NULL, '0.00', 1, '5.00', 'FACT1807_00081', '5.00', '5.00', '0.0'),
(112, 1, '', 82, 'ET0001', 'test1', '2018-07-03', 'ADHESION', 'ADHESION A LA VERRIERE', NULL, NULL, '0.00', 1, '5.00', 'AD1807_00082', '5.00', '0.00', '0.0'),
(113, 1, '', 83, 'ET0001', 'test1', '2018-07-03', 'ADHESION', 'ADHESION A LA VERRIERE', NULL, NULL, '0.00', 1, '5.00', 'AD1807_00083', '5.00', '0.00', '0.0'),
(114, 1, 'RÃ©servation nÂ° 517 du : 19 09 2018', 150, 'ET0001', 'test1', '2018-10-26', '', '', NULL, NULL, '5.00', 1, '5.00', 'CLO26102018ET0001000001', '', '0.00', '0.0'),
(115, 2, 'RÃ©servation nÂ° 518 du : 19 09 2018', 150, 'ET0001', 'test1', '2018-10-26', '', '', NULL, NULL, '5.00', 1, '5.00', 'CLO26102018ET0001000001', '', '0.00', '0.0'),
(116, 3, 'RÃ©servation nÂ° 519 du : 19 09 2018', 150, 'ET0001', 'test1', '2018-10-26', '', '', NULL, NULL, '5.00', 1, '5.00', 'CLO26102018ET0001000001', '', '0.00', '0.0'),
(117, 4, 'RÃ©servation nÂ° 520 du : 19 09 2018', 150, 'ET0001', 'test1', '2018-10-26', '', '', NULL, NULL, '5.00', 1, '5.00', 'CLO26102018ET0001000001', '', '0.00', '0.0'),
(118, 1, 'RÃ©servation nÂ° 542 du : 09 01 2019', 151, 'ET0001', 'C00000045', '2019-01-09', 'ARTCOWORK', 'RÃ©servation Coworking', NULL, NULL, '5.00', 1, '5.00', 'CLO09012019ET0001000002', '', '0.00', '0.0'),
(119, 2, 'RÃ©servation nÂ° 543 du : 10 01 2019', 151, 'ET0001', 'C00000045', '2019-01-09', 'SALLEMATIN', 'RÃ©servation Salle Matin', NULL, NULL, '5.00', 1, '5.00', 'CLO09012019ET0001000002', '', '0.00', '0.0'),
(120, 3, 'RÃ©servation nÂ° 543 du : 10 01 2019', 151, 'ET0001', 'C00000045', '2019-01-09', 'PLACEREUNION', 'Place salle de rÃ©union', NULL, NULL, '1.00', 5, '5.00', 'CLO09012019ET0001000002', '', '0.00', '0.0');

-- --------------------------------------------------------

--
-- Structure de la table `ligneattente`
--

CREATE TABLE `ligneattente` (
  `GLA_IDLIGNE` int(11) NOT NULL,
  `GLA_DATECREATE` datetime NOT NULL,
  `GLA_VALIDE` varchar(3) NOT NULL,
  `GLA_NUMLIGNE` int(11) NOT NULL,
  `GLA_NUMERO` int(11) NOT NULL,
  `GLA_LIBELLE` varchar(50) NOT NULL,
  `GLA_ETABLISSEMENT` varchar(6) NOT NULL,
  `GLA_USER` varchar(250) NOT NULL,
  `GLA_DATEPIECE` date NOT NULL,
  `GLA_ARTICLENO` varchar(15) NOT NULL,
  `GLA_ARTLIBELLE` varchar(50) NOT NULL,
  `GLA_ARTFAMILLE1` varchar(50) DEFAULT NULL,
  `GLA_ARTFAMILLE2` varchar(50) DEFAULT NULL,
  `GLA_ARTTARIF` decimal(12,2) NOT NULL,
  `GLA_QTEFACT` int(5) NOT NULL,
  `GLA_TOTALHT` decimal(12,2) NOT NULL,
  `GLA_REFFACTURE` varchar(30) NOT NULL,
  `GLA_COMMENTAIRE` text NOT NULL,
  `GLA_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GLA_PRIXPARPLACE` decimal(12,1) NOT NULL,
  `GLA_SESSIONTOKEN` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `lignecompl`
--

CREATE TABLE `lignecompl` (
  `GL2_NUMID` int(11) NOT NULL,
  `GL2_NUMLIGNE` int(11) NOT NULL,
  `GL2_INDEXLIGNE` int(11) NOT NULL,
  `GL2_LIBELLE1` varchar(250) NOT NULL,
  `GL2_LIBELLE2` varchar(250) NOT NULL,
  `GL2_REFFACTURE` varchar(50) NOT NULL,
  `GL2_DATEPIECE` date NOT NULL,
  `GL2_USER` varchar(50) NOT NULL,
  `GL2_ETABLISSEMENT` varchar(50) NOT NULL,
  `GL2_PRIXPLACE` decimal(12,2) NOT NULL,
  `GL2_PRIXPARPERSONNE` decimal(12,2) NOT NULL,
  `GL2_QTEPLACE` int(11) NOT NULL,
  `GL2_QTEPERSONNE` int(11) NOT NULL,
  `GL2_PRIXTOTALHT` decimal(12,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lignecompl`
--

INSERT INTO `lignecompl` (`GL2_NUMID`, `GL2_NUMLIGNE`, `GL2_INDEXLIGNE`, `GL2_LIBELLE1`, `GL2_LIBELLE2`, `GL2_REFFACTURE`, `GL2_DATEPIECE`, `GL2_USER`, `GL2_ETABLISSEMENT`, `GL2_PRIXPLACE`, `GL2_PRIXPARPERSONNE`, `GL2_QTEPLACE`, `GL2_QTEPERSONNE`, `GL2_PRIXTOTALHT`) VALUES
(49, 1, 1, 'Zone Coworking : 21 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(50, 1, 2, 'Zone Coworking : 21 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(51, 1, 3, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(52, 1, 4, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(53, 1, 5, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(54, 1, 6, 'Zone Coworking : 06 June 2018   Prix = 5.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(55, 1, 7, '', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(56, 2, 1, 'Zone Coworking : 21 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(57, 2, 2, 'Zone Coworking : 21 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(58, 2, 3, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(59, 2, 4, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(60, 2, 5, 'Zone Coworking : 27 October 2017   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(61, 2, 6, 'Zone Coworking : 06 June 2018   Prix = 5.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(62, 2, 7, '', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(63, 3, 1, 'Salle r&eacute;union matin : 10 January 2018   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(64, 3, 2, '', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(65, 4, 1, 'Salle r&eacute;union apr&egrave;s-midi : 10 January 2018   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(66, 4, 2, 'Salle r&eacute;union apr&egrave;s-midi : 29 January 2018   Prix = 0.00', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(67, 4, 3, '', '', 'FACT1806_00052', '2018-06-21', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(68, 1, 1, 'Zone Coworking : 29 June 2018   Prix = 5.00', '', 'FACT1806_00053', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(69, 1, 2, '', '', 'FACT1806_00053', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(70, 1, 1, 'Zone Coworking : 30 June 2018   Prix = 5.00', '', 'FACT1806_00054', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(71, 1, 2, '', '', 'FACT1806_00054', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(72, 1, 1, 'Zone Coworking : 28 June 2018   Prix = 5.00', '', 'FACT1806_00055', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(73, 1, 2, '', '', 'FACT1806_00055', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(74, 1, 1, 'Zone Coworking : 30 June 2018   Prix = 5.00', '', 'FACT1806_00056', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(75, 1, 2, '', '', 'FACT1806_00056', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(76, 2, 1, 'Salle rÃ©union matin : 29 June 2018   Prix = 12.00', '', 'FACT1806_00056', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(77, 2, 2, '', '', 'FACT1806_00056', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(78, 1, 1, 'Zone Coworking : 29 June 2018   Prix = 5.00', '', 'FACT1806_00057', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(79, 1, 2, '', '', 'FACT1806_00057', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(80, 2, 1, 'Salle rÃ©union matin : 28 June 2018   Prix = 13.00', '', 'FACT1806_00057', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(81, 2, 2, 'Salle rÃ©union matin : 30 June 2018   Prix = 16.00', '', 'FACT1806_00057', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(82, 2, 3, '', '', 'FACT1806_00057', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(83, 1, 2, '', '', 'FACT1806_00058', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(84, 1, 2, '', '', 'FACT1806_00059', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(85, 1, 1, 'Salle réunion soir : 2048 Prix = 9.00', '', 'FACT1806_00059', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 0, '0.00'),
(86, 1, 2, '', '', 'FACT1806_00060', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(87, 1, 2, '', '', 'FACT1806_00061', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(88, 1, 2, '', '', 'FACT1806_00062', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(89, 1, 1, 'Salle réunion matin : 27 juin 2018 Prix = 12.00', '', 'FACT1806_00062', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 0, '0.00'),
(90, 1, 1, 'Salle rÃ©union matin : 26 juin 2018   Prix = 14.00', '', 'FACT1806_00063', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 0, '14.00'),
(91, 1, 2, '', '', 'FACT1806_00063', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(92, 1, 2, '', '', 'FACT1806_00064', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(93, 1, 1, 'Salle rÃ©union matin : 28 juin 2018   Prix Total HT = 14.00 Euro ', '', 'FACT1806_00065', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 9, '14.00'),
(94, 1, 2, '', '', 'FACT1806_00065', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(95, 1, 2, '', '', 'FACT1806_00066', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(96, 1, 1, 'Salle rÃ©union matin : 29 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00067', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 6, '11.00'),
(97, 1, 2, '', '', 'FACT1806_00067', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(98, 1, 1, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00068', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(99, 1, 2, '', '', 'FACT1806_00068', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(100, 1, 1, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00069', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(101, 1, 2, '', '', 'FACT1806_00069', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(102, 2, 1, 'Salle rÃ©union soir : 28 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00069', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 6, '11.00'),
(103, 2, 2, '', '', 'FACT1806_00069', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(104, 1, 1, 'Zone Coworking : 21 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(105, 1, 2, 'Zone Coworking : 21 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(106, 1, 3, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(107, 1, 4, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(108, 1, 5, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(109, 1, 6, 'Zone Coworking : 06 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(110, 1, 7, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(111, 1, 8, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(112, 1, 9, 'Zone Coworking : 28 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(113, 1, 10, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(114, 1, 11, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(115, 1, 12, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(116, 1, 13, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '5.00'),
(117, 1, 14, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(118, 2, 1, 'Zone Coworking : 21 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '0.00'),
(119, 2, 2, 'Zone Coworking : 21 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '0.00'),
(120, 2, 3, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '0.00'),
(121, 2, 4, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '0.00'),
(122, 2, 5, 'Zone Coworking : 27 octobre 2017   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '0.00'),
(123, 2, 6, 'Zone Coworking : 06 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(124, 2, 7, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(125, 2, 8, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(126, 2, 9, 'Zone Coworking : 28 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(127, 2, 10, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(128, 2, 11, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(129, 2, 12, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(130, 2, 13, 'Zone Coworking : 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(131, 2, 14, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(132, 3, 1, 'Salle r&eacute;union matin : 10 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 3 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 3, '0.00'),
(133, 3, 2, 'Salle rÃ©union matin : 30 juin 2018   Prix Total HT = 16.00 Euro ', 'Emplacement = 5.00 Euro - 11 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 11, '16.00'),
(134, 3, 3, 'Salle rÃ©union matin : 27 juin 2018   Prix Total HT = 12.00 Euro ', 'Emplacement = 5.00 Euro - 7 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 7, '12.00'),
(135, 3, 4, 'Salle rÃ©union matin : 26 juin 2018   Prix Total HT = 14.00 Euro ', 'Emplacement = 5.00 Euro - 9 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 9, '14.00'),
(136, 3, 5, 'Salle rÃ©union matin : 28 juin 2018   Prix Total HT = 14.00 Euro ', 'Emplacement = 5.00 Euro - 9 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 9, '14.00'),
(137, 3, 6, 'Salle rÃ©union matin : 29 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 6, '11.00'),
(138, 3, 7, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(139, 4, 1, 'Salle r&eacute;union matin : 10 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 3 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 3, '0.00'),
(140, 4, 2, 'Salle rÃ©union matin : 30 juin 2018   Prix Total HT = 16.00 Euro ', 'Emplacement = 5.00 Euro - 11 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 11, '16.00'),
(141, 4, 3, 'Salle rÃ©union matin : 27 juin 2018   Prix Total HT = 12.00 Euro ', 'Emplacement = 5.00 Euro - 7 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 7, '12.00'),
(142, 4, 4, 'Salle rÃ©union matin : 26 juin 2018   Prix Total HT = 14.00 Euro ', 'Emplacement = 5.00 Euro - 9 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 9, '14.00'),
(143, 4, 5, 'Salle rÃ©union matin : 28 juin 2018   Prix Total HT = 14.00 Euro ', 'Emplacement = 5.00 Euro - 9 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 9, '14.00'),
(144, 4, 6, 'Salle rÃ©union matin : 29 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 6, '11.00'),
(145, 4, 7, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(146, 5, 1, 'Salle r&eacute;union apr&egrave;s-midi : 10 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(147, 5, 2, 'Salle r&eacute;union apr&egrave;s-midi : 29 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 1, '0.00'),
(148, 5, 3, 'Salle rÃ©union aprÃ¨s-midi : 30 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 6, '11.00'),
(149, 5, 4, 'Salle rÃ©union aprÃ¨s-midi : 29 juin 2018   Prix Total HT = 8.00 Euro ', 'Emplacement = 5.00 Euro - 3 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 3, '8.00'),
(150, 5, 5, 'Salle rÃ©union aprÃ¨s-midi : 28 juin 2018   Prix Total HT = 13.00 Euro ', 'Emplacement = 5.00 Euro - 8 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 8, '13.00'),
(151, 5, 6, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(152, 6, 1, 'Salle r&eacute;union apr&egrave;s-midi : 10 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 1, '0.00'),
(153, 6, 2, 'Salle r&eacute;union apr&egrave;s-midi : 29 janvier 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 0.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 1, '0.00'),
(154, 6, 3, 'Salle rÃ©union aprÃ¨s-midi : 30 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 6, '11.00'),
(155, 6, 4, 'Salle rÃ©union aprÃ¨s-midi : 29 juin 2018   Prix Total HT = 8.00 Euro ', 'Emplacement = 5.00 Euro - 3 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 3, '8.00'),
(156, 6, 5, 'Salle rÃ©union aprÃ¨s-midi : 28 juin 2018   Prix Total HT = 13.00 Euro ', 'Emplacement = 5.00 Euro - 8 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 8, '13.00'),
(157, 6, 6, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(158, 7, 1, 'Salle rÃ©union soir : 30 juin 2018   Prix Total HT = 9.00 Euro ', 'Emplacement = 5.00 Euro - 4 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 4, '9.00'),
(159, 7, 2, 'Salle rÃ©union soir : 29 juin 2018   Prix Total HT = 9.00 Euro ', 'Emplacement = 5.00 Euro - 4 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 4, '9.00'),
(160, 7, 3, 'Salle rÃ©union soir : 28 juin 2018   Prix Total HT = 11.00 Euro ', 'Emplacement = 5.00 Euro - 6 personne(s) X 1.00 Euro', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '5.00', '1.00', 0, 6, '11.00'),
(161, 7, 4, '', '', 'FACT1806_00070', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(162, 1, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00071', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(163, 1, 2, '', '', 'FACT1806_00071', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(164, 1, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00072', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(165, 1, 2, '', '', 'FACT1806_00072', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(166, 1, 1, 'Zone Coworking : 28 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00073', '2018-06-25', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(167, 1, 2, '', '', 'FACT1806_00073', '2018-06-25', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(168, 1, 1, 'Zone Coworking : 29 mai 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(169, 1, 2, 'Zone Coworking : 29 mai 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(170, 1, 3, 'Zone Coworking : 23 mai 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(171, 1, 4, 'Zone Coworking : 01 février 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(172, 1, 5, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00074', '2018-06-25', 'jr.menu@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(173, 1, 6, '', '', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(174, 2, 1, 'Salle rÃ©union matin : 27 mars 2018   Prix Total HT = 7.00 Euro ', 'Emplacement = 5.00 Euro - 2 personne(s) X 1.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '5.00', '1.00', 0, 2, '7.00'),
(175, 2, 2, '', '', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(176, 3, 1, 'Salle r&eacute;union apr&egrave;s-midi : 19 mars 2018   Prix Total HT = 0.00 Euro ', 'Emplacement = 0.00 Euro - 6 personne(s) X 0.00 Euro', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '0.00', 0, 6, '0.00'),
(177, 3, 2, '', '', 'FACT1806_00074', '2018-06-25', 'Jr.menu@gmail.com ', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(178, 1, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(179, 1, 2, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(180, 1, 3, '', '', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(181, 2, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(182, 2, 2, 'Zone Coworking : 30 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(183, 2, 3, '', '', 'FACT1806_00075', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(184, 1, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(185, 1, 2, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(186, 1, 3, '', '', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(187, 2, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(188, 2, 2, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(189, 2, 3, '', '', 'FACT1806_00076', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(190, 1, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00077', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(191, 1, 2, '', '', 'FACT1806_00077', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(192, 2, 1, 'Zone Coworking : 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00077', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(193, 2, 2, '', '', 'FACT1806_00077', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(194, 1, 1, 'RÃ©servation 425 du 29 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1806_00078', '2018-06-27', 'test1', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(195, 1, 2, '', '', 'FACT1806_00078', '2018-06-27', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(196, 1, 1, 'RÃ©servation 416 du 13 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(197, 1, 2, '', '', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(198, 2, 1, 'RÃ©servation 414 du 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(199, 2, 2, '', '', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(200, 3, 1, 'RÃ©servation 417 du 20 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(201, 3, 2, '', '', 'FACT1807_00079', '2018-07-02', '28-LV', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(202, 1, 1, 'RÃ©servation 349 du 06 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(203, 1, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(204, 2, 1, 'RÃ©servation 341 du 05 avril 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(205, 2, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(206, 3, 1, 'RÃ©servation 344 du 03 mai 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(207, 3, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(208, 4, 1, 'RÃ©servation 340 du 18 avril 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(209, 4, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(210, 5, 1, 'RÃ©servation 343 du 19 avril 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(211, 5, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(212, 6, 1, 'RÃ©servation 342 du 09 avril 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(213, 6, 2, '', '', 'FACT1807_00080', '2018-07-02', 'bjolibert@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(214, 1, 1, 'RÃ©servation 339 du 13 avril 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(215, 1, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(216, 2, 1, 'RÃ©servation 352 du 06 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(217, 2, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(218, 3, 1, 'RÃ©servation 387 du 22 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(219, 3, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(220, 4, 1, 'RÃ©servation 351 du 05 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(221, 4, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(222, 5, 1, 'RÃ©servation 386 du 18 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(223, 5, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(224, 6, 1, 'RÃ©servation 350 du 07 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(225, 6, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(226, 7, 1, 'RÃ©servation 353 du 08 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(227, 7, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(228, 8, 1, 'RÃ©servation 418 du 27 juin 2018   Prix Total HT = 5.00 Euro ', 'Emplacement = 0.00 Euro - 1 personne(s) X 5.00 Euro', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '5.00', 0, 1, '5.00'),
(229, 8, 2, '', '', 'FACT1807_00081', '2018-07-02', 'commeunebrouette@gmail.com', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(230, 1, 1, 'AdhÃ©sion valable du 03/07/2018 au 03/07/2019', 'Cette adhÃ©sion donne droit Ã  une remise de 5,00 Euro', 'AD1807_00082', '2018-07-03', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(231, 1, 2, '', '', 'AD1807_00082', '2018-07-03', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(232, 1, 1, 'AdhÃ©sion valable du 03/07/2018 au 03/07/2019', 'Cette adhÃ©sion donne droit Ã  une remise de 5,00 Euro', 'AD1807_00083', '2018-07-03', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00'),
(233, 1, 2, '', '', 'AD1807_00083', '2018-07-03', 'test1', 'ET0001', '0.00', '0.00', 0, 0, '0.00');

-- --------------------------------------------------------

--
-- Structure de la table `modepaie`
--

CREATE TABLE `modepaie` (
  `MO_NUMERO` int(11) NOT NULL,
  `MO_MODEPAIE` varchar(3) NOT NULL,
  `MO_LIBELLE` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `modepaie`
--

INSERT INTO `modepaie` (`MO_NUMERO`, `MO_MODEPAIE`, `MO_LIBELLE`) VALUES
(1, 'CHQ', 'CHEQUE'),
(2, 'VIR', 'VIREMENT'),
(3, 'ESP', 'ESPECE');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `RE_NUMRESA` int(6) NOT NULL,
  `RE_USER` varchar(250) NOT NULL,
  `RE_DATE` date NOT NULL,
  `RE_JOUR` int(11) NOT NULL,
  `RE_MOIS` int(11) NOT NULL,
  `RE_ANNEE` int(11) NOT NULL,
  `RE_VALIDEE` varchar(3) NOT NULL DEFAULT 'OUI',
  `RE_FACTURE` varchar(30) NOT NULL DEFAULT 'NON',
  `RE_REFFACTURE` varchar(15) NOT NULL,
  `RE_ETABLISSEMENT` varchar(6) NOT NULL,
  `RE_ETLIBELLE` varchar(50) NOT NULL,
  `RE_ETADRESSE1` varchar(50) NOT NULL,
  `RE_ETADRESSE2` varchar(50) NOT NULL,
  `RE_CODEPOSTAL` varchar(5) NOT NULL,
  `RE_VILLE` varchar(50) NOT NULL,
  `RE_EMPLACEMENT` int(5) NOT NULL,
  `RE_LIBELLEEMPLACEMENT` varchar(50) NOT NULL,
  `RE_NBRPLACE` int(2) NOT NULL,
  `RE_RESA` varchar(3) NOT NULL DEFAULT 'NON',
  `RE_DATERESA` datetime NOT NULL,
  `RE_USERANNUL` varchar(250) NOT NULL,
  `RE_DATEANNUL` datetime NOT NULL,
  `RE_ZONE` int(7) NOT NULL,
  `RE_ZONELIBELLE` varchar(50) NOT NULL,
  `RE_TYPEZONE` varchar(30) NOT NULL,
  `RE_USERMODIF` varchar(250) NOT NULL,
  `RE_SESSIONTOKEN` varchar(250) NOT NULL,
  `RE_PRIXPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXPARPERSONNE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXTOTALRESA` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_NBRRESAMAX` int(99) NOT NULL DEFAULT '0',
  `RE_NBRPLACESMAX` int(99) NOT NULL DEFAULT '0',
  `RE_HEUREDEBUT` time NOT NULL,
  `RE_HEUREFIN` time NOT NULL,
  `RE_CODEART1` varchar(20) NOT NULL,
  `RE_LIBART1` varchar(50) NOT NULL,
  `RE_QTEART1` int(2) NOT NULL,
  `RE_PRIXART1` decimal(5,2) NOT NULL,
  `RE_CODEART2` varchar(20) NOT NULL,
  `RE_LIBART2` varchar(50) NOT NULL,
  `RE_QTEART2` int(2) NOT NULL,
  `RE_PRIXART2` decimal(5,2) NOT NULL,
  `RE_CREDITCLI` varchar(3) NOT NULL,
  `RE_JOURTYPE` varchar(15) NOT NULL,
  `RE_ARTJOURCODE` varchar(30) NOT NULL,
  `RE_ARTJOURLIB` varchar(50) NOT NULL,
  `RE_ARTJOURPRIX` decimal(5,2) NOT NULL,
  `RE_ARTMATINCODE` varchar(30) NOT NULL,
  `RE_ARTMATINLIB` varchar(50) NOT NULL,
  `RE_ARTMATINPRIX` decimal(5,2) NOT NULL,
  `RE_ARTAPMCODE` varchar(30) NOT NULL,
  `RE_ARTAPMLIB` varchar(50) NOT NULL,
  `RE_ARTAPMPRIX` decimal(5,2) NOT NULL,
  `RE_PANIER` varchar(3) DEFAULT NULL,
  `RE_TOKENPANIER` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `piece`
--

CREATE TABLE `piece` (
  `GP_NUMERO` int(11) NOT NULL,
  `GP_TYPE` varchar(20) DEFAULT NULL,
  `GP_USER` varchar(250) NOT NULL,
  `GP_USERNOM` varchar(250) NOT NULL,
  `GP_DATEPIECE` date NOT NULL,
  `GP_ETABLISSEMENT` varchar(6) NOT NULL,
  `GP_USERPRENOM` varchar(250) NOT NULL,
  `GP_USERADRESSE1` varchar(250) NOT NULL,
  `GP_USERADRESSE2` varchar(250) NOT NULL,
  `GP_USERADRESSE3` varchar(250) DEFAULT NULL,
  `GP_USERVILLE` varchar(250) NOT NULL,
  `GP_USERCODEPOSTAL` varchar(250) NOT NULL,
  `GP_USERINITIALE` varchar(5) DEFAULT NULL,
  `GP_USERID` int(10) NOT NULL,
  `GP_ETLIBELLE` varchar(50) NOT NULL,
  `GP_ETADRESSE1` varchar(100) NOT NULL,
  `GP_ETADRESSE2` varchar(100) NOT NULL,
  `GP_ETADRESSE3` varchar(100) NOT NULL,
  `GP_ETCODEPOSTAL` varchar(5) NOT NULL,
  `GP_ETVILLE` varchar(50) NOT NULL,
  `GP_USEREMAIL` varchar(250) NOT NULL,
  `GP_TOTALHT` decimal(12,2) NOT NULL,
  `GP_TOTALQTEFACT` int(5) NOT NULL,
  `GP_REFFACTURE` varchar(50) DEFAULT NULL,
  `GP_TRANSMISE` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATETRANSMISSION` date NOT NULL,
  `GP_PAYEMENT` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATEPAYEMENT` date NOT NULL,
  `GP_RELANCE` varchar(3) NOT NULL DEFAULT 'NON',
  `GP_DATERELANCE` date NOT NULL,
  `GP_NOMBRERELANCE` int(2) NOT NULL DEFAULT '0',
  `GP_REFPAYMENT` varchar(30) DEFAULT NULL,
  `GP_MODEPAIE` varchar(3) DEFAULT NULL,
  `GP_SESSIONTOKEN` varchar(250) NOT NULL,
  `GP_DATEECHEANCE` date NOT NULL,
  `GP_CREATEUR` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `piece`
--

INSERT INTO `piece` (`GP_NUMERO`, `GP_TYPE`, `GP_USER`, `GP_USERNOM`, `GP_DATEPIECE`, `GP_ETABLISSEMENT`, `GP_USERPRENOM`, `GP_USERADRESSE1`, `GP_USERADRESSE2`, `GP_USERADRESSE3`, `GP_USERVILLE`, `GP_USERCODEPOSTAL`, `GP_USERINITIALE`, `GP_USERID`, `GP_ETLIBELLE`, `GP_ETADRESSE1`, `GP_ETADRESSE2`, `GP_ETADRESSE3`, `GP_ETCODEPOSTAL`, `GP_ETVILLE`, `GP_USEREMAIL`, `GP_TOTALHT`, `GP_TOTALQTEFACT`, `GP_REFFACTURE`, `GP_TRANSMISE`, `GP_DATETRANSMISSION`, `GP_PAYEMENT`, `GP_DATEPAYEMENT`, `GP_RELANCE`, `GP_DATERELANCE`, `GP_NOMBRERELANCE`, `GP_REFPAYMENT`, `GP_MODEPAIE`, `GP_SESSIONTOKEN`, `GP_DATEECHEANCE`, `GP_CREATEUR`) VALUES
(52, NULL, 'test1', 'bWY1ekVPSXUzRmw1TWZ5Q3VvTUM2UT09Ojr/3I+MwGjr4mbmmys1li87', '2018-06-21', 'ET0001', 'MEQ4OHc3elpsUXA3YjI0SzFISStjZz09OjoMi8j84ZgmX9H6joP7ZP6B', 'VW1PZlZLdlVpTVpJUkgxSXYrZmd0Zz09OjozwJ65i/zYah9RsRrFzanu', 'R0t2eFBLY0grVFowdVBPSGJCL0V3UT09OjoJLVPltPCsSrZTxq4Y0JZ4', NULL, 'dXRXaFJsTGo3TG9zcWphYktvTDdOUT09OjpZEZCCNr9euDEv8hcccGgL', 'aUdWRVVqTW9HRFhPYlVXSHV5VGh4Zz09OjoCjN6nQTykjPKiKqIYsV3n', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'ZGp2RGtITzJoR0xGKys0Zmx4QTdhckMzdXZZNEpnTDFJSWNQS2VCeUIvQT06Oh0Or9i69JV1mUATqhuer8k=', '5.00', 9, 'FACT1806_00052', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '37k962x3ksx444k75e5d86vvs', '0000-00-00', ''),
(53, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00053', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '89m42xp36cqv2xq2j8m36ex65', '0000-00-00', ''),
(54, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00054', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'itlvry1feml34wajclfq5cknm', '2018-07-05', ''),
(55, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00055', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'jp51a1v8sy3sypatmuhypwtuu', '2018-07-25', ''),
(56, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '17.00', 2, 'FACT1806_00056', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'sp65bt779lc1yxmwi1jmvm4cf', '2018-07-25', ''),
(57, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '34.00', 3, 'FACT1806_00057', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '3434559772fl71n6rvt750191', '2018-07-25', ''),
(58, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '11.00', 1, 'FACT1806_00058', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '71ju4nt46336bq0dls70556vq', '2018-07-25', ''),
(59, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '9.00', 1, 'FACT1806_00059', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '2mj1ww2cqk87nf7e3b853k5kv', '2018-07-25', ''),
(60, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '9.00', 1, 'FACT1806_00060', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'q1l7xb2f62u82g6m5ygk6yky6', '2018-07-25', ''),
(61, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '8.00', 1, 'FACT1806_00061', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '4n3u3u5el3df1qj3b1cfdp684', '2018-07-25', ''),
(62, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '12.00', 1, 'FACT1806_00062', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'et6y9q4kvi7rg3df2dp2dsdja', '2018-07-25', ''),
(63, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '14.00', 1, 'FACT1806_00063', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '8ejk1f6845999p8c5fw6n748s', '2018-07-25', ''),
(64, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '13.00', 1, 'FACT1806_00064', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'pexi43jjfdqb5bv7946cf3206', '2018-07-25', ''),
(65, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '14.00', 1, 'FACT1806_00065', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'jri0r3r4gx7ntnw3bj7y1mnnl', '2018-07-25', ''),
(66, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '11.00', 1, 'FACT1806_00066', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'whp8pbf12huvcx5v296umk203', '2018-07-25', ''),
(67, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '11.00', 1, 'FACT1806_00067', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 't3l79t77nq3xsq95ye7j5t779', '2018-07-25', ''),
(68, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00068', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'htud71wbqi919c31ukydi7833', '2018-07-25', ''),
(69, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '16.00', 2, 'FACT1806_00069', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'x85893sc2xgdbnp9ua48b8xc2', '2018-07-25', ''),
(70, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '168.00', 27, 'FACT1806_00070', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'mbcrfx6hij7fbi23b3i3yd224', '2018-07-25', ''),
(71, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00071', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'f3m09268vlv3y9up3q1i1y83d', '2018-07-25', ''),
(72, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00072', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'wlu9k333n42jt5s7ct38nhm1l', '2018-07-25', ''),
(73, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-25', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00073', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '46tjs8g3a05nk5r702331jap7', '2018-07-25', ''),
(74, NULL, 'Jr.menu@gmail.com ', 'TmcvVzB3OE1zSDllV05QU2FjKzFQUT09Ojp+KvhADZE2aM2I8rGf+gic', '2018-06-25', 'ET0001', 'TjkvYW9UL1lqN1JlNGEyOVRkK2NvZz09OjqkahrYE2c4twVqqvOWueev', 'K1hDWnJnWlEyQzkra0VRdGhXcWQ3dGYzR2tadnpFVzdFTjV4ckhEYXVVOD06OlM/RPJ99m1HKPo5VJWKsiQ=', 'SFRHWFFaMUhLUDRCbGQ1bHJZSnlrQT09OjogcqrjALmjNXTJBWZq80ao', NULL, 'MTBaQ2JyS2dRZUM0MUs2bHpRVmhVdz09Ojri8gBBSgMxIr4dDtrv6Dmv', 'TFJBN1U3d2krbzVQeEVaVktmT255QT09OjrnZVvbKGmkBnk2uabnLMbM', NULL, 153785, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'VmNIbmVvcWNNalFlazMrTDg0M08wMTFaSGZhcWRWU0UrL09tWTNYcXdzQT06OrxTUzURB4wJZ2Q3+mkvjVI=', '32.00', 7, 'FACT1806_00074', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'xui2e62q1cm0316xq2jk6exyh', '2018-07-25', ''),
(75, NULL, 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-27', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '10.00', 2, 'FACT1806_00075', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '8kgbgb24f6u67136vxhgj5v7e', '2018-07-27', ''),
(76, 'FACTURE', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-27', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '10.00', 2, 'FACT1806_00076', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'a68u7fp8j6753blkxxb5i1n25', '2018-07-27', ''),
(77, 'FACTURE', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-27', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '10.00', 2, 'FACT1806_00077', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '7m94f374tu099u08vmg1c2l56', '2018-07-27', ''),
(78, 'FACTURE', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-06-27', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'FACT1806_00078', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '24ucnw65rrw4jq4ry964ccmb2', '2018-07-27', ''),
(79, 'FACTURE', '28-LV', 'cmlIcDRIRXY4R1FZMEtpMWkrOFhadz09Ojrvae8ZqDi2DK3TnctRTNI+', '2018-07-02', 'ET0001', 'WFhXdWxsM3RKTytleUNCK0dCRVNZZz09Ojr4pW2YlBnDLjFfgNDyjtLl', 'MkdDQnlVTlR4VkJuZ3J6ajNWVGpvZz09OjrK3PVbFqus/XDn3+WshdHH', 'R29WREEzbldGNEhoSUJhMTF1SHlRdz09OjqH6fOnQcZvDSari+SNixx0', NULL, 'UVdBeTl6ZlVuYk1WM2RaSkIwTE1oUT09OjqPMcl/938VGmD/cOo7rcsR', 'eEl2ejc3YVlWajBPSm9KblpMY2tmZz09OjqMkxVrRIropKYkNF1Gr39g', NULL, 25978101, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'c2JPVnhNdGZ2MUtPT2VYOWZGMmdRYUF6QkZCMXYwU205STZreFJLenFyYz06Ov0ImkQVyCJu/DwZH+zTEjA=', '15.00', 3, 'FACT1807_00079', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'f3cjef4h7a34n10683sbwu1bl', '2018-08-01', ''),
(80, 'FACTURE', 'bjolibert@gmail.com', 'WUNyMi9SSGFuVWlRZ2VRTHRVNDczZz09Ojo72KUSXR4Q4d+Nw6ZOBKIB', '2018-07-02', 'ET0001', 'VlArUnJBSUxBQnZxZXRmUittdHVXUT09OjoIRpfp5UsvPsv72eY6LBQK', 'MitEblBzUkZ0NGpkMDZjS2UrcUt3Zz09OjodoxKGlli4c+QhPDWI1bXJ', 'R2pGYWtCUlJzcm90YlRHd1FIYTV1dz09OjojWwVo6T+HPq4cOIrqfWy1', NULL, 'MkFieFRjb1A1NFZZVXlXbGZRRUM5Zz09OjqrYALlJRRHSiAPxQKtsadt', 'U2NqZzM3T0lxaEJBU3NRRXltWVdEUT09OjoNX31qXGNI2tVFPKTTJIuU', NULL, 38209828, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UUVvUS82U21rNGxtdnU3Y3VpN2EvRUpiZm9XR0VoajNaUkYwb3ZSaWJSbz06OqXziXr3rQATNVeFemYCkGg=', '30.00', 6, 'FACT1807_00080', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'ldpwysvv2h0tlnug5j9mlt283', '2018-08-01', ''),
(81, 'FACTURE', 'commeunebrouette@gmail.com', 'd2lFbXdWYmhZM3RyekNnQ1RYKzBFUT09OjpsmTShbfARrpgIPyCMbCGn', '2018-07-02', 'ET0001', 'SnFFWHhkNHRlbjJYbU5ETXRwRElHdz09Ojr5lQPRAq21wtIGYiMiwuJF', 'VnRMK3hBcnIrcWZISTFUd1lyaHZ1K0hJbk02UGJ4UFlIVzNTdWUwTk1HYz06OtrtDE8hSDkwAgi09o5Zw9o=', 'czhJVjN5VjNpWHJjRnZkUDVHMGNLQT09Ojo6aapGGATHs255duAtbxjP', NULL, 'ajFCaERqaUlENkpOUXBkYUJnREY0QT09OjoSRo07WIw7sTHjFDnfEAbR', 'MUVrcUJWc2ZScTJJZ3NqUFloc05KQT09OjpiE/X07SC53bPDeM91rEmO', NULL, 49044072, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UStQODdGZjRPdlpMVCtucFNkeE9HVmd5UmJKVy92bWJQZG1HR09pdzRCOD06OpwSZeDIiCIOVz4eF7l4oZQ=', '40.00', 8, 'FACT1807_00081', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '1u778282ae4dtv4959yru584h', '2018-08-01', ''),
(82, 'ADHESION', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-07-03', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '1.00', 0, 'AD1807_00082', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '5e7x0rn9xhs0c06r47wr961lh', '2018-08-02', ''),
(83, 'ADHESION', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-07-03', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', NULL, 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', NULL, 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '5.00', 1, 'AD1807_00083', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'xq47ddesj9lkmabk1u42vj86e', '2018-08-02', ''),
(148, 'ADHESION', '28-LV', 'cmlIcDRIRXY4R1FZMEtpMWkrOFhadz09Ojrvae8ZqDi2DK3TnctRTNI+', '2018-08-20', 'ET0001', 'WFhXdWxsM3RKTytleUNCK0dCRVNZZz09Ojr4pW2YlBnDLjFfgNDyjtLl', 'MkdDQnlVTlR4VkJuZ3J6ajNWVGpvZz09OjrK3PVbFqus/XDn3+WshdHH', 'R29WREEzbldGNEhoSUJhMTF1SHlRdz09OjqH6fOnQcZvDSari+SNixx0', NULL, 'UVdBeTl6ZlVuYk1WM2RaSkIwTE1oUT09OjqPMcl/938VGmD/cOo7rcsR', 'eEl2ejc3YVlWajBPSm9KblpMY2tmZz09OjqMkxVrRIropKYkNF1Gr39g', NULL, 25978101, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'c2JPVnhNdGZ2MUtPT2VYOWZGMmdRYUF6QkZCMXYwU205STZreFJLenFyYz06Ov0ImkQVyCJu/DwZH+zTEjA=', '1.00', 0, 'AD1808_00148', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, '0pij87bn46n75tyqvu4ak2ar3', '2018-09-19', ''),
(149, 'ADHESION', 'jr.menu@gmail.com', 'TmcvVzB3OE1zSDllV05QU2FjKzFQUT09Ojp+KvhADZE2aM2I8rGf+gic', '2018-08-20', 'ET0001', 'TjkvYW9UL1lqN1JlNGEyOVRkK2NvZz09OjqkahrYE2c4twVqqvOWueev', 'K1hDWnJnWlEyQzkra0VRdGhXcWQ3dGYzR2tadnpFVzdFTjV4ckhEYXVVOD06OlM/RPJ99m1HKPo5VJWKsiQ=', 'SFRHWFFaMUhLUDRCbGQ1bHJZSnlrQT09OjogcqrjALmjNXTJBWZq80ao', NULL, 'MTBaQ2JyS2dRZUM0MUs2bHpRVmhVdz09Ojri8gBBSgMxIr4dDtrv6Dmv', 'TFJBN1U3d2krbzVQeEVaVktmT255QT09OjrnZVvbKGmkBnk2uabnLMbM', NULL, 153785, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'VmNIbmVvcWNNalFlazMrTDg0M08wMTFaSGZhcWRWU0UrL09tWTNYcXdzQT06OrxTUzURB4wJZ2Q3+mkvjVI=', '1.00', 0, 'AD1808_00149', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, NULL, NULL, 'h6cvr64nr0rb7qfy27n8616jt', '2018-09-19', ''),
(150, 'CLOTURE', 'test1', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', '2018-10-26', 'ET0001', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', '', 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', '', 87781290, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', '20.00', 4, 'CLO26102018ET0001000001', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, '', '', '1cew2af8shlr805180siwg21s', '2018-11-25', 'VWlWaGt2R3ZXQSt6WVhBbTh0TG0rZz09OjrV3SK5WUGqoQL+1nMxqpIQ'),
(151, 'CLOTURE', 'C00000045', 'VHVhZFMzdmZNd3gvTk8rWURrY2FQQT09OjrOeRXJNX0H+5VRWY5air8e', '2019-01-09', 'ET0001', 'ZHkrdzYreWxpK2p5alJXcWdRKy85Zz09OjqQRpMUE23Q6nH1W6JVEUKE', '', '', '', '', '', '', 75119018, 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '', '59530', 'LE QUESNOY', 'ODhobWdwRTlNWUVqZkVta0s5eVdQRVQ2eHZNT2pZeDdKaGRIWHkwUDJhZz06OvaZ4IsQkLTdENP8Oo/+OHE=', '15.00', 2, 'CLO09012019ET0001000002', 'NON', '1900-01-01', 'NON', '1900-01-01', 'NON', '1900-01-01', 0, '', '', 'si7uu7ade5e66r67f16c4fy87', '2019-02-08', 'SDMvQjVBeG5rd0tlVksrRSs4NGdpQT09OjoDy4TR048b5xaOgZC+vRXA');

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE `promotion` (
  `PR_NUMERO` int(6) NOT NULL,
  `PR_LIBELLE` varchar(30) NOT NULL,
  `PR_ETABLISSEMENT` varchar(6) NOT NULL,
  `PR_DECLENCHEUR` varchar(10) NOT NULL,
  `PR_QTEDECLENCHEUR` int(2) NOT NULL,
  `PR_TYPEREMISE` int(1) NOT NULL COMMENT '1 - REMISE VALEUR 0 = REMISE EN POURCENTAGE',
  `PR_VALEUR` decimal(5,0) NOT NULL,
  `PR_REMISEDIRECT` int(1) NOT NULL COMMENT '0 : CRÉATION D''UN VOUCHER | 1 : REMISE DIRECT',
  `PR_ACTIVE` varchar(3) NOT NULL COMMENT 'NON OU OUI'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `promotion`
--

INSERT INTO `promotion` (`PR_NUMERO`, `PR_LIBELLE`, `PR_ETABLISSEMENT`, `PR_DECLENCHEUR`, `PR_QTEDECLENCHEUR`, `PR_TYPEREMISE`, `PR_VALEUR`, `PR_REMISEDIRECT`, `PR_ACTIVE`) VALUES
(2, 'Promo adhÃ©sion', 'ET0001', 'ADHESION', 1, 0, '5', 0, 'OUI');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `RE_NUMRESA` int(6) NOT NULL,
  `RE_USER` varchar(250) NOT NULL,
  `RE_DATE` date NOT NULL,
  `RE_JOUR` int(11) NOT NULL,
  `RE_MOIS` int(11) NOT NULL,
  `RE_ANNEE` int(11) NOT NULL,
  `RE_VALIDEE` varchar(3) NOT NULL DEFAULT 'OUI',
  `RE_FACTURE` varchar(50) NOT NULL DEFAULT 'NON',
  `RE_REFFACTURE` varchar(30) NOT NULL,
  `RE_ETABLISSEMENT` varchar(6) NOT NULL,
  `RE_ETLIBELLE` varchar(50) NOT NULL,
  `RE_ETADRESSE1` varchar(50) NOT NULL,
  `RE_ETADRESSE2` varchar(50) NOT NULL,
  `RE_CODEPOSTAL` varchar(5) NOT NULL,
  `RE_VILLE` varchar(50) NOT NULL,
  `RE_EMPLACEMENT` int(5) NOT NULL,
  `RE_LIBELLEEMPLACEMENT` varchar(50) NOT NULL,
  `RE_NBRPLACE` int(2) NOT NULL,
  `RE_RESA` varchar(3) NOT NULL DEFAULT 'NON',
  `RE_DATERESA` datetime NOT NULL,
  `RE_USERANNUL` varchar(250) NOT NULL,
  `RE_DATEANNUL` datetime NOT NULL,
  `RE_ZONE` int(7) NOT NULL,
  `RE_ZONELIBELLE` varchar(50) NOT NULL,
  `RE_TYPEZONE` varchar(30) NOT NULL,
  `RE_USERMODIF` varchar(250) NOT NULL,
  `RE_SESSIONTOKEN` varchar(250) NOT NULL,
  `RE_PRIXPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXPARPERSONNE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_PRIXTOTALRESA` decimal(6,2) NOT NULL DEFAULT '0.00',
  `RE_NBRRESAMAX` int(99) NOT NULL DEFAULT '0',
  `RE_NBRPLACESMAX` int(99) NOT NULL DEFAULT '0',
  `RE_HEUREDEBUT` time NOT NULL,
  `RE_HEUREFIN` time NOT NULL,
  `RE_CODEART1` varchar(20) NOT NULL,
  `RE_LIBART1` varchar(50) NOT NULL,
  `RE_QTEART1` int(2) NOT NULL,
  `RE_PRIXART1` decimal(5,2) NOT NULL,
  `RE_CODEART2` varchar(20) NOT NULL,
  `RE_LIBART2` varchar(50) NOT NULL,
  `RE_QTEART2` int(2) NOT NULL,
  `RE_PRIXART2` decimal(5,2) NOT NULL,
  `RE_CREDITCLI` varchar(3) NOT NULL,
  `RE_JOURTYPE` varchar(15) NOT NULL,
  `RE_ARTJOURCODE` varchar(30) NOT NULL,
  `RE_ARTJOURLIB` varchar(50) NOT NULL,
  `RE_ARTJOURPRIX` decimal(5,2) NOT NULL,
  `RE_ARTMATINCODE` varchar(30) NOT NULL,
  `RE_ARTMATINLIB` varchar(50) NOT NULL,
  `RE_ARTMATINPRIX` decimal(5,2) NOT NULL,
  `RE_ARTAPMCODE` varchar(30) NOT NULL,
  `RE_ARTAPMLIB` varchar(50) NOT NULL,
  `RE_ARTAPMPRIX` decimal(5,2) NOT NULL,
  `RE_PANIER` varchar(3) DEFAULT NULL,
  `RE_TOKENPANIER` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`RE_NUMRESA`, `RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_REFFACTURE`, `RE_ETABLISSEMENT`, `RE_ETLIBELLE`, `RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`, `RE_DATERESA`, `RE_USERANNUL`, `RE_DATEANNUL`, `RE_ZONE`, `RE_ZONELIBELLE`, `RE_TYPEZONE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`, `RE_HEUREDEBUT`, `RE_HEUREFIN`, `RE_CODEART1`, `RE_LIBART1`, `RE_QTEART1`, `RE_PRIXART1`, `RE_CODEART2`, `RE_LIBART2`, `RE_QTEART2`, `RE_PRIXART2`, `RE_CREDITCLI`, `RE_JOURTYPE`, `RE_ARTJOURCODE`, `RE_ARTJOURLIB`, `RE_ARTJOURPRIX`, `RE_ARTMATINCODE`, `RE_ARTMATINLIB`, `RE_ARTMATINPRIX`, `RE_ARTAPMCODE`, `RE_ARTAPMLIB`, `RE_ARTAPMPRIX`, `RE_PANIER`, `RE_TOKENPANIER`) VALUES
(1, 'jr.menu@gmail.com', '2017-08-11', 11, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 17:52:49', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:32:31', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(2, 'jr.menu@gmail.com', '2017-08-12', 12, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 17:53:42', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:39:22', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(3, 'jr.menu@gmail.com', '2017-08-10', 10, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 17:53:53', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:35:53', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(4, 'jr.menu@gmail.com', '2017-08-13', 13, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 21:35:40', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:41:43', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(5, 'jr.menu@gmail.com', '2017-08-10', 10, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-11 21:38:43', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:38:43', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(6, 'test1', '2017-08-08', 8, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 21:42:28', 'test1', '2017-08-12 09:20:39', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(7, 'jr.menu@gmail.com', '2017-08-09', 9, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 21:43:48', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:35:48', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(8, 'commeunebrouette@gmail.com', '2017-08-11', 11, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-11 21:59:20', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:32:02', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(9, 'louis.veyer@ateliers-art-strong.fr', '2017-08-07', 7, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-11 22:01:30', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:35:28', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(10, 'louis.veyer@ateliers-art-strong.fr', '2017-08-10', 10, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 22:37:58', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:39:09', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(11, 'brohette.nicolas@gmail.com', '2017-08-10', 10, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 19, 'Place de coworking', 1, 'OUI', '2017-08-11 22:38:12', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:38:56', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(12, 'jr.menu@gmail.com', '2017-08-10', 10, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 23, 'Place de coworking', 1, 'OUI', '2017-08-11 22:38:36', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:39:02', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(13, 'bjolibert@gmail.com', '2017-08-12', 12, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-11 22:41:30', 'louis.veyer@ateliers-art-strong.fr', '2017-08-11 22:41:37', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(14, 'louis.veyer@ateliers-art-strong.fr', '2017-08-19', 19, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:07', 'jr.menu@gmail.com', '2017-08-14 11:17:45', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(15, 'bjolibert@gmail.com', '2017-08-19', 19, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 19, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:16', 'jr.menu@gmail.com', '2017-08-14 11:17:25', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(16, 'bjolibert@gmail.com', '2017-08-19', 19, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:25', 'jr.menu@gmail.com', '2017-08-14 11:17:31', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(17, 'bjolibert@gmail.com', '2017-08-19', 19, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 23, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:32', 'jr.menu@gmail.com', '2017-08-14 11:17:36', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(18, 'bjolibert@gmail.com', '2017-08-19', 19, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 24, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:41', 'jr.menu@gmail.com', '2017-08-14 11:17:41', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(19, 'bjolibert@gmail.com', '2017-08-20', 20, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-12 09:28:59', 'jr.menu@gmail.com', '2017-08-14 11:17:52', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(20, 'test1', '2017-08-18', 18, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-12 20:52:44', 'jr.menu@gmail.com', '2017-08-14 11:17:19', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(21, 'test1', '2017-08-09', 9, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-12 20:53:01', 'jr.menu@gmail.com', '2017-08-14 08:20:32', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(22, 'test1', '2017-08-20', 20, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-12 21:45:02', 'test1', '2017-08-12 21:51:40', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(23, 'brohette.nicolas@gmail.com', '2017-08-14', 14, 8, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 10:26:39', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(24, 'test1', '2017-08-17', 17, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 24, 'Place de coworking', 1, 'OUI', '2017-08-16 15:32:10', 'jr.menu@gmail.com', '2017-08-21 12:41:28', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(25, 'bjolibert@gmail.com', '2017-05-05', 5, 5, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:52:45', 'jr.menu@gmail.com', '2017-08-16 15:52:53', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(26, 'bjolibert@gmail.com', '2017-05-12', 12, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:01', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(27, 'bjolibert@gmail.com', '2017-05-18', 18, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:10', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(28, 'bjolibert@gmail.com', '2017-05-19', 19, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:17', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(29, 'bjolibert@gmail.com', '2017-05-30', 30, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:27', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(30, 'bjolibert@gmail.com', '2017-06-02', 2, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(31, 'bjolibert@gmail.com', '2017-06-13', 13, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:50', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(32, 'bjolibert@gmail.com', '2017-06-14', 14, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:53:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(33, 'bjolibert@gmail.com', '2017-06-27', 27, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:54:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(34, 'bjolibert@gmail.com', '2017-07-26', 26, 7, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:54:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(35, 'bjolibert@gmail.com', '2017-07-27', 27, 7, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:54:31', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(36, 'brohette.nicolas@gmail.com', '2017-01-03', 3, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(37, 'brohette.nicolas@gmail.com', '2017-01-04', 4, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(38, 'brohette.nicolas@gmail.com', '2017-01-06', 6, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:17', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(39, 'brohette.nicolas@gmail.com', '2017-01-09', 9, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:28', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(40, 'brohette.nicolas@gmail.com', '2017-01-10', 10, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:36', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(41, 'brohette.nicolas@gmail.com', '2017-01-12', 12, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:44', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(42, 'brohette.nicolas@gmail.com', '2017-01-16', 16, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:55:53', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(43, 'brohette.nicolas@gmail.com', '2017-01-17', 17, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(44, 'brohette.nicolas@gmail.com', '2017-01-23', 23, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:10', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(45, 'brohette.nicolas@gmail.com', '2017-01-26', 26, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:20', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(46, 'brohette.nicolas@gmail.com', '2017-01-30', 30, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:28', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(47, 'brohette.nicolas@gmail.com', '2017-02-02', 2, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:39', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(48, 'brohette.nicolas@gmail.com', '2017-02-20', 20, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:47', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(49, 'brohette.nicolas@gmail.com', '2017-03-21', 21, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:56:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(50, 'commeunebrouette@gmail.com', '2017-01-03', 3, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:57:38', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(51, 'commeunebrouette@gmail.com', '2017-01-04', 4, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:57:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(52, 'commeunebrouette@gmail.com', '2017-01-06', 6, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:57:58', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(53, 'commeunebrouette@gmail.com', '2017-01-09', 9, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:58:13', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(54, 'commeunebrouette@gmail.com', '2017-01-10', 10, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:58:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(55, 'commeunebrouette@gmail.com', '2017-01-11', 11, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:58:32', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(56, 'commeunebrouette@gmail.com', '2017-01-12', 12, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:58:39', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(57, 'commeunebrouette@gmail.com', '2017-01-17', 17, 1, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 15:58:49', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(58, 'commeunebrouette@gmail.com', '2017-02-14', 14, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:03', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(59, 'commeunebrouette@gmail.com', '2017-02-13', 13, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:11', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(60, 'commeunebrouette@gmail.com', '2017-02-16', 16, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:20', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(61, 'commeunebrouette@gmail.com', '2017-02-17', 17, 2, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(62, 'commeunebrouette@gmail.com', '2017-03-02', 2, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:43', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(63, 'commeunebrouette@gmail.com', '2017-03-03', 3, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 15:59:51', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(64, 'commeunebrouette@gmail.com', '2017-03-06', 6, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(65, 'commeunebrouette@gmail.com', '2017-03-07', 7, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:10', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(66, 'commeunebrouette@gmail.com', '2017-03-13', 13, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:19', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(67, 'commeunebrouette@gmail.com', '2017-03-14', 14, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(68, 'commeunebrouette@gmail.com', '2017-03-15', 15, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:33', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(69, 'commeunebrouette@gmail.com', '2017-03-16', 16, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(70, 'commeunebrouette@gmail.com', '2017-03-17', 17, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(71, 'commeunebrouette@gmail.com', '2017-03-23', 23, 3, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:00:56', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(72, 'commeunebrouette@gmail.com', '2017-04-18', 18, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(73, 'commeunebrouette@gmail.com', '2017-04-19', 19, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:17', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(74, 'commeunebrouette@gmail.com', '2017-04-24', 24, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:24', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(75, 'commeunebrouette@gmail.com', '2017-04-25', 25, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:32', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(76, 'commeunebrouette@gmail.com', '2017-04-26', 26, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:40', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(77, 'commeunebrouette@gmail.com', '2017-05-15', 15, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:51', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(78, 'commeunebrouette@gmail.com', '2017-05-16', 16, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:01:58', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(79, 'commeunebrouette@gmail.com', '2017-05-17', 17, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:06', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(80, 'commeunebrouette@gmail.com', '2017-05-18', 18, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:14', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(81, 'commeunebrouette@gmail.com', '2017-05-24', 24, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:23', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(82, 'commeunebrouette@gmail.com', '2017-05-30', 30, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:31', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(83, 'commeunebrouette@gmail.com', '2017-05-31', 31, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:38', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(84, 'commeunebrouette@gmail.com', '2017-06-01', 1, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:02:53', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(85, 'commeunebrouette@gmail.com', '2017-06-02', 2, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:01', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(86, 'commeunebrouette@gmail.com', '2017-06-06', 6, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(87, 'commeunebrouette@gmail.com', '2017-06-07', 7, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:17', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(88, 'commeunebrouette@gmail.com', '2017-06-08', 8, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(89, 'commeunebrouette@gmail.com', '2017-06-09', 9, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:32', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(90, 'commeunebrouette@gmail.com', '2017-06-12', 12, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(91, 'commeunebrouette@gmail.com', '2017-06-13', 13, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(92, 'commeunebrouette@gmail.com', '2017-06-14', 14, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:03:56', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(93, 'commeunebrouette@gmail.com', '2017-06-19', 19, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:04:05', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(94, 'commeunebrouette@gmail.com', '2017-06-20', 20, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:04:11', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(95, 'commeunebrouette@gmail.com', '2017-06-21', 21, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:04:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(96, 'commeunebrouette@gmail.com', '2017-06-27', 27, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:04:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(97, 'mathilde@transversaldesign.fr', '2017-05-23', 23, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:05:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(98, 'mathilde@transversaldesign.fr', '2017-05-25', 25, 5, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:05:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(99, 'mathilde@transversaldesign.fr', '2017-06-21', 21, 6, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-08-16 16:05:45', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(100, 'nbernier@centre-britannique.org', '2017-04-14', 14, 4, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-16 16:06:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(101, 'test1', '2017-08-15', 15, 8, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 21, 'Salle de rÃ©union aprÃ¨s-midi', 6, 'OUI', '2017-08-16 21:57:05', 'jr.menu@gmail.com', '2017-08-28 18:33:23', 2, 'Salle rÃ©union 1', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(102, 'bjolibert@gmail.com', '2017-08-28', 28, 8, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-08-28 14:41:51', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(103, 'test1', '2017-09-02', 2, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-01 13:27:34', 'jr.menu@gmail.com', '2017-10-13 16:20:01', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(104, 'test1', '2017-09-02', 2, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 21, 'Salle de rÃ©union aprÃ¨s-midi', 9, 'OUI', '2017-09-01 13:28:00', 'jr.menu@gmail.com', '2017-10-13 16:19:16', 2, 'Salle rÃ©union 1', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(105, 'test1', '2017-09-02', 2, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-09-01 13:28:43', 'jr.menu@gmail.com', '2017-10-13 16:20:11', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(106, 'bjolibert@gmail.com', '2017-09-02', 2, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 19, 'Place de coworking', 1, 'OUI', '2017-09-01 13:31:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL);
INSERT INTO `reservation` (`RE_NUMRESA`, `RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_REFFACTURE`, `RE_ETABLISSEMENT`, `RE_ETLIBELLE`, `RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`, `RE_DATERESA`, `RE_USERANNUL`, `RE_DATEANNUL`, `RE_ZONE`, `RE_ZONELIBELLE`, `RE_TYPEZONE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`, `RE_HEUREDEBUT`, `RE_HEUREFIN`, `RE_CODEART1`, `RE_LIBART1`, `RE_QTEART1`, `RE_PRIXART1`, `RE_CODEART2`, `RE_LIBART2`, `RE_QTEART2`, `RE_PRIXART2`, `RE_CREDITCLI`, `RE_JOURTYPE`, `RE_ARTJOURCODE`, `RE_ARTJOURLIB`, `RE_ARTJOURPRIX`, `RE_ARTMATINCODE`, `RE_ARTMATINLIB`, `RE_ARTMATINPRIX`, `RE_ARTAPMCODE`, `RE_ARTAPMLIB`, `RE_ARTAPMPRIX`, `RE_PANIER`, `RE_TOKENPANIER`) VALUES
(107, 'bjolibert@gmail.com', '2017-09-06', 6, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-06 15:57:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(108, 'bjolibert@gmail.com', '2017-09-08', 8, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-08 09:51:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(109, 'commeunebrouette@gmail.com', '2017-09-11', 11, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-11 14:50:17', 'commeunebrouette@gmail.com', '2017-09-20 17:03:17', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(110, 'bjolibert@gmail.com', '2017-09-12', 12, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-12 07:37:52', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(111, 'commeunebrouette@gmail.com', '2017-09-18', 18, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-20 13:29:01', 'commeunebrouette@gmail.com', '2017-09-20 15:06:46', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(112, 'commeunebrouette@gmail.com', '2017-09-19', 19, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-20 13:29:10', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(113, 'commeunebrouette@gmail.com', '2017-09-20', 20, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-09-20 13:29:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(114, 'commeunebrouette@gmail.com', '2017-09-18', 18, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-20 15:06:55', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(115, 'commeunebrouette@gmail.com', '2017-09-11', 11, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-09-20 17:03:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(116, 'commeunebrouette@gmail.com', '2017-09-24', 24, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-09-21 10:06:27', 'jr.menu@gmail.com', '2017-09-21 10:06:42', 1, 'Zone Coworking', '', 'jr.menu@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(117, 'commeunebrouette@gmail.com', '2017-09-21', 21, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-22 09:21:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'commeunebrouette@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(118, 'bjolibert@gmail.com', '2017-09-22', 22, 9, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-22 09:30:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(119, 'test1', '2017-09-07', 7, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-09-27 21:13:09', 'jr.menu@gmail.com', '2017-10-13 16:21:02', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(120, 'mathilde@transversaldesign.fr', '2017-10-05', 5, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-10-02 23:20:45', 'mathilde@transversaldesign.fr', '2017-10-02 23:20:52', 1, 'Zone Coworking', '', 'mathilde@transversaldesign.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(121, 'test1', '2017-10-04', 4, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-10-03 12:20:46', 'jr.menu@gmail.com', '2017-10-13 16:16:40', 1, 'Zone Coworking', '', 'test1', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(122, 'bjolibert@gmail.com', '2017-10-05', 5, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 17, 'Place de coworking', 1, 'OUI', '2017-10-05 09:14:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(123, 'hugo@hdwebmarketing.fr', '2017-10-05', 5, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 18, 'Place de coworking', 1, 'OUI', '2017-10-05 11:27:42', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'hugo@hdwebmarketing.fr', '', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(124, 'test1', '2017-09-16', 16, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 21:37:28', 'jr.menu@gmail.com', '2017-10-13 16:20:34', 1, 'Zone Coworking', '', 'test1', '1mg5sbtvnl1254b49p37ixjc2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(125, 'test1', '2017-09-16', 16, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 21:37:28', 'jr.menu@gmail.com', '2017-10-13 16:20:39', 1, 'Zone Coworking', '', 'test1', '1mg5sbtvnl1254b49p37ixjc2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(126, 'test1', '2017-09-16', 16, 9, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 21:39:41', 'jr.menu@gmail.com', '2017-10-13 16:20:45', 1, 'Zone Coworking', '', 'test1', '334pdsfevtg1hc8d2fqipl846', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(127, 'test1', '2017-10-13', 13, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:34', 'test1', '2017-10-06 22:11:42', 1, 'Zone Coworking', '', 'test1', '84lc82x79kvxa2npi407r3a06', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(128, 'test1', '2017-10-13', 13, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:34', 'test1', '2017-10-06 22:11:42', 1, 'Zone Coworking', '', 'test1', '84lc82x79kvxa2npi407r3a06', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(129, 'test1', '2017-10-13', 13, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:34', 'test1', '2017-10-06 22:11:42', 1, 'Zone Coworking', '', 'test1', '84lc82x79kvxa2npi407r3a06', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(130, 'test1', '2017-10-06', 6, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:47', 'jr.menu@gmail.com', '2017-10-13 16:16:57', 1, 'Zone Coworking', '', 'test1', 'f353es5f64a595r8hh45961kd', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(131, 'test1', '2017-10-06', 6, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:47', 'jr.menu@gmail.com', '2017-10-13 16:17:03', 1, 'Zone Coworking', '', 'test1', 'f353es5f64a595r8hh45961kd', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(132, 'test1', '2017-10-06', 6, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:47', 'jr.menu@gmail.com', '2017-10-13 16:17:10', 1, 'Zone Coworking', '', 'test1', 'f353es5f64a595r8hh45961kd', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(133, 'test1', '2017-10-06', 6, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:11:47', 'jr.menu@gmail.com', '2017-10-13 16:17:15', 1, 'Zone Coworking', '', 'test1', 'f353es5f64a595r8hh45961kd', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(134, 'Test1', '2017-10-13', 13, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-06 22:13:37', 'test1', '2017-10-06 22:13:56', 1, 'Zone Coworking', '', 'Test1', 'yty9yd9kqplef8byp7b358x4m', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(135, 'test1', '2017-10-07', 7, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle réunion matin', 8, 'OUI', '2017-10-06 23:31:33', 'test1', '2017-10-06 23:31:38', 2, 'Salle réunion matin', '', 'test1', '2wpq3fsnh792j61q58vxw88cb', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(136, 'test1', '2017-10-14', 14, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle réunion matin', 6, 'OUI', '2017-10-06 23:31:45', 'test1', '2017-10-09 11:44:34', 2, 'Salle réunion matin', '', 'test1', '58etsg2ql9nu3w2hfavjtf054', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(137, 'test1', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 11:45:43', 'test1', '2017-10-09 11:46:00', 1, 'Zone Coworking', '', 'test1', 'ty45pl91sj4b4vpl6u2jmhmql', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(138, 'test1', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 11:45:43', 'test1', '2017-10-09 11:46:00', 1, 'Zone Coworking', '', 'test1', 'ty45pl91sj4b4vpl6u2jmhmql', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(139, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:48:30', 'Jr.menu@gmail.com ', '2017-10-09 16:48:48', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'chknu6f37mw52xj8s458494b8', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(140, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:48:48', 'Jr.menu@gmail.com ', '2017-10-09 16:48:57', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'cl4g0t2ke53i51j8tgb820k7p', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(141, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:48:48', 'Jr.menu@gmail.com ', '2017-10-09 16:48:57', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'cl4g0t2ke53i51j8tgb820k7p', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(142, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:48:57', 'Jr.menu@gmail.com ', '2017-10-09 16:49:04', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'b4nif9j31rt6ger837nm348i0', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(143, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:49:04', 'Jr.menu@gmail.com ', '2017-10-09 16:49:44', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'vup1h6l1hqf9542c35m52l7cj', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(144, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:49:04', 'Jr.menu@gmail.com ', '2017-10-09 16:49:54', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'vup1h6l1hqf9542c35m52l7cj', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(145, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:49:44', 'Jr.menu@gmail.com ', '2017-10-09 16:49:54', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'wd6s6tk4mkrukpit5abnt84xi', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(146, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:49:44', 'Jr.menu@gmail.com ', '2017-10-09 16:49:54', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'wd6s6tk4mkrukpit5abnt84xi', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(147, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:50:01', 'Jr.menu@gmail.com ', '2017-10-09 16:50:09', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'f8fnpscla9i52c38gwxy8shn4', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(148, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:50:09', 'Jr.menu@gmail.com ', '2017-10-09 16:50:23', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', '75tew8xjs9h13lvc429tc6vi3', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(149, 'Jr.menu@gmail.com ', '2017-10-20', 20, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-09 16:50:23', 'Jr.menu@gmail.com ', '2017-10-09 16:50:58', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', '7mln45x65vm821sdx11qx5e91', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(150, 'Test1', '2017-10-25', 25, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-10 15:37:43', 'test1', '2017-10-11 10:27:39', 1, 'Zone Coworking', '', 'Test1', '5qcy1t99u4g7g26we1vf51ys6', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(151, 'Test1', '2017-11-22', 22, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-10 15:37:56', 'jr.menu@gmail.com', '2017-11-20 21:51:42', 1, 'Zone Coworking', '', 'Test1', '9g9ds412h72c82wl4u93xnip2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(152, 'Test1', '2017-11-22', 22, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-10 15:37:56', 'jr.menu@gmail.com', '2017-11-20 21:51:47', 1, 'Zone Coworking', '', 'Test1', '9g9ds412h72c82wl4u93xnip2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(153, 'Test1', '2017-11-22', 22, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-10 15:37:56', 'jr.menu@gmail.com', '2017-11-20 21:51:52', 1, 'Zone Coworking', '', 'Test1', '9g9ds412h72c82wl4u93xnip2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(154, 'Test1', '2017-11-22', 22, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-10 15:37:56', 'jr.menu@gmail.com', '2017-11-20 21:51:58', 1, 'Zone Coworking', '', 'Test1', '9g9ds412h72c82wl4u93xnip2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(155, 'test1', '2017-10-05', 5, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 12, 'OUI', '2017-10-11 15:33:28', 'jr.menu@gmail.com', '2017-10-13 16:17:55', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'test1', 'a52p7glv69pte11gd5t43qval', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(156, 'test1', '2017-10-12', 12, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 12, 'OUI', '2017-10-11 15:34:03', 'test1', '2017-10-11 15:35:22', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'test1', 'c435akdcr99llnn1il17rtnpe', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(157, 'bjolibert@gmail.com', '2017-10-11', 11, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-11 17:02:05', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'r5ei36m55gy1pq8am1ji47tdk', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(158, 'bjolibert@gmail.com', '2017-10-10', 10, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-11 17:02:12', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'nw916f7l2pdlx22psfb2tca4m', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(159, 'test1', '2017-10-18', 18, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-12 20:47:18', 'jr.menu@gmail.com', '2017-10-13 16:14:03', 1, 'Zone Coworking', '', 'test1', 'cx7165lwj88vv3m95ewj3q9pc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(160, 'test1', '2017-10-18', 18, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-12 20:47:18', 'jr.menu@gmail.com', '2017-10-13 16:14:10', 1, 'Zone Coworking', '', 'test1', 'cx7165lwj88vv3m95ewj3q9pc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(161, 'louis.veyer@ateliers-art-strong.fr', '2017-10-02', 2, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:34', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'k5n8556j06557fvcp53y0b3j6', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(162, 'louis.veyer@ateliers-art-strong.fr', '2017-10-03', 3, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:37', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '5nic4p90hxlj9x35u8175674v', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(163, 'louis.veyer@ateliers-art-strong.fr', '2017-10-04', 4, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '2byslmx483c3x1xb5pdhe2ia9', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(164, 'louis.veyer@ateliers-art-strong.fr', '2017-10-05', 5, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:45', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '2fg9d52tu8bph7i4f5x3se6n1', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(165, 'louis.veyer@ateliers-art-strong.fr', '2017-10-06', 6, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'vjwcdh225t7se7702cs90946v', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(166, 'louis.veyer@ateliers-art-strong.fr', '2017-10-09', 9, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:54', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'wn6a1856y4i42ei13875am330', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(167, 'louis.veyer@ateliers-art-strong.fr', '2017-10-10', 10, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:13:58', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '98f34hisxsg4sn3l27rcjkg25', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(168, 'louis.veyer@ateliers-art-strong.fr', '2017-10-11', 11, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:14:02', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'gxr9e08m9g95y2i9d7dxtncpb', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(169, 'louis.veyer@ateliers-art-strong.fr', '2017-10-13', 13, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 16:14:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'edv6m6217wk58p73m4u3in254', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(170, 'test1', '2017-10-21', 21, 10, 2017, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 17:45:39', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(171, 'test1', '2017-10-21', 21, 10, 2017, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-13 17:45:39', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(172, 'louis.veyer@ateliers-art-strong.fr', '2017-10-16', 16, 10, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-14 01:12:39', 'louis.veyer@ateliers-art-strong.fr', '2017-10-14 01:12:47', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'bhp8ldjwq2f789x84u92drf27', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(173, 'j.cappel@pnr-scarpe-escaut.fr', '2017-10-25', 25, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-17 17:24:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'j.cappel@pnr-scarpe-escaut.fr', '0542lhdaqmw8aqpx538yval8l', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(174, 'bjolibert@gmail.com', '2017-10-20', 20, 10, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-20 08:12:02', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 's8ntf6hbn496ny294ih3j6vd3', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(175, 'test1', '2017-10-27', 27, 10, 2017, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-24 15:11:30', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(176, 'test1', '2017-10-27', 27, 10, 2017, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-24 15:11:30', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(177, 'test1', '2017-10-27', 27, 10, 2017, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-10-24 15:11:56', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(178, 'antoine.demailly@lessensdugout.fr', '2017-12-01', 1, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-06 13:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'antoine.demailly@lessensdugout.fr', '6bwrdxl14uhbf4f6y1qg38lcs', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(179, 'antoine.demailly@lessensdugout.fr', '2017-12-01', 1, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-06 13:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'antoine.demailly@lessensdugout.fr', '6bwrdxl14uhbf4f6y1qg38lcs', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(180, 'antoine.demailly@lessensdugout.fr', '2017-12-01', 1, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-06 13:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'antoine.demailly@lessensdugout.fr', '6bwrdxl14uhbf4f6y1qg38lcs', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(181, 'antoine.demailly@lessensdugout.fr', '2017-12-01', 1, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-06 13:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'antoine.demailly@lessensdugout.fr', '6bwrdxl14uhbf4f6y1qg38lcs', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(182, 'antoine.demailly@lessensdugout.fr', '2017-12-01', 1, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-06 13:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'antoine.demailly@lessensdugout.fr', '6bwrdxl14uhbf4f6y1qg38lcs', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(183, 'test1', '2017-11-08', 8, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-13 20:51:30', 'jr.menu@gmail.com', '2017-11-20 21:51:18', 1, 'Zone Coworking', '', 'test1', 'yuhli217cp4dm1318fmqh31uw', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(184, 'test1', '2017-11-08', 8, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-13 20:51:30', 'jr.menu@gmail.com', '2017-11-20 21:51:24', 1, 'Zone Coworking', '', 'test1', 'yuhli217cp4dm1318fmqh31uw', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(185, 'test1', '2017-11-08', 8, 11, 2017, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-13 20:51:30', 'jr.menu@gmail.com', '2017-11-20 21:51:12', 1, 'Zone Coworking', '', 'test1', 'yuhli217cp4dm1318fmqh31uw', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(186, 'bjolibert@gmail.com', '2017-11-15', 15, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-16 13:29:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'k42201xx4wtmqfbcufvk16h4c', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(187, 'bjolibert@gmail.com', '2017-11-16', 16, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-16 13:29:34', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'npe5e8w7313q7thlgf89s9cby', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(188, 'bjolibert@gmail.com', '2017-11-17', 17, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-17 10:05:01', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 't40s94s3j4b9e757ax4gb5c01', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(189, 'bjolibert@gmail.com', '2017-11-20', 20, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-20 09:52:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'de66xrbgadxmqqfixi2p642uc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(190, 'annie.tacquet@yahoo.fr', '2017-11-27', 27, 11, 2017, 'OUI', 'OUI', 'FACT_1_19062018', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 8, 'OUI', '2017-11-20 21:46:28', '', '0000-00-00 00:00:00', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'annie.tacquet@yahoo.fr', 'e06icv9q5ag86w32pqkcl5y0g', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(191, 'martial.dubois28@outlook.fr', '2017-11-27', 27, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-27 10:07:04', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'f8d0fky647f3g8hkrue1u42g7', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(192, 'martial.dubois28@outlook.fr', '2017-11-28', 28, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-28 10:13:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'gg2l28qus97km007df7h3924k', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(193, 'martial.dubois28@outlook.fr', '2017-11-30', 30, 11, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-11-28 14:46:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 't6sid755g53r7f4fmfu7luw8u', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(194, 'martial.dubois28@outlook.fr', '2017-12-04', 4, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-04 09:56:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '0361i5t9hr7p2j6exy94vs426', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(195, 'martial.dubois28@outlook.fr', '2017-12-05', 5, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-04 09:56:16', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '9yvut1m0ede8wgph26p0ie4mv', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(196, 'bjolibert@gmail.com', '2017-12-05', 5, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-06 09:56:23', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '6t2paferqi3dichf7cqsuavq7', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(197, 'martial.dubois28@outlook.fr', '2017-12-07', 7, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-07 09:52:34', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'hj1knkk4ahd7g67j8m84ek57e', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(198, 'bjolibert@gmail.com', '2017-12-07', 7, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-08 10:39:30', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'am9k27cb6w11d2t04lmlipb51', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(199, 'bjolibert@gmail.com', '2017-12-14', 14, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-14 18:19:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'ua534tcd7ahs1g71y8ckrcq19', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(200, 'bjolibert@gmail.com', '2017-12-15', 15, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-15 07:34:20', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '8p6xr3y82ki7e763wv45t220n', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(201, 'martial.dubois28@outlook.fr', '2017-12-18', 18, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-18 12:08:36', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'hwir0jt197raw2skbv0fldg2w', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(202, 'martial.dubois28@outlook.fr', '2017-12-19', 19, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-18 12:08:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'p8081e1u9l8fp7yx27u18b12y', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(203, 'martial.dubois28@outlook.fr', '2017-12-22', 22, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-18 12:08:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '467xhjy08br759hn67i9vdsni', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(204, 'louis.veyer@ateliers-art-strong.fr', '2017-12-18', 18, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-26 19:58:12', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '6erf1ti6wtpjpy9155g18p11f', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(205, 'martial.dubois28@outlook.fr', '2017-12-27', 27, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-27 10:51:19', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'bi2yhmwl13s169tej2ys25sgk', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(206, 'martial.dubois28@outlook.fr', '2017-12-28', 28, 12, 2017, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2017-12-27 10:51:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '84p80rc97c3vynw34skccekyr', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(207, 'bjolibert@gmail.com', '2018-01-03', 3, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-03 08:46:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '8633a0r975cil06t8h67jlf55', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(208, 'martial.dubois28@outlook.fr', '2018-01-08', 8, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-08 09:35:29', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'cd0xgxkys1im6j23r5es9dvk8', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL);
INSERT INTO `reservation` (`RE_NUMRESA`, `RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_REFFACTURE`, `RE_ETABLISSEMENT`, `RE_ETLIBELLE`, `RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`, `RE_DATERESA`, `RE_USERANNUL`, `RE_DATEANNUL`, `RE_ZONE`, `RE_ZONELIBELLE`, `RE_TYPEZONE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`, `RE_HEUREDEBUT`, `RE_HEUREFIN`, `RE_CODEART1`, `RE_LIBART1`, `RE_QTEART1`, `RE_PRIXART1`, `RE_CODEART2`, `RE_LIBART2`, `RE_QTEART2`, `RE_PRIXART2`, `RE_CREDITCLI`, `RE_JOURTYPE`, `RE_ARTJOURCODE`, `RE_ARTJOURLIB`, `RE_ARTJOURPRIX`, `RE_ARTMATINCODE`, `RE_ARTMATINLIB`, `RE_ARTMATINPRIX`, `RE_ARTAPMCODE`, `RE_ARTAPMLIB`, `RE_ARTAPMPRIX`, `RE_PANIER`, `RE_TOKENPANIER`) VALUES
(209, 'test1', '2018-01-10', 10, 1, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle r&eacute;union matin', 3, 'OUI', '2018-01-09 09:55:46', '', '0000-00-00 00:00:00', 2, 'Salle r&eacute;union matin', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(210, 'test1', '2018-01-10', 10, 1, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 1, 'OUI', '2018-01-09 09:56:00', '', '0000-00-00 00:00:00', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(211, 'test1', '2018-01-29', 29, 1, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 1, 'OUI', '2018-01-09 10:00:27', '', '0000-00-00 00:00:00', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(212, 'martial.dubois28@outlook.fr', '2018-01-10', 10, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-10 12:08:57', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'pc1jbv44vf9p518c2km654iji', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(213, 'martial.dubois28@outlook.fr', '2018-01-11', 11, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-11 09:20:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'b1h06c888rhai5l36ppqmk708', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(214, 'bjolibert@gmail.com', '2018-01-04', 4, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-12 07:31:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'wdx8vc4iyt8196mlsix7wah35', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(215, 'bjolibert@gmail.com', '2018-01-05', 5, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-12 07:31:14', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '37t81xqdh89ai0hbgq25d87bn', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(216, 'bjolibert@gmail.com', '2018-01-12', 12, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-12 07:31:23', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '7023gu19eq9h28ep0vf8u8p1d', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(217, 'bjolibert@gmail.com', '2018-01-09', 9, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-12 07:31:35', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'lpsy2512966n735c9cl38six2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(218, 'martial.dubois28@outlook.fr', '2018-01-15', 15, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-15 10:14:04', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'n9s30qjm75car86sky97x0965', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(219, 'bjolibert@gmail.com', '2018-01-18', 18, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-18 17:08:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '3igxb9r974huk9sxug256x7c3', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(220, 'bjolibert@gmail.com', '2018-01-19', 19, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-19 11:08:52', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '6t989b87s6767u1f0wpnd1hg2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(221, 'annie.tacquet@yahoo.fr', '2018-02-06', 6, 2, 2018, 'OUI', 'OUI', 'FACT_1_19062018', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 3, 'OUI', '2018-01-20 17:12:26', '', '0000-00-00 00:00:00', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'annie.tacquet@yahoo.fr', 'y8y7392pk3pbu21scen3f7t3b', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(222, 'martial.dubois28@outlook.fr', '2018-01-22', 22, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-22 09:34:24', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '6lv8eu5sq35487w9b4kkf14v1', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(223, 'bjolibert@gmail.com', '2018-01-25', 25, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-25 08:44:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'rn9wrc8p63u6h5r95il6ej9gb', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(224, 'bjolibert@gmail.com', '2018-01-26', 26, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-26 08:17:40', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 's6s676e3y6b2431bu5y92u3am', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(225, 'martial.dubois28@outlook.fr', '2018-01-29', 29, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-01-29 09:25:59', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', 'gf017vmk5p1wc7k297vskw5w7', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(226, 'louis.veyer@ateliers-art-strong.fr', '2018-01-29', 29, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-02 16:30:00', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '3ti4j727j73j9lx14596v39lr', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(227, 'louis.veyer@ateliers-art-strong.fr', '2018-02-02', 2, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-02 16:30:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'aqxx68j5thg0esl7tm13p1x23', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(228, 'bjolibert@gmail.com', '2018-02-02', 2, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-04 19:21:43', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '4l9j38515cppt969kseu05ski', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(229, 'bjolibert@gmail.com', '2018-02-09', 9, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-09 07:56:36', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 't8im7v7990ut18hg2tr9fb54w', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(230, 'louis.veyer@ateliers-art-strong.fr', '2018-02-07', 7, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-10 19:31:36', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', 'c3qcb05v2ar3pd32pfqffm1c1', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(231, 'louis.veyer@ateliers-art-strong.fr', '2018-02-12', 12, 2, 2018, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-10 19:31:51', 'louis.veyer@ateliers-art-strong.fr', '2018-02-10 19:32:01', 1, 'Zone Coworking', '', 'louis.veyer@ateliers-art-strong.fr', '30ch6vy8bptfag73v2gg3cx7t', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(232, 'bjolibert@gmail.com', '2018-02-13', 13, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-13 09:27:19', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '847g3iq36pu6jwc9k3j6775rt', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(233, 'j.cappel@pnr-scarpe-escaut.fr', '2018-02-14', 14, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-14 17:04:41', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'j.cappel@pnr-scarpe-escaut.fr', 'csvgcyfsl17kisy17dk74pg97', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(234, 'j.cappel@pnr-scarpe-escaut.fr', '2018-01-17', 17, 1, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-14 17:04:56', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'j.cappel@pnr-scarpe-escaut.fr', 'lb9g44c9jkcn2q9gspudwss86', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(235, 'j.cappel@pnr-scarpe-escaut.fr', '2018-02-28', 28, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-14 17:05:09', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'j.cappel@pnr-scarpe-escaut.fr', 'g7i2sdt1l8sc83g2g2wwavf9r', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(236, 'bjolibert@gmail.com', '2018-02-19', 19, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-19 11:15:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '5iaq4i65vkq7ivi72i7aji8cq', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(237, 'bjolibert@gmail.com', '2018-02-20', 20, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-20 14:08:37', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 't3jcmjq28v6ef28lb851hup2v', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(238, 'nbernier@centre-britannique.org', '2018-02-22', 22, 2, 2018, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-21 08:49:34', 'nbernier@centre-britannique.org', '2018-02-21 08:49:40', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', '2hx13j2l4qp82cmdf66uk9t22', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(239, 'bjolibert@gmail.com', '2018-02-23', 23, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-02-23 15:32:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '95fxvg4vgu78d3a895uf74294', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(240, 'Test1', '2018-03-09', 9, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-02 10:05:08', 'Jr.menu@gmail.com ', '2018-03-02 10:25:52', 1, 'Zone Coworking', '', 'Test1', '4j24e7th20987w1ba750s8987', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(241, 'nbernier@centre-britannique.org', '2018-03-12', 12, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-05 19:51:45', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', 'f9uw8we3944gu7m0n27178ffl', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(242, 'nbernier@centre-britannique.org', '2018-03-19', 19, 3, 2018, 'NON', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-05 19:53:22', 'nbernier@centre-britannique.org', '2018-03-18 14:17:29', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', 'apq6a46p7hn03x07qv6t24lkt', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(243, 'nbernier@centre-britannique.org', '2018-03-26', 26, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-05 19:53:31', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', 'wg5y94am69ndf0w1c8se6scx2', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(244, 'coeur d etoile', '2018-02-19', 19, 2, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle r&eacute;union soir', 10, 'OUI', '2018-03-06 08:23:27', '', '0000-00-00 00:00:00', 4, 'Salle r&eacute;union soir', '', 'coeur d etoile', 'ycn84wirsx9c3860puj1265r9', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(245, 'Jr.menu@gmail.com ', '2018-03-19', 19, 3, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle r&eacute;union apr&egrave;s-midi', 6, 'OUI', '2018-03-10 04:08:45', '', '0000-00-00 00:00:00', 3, 'Salle r&eacute;union apr&egrave;s-midi', '', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(246, 'Jr.menu@gmail.com ', '2018-03-12', 12, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-11 21:16:41', 'Jr.menu@gmail.com ', '2018-03-11 21:16:49', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'hgwcvd4yuvqq7xus3g9aa9u7f', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(247, 'nbernier@centre-britannique.org', '2018-03-15', 15, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-12 09:50:19', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', 'cfgh8dyvj9wf6i4d6dyjs5ipv', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(248, 'martial.dubois28@outlook.fr', '2018-03-12', 12, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-12 10:14:46', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '8d5f3kb5anp7hvftbnive2dqm', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(249, 'coeur d etoile', '2018-03-19', 19, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle r&eacute;union soir', 10, 'OUI', '2018-03-12 21:11:30', '', '0000-00-00 00:00:00', 4, 'Salle r&eacute;union soir', '', 'coeur d etoile', 'g4ife4tu1ridk0kk38g24vhu1', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(250, 'bjolibert@gmail.com', '2018-03-13', 13, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-14 08:44:43', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'ftcg8v2ak714ejmp383b8c862', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(251, 'bjolibert@gmail.com', '2018-03-14', 14, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-14 08:44:48', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '867t1gpusq1v019f1d53f23pm', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(252, 'bjolibert@gmail.com', '2018-03-15', 15, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-15 11:17:53', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', '4ek6uiyu84797i1k25ynbatmr', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(253, 'martial.dubois28@outlook.fr', '2018-03-19', 19, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-19 09:41:04', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'martial.dubois28@outlook.fr', '387v9b6a8g03pb29bx4mb2913', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(254, 'bjolibert@gmail.com', '2018-03-23', 23, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-23 14:45:57', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'bjolibert@gmail.com', 'ljs6n4b98mim1qav56na2fwk5', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(255, 'Jr.menu@gmail.com ', '2018-03-27', 27, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle r&eacute;union matin', 2, 'OUI', '2018-03-23 17:36:02', 'Jr.menu@gmail.com ', '2018-03-28 23:50:17', 2, 'Salle r&eacute;union matin', '', 'Jr.menu@gmail.com ', '1216uex6vyebe7s7uk506si66', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(256, 'perm1', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:06', 'perm1', '2018-03-28 21:41:03', 5, 'Zone Coworking', '', 'perm1', 'l81u2mq52me42f9sy6byi9tsc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(257, 'perm1', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:06', 'perm1', '2018-03-28 21:40:56', 5, 'Zone Coworking', '', 'perm1', 'l81u2mq52me42f9sy6byi9tsc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(258, 'perm1', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:06', 'perm1', '2018-03-28 21:41:12', 5, 'Zone Coworking', '', 'perm1', 'l81u2mq52me42f9sy6byi9tsc', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(259, 'perm1', '2018-03-14', 14, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:40', 'perm1', '2018-03-23 20:15:44', 5, 'Zone Coworking', '', 'perm1', 'dxau80eglsavhn7852ib5kf7k', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(260, 'perm1', '2018-03-14', 14, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:41', 'perm1', '2018-03-23 20:16:17', 5, 'Zone Coworking', '', 'perm1', 'dxau80eglsavhn7852ib5kf7k', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(261, 'perm1', '2018-03-14', 14, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:41', 'perm1', '2018-03-23 20:16:37', 5, 'Zone Coworking', '', 'perm1', 'dxau80eglsavhn7852ib5kf7k', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(262, 'perm1', '2018-03-14', 14, 3, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-03-23 20:03:41', 'perm1', '2018-03-23 20:16:44', 5, 'Zone Coworking', '', 'perm1', 'dxau80eglsavhn7852ib5kf7k', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(263, 'nbernier@centre-britannique.org', '2018-03-29', 29, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-26 20:34:25', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', '', 'nbernier@centre-britannique.org', '77qbcy7fe3ufrlb4wk17rnypg', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(264, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 19:54:21', 'Jr.menu@gmail.com ', '2018-03-28 20:50:59', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'xy343v194e2q442m4r5v708ac', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(265, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 19:56:07', 'Jr.menu@gmail.com ', '2018-03-28 20:51:48', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'p3dk8k071ew24r66g37vu42tn', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(266, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 20:51:54', 'Jr.menu@gmail.com ', '2018-03-28 20:52:06', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'ffmf2jd34bmrkw712h7ay5wn4', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(267, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 20:52:11', 'Jr.menu@gmail.com ', '2018-03-28 20:52:16', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'j459h340f6599vwnwbf7isfc6', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(268, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 20:52:11', 'Jr.menu@gmail.com ', '2018-03-28 20:52:16', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'j459h340f6599vwnwbf7isfc6', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(269, 'Jr.menu@gmail.com ', '2018-03-23', 23, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 20:53:28', 'Jr.menu@gmail.com ', '2018-03-28 23:50:00', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', '599ejw6f6h5i093whtj13y723', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(270, 'Jr.menu@gmail.com ', '2018-03-01', 1, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 20:53:56', 'Jr.menu@gmail.com ', '2018-03-28 23:49:23', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', '9k053sg2hi14t5xrav2q0py6c', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(271, 'Jr.menu@gmail.com ', '2018-03-01', 1, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:10:05', 'Jr.menu@gmail.com ', '2018-03-28 23:49:29', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'x5pd3pdxk45389d3106pfu7lq', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(272, 'Jr.menu@gmail.com ', '2018-03-01', 1, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:10:05', 'Jr.menu@gmail.com ', '2018-03-28 23:49:35', 1, 'Zone Coworking', '', 'Jr.menu@gmail.com ', 'x5pd3pdxk45389d3106pfu7lq', '0.00', '0.00', '0.00', 0, 0, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(273, 'Jr.menu@gmail.com ', '2018-03-01', 1, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:17:08', 'Jr.menu@gmail.com ', '2018-03-28 23:49:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 's379e5ismuee1cn0ipttlle45', '5.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(274, 'Jr.menu@gmail.com ', '2018-03-01', 1, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:17:45', 'Jr.menu@gmail.com ', '2018-03-28 23:49:47', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '9x5bc6052cuv390eh0k22g513', '5.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(275, 'Jr.menu@gmail.com ', '2018-03-28', 28, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:19:04', 'Jr.menu@gmail.com ', '2018-03-28 22:47:58', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '425604att92gejtxaeb4692fj', '5.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(276, 'Jr.menu@gmail.com ', '2018-03-28', 28, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:19:04', 'Jr.menu@gmail.com ', '2018-03-28 22:47:58', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '425604att92gejtxaeb4692fj', '5.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(277, 'Jr.menu@gmail.com ', '2018-03-28', 28, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:19:04', 'Jr.menu@gmail.com ', '2018-03-28 22:47:58', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '425604att92gejtxaeb4692fj', '5.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(278, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 6, 'OUI', '2018-03-28 21:20:04', 'Jr.menu@gmail.com ', '2018-03-28 21:26:30', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'evpfg17yvw3e6nn4c34mux7l1', '5.00', '1.00', '0.00', 1, 12, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(279, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 6, 'OUI', '2018-03-28 21:26:39', 'Jr.menu@gmail.com ', '2018-03-28 21:28:56', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'j9tg3n1193gvmk4xag94v5p3g', '5.00', '1.00', '9.99', 1, 12, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(280, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:27:42', 'Jr.menu@gmail.com ', '2018-03-28 21:28:47', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'wuci65b5042h8iq25c9v131s8', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(281, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 21:27:42', 'Jr.menu@gmail.com ', '2018-03-28 21:28:47', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'wuci65b5042h8iq25c9v131s8', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(282, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 7, 'OUI', '2018-03-28 21:29:03', 'Jr.menu@gmail.com ', '2018-03-28 21:31:07', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', '5d8xki6hks2tkqt1n84g7mbuw', '5.00', '1.00', '9.99', 1, 12, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(283, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 9, 'OUI', '2018-03-28 21:31:13', 'Jr.menu@gmail.com ', '2018-03-28 22:27:24', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'w6e4n8pf9rapixsjx6rl014fn', '5.00', '1.00', '14.00', 1, 12, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(284, 'Jr.menu@gmail.com ', '2018-03-28', 28, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:48:03', 'Jr.menu@gmail.com ', '2018-03-28 22:48:48', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'gtf2842mde7ubwd8q5f64hh68', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(285, 'Jr.menu@gmail.com ', '2018-03-28', 28, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:48:20', 'Jr.menu@gmail.com ', '2018-03-28 22:48:47', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'cwi491fhgh8vu4b7yhw3ceqs3', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(286, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:48:53', 'Jr.menu@gmail.com ', '2018-03-28 22:53:54', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'tf4934dhd8pmec8n0y489rcg5', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(287, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:48:53', 'Jr.menu@gmail.com ', '2018-03-28 22:53:54', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'tf4934dhd8pmec8n0y489rcg5', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(288, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:54:01', 'Jr.menu@gmail.com ', '2018-03-28 23:04:59', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'b55h5nb45fc2b2wy3c53x3qm4', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(289, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 22:56:44', 'Jr.menu@gmail.com ', '2018-03-28 23:04:59', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'x9by1hhx7hdq8mjp383qebww1', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(290, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 23:00:47', 'Jr.menu@gmail.com ', '2018-03-28 23:04:59', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 't4ara1j0lm467q39nd637b6f5', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(291, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 23:04:59', 'Jr.menu@gmail.com ', '2018-03-28 23:49:05', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'y74v4l35mv2k85ic85jv6yj54', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(292, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 23:05:50', 'Jr.menu@gmail.com ', '2018-03-28 23:49:05', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '8s5mg7ng2qryxlg65fe56fl2j', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(293, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 23:08:43', 'Jr.menu@gmail.com ', '2018-03-28 23:49:05', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '3lj9tf410r69t4ss2gbl289jj', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(294, 'Jr.menu@gmail.com ', '2018-03-29', 29, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-28 23:11:32', 'Jr.menu@gmail.com ', '2018-03-28 23:49:05', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'b1u5ray3vgc391rv37d28mjew', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(295, 'Jr.menu@gmail.com ', '2018-03-27', 27, 3, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 2, 'OUI', '2018-03-28 23:50:44', '', '0000-00-00 00:00:00', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '5.00', '1.00', '7.00', 1, 12, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(296, 'bjolibert@gmail.com', '2018-03-29', 29, 3, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 07:35:03', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'd31w0ytbpmjm9tv9v3lddfc60', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(297, 'Jr.menu@gmail.com ', '2018-03-31', 31, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 22:49:20', 'Jr.menu@gmail.com ', '2018-03-29 23:25:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'p06vynjgh3hhmxjyru48ylwrj', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(298, 'Jr.menu@gmail.com ', '2018-03-31', 31, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 22:51:17', 'Jr.menu@gmail.com ', '2018-03-29 23:25:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '54q02x3nx429mdcypunx37hk5', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(299, 'Jr.menu@gmail.com ', '2018-03-31', 31, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:05:24', 'Jr.menu@gmail.com ', '2018-03-29 23:25:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'k5c0tc0h4u9m4g1ta5na1x496', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(300, 'Jr.menu@gmail.com ', '2018-03-31', 31, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:06:41', 'Jr.menu@gmail.com ', '2018-03-29 23:25:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '58y47nhak44upg4cau7ta3rvu', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(301, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:15:30', 'Jr.menu@gmail.com ', '2018-03-29 23:25:33', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'ji3gsdg21emn6trmbt5f45a5i', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(302, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:22:50', 'Jr.menu@gmail.com ', '2018-03-29 23:25:33', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '4f58jj5kgw943cans202m5609', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(303, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:23:49', 'Jr.menu@gmail.com ', '2018-03-29 23:25:33', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'av82iu4wr1ak7e4nr2tra534w', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(304, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:25:01', 'Jr.menu@gmail.com ', '2018-03-29 23:25:33', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '5i8yj357cm764p288u7bvex39', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(305, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:40:41', 'Jr.menu@gmail.com ', '2018-03-29 23:46:09', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'i65jd622j0l3428mllmgn7rlg', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(306, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:41:38', 'Jr.menu@gmail.com ', '2018-03-29 23:46:09', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '3g553h8txssbp87n3n3i6gpb7', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(307, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:43:50', 'Jr.menu@gmail.com ', '2018-03-29 23:46:09', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '8s5m8c811kd7f9w2di87j428y', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(308, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:44:31', 'Jr.menu@gmail.com ', '2018-03-29 23:46:09', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'eqxb0t4p4v13wt4c12r0k35fu', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL);
INSERT INTO `reservation` (`RE_NUMRESA`, `RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_REFFACTURE`, `RE_ETABLISSEMENT`, `RE_ETLIBELLE`, `RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`, `RE_DATERESA`, `RE_USERANNUL`, `RE_DATEANNUL`, `RE_ZONE`, `RE_ZONELIBELLE`, `RE_TYPEZONE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`, `RE_HEUREDEBUT`, `RE_HEUREFIN`, `RE_CODEART1`, `RE_LIBART1`, `RE_QTEART1`, `RE_PRIXART1`, `RE_CODEART2`, `RE_LIBART2`, `RE_QTEART2`, `RE_PRIXART2`, `RE_CREDITCLI`, `RE_JOURTYPE`, `RE_ARTJOURCODE`, `RE_ARTJOURLIB`, `RE_ARTJOURPRIX`, `RE_ARTMATINCODE`, `RE_ARTMATINLIB`, `RE_ARTMATINPRIX`, `RE_ARTAPMCODE`, `RE_ARTAPMLIB`, `RE_ARTAPMPRIX`, `RE_PANIER`, `RE_TOKENPANIER`) VALUES
(309, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:45:57', 'Jr.menu@gmail.com ', '2018-03-29 23:46:09', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1mcc155av5j8f4n39n7n93mud', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(310, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:47:23', 'Jr.menu@gmail.com ', '2018-03-29 23:54:24', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 's8glv04kfb6l11ircr6ly5eui', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(311, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:49:10', 'Jr.menu@gmail.com ', '2018-03-29 23:54:25', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'an04l6tyu8bfj547k2k68841h', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(312, 'Jr.menu@gmail.com ', '2018-03-30', 30, 3, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-03-29 23:51:55', 'Jr.menu@gmail.com ', '2018-03-29 23:54:25', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'fsb6919827mp6mw0qrf2qckvw', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(313, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 13:07:12', 'Jr.menu@gmail.com ', '2018-04-02 21:16:56', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'gkq66mrpbn6ci28h5461vjgc8', '0.00', '5.00', '5.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(314, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 18:56:34', 'Jr.menu@gmail.com ', '2018-04-02 19:20:16', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'g7vn4ycvch9jjg2t7k80s60jw', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(315, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 18:57:16', 'Jr.menu@gmail.com ', '2018-04-02 19:04:30', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '46tfi46elr83m9v9g58w7picu', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(316, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 18:59:51', 'Jr.menu@gmail.com ', '2018-04-02 19:20:16', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '46tfi46elr83m9v9g58w7picu', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(317, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 19:04:35', 'Jr.menu@gmail.com ', '2018-04-02 19:20:16', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '744x31ny47aj6w3d67d62axn8', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(318, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 19:06:08', 'Jr.menu@gmail.com ', '2018-04-02 19:20:16', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'rbu9v82k645gs9r8ms67xlqaj', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(319, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 19:20:22', 'Jr.menu@gmail.com ', '2018-04-02 21:16:56', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '46lu6sy8i0q5h0p8acrmirp7y', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(320, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 19:56:09', 'Jr.menu@gmail.com ', '2018-04-02 21:16:56', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'h71694t772c8ecp8wbnnn6wb1', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(321, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 19:57:58', 'Jr.menu@gmail.com ', '2018-04-02 21:16:56', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1les8pyt4bh8d7956i1i9t98b', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(322, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 20:02:17', 'Jr.menu@gmail.com ', '2018-04-02 21:16:56', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'aal8ihnmfpirvn3121dx4r0i4', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(323, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:17:02', 'Jr.menu@gmail.com ', '2018-04-02 22:04:53', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1g36tgpt41hjj9r31w3erq2gf', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(324, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:19:53', 'Jr.menu@gmail.com ', '2018-04-02 22:05:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1881h3a4a2s543k91yj1r2kt2', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(325, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:21:41', 'Jr.menu@gmail.com ', '2018-04-02 22:05:08', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'u78s6s5p5bi1ucv2141lhbj76', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(326, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:25:21', 'Jr.menu@gmail.com ', '2018-04-02 22:05:14', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'i7scqksilhqgd8ssk1i704um3', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(327, 'Jr.menu@gmail.com ', '2018-04-04', 4, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:26:52', 'Jr.menu@gmail.com ', '2018-04-02 22:06:16', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'c5w5b4rc6qli98wk4q4d08l56', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(328, 'Jr.menu@gmail.com ', '2018-04-04', 4, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:38:40', 'Jr.menu@gmail.com ', '2018-04-02 22:06:16', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '9csjyl5d56jb5tkc70443u27y', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(329, 'Jr.menu@gmail.com ', '2018-04-04', 4, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:40:40', 'Jr.menu@gmail.com ', '2018-04-02 22:06:16', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 's8tdmc3ds6n8na4db98llg6t3', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(330, 'Jr.menu@gmail.com ', '2018-04-04', 4, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 21:54:57', 'Jr.menu@gmail.com ', '2018-04-02 22:06:16', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'f66jay1xlb5buhmw4f0502b2w', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(331, 'Jr.menu@gmail.com ', '2018-04-19', 19, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 22:20:17', 'Jr.menu@gmail.com ', '2018-04-02 22:29:26', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '10aneauye8pq2w5514ttv1siv', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(332, 'Jr.menu@gmail.com ', '2018-04-19', 19, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 22:20:17', 'Jr.menu@gmail.com ', '2018-04-02 22:29:26', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '10aneauye8pq2w5514ttv1siv', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(333, 'Jr.menu@gmail.com ', '2018-04-19', 19, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 22:20:17', 'Jr.menu@gmail.com ', '2018-04-02 22:29:26', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '10aneauye8pq2w5514ttv1siv', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(334, 'Jr.menu@gmail.com ', '2018-04-19', 19, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 22:20:17', 'Jr.menu@gmail.com ', '2018-04-02 22:29:26', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '10aneauye8pq2w5514ttv1siv', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(335, 'Jr.menu@gmail.com ', '2018-04-19', 19, 4, 2018, 'NON', 'NON', '', 'ET0002', 'JE TRAVAILLE AU VERT', '350 Rue du Moulin', '', '59246', ' Mons-en-Pévèle', 5, 'Zone Coworking', 1, 'OUI', '2018-04-02 22:20:17', 'Jr.menu@gmail.com ', '2018-04-02 22:29:26', 5, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '10aneauye8pq2w5514ttv1siv', '0.00', '0.00', '0.00', 5, 1, '00:00:00', '00:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(336, 'Jr.menu@gmail.com ', '2018-04-12', 12, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-02 23:40:58', 'Jr.menu@gmail.com ', '2018-04-02 23:45:54', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '30vse4tp4x3d3s1lmbpimkmw6', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(337, 'Jr.menu@gmail.com ', '2018-04-11', 11, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-03 14:36:19', 'Jr.menu@gmail.com ', '2018-04-04 22:01:32', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '7sxr2xp8y6a4176rn30449g7j', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(338, 'Jr.menu@gmail.com ', '2018-04-12', 12, 4, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-04 22:01:09', 'Jr.menu@gmail.com ', '2018-04-04 22:01:38', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'rt6q971qt43vni06lq15iqdca', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(339, 'commeunebrouette@gmail.com', '2018-04-13', 13, 4, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-10 10:31:11', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'commeunebrouette@gmail.com', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(340, 'bjolibert@gmail.com', '2018-04-18', 18, 4, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-04-18 18:49:38', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(341, 'bjolibert@gmail.com', '2018-04-05', 5, 4, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-03 09:30:47', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(342, 'bjolibert@gmail.com', '2018-04-09', 9, 4, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-03 09:31:08', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(343, 'bjolibert@gmail.com', '2018-04-19', 19, 4, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-03 09:31:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(344, 'bjolibert@gmail.com', '2018-05-03', 3, 5, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-03 09:31:49', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(345, 'Jr.menu@gmail.com ', '2018-05-29', 29, 5, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-28 09:39:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(346, 'Jr.menu@gmail.com ', '2018-05-29', 29, 5, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-28 09:40:47', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(347, 'Jr.menu@gmail.com ', '2018-05-23', 23, 5, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-28 10:07:26', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(348, 'Jr.menu@gmail.com ', '2018-02-01', 1, 2, 2018, 'OUI', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-05-28 10:08:45', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(349, 'bjolibert@gmail.com', '2018-06-06', 6, 6, 2018, 'OUI', 'OUI', 'FACT1807_00080', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-06 15:45:52', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'ldpwysvv2h0tlnug5j9mlt283', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(350, 'commeunebrouette@gmail.com', '2018-06-07', 7, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-07 09:30:37', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'commeunebrouette@gmail.com', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(351, 'commeunebrouette@gmail.com', '2018-06-05', 5, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-07 09:30:54', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'commeunebrouette@gmail.com', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(352, 'commeunebrouette@gmail.com', '2018-06-06', 6, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-07 09:31:04', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'commeunebrouette@gmail.com', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(353, 'commeunebrouette@gmail.com', '2018-06-08', 8, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-07 09:31:10', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'commeunebrouette@gmail.com', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(354, 'nbernier@centre-britannique.org', '2018-06-11', 11, 6, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-11 16:51:28', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'nbernier@centre-britannique.org', 'kxf4x2ex16736r9xk7u77dn5d', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(355, 'Jr.menu@gmail.com ', '2018-06-15', 15, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-12 16:54:34', 'test1', '2018-06-18 13:57:44', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '854u53r3kdap4f449i5hgm624', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(356, 'Jr.menu@gmail.com ', '2018-06-15', 15, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-12 16:54:34', 'Jr.menu@gmail.com ', '2018-06-21 09:15:45', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '854u53r3kdap4f449i5hgm624', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(357, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 09:24:12', 'test1', '2018-06-14 15:07:55', 1, 'Zone Coworking', 'COWORKING', 'test1', 'jpc9c22ai143w6v44pr5u66h3', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(358, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 09:43:47', 'test1', '2018-06-14 15:07:55', 1, 'Zone Coworking', 'COWORKING', 'test1', 'nn6sn88hajk9wr40je7uy82pg', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(359, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 09:47:53', 'test1', '2018-06-14 15:07:55', 1, 'Zone Coworking', 'COWORKING', 'test1', 'yd5cvhn833sg9xa219ec68vk2', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(360, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 10:22:17', 'test1', '2018-06-14 15:07:55', 1, 'Zone Coworking', 'COWORKING', 'test1', 'hj7nnuwsyi85t6w5367i96qu8', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(361, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 10:24:01', 'test1', '2018-06-14 15:07:55', 1, 'Zone Coworking', 'COWORKING', 'test1', '858apj5iav10js4rctidvnl0v', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(362, 'test1', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 10:25:30', 'test1', '2018-06-14 15:07:48', 1, 'Zone Coworking', 'COWORKING', 'test1', '2t1212chemhthlevagcrj9wv3', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(363, 'test1', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 14:04:45', 'test1', '2018-06-14 15:07:48', 1, 'Zone Coworking', 'COWORKING', 'test1', 'p435s93h7ve9caby770k6f6wd', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(364, 'Jr.menu@gmail.com ', '2018-06-16', 16, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 14:16:10', 'Jr.menu@gmail.com ', '2018-06-21 09:15:50', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'buq8cwr8s16795w11bw3lx849', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(365, 'Jr.menu@gmail.com ', '2018-06-16', 16, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 15:09:38', 'Jr.menu@gmail.com ', '2018-06-14 15:09:57', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1hnesd4u9885p0l52y655fehg', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(366, 'test1', '2018-06-06', 6, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 15:20:30', 'Jr.menu@gmail.com ', '2018-06-26 22:04:32', 1, 'Zone Coworking', 'COWORKING', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(367, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 15:20:52', 'test1', '2018-06-14 16:04:56', 1, 'Zone Coworking', 'COWORKING', 'test1', '3egjn35u73747hddmt77x0li5', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(368, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:05:41', 'test1', '2018-06-14 16:11:58', 1, 'Zone Coworking', 'COWORKING', 'test1', 'i5qhmv03h46wl6kjb0103617d', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(369, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:12:46', 'test1', '2018-06-14 16:12:51', 1, 'Zone Coworking', 'COWORKING', 'test1', '63yb3c9vnt54hjry6472c3tig', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(370, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:17:35', 'test1', '2018-06-14 16:18:14', 1, 'Zone Coworking', 'COWORKING', 'test1', 'te89n68554hjhu98283hf3be0', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(371, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:19:41', 'test1', '2018-06-14 16:36:55', 1, 'Zone Coworking', 'COWORKING', 'test1', 'hj3269qx5y57uj3831qp0gi9l', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(372, 'Jr.menu@gmail.com ', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:28:11', 'Jr.menu@gmail.com ', '2018-06-21 09:15:58', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'du13le8xugq182uc7rmvxbglg', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(373, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:37:03', 'test1', '2018-06-14 16:55:29', 1, 'Zone Coworking', 'COWORKING', 'test1', 'k3gg9d73f2pcb9nsxgwxwiwwy', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(374, 'Jr.menu@gmail.com ', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:38:57', 'Jr.menu@gmail.com ', '2018-06-14 16:39:46', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'w574w2ib5c979xt4jpmryet85', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(375, 'Jr.menu@gmail.com ', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:42:00', 'Jr.menu@gmail.com ', '2018-06-14 16:42:42', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'yc4t1i114w40b86av85jmx1l9', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(376, 'Jr.menu@gmail.com ', '2018-06-23', 23, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:46:09', 'Jr.menu@gmail.com ', '2018-06-14 16:46:57', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1s5pi8nmsy7tvb222x2782d9y', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(377, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 16:56:02', 'test1', '2018-06-14 16:56:48', 1, 'Zone Coworking', 'COWORKING', 'test1', 'c4dsu2yahcs6jr3c1l4k2c682', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(378, 'test1', '2018-06-22', 22, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 17:03:44', 'test1', '2018-06-14 17:06:04', 1, 'Zone Coworking', 'COWORKING', 'test1', 'fw2fteh3if1jc603d6ev98pjm', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(379, 'test1', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 17:06:56', 'test1', '2018-06-14 17:07:15', 1, 'Zone Coworking', 'COWORKING', 'test1', '48d5sydam41wiv1c50lrbe7bv', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(380, 'test1', '2018-06-21', 21, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-14 17:08:26', 'test1', '2018-06-14 17:08:48', 1, 'Zone Coworking', 'COWORKING', 'test1', 'u10gnvib6ht96889p41232a75', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(381, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-15 09:53:06', 'test1', '2018-06-15 09:56:22', 1, 'Zone Coworking', 'COWORKING', 'test1', '8w3uq081f47uc4d8r29il149y', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(382, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-15 10:31:16', 'test1', '2018-06-15 11:27:17', 1, 'Zone Coworking', 'COWORKING', 'test1', '3b9wdla65nntv49sm1d78s58p', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(383, 'bjolibert@gmail.com', '2018-06-15', 15, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-19 09:03:25', 'Jr.menu@gmail.com ', '2018-06-21 09:15:29', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(384, 'bjolibert@gmail.com', '2018-06-15', 15, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-19 09:03:25', 'Jr.menu@gmail.com ', '2018-06-21 09:15:37', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(385, 'test1', '2018-06-20', 20, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-20 09:33:31', 'test1', '2018-06-20 09:34:35', 1, 'Zone Coworking', 'COWORKING', 'test1', 'tr201pit498dqg33t62tn874y', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(386, 'commeunebrouette@gmail.com', '2018-06-18', 18, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-21 09:16:13', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(387, 'commeunebrouette@gmail.com', '2018-06-22', 22, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-22 22:50:27', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(388, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 11:13:13', 'test1', '2018-06-26 22:01:35', 1, 'Zone Coworking', 'COWORKING', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(389, 'test1', '2018-06-30', 30, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 14:57:00', 'test1', '2018-06-26 22:02:17', 1, 'Zone Coworking', 'COWORKING', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(390, 'test1', '2018-06-28', 28, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 15:07:41', 'test1', '2018-06-26 22:01:24', 1, 'Zone Coworking', 'COWORKING', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(391, 'test1', '2018-06-30', 30, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 16:39:11', 'test1', '2018-06-26 22:02:17', 1, 'Zone Coworking', 'COWORKING', 'test1', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(392, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'NON', 'FACT1806_00056', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 7, 'OUI', '2018-06-25 16:39:20', 'Jr.menu@gmail.com ', '2018-06-25 19:46:24', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'test1', 'sp65bt779lc1yxmwi1jmvm4cf', '5.00', '1.00', '12.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(393, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 18:08:55', 'test1', '2018-06-26 22:01:35', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(394, 'test1', '2018-06-28', 28, 6, 2018, 'NON', 'NON', 'FACT1806_00057', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 8, 'OUI', '2018-06-25 18:09:12', 'Jr.menu@gmail.com ', '2018-06-25 19:44:53', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', '3434559772fl71n6rvt750191', '5.00', '1.00', '13.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(395, 'test1', '2018-06-30', 30, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 11, 'OUI', '2018-06-25 18:09:25', 'Jr.menu@gmail.com ', '2018-06-26 22:03:19', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '16.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(396, 'test1', '2018-06-30', 30, 6, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle rÃ©union aprÃ¨s-midi', 6, 'OUI', '2018-06-25 18:23:41', '', '1900-01-01 00:00:00', 3, 'Salle rÃ©union aprÃ¨s-midi', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '11.00', 1, 12, '14:00:00', '17:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(397, 'test1', '2018-06-30', 30, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle rÃ©union soir', 4, 'OUI', '2018-06-25 18:26:10', 'Jr.menu@gmail.com ', '2018-06-28 09:02:24', 4, 'Salle rÃ©union soir', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '9.00', 1, 12, '17:00:00', '20:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(398, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle rÃ©union soir', 4, 'OUI', '2018-06-25 18:36:31', 'Jr.menu@gmail.com ', '2018-06-28 09:02:19', 4, 'Salle rÃ©union soir', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '9.00', 1, 12, '17:00:00', '20:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(399, 'test1', '2018-06-29', 29, 6, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle rÃ©union aprÃ¨s-midi', 3, 'OUI', '2018-06-25 18:37:40', '', '1900-01-01 00:00:00', 3, 'Salle rÃ©union aprÃ¨s-midi', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '8.00', 1, 12, '14:00:00', '17:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(400, 'test1', '2018-06-27', 27, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 7, 'OUI', '2018-06-25 18:46:04', 'Jr.menu@gmail.com ', '2018-06-26 22:03:40', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '12.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(401, 'test1', '2018-06-26', 26, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 9, 'OUI', '2018-06-25 18:50:43', 'Jr.menu@gmail.com ', '2018-06-26 22:03:46', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '14.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(402, 'test1', '2018-06-28', 28, 6, 2018, 'OUI', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle rÃ©union aprÃ¨s-midi', 8, 'OUI', '2018-06-25 19:39:48', '', '1900-01-01 00:00:00', 3, 'Salle rÃ©union aprÃ¨s-midi', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '13.00', 1, 12, '14:00:00', '17:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(403, '-1', '2018-06-27', 27, 6, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 3, 'Salle rÃ©union aprÃ¨s-midi', 10, 'OUI', '2018-06-25 19:40:51', '', '1900-01-01 00:00:00', 3, 'Salle rÃ©union aprÃ¨s-midi', 'SALLE REUNION', 'Jr.menu@gmail.com ', '', '5.00', '1.00', '15.00', 1, 12, '14:00:00', '17:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(404, 'test1', '2018-06-28', 28, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 9, 'OUI', '2018-06-25 19:45:03', 'Jr.menu@gmail.com ', '2018-06-26 22:03:33', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '14.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(405, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'NON', 'FACT1806_00066', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 6, 'OUI', '2018-06-25 19:46:34', 'Jr.menu@gmail.com ', '2018-06-25 19:47:39', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'whp8pbf12huvcx5v296umk203', '5.00', '1.00', '11.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(406, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 6, 'OUI', '2018-06-25 19:47:48', 'Jr.menu@gmail.com ', '2018-06-26 22:03:26', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '11.00', 1, 12, '09:00:00', '12:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(407, 'test1', '2018-06-27', 27, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 20:37:03', 'test1', '2018-06-26 22:01:18', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL);
INSERT INTO `reservation` (`RE_NUMRESA`, `RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_REFFACTURE`, `RE_ETABLISSEMENT`, `RE_ETLIBELLE`, `RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`, `RE_DATERESA`, `RE_USERANNUL`, `RE_DATEANNUL`, `RE_ZONE`, `RE_ZONELIBELLE`, `RE_TYPEZONE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`, `RE_HEUREDEBUT`, `RE_HEUREFIN`, `RE_CODEART1`, `RE_LIBART1`, `RE_QTEART1`, `RE_PRIXART1`, `RE_CODEART2`, `RE_LIBART2`, `RE_QTEART2`, `RE_PRIXART2`, `RE_CREDITCLI`, `RE_JOURTYPE`, `RE_ARTJOURCODE`, `RE_ARTJOURLIB`, `RE_ARTJOURPRIX`, `RE_ARTMATINCODE`, `RE_ARTMATINLIB`, `RE_ARTMATINPRIX`, `RE_ARTAPMCODE`, `RE_ARTAPMLIB`, `RE_ARTAPMPRIX`, `RE_PANIER`, `RE_TOKENPANIER`) VALUES
(408, 'test1', '2018-06-28', 28, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle rÃ©union soir', 6, 'OUI', '2018-06-25 20:41:44', 'Jr.menu@gmail.com ', '2018-06-28 09:02:13', 4, 'Salle rÃ©union soir', 'SALLE REUNION', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '5.00', '1.00', '11.00', 1, 12, '17:00:00', '20:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(409, 'test1', '2018-06-27', 27, 6, 2018, 'NON', 'OUI', 'FACT1806_00070', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 20:42:01', 'test1', '2018-06-26 22:01:18', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'mbcrfx6hij7fbi23b3i3yd224', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(410, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00071', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 21:29:44', 'test1', '2018-06-26 22:01:35', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'f3m09268vlv3y9up3q1i1y83d', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(411, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00072', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 21:32:20', 'test1', '2018-06-26 22:01:35', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'wlu9k333n42jt5s7ct38nhm1l', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(412, 'test1', '2018-06-28', 28, 6, 2018, 'NON', 'OUI', 'FACT1806_00073', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 21:33:35', 'test1', '2018-06-26 22:01:24', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '46tjs8g3a05nk5r702331jap7', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(413, 'jr.menu@gmail.com', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00074', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-25 22:01:16', 'Jr.menu@gmail.com ', '2018-06-26 22:02:47', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'xui2e62q1cm0316xq2jk6exyh', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(414, '28-LV', '2018-06-27', 27, 6, 2018, 'OUI', 'OUI', 'FACT1807_00079', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-26 12:03:43', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'f3cjef4h7a34n10683sbwu1bl', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(415, '28-LV', '2018-06-06', 6, 6, 2018, 'OUI', 'OUI', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 11:44:11', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', '91jla4m7h5u5547tt5m8u6721', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(416, '28-LV', '2018-06-13', 13, 6, 2018, 'OUI', 'OUI', 'FACT1807_00079', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 11:44:18', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'f3cjef4h7a34n10683sbwu1bl', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(417, '28-LV', '2018-06-20', 20, 6, 2018, 'OUI', 'OUI', 'FACT1807_00079', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 11:44:22', '', '0000-00-00 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'f3cjef4h7a34n10683sbwu1bl', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(418, 'commeunebrouette@gmail.com', '2018-06-27', 27, 6, 2018, 'OUI', 'OUI', 'FACT1807_00081', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 21:36:52', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '1u778282ae4dtv4959yru584h', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(419, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00075', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 21:37:21', 'Jr.menu@gmail.com ', '2018-07-05 15:37:28', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '8kgbgb24f6u67136vxhgj5v7e', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(420, 'test1', '2018-06-30', 30, 6, 2018, 'NON', 'OUI', 'FACT1806_00075', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 21:37:31', 'Jr.menu@gmail.com ', '2018-07-05 15:37:20', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '8kgbgb24f6u67136vxhgj5v7e', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(421, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00076', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 21:55:39', 'Jr.menu@gmail.com ', '2018-06-27 22:06:44', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'a68u7fp8j6753blkxxb5i1n25', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(422, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00076', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 21:55:49', 'Jr.menu@gmail.com ', '2018-07-05 15:37:35', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'a68u7fp8j6753blkxxb5i1n25', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(423, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00077', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 22:02:12', 'Jr.menu@gmail.com ', '2018-07-05 15:37:41', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '7m94f374tu099u08vmg1c2l56', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(424, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00077', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 22:02:23', 'Jr.menu@gmail.com ', '2018-06-27 22:06:37', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '7m94f374tu099u08vmg1c2l56', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(425, 'test1', '2018-06-29', 29, 6, 2018, 'NON', 'OUI', 'FACT1806_00078', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-06-27 22:06:54', 'Jr.menu@gmail.com ', '2018-07-05 15:37:46', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '24ucnw65rrw4jq4ry964ccmb2', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(426, 'coeur d etoile', '2018-05-30', 30, 5, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle rÃ©union soir', 10, 'OUI', '2018-06-28 09:01:51', '', '1900-01-01 00:00:00', 4, 'Salle rÃ©union soir', 'SALLE REUNION', 'Jr.menu@gmail.com ', '', '5.00', '1.00', '15.00', 1, 12, '17:00:00', '20:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(427, 'test1', '2018-07-27', 27, 7, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-07-03 22:18:02', 'test1', '2018-07-03 22:18:15', 1, 'Zone Coworking', 'COWORKING', 'test1', 'w33s2a0al459va6bb14qm26al', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(428, 'coeur d etoile', '2018-07-11', 11, 7, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 4, 'Salle rÃ©union soir', 10, 'OUI', '2018-07-05 15:38:39', '', '1900-01-01 00:00:00', 4, 'Salle rÃ©union soir', 'SALLE REUNION', 'Jr.menu@gmail.com ', '', '5.00', '1.00', '15.00', 1, 12, '17:00:00', '20:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(429, '28-LV', '2018-07-11', 11, 7, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-07-10 16:35:08', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'd8kesr2h0575awgffk57srq51', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 0, '0.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(511, 'jr.menu@gmail.com', '2018-08-31', 31, 8, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', 'Jr.menu@gmail.com ', '2018-08-31 22:39:28', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(512, 'louis.veyer@ateliers-art-strong.fr', '2018-09-03', 3, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'louis.veyer@ateliers-art-strong.fr', 'y34s3g7i0316gy97cv1uj0lt4', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(513, 'louis.veyer@ateliers-art-strong.fr', '2018-09-04', 4, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'louis.veyer@ateliers-art-strong.fr', 'y8437sqw8vjt34mfd281di945', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(514, 'bjolibert@gmail.com', '2018-09-04', 4, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'cr39ec8u2p653ib148a2arft2', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(515, 'bjolibert@gmail.com', '2018-09-05', 5, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', '20124327aygbmn8l8axvxr687', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(516, 'bjolibert@gmail.com', '2018-09-06', 6, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'k889ws98vlt2r6202ydsas0hq', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(517, 'test1', '2018-09-19', 19, 9, 2018, 'OUI', 'OUI', 'CLO26102018ET0001000001', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'test1', '1cew2af8shlr805180siwg21s', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(518, 'test1', '2018-09-19', 19, 9, 2018, 'OUI', 'OUI', 'CLO26102018ET0001000001', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'test1', '1cew2af8shlr805180siwg21s', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(519, 'test1', '2018-09-19', 19, 9, 2018, 'OUI', 'OUI', 'CLO26102018ET0001000001', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'test1', '1cew2af8shlr805180siwg21s', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(520, 'test1', '2018-09-19', 19, 9, 2018, 'OUI', 'OUI', 'CLO26102018ET0001000001', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'test1', '1cew2af8shlr805180siwg21s', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(521, 'Jr.menu@gmail.com ', '2018-09-28', 28, 9, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', 'Jr.menu@gmail.com ', '2018-10-02 14:07:12', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '69e1kg57eq2c4764mf2fwupnn', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(522, 'Jr.menu@gmail.com ', '2018-09-28', 28, 9, 2018, 'NON', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', 'Jr.menu@gmail.com ', '2018-10-02 14:07:19', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '18vpswcnye35xa1jt3x389k97', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(523, 'bjolibert@gmail.com', '2018-09-13', 13, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'rpi79dv0b2d7f290jhxm1w95p', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(524, '28-LV', '2018-09-26', 26, 9, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'jr26sm26qt13wj6nj8f9rr5i7', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(525, 'bjolibert@gmail.com', '2018-10-03', 3, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', '12h8d156bu7469retfls98yle', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(526, 'Jr.menu@gmail.com ', '2018-10-18', 18, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'fh6lsp7m2168htygtpi02g5hj', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(527, 'bjolibert@gmail.com', '2018-10-10', 10, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', '06iye1sn61979ru6df364a624', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(528, 'bjolibert@gmail.com', '2018-10-09', 9, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'bjolibert@gmail.com', 'c3pqh5i1f5k5d9lnr9g9xw91l', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(529, '28-LV', '2018-10-16', 16, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', '24146fc4bdrkyc288f8l8qkqv', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(530, '28-LV', '2018-10-23', 23, 10, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '0000-00-00 00:00:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', '28-LV', 'n21hek4h45w0k7a0367l2d6k9', '0.00', '5.00', '5.00', 5, 1, '09:00:00', '18:00:00', '', '', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', NULL, NULL),
(531, 'Jr.menu@gmail.com ', '2018-11-08', 8, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-01 19:00:30', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '2p6914w6j882b5m11kytyd7k5', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', ''),
(532, 'Jr.menu@gmail.com ', '2018-11-09', 9, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-01 19:01:19', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '53sn5u24thak7jfm3v3e23svg', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', ''),
(533, 'Jr.menu@gmail.com ', '2018-11-08', 8, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-02 15:45:48', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'ksahg9jr2bhsvv95u6k1ysb1s', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', ''),
(534, 'Jr.menu@gmail.com ', '2018-11-15', 15, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:32:11', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '7h1uy2d659x7gc7l1ffkws161', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', ''),
(535, 'Jr.menu@gmail.com ', '2018-11-16', 16, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:37:45', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '5b9y9m5913wdx79fesd5r6mbd', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', '', '2uxc3bnsjybl8n64c126b9p4u'),
(536, 'Jr.menu@gmail.com ', '2018-11-16', 16, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:40:03', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'jpv39s622l5g3c62ficcbg9r6', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', '', 'n8n5t40ihfncghgq1m84mvcj5'),
(537, 'Jr.menu@gmail.com ', '2018-11-16', 16, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:40:29', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'l61dj9m3pqv632bhjcf8l48yr', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', '', 'n8n5t40ihfncghgq1m84mvcj5'),
(538, 'Jr.menu@gmail.com ', '2018-11-23', 23, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:42:17', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '016l6i9l9w5vdgh4jemrsv6s3', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', '', 'n8n5t40ihfncghgq1m84mvcj5'),
(539, 'Jr.menu@gmail.com ', '2018-11-22', 22, 11, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-11-06 19:43:05', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'b8nk58slheybklv96hdhydx9f', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', '', 'n8n5t40ihfncghgq1m84mvcj5'),
(540, 'Jr.menu@gmail.com ', '2018-12-28', 28, 12, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-12-21 12:04:53', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', 'p7qnxr5mgkhh0egw849h01733', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', 'd2u4t2e8lb11p22x1p35ennaq'),
(541, 'Jr.menu@gmail.com ', '2018-12-28', 28, 12, 2018, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2018-12-21 15:19:00', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'Jr.menu@gmail.com ', '3mn2yq952awwd8x61ragac6iw', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', '', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', 'd2u4t2e8lb11p22x1p35ennaq'),
(542, 'C00000045', '2019-01-09', 9, 1, 2019, 'OUI', 'OUI', 'CLO09012019ET0001000002', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2019-01-09 11:01:50', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'C00000045', 'si7uu7ade5e66r67f16c4fy87', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', 'NON', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', '7d4atry7x43fq27pdplsgvfd9'),
(543, 'C00000045', '2019-01-10', 10, 1, 2019, 'OUI', 'OUI', 'CLO09012019ET0001000002', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 2, 'Salle rÃ©union matin', 5, 'OUI', '2019-01-09 11:05:51', '', '1900-01-01 00:00:00', 2, 'Salle rÃ©union matin', 'SALLE REUNION', 'C00000045', 'si7uu7ade5e66r67f16c4fy87', '5.00', '1.00', '10.00', 1, 12, '00:00:00', '19:00:00', 'SALLEMATIN', 'RÃ©servation Salle Matin', 1, '5.00', 'PLACEREUNION', 'Place salle de rÃ©union', 5, '1.00', 'NON', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', '7d4atry7x43fq27pdplsgvfd9'),
(544, 'C00000045', '2019-01-18', 18, 1, 2019, 'OUI', 'NON', '', 'ET0001', 'ASSOCIATION LA VERRIERE', '7 rue George V', '', '59530', 'LE QUESNOY', 1, 'Zone Coworking', 1, 'OUI', '2019-01-09 11:19:11', '', '1900-01-01 00:00:00', 1, 'Zone Coworking', 'COWORKING', 'C00000045', 'stvelc74ruhgqg24f6ka3lr3b', '5.00', '0.00', '5.00', 5, 1, '00:00:00', '19:00:00', 'ARTCOWORK', 'RÃ©servation Coworking', 1, '5.00', '', '', 0, '0.00', 'NON', '', '', '', '0.00', '', '', '0.00', '', '', '0.00', 'NON', '17j0vd5j0cfxw3m785h1453jw');

-- --------------------------------------------------------

--
-- Structure de la table `tiers`
--

CREATE TABLE `tiers` (
  `T_IDUSER` int(10) NOT NULL,
  `T_NUMERO` int(11) NOT NULL,
  `T_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `T_PASSWORD` varchar(250) NOT NULL,
  `T_NOM` varchar(250) NOT NULL,
  `T_PRENOM` varchar(250) NOT NULL,
  `T_ADRESSE1` varchar(250) NOT NULL,
  `T_ADRESSE2` varchar(250) NOT NULL,
  `T_VILLE` varchar(250) NOT NULL,
  `T_CODEPOSTAL` varchar(250) NOT NULL,
  `T_PAYS` varchar(250) NOT NULL,
  `T_EMAIL` varchar(250) NOT NULL,
  `T_STATUT` varchar(20) NOT NULL,
  `T_ID1` varchar(20) NOT NULL,
  `T_ID2` varchar(20) NOT NULL,
  `T_ETABLISSEMENT` varchar(6) NOT NULL,
  `T_VALIDE` varchar(1) NOT NULL DEFAULT 'N',
  `T_INITIALE` varchar(5) NOT NULL,
  `T_IDMODIF` varchar(50) NOT NULL,
  `T_CIVILITE` varchar(5) NOT NULL,
  `T_IDDATEDEMANDE` date NOT NULL,
  `T_TIERSGESTION` varchar(50) NOT NULL,
  `T_ADMINSITE` varchar(6) NOT NULL,
  `T_VERIF` varchar(20) NOT NULL,
  `T_TEL` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tiersetab`
--

CREATE TABLE `tiersetab` (
  `TE_NUMERO` int(6) NOT NULL,
  `TE_LOGIN` varchar(50) DEFAULT NULL,
  `TE_ETABLISSEMENT` varchar(6) DEFAULT NULL,
  `TE_DATEVALIDATION` date DEFAULT NULL,
  `TE_DEBUTADHESION` date NOT NULL,
  `TE_DATEFINADHESION` date NOT NULL,
  `TE_COCHE1` varchar(3) DEFAULT NULL,
  `TE_TEXTCOCHE1` longtext,
  `TE_COCHE2` varchar(3) DEFAULT NULL,
  `TE_TEXTCOCHE2` longtext,
  `TE_TEXTRGPD` longtext,
  `TE_STATUT` varchar(15) NOT NULL,
  `TE_CREDITOK` varchar(3) DEFAULT NULL,
  `TE_CREDITVAL` decimal(5,0) DEFAULT NULL,
  `TE_ARTADHESIONCODE` varchar(20) DEFAULT NULL,
  `TE_ARTADHESIONLIB` varchar(50) DEFAULT NULL,
  `TE_ARTADHESIONPRIX` decimal(5,2) DEFAULT NULL,
  `TE_ADHESIONPAYE` varchar(3) DEFAULT NULL,
  `TE_VALIDEUR` varchar(250) DEFAULT NULL,
  `TE_ID` varchar(20) DEFAULT NULL,
  `TE_COMPTATIERS` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tiersetab`
--

INSERT INTO `tiersetab` (`TE_NUMERO`, `TE_LOGIN`, `TE_ETABLISSEMENT`, `TE_DATEVALIDATION`, `TE_DEBUTADHESION`, `TE_DATEFINADHESION`, `TE_COCHE1`, `TE_TEXTCOCHE1`, `TE_COCHE2`, `TE_TEXTCOCHE2`, `TE_TEXTRGPD`, `TE_STATUT`, `TE_CREDITOK`, `TE_CREDITVAL`, `TE_ARTADHESIONCODE`, `TE_ARTADHESIONLIB`, `TE_ARTADHESIONPRIX`, `TE_ADHESIONPAYE`, `TE_VALIDEUR`, `TE_ID`, `TE_COMPTATIERS`) VALUES
(1, 'jr.menu@gmail.com', 'ET0001', NULL, '2018-08-20', '2019-08-20', NULL, NULL, NULL, NULL, NULL, 'VALIDE', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'test1', 'ET0001', NULL, '2018-07-03', '2019-07-03', NULL, NULL, NULL, NULL, NULL, 'VALIDE', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'bjolibert@gmail.com', 'ET0001', '2018-10-24', '2018-10-24', '2019-10-24', 'OUI', '', 'NON', '', '', 'VALIDE', '', '0', 'ADHESION', 'ADHESION A LA VERRIERE', '5.00', 'NON', 'TlpiUVpPbWZvUTcyNGJ5c1FNTkNXdz09Ojoxdj/xsbMOWKivDMTUUE0K', '1530286', NULL),
(6, 'commeunebrouette@gmail.com', 'ET0001', NULL, '2018-01-01', '2018-12-31', NULL, NULL, NULL, NULL, NULL, '', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'nbernier@centre-britannique.org', 'ET0001', NULL, '2000-01-01', '2018-12-31', NULL, NULL, NULL, NULL, NULL, '', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '28-LV', 'ET0001', NULL, '2018-08-20', '2019-08-20', NULL, NULL, NULL, NULL, NULL, 'VALIDE', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'louis.veyer@ateliers-art-strong.fr', 'ET0001', NULL, '2000-01-01', '2018-12-31', NULL, NULL, NULL, NULL, NULL, 'VALIDE', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'jr.menu@gmail.com', '', NULL, '2000-01-01', '2000-01-01', NULL, NULL, NULL, NULL, NULL, 'NOUVEAU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'C00000038', 'ET0001', '1900-01-01', '1900-01-01', '1900-01-01', 'NON', '', 'NON', '', '', 'ATTENTE', 'NON', '0', '', '', '0.00', 'NON', '', '', NULL),
(23, 'C00000039', 'ET0001', '2018-11-28', '1900-01-01', '1900-01-01', 'NON', '', 'NON', '', '', 'ATTENTE', 'NON', '0', '', '', '0.00', 'NON', '', '', '123456'),
(24, 'C00000040', 'ET0001', '2018-11-28', '1900-01-01', '1900-01-01', 'NON', '', 'NON', '', '', 'ATTENTE', 'NON', '0', '', '', '0.00', 'NON', '', '', '456789'),
(25, 'C0043', 'ET0001', '1900-01-01', '2018-11-01', '2018-11-09', 'NON', '', 'NON', '', '', 'VALIDE', '', '0', '', '', '0.00', 'NON', '', '', '123456'),
(26, 'C0044', 'ET0001', '1900-01-01', '2018-11-03', '2018-11-29', 'NON', '', 'NON', '', '', 'VALIDE', '', '0', '', '', '0.00', 'NON', '', '', '123456'),
(27, '31-LV', 'ET0001', NULL, '2018-12-11', '2018-12-30', 'NON', '', 'NON', '', '', 'VALIDE', 'NON', '0', '', '', '0.00', 'NON', '', '', '123456'),
(28, 'C00000045', 'ET0001', NULL, '2000-01-01', '2020-01-01', NULL, NULL, NULL, NULL, NULL, 'VALIDE', 'NON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `tmpresa`
--

CREATE TABLE `tmpresa` (
  `USER` varchar(250) NOT NULL,
  `TOKEN` varchar(250) NOT NULL,
  `JOUR` int(11) NOT NULL,
  `MOIS` int(11) NOT NULL,
  `ANNEE` int(11) NOT NULL,
  `NBJOURUSED` int(11) NOT NULL,
  `NBMATINUSED` int(11) NOT NULL,
  `NBAPMUSED` int(11) NOT NULL,
  `NBJOURFREE` int(11) NOT NULL,
  `NBMATINFREE` int(11) NOT NULL,
  `NBAPMFREE` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `UT_IDUSER` int(10) NOT NULL,
  `UT_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `UT_PASSWORD` varchar(250) NOT NULL,
  `compteur` int(101) NOT NULL,
  `mdp1` varchar(250) NOT NULL,
  `mdp2` varchar(250) DEFAULT NULL,
  `dateMdp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UT_NOM` varchar(250) NOT NULL,
  `UT_PRENOM` varchar(250) NOT NULL,
  `UT_CONTACTNOM` varchar(250) DEFAULT NULL,
  `UT_CONTACTPRENOM` varchar(250) DEFAULT NULL,
  `UT_ADRESSE1` varchar(250) DEFAULT NULL,
  `UT_ADRESSE2` varchar(250) DEFAULT NULL,
  `UT_VILLE` varchar(250) DEFAULT NULL,
  `UT_CODEPOSTAL` varchar(250) DEFAULT NULL,
  `UT_PAYS` varchar(250) DEFAULT NULL,
  `UT_EMAIL` varchar(250) NOT NULL,
  `UT_STATUT` varchar(20) NOT NULL,
  `UT_SUPERADMIN` varchar(3) DEFAULT 'NON',
  `UT_ID1` varchar(20) NOT NULL,
  `UT_ID2` varchar(20) NOT NULL,
  `UT_ETABLISSEMENT` varchar(6) DEFAULT NULL,
  `UT_VALIDE` varchar(1) DEFAULT 'N',
  `UT_INITIALE` varchar(5) DEFAULT NULL,
  `UT_IDMODIF` varchar(50) DEFAULT NULL,
  `UT_CIVILITE` varchar(250) DEFAULT NULL,
  `UT_IDDATEDEMANDE` date DEFAULT NULL,
  `UT_TIERSGESTION` varchar(50) DEFAULT NULL,
  `UT_ADMINSITE` varchar(6) DEFAULT NULL,
  `UT_VERIF` varchar(20) DEFAULT NULL,
  `UT_TEL` varchar(250) DEFAULT NULL,
  `UT_LASTCONNECT` datetime NOT NULL,
  `UT_LASTMODIF` datetime NOT NULL,
  `UT_TOKEN` varchar(250) DEFAULT NULL,
  `UT_TYPE` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`UT_IDUSER`, `UT_LOGIN`, `UT_PASSWORD`, `compteur`, `mdp1`, `mdp2`, `dateMdp`, `UT_NOM`, `UT_PRENOM`, `UT_CONTACTNOM`, `UT_CONTACTPRENOM`, `UT_ADRESSE1`, `UT_ADRESSE2`, `UT_VILLE`, `UT_CODEPOSTAL`, `UT_PAYS`, `UT_EMAIL`, `UT_STATUT`, `UT_SUPERADMIN`, `UT_ID1`, `UT_ID2`, `UT_ETABLISSEMENT`, `UT_VALIDE`, `UT_INITIALE`, `UT_IDMODIF`, `UT_CIVILITE`, `UT_IDDATEDEMANDE`, `UT_TIERSGESTION`, `UT_ADMINSITE`, `UT_VERIF`, `UT_TEL`, `UT_LASTCONNECT`, `UT_LASTMODIF`, `UT_TOKEN`, `UT_TYPE`) VALUES
(28, '28-LV', 'EDFRTGHYJMM', 0, '', '', '2019-05-28 10:38:32', 'cmlIcDRIRXY4R1FZMEtpMWkrOFhadz09Ojrvae8ZqDi2DK3TnctRTNI+', 'WFhXdWxsM3RKTytleUNCK0dCRVNZZz09Ojr4pW2YlBnDLjFfgNDyjtLl', NULL, NULL, 'MkdDQnlVTlR4VkJuZ3J6ajNWVGpvZz09OjrK3PVbFqus/XDn3+WshdHH', 'R29WREEzbldGNEhoSUJhMTF1SHlRdz09OjqH6fOnQcZvDSari+SNixx0', 'UVdBeTl6ZlVuYk1WM2RaSkIwTE1oUT09OjqPMcl/938VGmD/cOo7rcsR', 'eEl2ejc3YVlWajBPSm9KblpMY2tmZz09OjqMkxVrRIropKYkNF1Gr39g', 'SzFLa3VWd293MW4vQUlWdVhjMmV2Zz09OjqiMZzG/8YciJ75Z5NCKXg5', 'c2JPVnhNdGZ2MUtPT2VYOWZGMmdRYUF6QkZCMXYwU205STZreFJLenFyYz06Ov0ImkQVyCJu/DwZH+zTEjA=', 'COWORKER', 'NON', '17093583', '25978101', '', 'O', '', '', 'WmkwZ', '2018-06-20', '', '', '89791870', 'dUhqKzU1M2lrSUxUaGltZXpuWGkzQT09OjrExRuC4CZvKVs/hnvJ7b5U', '2018-10-23 19:57:34', '0000-00-00 00:00:00', NULL, NULL),
(31, '31-LV', 'EDFRTGHYJMM', 0, '', '', '2019-05-28 10:38:32', 'eVUxT2NUVWV5RzZkM05Kb2pBZjRWdz09Ojpi8oK4ecLvA9fYsjdFU5JV', 'MktydHdYbTQ5eUFZa1U1bmw1VVRLZz09Ojp5Qy/6r7wQkYxiR4iIBZv9', 'dFJweXBXNmRKQU8zVHVFeEVIUVpCdz09OjoGJnb311ahAkN9sQjGnLhJ', 'L2lpcmpnME1QR3pKRVlWTUlNN042Zz09OjqXH+XNgS0a6jYjITVeJ88P', 'aDFkUzdSRkFtRy81dERFcEkxSGN5dz09Ojq1Yv0/hYeBj328R8v0vKIj', 'RUIxS3FYbjN6ZHVSRnZ0bi9xWk5aZz09OjqgQE+f2Sv8MHKEe5fuxIdZ', 'WXk2OFI4Nm5lL0xSSFNvS2NvanlWUT09OjpH5MUKuJBUOrM3aOzwIvld', 'SlZqblh1V0tieVlvMlJqcUVadTlpUT09OjqNinjtS3EjG1h7dDZmYkM9', '', 'MGpsM2RLcGNUYU81azR3Q2lNckRMOFlXSjhNWkRFMlRQYVZKOG1GWmFhUT06OtXQGGnwqnwYhPmCQRn0vm0=', '', 'NON', '', '', '', '', '', '', 'SmJFY1I1WXFjT0RWWGtMcERCR0ErZz09OjpKQkP2EyxMZLOq7FtPi2FQ', '2018-06-26', '', '', '4159546', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 'PHYSIQUE'),
(32, '32-LV', '109014df6093f1a780e166df0acfdec934b99d3b334a37bd79d02aae88e2d04c409a8c6ebd1b33aa72ce6889231ddca28eba80cae1249ebebade0c9986758e53', 0, '', '', '2019-05-28 10:38:32', 'Q1NvUjZxdnpvaXl5ajF1NWZIYUcrdz09OjoQjCjPDGnSc+zNRso8VpWw', 'TVkwWVFmNlZvVnVZT1VJOHBobVpwUT09OjpdzlQFhMpM0wMDP+8VK8ja', NULL, NULL, 'VjVFWEcvTk85QkRocWxBc0FvL1k4dz09Ojo/JkqGbxPbj/i9+TYFaXWl', 'NVRlZE9JOE9kb0VQS0Y3MkZLWjlKZz09OjqoLfr6NoVphKYmeyL07wmv', 'N2ozR3U2T2psSHNQZERleUxCYmJlZz09OjpbrwwI3RIF3mYgdcdwyosq', 'NTFvMkhaZWwwMmhRblZQQ3dTaUVob2ZBaHFLRCtDMmRIaTdvVldWU0VJUT06Ot58mkBHfZsXgoScXYqq+UM=', 'Q2IyanAvbldqeko4LzlWOEhYZTBndz09OjpSgDZDh8F9eON8C4/uojHq', 'cWRBZzhYSlU4cnk5ZGdjT1dQUVFMTWVBZVB0d3R5RjloLzVaQ2I5amhVdz06OjMEqkqshz48GjLZm4c0U+4=', 'COWORKER', 'NON', '31815805', '88187945', '', 'O', '', '', 'NjhFa', '2018-06-28', '', '', 'NULL', 'UlJ0Ui9VZGovKzV6VThDbG9KVVl4dz09OjoVruo3zoeCwJ3Z7TG718mH', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(33, '33-LV', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'QjZINzQ3YnIvbHd2QU5SWHZPOXI2NmhoUkwxd3NZTytuWVdYYmZXTzNjbz06Ohag1WADB9AckCADHr/HVMU=', '', 'NON', '', '', '', '', '', '', '', '2018-07-02', '', '', 'w6k3nrk72r7453bfbd73', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(1, 'annie.tacquet@yahoo.fr', '0d17c73cb1ec713c0bca20646e51a7e79cfcc0cca84eb9ac2e79cbeaaaefdafc60cc24b282581346b35273b708c8003ed77711c939e199c1fefbd06ac43ccf47', 0, '', '', '2019-05-28 10:38:32', 'NFZqSnRNdmtJdmM3OGYzcy9KT0ZmQT09OjqxNfHvaMVWB02EtWT41hcg', 'eFRvRVgzRlFMZ1pnZGNkOTc2eVg0QT09OjqagJtyBdMhV2VuLrKnUNFC', NULL, NULL, 'S1h5MnFkMXhGN21SNnlzbGEzYTN6dz09Ojpk+AWXPuMnO4z+xKy7gJn3', 'NzFTRTFOVjFnTEF0NGZwWVl5aGY2Zz09OjrFn+aauZOgJiiXFR4UO5lB', 'VXMweStVUXJQcE9nTWgwZEJJeUZ5QT09Ojp3ISRPxqhtwyJ3H9PY2DBg', 'dVM5VlE2bGZhWnFZMlQzUmoxYUFIQT09Ojo+YZgpukKA9hyXLEZ3Lwge', 'SGdPT0MreXhjWmp3RnZEbUY4SzkvQT09OjqVvNJN3iZ7jXacn4wNip3j', 'SWsyWkdSRHo1NDJlVmQ2M2NXUkZIbjhYd01yVGpxT0VFUTZ0eE1OSjlxND06Ok3+Z9NisTr53S0ww8cz5ek=', '', 'NON', '11805143', '23366777', '', 'O', 'Z3piQ', 'Z05iaGZlVzMzRDN5YXlXOXo5dVdDZz09OjrE665gWSiduEpnxi', 'bjMzd', '2018-06-15', '', '', '', 'd2lhT05kQ25UQUlROHI3WnFlUG5Zdz09Ojp7ttAlldDKCVHd5L5ncnAt', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'antoine.demailly@lessensdugout.fr', 'ef4efcc6a386c3cd1b422ca1f046f5be5d6e62e6590db3a1531cc6c3616f52bd2f1643c12fbdb278df287751576f717eb2b2683037fa25e108126172b9b3f331', 0, '', '', '2019-05-28 10:38:32', 'M0lqQ1ZWcWpkd3pjZ09pRC9IcE5VUT09Ojq8nhiM6OgKtdmeeyb4J7yE', 'bjhPdUdKNDZwRlFkaktNRVA3QTErdz09Ojp2UlNbWMbMlVef3vH1P2kb', NULL, NULL, 'THVrU29QWC9TTXBPS2VZSGRzTEtnQT09OjoyQD6fU4FodOfRZ24+MXW/', 'V3VJa1JzeFRoU0xWckEzUTYvMVplZz09Ojo0Ucmq1sHGKmcLpnieN3C1', 'N1hPeFJwTFJzZ2pwb2tHRy9SV0xOZz09OjowoyI1eE/nSPtfnA9TfEO8', 'dVFza1FaVG9zRUJGbmJNb1gyRHJSQT09Ojo7YGAaR44qouSAUylnhRp8', 'WUh2WnR1aGNFd1J5Q1Z5ZXJOVUtMQT09OjouAddfeXHLPNu5xJ13GZEW', 'THd0WkxQV043VktnbHFFUzg2VTUwb0p4NGRIYXdkYktVQVppdXljeHYrUGJpaWFuQThFc293bGhnR2ZsN1UxNDo60sw97nh03JAQsA8Hr41isA==', 'COWORKER', 'NON', '47123781', '50225190', '', 'O', 'TVdNR', 'Z0FNeDlITzFNQkJ1aFd5RWtuYVAyZz09Ojqafzt3UxyQMUAPKy', 'NFJNa', '2018-06-15', '', '', '', 'bGNCRGNBYmNGZHlEb25FRlRpd1U1UT09Ojo2AQaD1VzxS7EwlhnTurMy', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'bjolibert@gmail.com', 'fa763833a4de18cfd303490896f6859afdc8ae42692e6b34b4d2a5ba091941a50632cbeb564c3e11201d3a1b2c97e0e39d21cd8949e32fbceee5ee5ac0c9f16c', 0, '', '', '2019-05-28 10:38:32', 'elN4NlM2dVJ1eHNyUFRhZE9RNnhKdz09OjrWMabsElGb7YkmYtiwU2VF', 'ZDE2Y0FJSmNYVm9xZnBEYlU4RWFiQT09Ojo0kMh4Ch4gszcpKToJO7RW', 'SDRBYm5helZMNnM3cHQvTUQvQTZ4UT09OjrqKj1SfdEHfOIbJrfrgqSa', 'OGtOMVRqZVNWb1FvMXlSeFFCMWZ5QT09OjpAvDVpmCCUlFAwvxNQ1vC8', 'MitEblBzUkZ0NGpkMDZjS2UrcUt3Zz09OjodoxKGlli4c+QhPDWI1bXJ', 'R2pGYWtCUlJzcm90YlRHd1FIYTV1dz09OjojWwVo6T+HPq4cOIrqfWy1', 'MkFieFRjb1A1NFZZVXlXbGZRRUM5Zz09OjqrYALlJRRHSiAPxQKtsadt', 'U2NqZzM3T0lxaEJBU3NRRXltWVdEUT09OjoNX31qXGNI2tVFPKTTJIuU', 'NDM3eXFxYkx0aE1hVXpnaWVjQzc3dz09OjrHvTJvtxbQqvlqlkvoABKm', 'ZTBSb1o0WUdBcktXUFNQMCtYU0F6bDBPSVZERXZkU1lkOFZPVlFGcVhsTT06OlilmA8ktueL6GNwIO6QuMk=', 'COWORKER', 'NON', '77332828', '38209828', 'ET0001', 'O', 'S1FXZ', 'cWpBSDVjMUdYNkFabDVFRlhCcDlhZz09OjqRC/yTdCZqVG8APw', 'Z2FDNytFQlppQzdxeGc1ZGJobFlQdz09Ojo5vvZpWgbgsKqqiDRmeKsc', '2018-06-15', '', '', '', 'UE14SXpEdVpLMDdybVV3NERCYmFvZz09OjpKJhrblVKT8ZvJleEHsFhU', '2018-10-11 16:04:11', '0000-00-00 00:00:00', NULL, ''),
(5, 'brohette.nicolas@gmail.com', 'c2c678528a92b27a84deaf9f4708965c277d5c8784f62a64501b9f00222846f5a863c69435282c348ad93d4694fd78907cdc9030d4a35e03b7dbecadf5ec4c0b', 0, '', '', '2019-05-28 10:38:32', 'UVNGZVZxdjhsSk8wb0s2bmNML1dhdz09OjpA/fMEIl9Gvont4roPOltx', 'UlY2R3VEMTduZHlnTmcwN1ZYS282Zz09Ojq9daCDLK66caJOCl0qbl2W', NULL, NULL, 'NStleVBUeFBtSW5VWklZb0RBbTJDNXl2RmRMS1JTem5NdVowZi9KY3ErZz06Oo+qqlk1LlYvhVa3aDb1o+4=', 'cUFFaThKdjIrbXVFNWFkK2E0Mk13QT09OjqJmH6UcO4VDKd3/JLjNiCy', 'T0ltUmhPT2JJUDdjeWVoOVVKL0N3UT09OjrEwObaroSHOo+gHH3PJS0k', 'eDBWK0tZM080U2x1T1JNQ0ZMREh2UT09OjrWBzYZvoUX2SKueUVpeigV', 'SHJ1RmlsYjNZdVNJdFcwRWhhRXBtUT09OjrnjW1KS2GMp2LEY7wo5BTY', 'VGZNK0h6UDNObUVIbnduaTM0ekVLQzJDeldEbXF1NGhWaU9iZ1hKYXAvbz06OgvQnYTvIlZ1hbwvPKsO4QU=', 'COWORKER', 'NON', '67919086', '17473891', 'ET0001', 'N', 'Mmxke', 'UjZyS2IzOFJoaUZtUk0rMFVFNmt2UT09OjqPGbiv5k6eYnE4PJ', 'TlErZ', '2018-06-15', '', '', '', 'TWF5amJoTkVjL3BXaXQrK1BteUxjUT09OjrKXpRyWryHTyN13+lmMPSg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(38, 'C00000038', '', 0, '', '', '2019-05-28 10:38:32', 'eVQ5KzU5ZWVuRTB5WGV4dmpEd3F2dz09OjpckZuOXg9qhMkKbOfD6srQ', 'Tk11d2JZQzR5eHlSY1RHVExHS1FFZz09OjpAn+n6vpOc9WLp5Zv14Ruh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'emVrTjl6YmovbkhvL3hOSUtPNmhhdz09OjoihKYs0jJtInXcGa+PlcSJ', 'COWORKER', 'NON', '13903809', '84439087', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '1900-01-01 00:00:00', '', NULL),
(39, 'C00000039', '', 0, '', '', '2019-05-28 10:38:32', 'RHAxVTFDK1RwZHNVdFBMREZSZ25kdz09OjrHCVHB36zxbbh70ub+wb7p', 'UVpRUnppMmJEVTl6eHZRUGtOWFA0dz09OjofVXyDq1/LciiRv9rmSMPu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'QVZqZlpRbEN4TjdnbXdNc2U4UFJuVlRrNzd1b3FDUTRBNE9rdHJMd2xEST06OtAq1sCIRZc7Z4uZWYSDA+8=', 'COWORKER', 'NON', '9869385', '85311889', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '1900-01-01 00:00:00', '', NULL),
(40, 'C00000040', '45879240b35d232b31a9ab0a74edac8652fab6f3ae2d2bda3f48edc7bf223dfc4b9c1a35653153cb7b2ad3298c3b2f17ec5bb54a020db5458f45a3301720a789', 0, '', '', '2019-05-28 10:38:32', 'dXVsY05pYmtEQ0k5VFE3VVQvcmZOUT09OjqKT0dS5Dqsck0N0RtSoG1h', 'VEZ1eVROM04zVThiSmNaSitsWlNBdz09OjqQO/y7ZzlMOXEKxmT0+lS1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aklFSTRRKzFyWEpmaEhNT0Fzc1B1Q2o1QkNpcU5rVUtFUEFtNmZnMHpIMD06OjQmyf4wNpo4f5jPmT1DLyM=', 'COWORKER', 'NON', '79040527', '34341431', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '1900-01-01 00:00:00', '', NULL),
(45, 'C00000045', '2945d744451056765fba98ff3101253d991eab8cfb735b21f70ec7ea7b739020a4e715bf8dce1329ab00197f9f4544dc53da8e54e69f7c601d5499dafcc2b3ca', 0, '', '', '2019-05-28 10:38:32', 'VHVhZFMzdmZNd3gvTk8rWURrY2FQQT09OjrOeRXJNX0H+5VRWY5air8e', 'ZHkrdzYreWxpK2p5alJXcWdRKy85Zz09OjqQRpMUE23Q6nH1W6JVEUKE', NULL, NULL, '', '', '', '', '', 'ODhobWdwRTlNWUVqZkVta0s5eVdQRVQ2eHZNT2pZeDdKaGRIWHkwUDJhZz06OvaZ4IsQkLTdENP8Oo/+OHE=', 'COWORKER', 'NON', '77264404', '75119018', '', 'O', '', '', '', '2019-01-09', '', '', 'NULL', NULL, '2019-01-09 11:16:47', '1900-01-01 00:00:00', NULL, NULL),
(46, 'C00000046', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'ZlJBcC81NWQ4K0x0MkJ2cUgwN1l4dS91TnJLQkQwcU9LdjY0a1hZUDFvUT06OmAqclFki0KfeMUUox28fBE=', '', 'NON', '', '78041862', '', '', '', '', '', '2019-05-27', '', '', 'if7vrtve2c192si1e9mv', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(47, 'C00000047', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'dDRXWDk0ejhHWWVHQ3FrS1NOVkJXblg4cytZTk1zR0hDZGFpNk5FUUNPND06OsCzQ1vuyEYJJCbPqDv/ZqE=', '', 'NON', '', '42535229', '', '', '', '', '', '2019-05-27', '', '', 'r1w6kj47h7x2kqd8sap8', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(48, 'C00000048', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'SWdTbWtIekdEZkMraCt1YWxIZnFwZWlPZkxpRXZlRnY0M2ZjcjBaYm02RT06OpTCsqZOgOfL0Cu6Y84Plbg=', '', 'NON', '', '79244241', '', '', '', '', '', '2019-05-27', '', '', 'q25xjjyed724pqlg1mt5', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(49, 'C00000049', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'MW5sWVhNOTl5M0FBbFZ1eWt4WUQzU3F4Q0hyVERLVlppVDdYU29ObjhlVT06OsgfBZqymEum6rVzUvlTl8Q=', '', 'NON', '', '45520923', '', '', '', '', '', '2019-05-27', '', '', 'awd2q758r3w5v3fh6y12', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(50, 'C00000050', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'NjFkTGtBRHRrdExLOStCUU96T3VuZktNa002cFBzZ2taUFFvRHI2TEdFST06OtIMsqZZR6jAkSZvjkzNB4I=', '', 'NON', '', '2316059', '', '', '', '', '', '2019-05-27', '', '', 'x546bdg4l1mry9r85s9t', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(51, 'C00000051', '', 0, '', '', '2019-05-28 10:38:32', '', '', NULL, NULL, '', '', '', '', '', 'd1hEQ3Z3TjU2SEs2bHFQdmMvWmRXNHowWFRBaWlObGZUUTNRdSttajBvTT06OtvORdTqxraXleYSS7eFAOI=', '', 'NON', '', '88082172', '', '', '', '', '', '2019-05-27', '', '', 'f6rv19pc6hihea1p9c9f', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(52, 'C00000052', '228db813a40d587abd1c8f5453eb35cebde749e83a8fb9b3e797a1a8668da8ce95439ea4a13276a8203c23bdc1c9e37045294234ee49fe615b569971de35303a', 0, '', NULL, '2019-05-28 10:38:32', 'Tm92dVpFd3JnbFNxSHEya3RJRFBTZz09Ojoh6KPr/KDPzuQ0EetIwJya', 'a052UXVwSWdKZVhGOTFxQS9XL2RzQT09OjqC1O4xO7q8VbwBLXCyFe1j', NULL, NULL, '', '', '', '', '', 'RUYrYVRWMnJ4bjQvaHVxakxmRGlDTUpFdGI5aUZIVUxEdjd4WXVhN1dhWT06OhH3o/nLpa40GQEWuHHVgn4=', 'COWORKER', 'NON', '58459182', '87066563', '', 'O', '', '', '', '2019-05-27', '', '', 'NULL', NULL, '2019-05-28 09:08:45', '1900-01-01 00:00:00', NULL, NULL),
(53, 'C00000053', '2506fb63948697814f1b0c4435e3cfbd72af006c1af7de779d1d27d4c3b22249dd3d3b18e6e2646b4331a53a391ed49da7be0c79c35792ed41ce5f49e7ece183', 0, '', NULL, '2019-05-28 11:00:49', 'S0NIOVYyZkxYQW1rcUxUaVVxQVNMUT09OjrpV5noj0MKAjXTeBexAdyX', 'eFh3UGhDVXNFVFVSTVRmWFZhZUFkZz09OjrtQPiRl8FUrFjuaFWn1i+X', NULL, NULL, '', '', '', '', '', 'dHFidEdLcXB5QVJnOEV4b0kvVDlrSTI1cHFYdzBIbFAvNDcwSWFVL0RGbz06OjKdB48BMufHEmGY+WeFy6U=', 'COWORKER', 'NON', '89371066', '93765657', '', 'O', '', '', '', '2019-05-28', '', '', 'NULL', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(54, 'C00000054', '3717a50f220bb10822f2dced90d9f144c7ff0d60adf967b54b90fe8c335b44d8999208ff0510b160eb39f3146b2db4ef6f87c1762a73d5df076f2c586d64fb55', 0, '', NULL, '2019-05-28 11:09:42', 'NldwZlEwZDFqazRrZWxVQXlZN3AzZz09Ojpi4qktJe73GvCp/e+RRzpB', 'UkNQVVpFbDhzd0lYU2g4QTBRcWVqZz09Ojqponfi3VyhX8T2+1usB3Wf', NULL, NULL, '', '', '', '', '', 'TS80cFFYMzNVQ2lDQmZ1bnViSGZ2TWpuNklzMjJ3dXNtMnpvQWpPK1Vpcz06Oumv4Db+V8jRAN/mc5LUbUc=', 'COWORKER', 'NON', '71933218', '60328878', '', 'O', '', '', '', '2019-05-28', '', '', 'NULL', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(55, 'C00000055', '8c76c6af6ee4ab507d8c4a1bcaf2665493fbbfc6bef26e43a6fb86cf440681371e1a9a7a4e957992777aba5574b1056a48a1b4817af7d291f2905a3966ddb965', 0, 'mdp', NULL, '2019-05-28 11:18:17', 'RHBRemJOZVZ4YzlUYkcybVlFV1hDUT09OjqomTJPz0WO3Uqn/4hfzKCV', 'QXcySS91RE1JQVY2bWpPM0UvZUowQT09OjqUnLo0LhzD2dazTjkHuEl2', NULL, NULL, '', '', '', '', '', 'NVdDeGpHVkVGQVB0NnVoeWtLR2hVaDNDQWloc3FQZ0hLOWloYXR5dkI2WT06OjdKgkcG50picaZMbDhr/mU=', 'COWORKER', 'NON', '27594132', '99418835', '', 'O', '', '', '', '2019-05-28', '', '', 'NULL', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(56, 'C00000056', '$mdp', 0, '$mdp', NULL, '2019-05-28 11:23:19', '$nom', '$prenom', NULL, NULL, '', '', '', '', '', '$email', 'COWORKER', 'NON', '$id1', '$id2', '', 'O', '', '', '', '2019-05-28', '', '', 'NULL', NULL, '1900-01-01 00:00:00', '1900-01-01 00:00:00', NULL, NULL),
(57, 'C00000057', 'a53a7cddd13a5e96deb9f4caccc9676982af4a2dba19fbeaffea159423b729ba0506d02cc1e4086a796efa8ffa8d314048a9956458f7aab8ee3dd89e86e32ed6', 0, '', NULL, '2019-05-28 11:37:38', 'b0tKTWpSazE3eUpaK3BxUE5CMHlUZz09Ojp1AMWKpqorxCOoUPkYf7uM', 'VXEzVjhuZ1p5WkdVN0Q5ekFxOUNhQT09OjoPYjUCYLJdhqBEe+sMVdxa', NULL, NULL, '', '', '', '', '', 'UlJRV0k3UlBtb3dxRDJ3ZGdJcTBDY3dNR2tnNlloN2pCem5BNWtnai8yaz06OuaGLA2YyTFzkq09ET/+ayo=', 'COWORKER', 'NON', '46799847', '61088101', '', 'O', '', '', '', '2019-05-28', '', '', 'NULL', NULL, '2019-05-28 11:52:51', '1900-01-01 00:00:00', NULL, NULL),
(41, 'C0041', '', 0, '', '', '2019-05-28 10:38:32', 'VDR6YmRnaDlwSFpCcnN0d1AxMmdZQT09OjogFqW+vcOTtxVCuGC1bq3x', 'Q2FJUjQ5RTdDQ2ZFWDA4VDI4SWJjUT09Ojqx6NeNwGBFo+tPb20F4LV1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'anlkUWptbEN3elRiV3l0SFE3Z0RlK2VQR2VJWTJ3K1RxNTZWbmx3dmJZVT06OqQ3y+6F+huouHG9MbKHan8=', 'COWORKER', 'NON', '44482422', '11257935', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '2018-11-28 22:38:18', '', NULL),
(42, 'C0042', '', 0, '', '', '2019-05-28 10:38:32', 'Tkx2RkFNeWhtdWJ0ZFFCNmJYMkxiQT09OjoVxNz4YlujudtfPBCuAtbL', 'Y05SY3pxazZqOGFYOERhOE9jQmwrQT09OjofItQq9c7T6mWUv5C+m9yc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TkJiakZxOTA4VHl5U21pRnYxRnl3K1dDY3dqTFFObUFNZHR2TWNadnVjMD06Or1qh1xAqje+aktcj6UiPY8=', 'COWORKER', 'NON', '32467652', '90710449', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '2018-11-28 22:40:08', '', NULL),
(43, 'C0043', '', 0, '', '', '2019-05-28 10:38:32', 'eC8vaWF5ZHFLZjVENUtBQ1JMemVDdz09OjrtUaUCAIgXaaohH89ElxJr', 'ODkrRk01WFc5M2QwSDJ3UmUvTE5lQT09Ojpw8M0Uqtg5zNgO0se2YPaB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'OGpvOENJYzJZN2kxQlBJb2FjWW1KVVY1K3VBblExcVFOUWVuQ291NktJOD06Opp1UQfErVEwIcPCPRaj1A0=', 'COWORKER', 'NON', '10919190', '62240600', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '2018-11-28 22:45:39', '', NULL),
(44, 'C0044', '', 0, '', '', '2019-05-28 10:38:32', 'MzNKL0pqcWtMTzNFYmpIeG9HaWlBUT09Ojqhkaar7qlrVPRDE466R3xV', 'a2RadVhRRUdvZm05a2pMaU1ETU5OZz09OjpcfCenhHOAbyMWCCgG3sUm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'VEh6YVpzVXlEZlNrM1c2Q2dKeVVuaXoxOEF2Q1FSendzeE0wbkVpdG16QT06OjCR4BKGA+dU3FJfQ45+dDU=', 'COWORKER', 'NON', '98724365', '26754761', NULL, 'N', NULL, '', NULL, '1900-01-01', NULL, '', '', '', '1900-01-01 00:00:00', '2018-11-28 22:46:00', '', NULL),
(6, 'coeur d etoile', '617eee3948e145c8e0845c140be63eeb02910783a77d6297c1f585ff63623affd598da749a07a1b6fe29da3c531804ce0cd4766732d4c0483e45ef396f3aa748', 0, '', '', '2019-05-28 10:38:32', 'VENIdExYNm1XR1F3OFN5RGpPL3BvQT09OjoIl9mXqQ5rorIrW1QI3HsC', 'dFR6bGJXZENNYmg0d3lCc2FFemlodz09OjplzhCQEanQYmoGHrr6rvUy', NULL, NULL, 'RzRnQWxyd0VsM3JVeVYybVpPNlBHUT09Ojqakx7xz+RMsyKI5sqgV2N6', 'QmZ0ZVpkbDZRaUlEVGptbVRRMmZwQT09OjrKAOUce7BHGY+R48wa78NY', 'ajdDbHNRNjFvZDJXbURDVlphRm9TZz09OjoS+drWfHb4y23pt0fBSiPR', 'ZjByNXlJQVQ2QXVmdHhVNFZRei9NUT09OjrD9/wMghE+gh6Kes/dZvCh', 'R2kwS0FhcG5mTnFkeE9YSHpRcWZwQT09OjrsLEkBwbwh/a9GkpVgfL12', 'WTNLZVpoVUVreC9ZNnB0dnAyM1JKdz09OjoFN1M1PMKPqgJQUg1tqKrk', 'COWORKER', 'NON', '12229975', '43838018', 'ET0001', 'O', 'dUVNS', 'K1J5S2VBampNcFpsandXYngvVWF1Zz09OjqL9KxPauFkOk7pma', 'djhQb', '2018-06-15', '', '', '', 'SGxiR3hDZWJTd2dpOFhkdkEvdXJ5Zz09OjrXMCiH3k5bpJX5AlgtxpDu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 'commeunebrouette@gmail.com', '097c0c147ffcf5232e7d7314d70ada09463b4f85873e97690ab44d8c2710723b253da70e97d5131645e0d48ffaabfdd0ac8c558ee62692f3f9c458d0e49d5fec', 0, '', '', '2019-05-28 10:38:32', 'd2lFbXdWYmhZM3RyekNnQ1RYKzBFUT09OjpsmTShbfARrpgIPyCMbCGn', 'SnFFWHhkNHRlbjJYbU5ETXRwRElHdz09Ojr5lQPRAq21wtIGYiMiwuJF', NULL, NULL, 'VnRMK3hBcnIrcWZISTFUd1lyaHZ1K0hJbk02UGJ4UFlIVzNTdWUwTk1HYz06OtrtDE8hSDkwAgi09o5Zw9o=', 'czhJVjN5VjNpWHJjRnZkUDVHMGNLQT09Ojo6aapGGATHs255duAtbxjP', 'ajFCaERqaUlENkpOUXBkYUJnREY0QT09OjoSRo07WIw7sTHjFDnfEAbR', 'MUVrcUJWc2ZScTJJZ3NqUFloc05KQT09OjpiE/X07SC53bPDeM91rEmO', 'OVV2NXN3elFOZzNCSHpmMXJYNU04QT09OjokHTtdolNGg8s3wgZY74lM', 'UStQODdGZjRPdlpMVCtucFNkeE9HVmd5UmJKVy92bWJQZG1HR09pdzRCOD06OpwSZeDIiCIOVz4eF7l4oZQ=', 'COWORKER', 'NON', '43181009', '49044072', 'ET0001', 'N', 'V2ZCc', 'SXFIN0cxeGVHVXVhQjVVa2NNanFNZz09OjqwuO8ApJsa0obiST', 'N1lmc', '2018-06-15', '', '', '', 'YXRQYy8vbkRPN1ZsRTh5UFNlcWVtZz09OjpLt7sg2Vf6EvC5A7yn+wyY', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(8, 'elisabeth.gruson@gmail.com', '356a1a935de4304b2a395edeb77187ba09dab64e8f0d0f759e4b1d55af3e48a93fb4f89195072aa41066e2a36edd2796ea9d28a5397a0a4067e61121063d28a8', 0, '', '', '2019-05-28 10:38:32', 'ZDJKTE04bWhLdks5NVVBVEQ5VkZvZz09OjqM7yy0wLyVhdLNfV4czAPs', 'UXF3eE9QT0NnNWJ2RDVQMG9TVjBUQT09OjptRmDmgkag1E868CiMVQrW', NULL, NULL, 'K2FzN0xhajFoOCtHaVNDNWFFZ2VSWFdqV0k0cEZuWER3QVI2elgwMG5LND06Oo6CWLYDIcqwDCJJVevfwqE=', 'UVhibmw1S1BSdE00cWxLaHpDRUFrUT09OjocVBbSwi5qeg4CsEEENyva', 'bEduSDg0MmlVbk14ZFl2OXFWSXZhQT09OjpBCSKKfIab4gn6f8WRPi29', 'UEhzblUwM0JralJjb2xOTHhsUWxqQT09Ojqmg0J5Im7H/AduoLF2IgDC', 'bXo1NWtQdU9jZVVqWFI3cVdGaXdEdz09OjqgpWjf4SQsCTeFwpJ/hEXo', 'dVhTUU96bFJjVHJHemM3K0RjTGlmMCsrbmwzRmVzdFNqdkpQVkJyeWR2QT06OlTsaolY6yYo83RwjDEomwc=', 'COWORKER', 'NON', '23746014', '63745227', 'ET0001', 'N', 'YlpDW', 'MTNTZURkNE93WjdCcHBkd1VOVFE4Zz09OjqMOCMLpjJ0hNnuii', 'dHdxc', '2018-06-15', '', '', '', 'RHJ0ZHlNT2UvUTRVU2Z2QW1tNzR6QT09Ojp5gVKiU18fLmacjBB6PQkT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 'herve.hazard.lessensdugout@gmail.com', '68127423422049bd63f7a2b3c52c29b679e8404c6c3560352f8408f436e28d0410c708d81b5e0306ec516acf5be453fe60413d16cccb33b5bb967284b08b4c09', 0, '', '', '2019-05-28 10:38:32', 'TVJucDdFWi9zdkZ2dHRReFk4WXFpdz09OjrsUR9Li7F725c04gC25IW/', 'UWRPd091Wm0veTBqVS9Oa1BRQVRkUT09Ojp5LhUB7TWurfrVcS5FrTBS', NULL, NULL, 'VjJDbGRKU1MwZndOYnJoOFc0NU5VUT09Ojr1W/jmuVhB4lfWcTm4Vebm', 'WUNCemt2aUkvSGQrbXY4T2NMRFJkZz09OjoGnJkf1ATryqO94AxVeCSG', 'UTlRTy94T09RemwydUw5bWRSWFhFZz09OjqHuoOmPZWFVsxxMpVS0ztb', 'RW1mM2pHbDRHaTJmcTEreCtOMFhwUT09Ojr5SJlEyFU45AgCwFllzUAJ', 'K0NWU3VRTmZKdmFHYlpHSko5cnA2dz09OjpDVG5WghaYF3uvjqfBUw5m', 'c1BWS093SGZzUFdubXo0MGQxYkxXekhPSzRHNUJSNE5RL0JSTUJjcFY5MzZCZTByaFNNN3hVNnhFK0FlVHhrMDo6pKGKOvGU17GSHqTkE20k2Q==', 'COWORKER', 'NON', '98127506', '1559919', '', 'O', 'bnF1Z', 'WTVhQTNCOElkZVMxUXBzQWRUYWw5UT09OjqIgcj/wVQL79TynJ', 'Z1MvS', '2018-06-15', '', '', '', 'MHFQVmoxSFpTQkROQkc5ZmxtQ3FWZz09Ojoy7QVSNl9h4o68X20k6bMB', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 'hugo@hdwebmarketing.fr', 'b224890806074d8a79cda3be60a170878fa2d78721dada563b865a49df3e7d5eb55eff10fb75a14b2ec3254edf6ced8d075dafacf7384c644e5962c7a0cd7762', 0, '', '', '2019-05-28 10:38:32', 'NlJJcFowdUhtek1kaWhCUFpXSERPZz09OjpSySQlFNc3YthkxwsUea+I', 'VkRPalhibkRVQ0pPVDNhYk1iMkVLQT09Ojo7xT+1LhSqpEZIN6SDh2/g', NULL, NULL, 'L3FQKzNscGZrSHNYSGRhdFRGdW9MUT09OjrfMm1a7BsQg0h7zNt2O5VB', 'T291RlJnZENXMzBLN2dDczJqNHpOZz09OjpmL2y67gzNlerQzV42uxgX', 'TzlxeXh3VHFjRWFjamk0K2tLVUNLUT09Ojopzcv266PoXGYe/o7Ph1Nu', 'a1ZGSVpyWmR2NWVtY1hXTjM5WnE0QT09OjqdWd3s2mxDpYVZDM+cq/70', 'dVlMSGthd0ZrcG9nT1FiZHRXRnBHZz09OjoPiaBm2SiWXHzDAUwrlt/z', 'SXZxTUpsdzJ0bnoyeXlUazZZZkJxRjhoZmk5cTI0S1BxdklOdWR6ZVNVOD06OjQmFzDi+2x8n2+ZBY+fgT0=', '', 'NON', '44348011', '24770185', '', 'O', 'NGVBa', 'SURXb1d2MjhmeDY3NVpNL2xSMlJGUT09OjpI7EWs51GmMB7wF4', 'L3VkR', '2018-06-15', '', '', '', 'WG50ZmZBV2lCR1kwT0xBL0hybkc4UT09OjpDXA2cwxsJJGWiDlDRMLLG', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 'j.cappel@pnr-scarpe-escaut.fr', '13b6f33ff0fcb3cbabe810494097a3058308d6b2f6e4f96bfbed2399c2452db46168fc6a0d92e47099a67779a02d45db248fe4cb2ba7ed458b537d016158536d', 0, '', '', '2019-05-28 10:38:32', 'dmxZWFJ4WUI1SkZpQkZUVkszT2R4UT09OjoWTNgqiQ1+7aZD659Wkn56', 'QTJwMTF1TThRWEtvVmZVTERSTWpKZz09Ojo8gKmDbgBKdSSKVN0yDDZJ', NULL, NULL, 'dzJFeVpacXVWVk5JR2hSbXg0a2lHdz09OjrBl3iTqMnmYAEHejuyP0aG', 'NitXRUVKejVIQXBTQ0lWYTV6eVduUT09OjqMyve9TICiIEAnyv4umdDy', 'SkluVXBGL3Z4TTdlYmVWMzc0N1M0UT09OjrJ7ESfsPiIf7S2NdCMq1x1', 'WEpmQ25rVmd6REhGUWJVaW9FM2cvZz09OjpcghsBceaN5K/rlsx4/aBW', 'aWtnSnZBVXhLQkFNOUxzUUphK2VjUT09OjoZz1OaPCCCmKzZ47wiGI7C', 'L3hkZTk1cndYNjVUbWxZZktVTFZHc3N5V1NueU1MTDJ1SktlcjdIRWM0Yz06OgooB8vH+faldnel1O8HGKw=', 'COWORKER', 'NON', '79245971', '6946241', 'ET0001', 'O', 'RThrN', 'S3V5WHRQSXhYQlhCTkRWN1VhRjZvUT09OjovDxA9Iz/0hnfBnH', 'K2J6Z', '2018-06-15', '', '', '', 'VCtMY1h1djZWK0V0d1hVUFhQVnRVQT09OjrOPRF3kyGayQjMfjm+HI7R', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 'jr.menu@gmail.com', '5f31da414adb0e51b01faf0c75a39679e7503570542706dc12b9c0d10c7cbf3c3f575c53b1b371d4e238a25a3aba074052eabcc6d710389834a3ebc9be835015', 0, '', '', '2019-05-28 10:38:32', 'TmcvVzB3OE1zSDllV05QU2FjKzFQUT09Ojp+KvhADZE2aM2I8rGf+gic', 'TjkvYW9UL1lqN1JlNGEyOVRkK2NvZz09OjqkahrYE2c4twVqqvOWueev', NULL, NULL, 'K1hDWnJnWlEyQzkra0VRdGhXcWQ3dGYzR2tadnpFVzdFTjV4ckhEYXVVOD06OlM/RPJ99m1HKPo5VJWKsiQ=', 'SFRHWFFaMUhLUDRCbGQ1bHJZSnlrQT09OjogcqrjALmjNXTJBWZq80ao', 'MTBaQ2JyS2dRZUM0MUs2bHpRVmhVdz09Ojri8gBBSgMxIr4dDtrv6Dmv', 'TFJBN1U3d2krbzVQeEVaVktmT255QT09OjrnZVvbKGmkBnk2uabnLMbM', 'N0llRjRyNnFkeU1VdHhBbjJRN1hVQT09OjrYnFwKBdNhnaPnAHybdvE0', 'VmNIbmVvcWNNalFlazMrTDg0M08wMTFaSGZhcWRWU0UrL09tWTNYcXdzQT06OrxTUzURB4wJZ2Q3+mkvjVI=', 'COWORKER', 'OUI', '96861985', '153785', 'ET0001', 'O', 'S2J2e', 'RDB1SEVmSEE4VXRuendJb1JwcDFoUT09OjoHOqwpMP1ZInku6e', 'eXlRM', '2018-06-15', '', 'ET0001', 'NULL', 'SWM0KytlNmRQc3A3aU93U1F3STZPZz09OjpqBN7jiKC/VaVjgYcY/afM', '2019-01-09 10:53:50', '0000-00-00 00:00:00', NULL, NULL),
(13, 'louis.veyer@ateliers-art-strong.fr', 'cd643a804a315d92b1cedc17b07011a0dece14f1f5661f661641fc77f74af9720b7e8222cc463e11e78cb7dd74e0f02e288d4d90cf83c72d089360ce82f88bba', 0, '', '', '2019-05-28 10:38:32', 'Q3pHOWlWVEU4dVJLN1daVDZyS2Nodz09OjoXQxLygxKV57oZFA+OPDlM', 'V3Z5V012Rjh4V1VFcEtNdHdDS0lWUT09OjoTeytfcW5+60x2Tgqp1koa', NULL, NULL, 'UmV0YUh1UFRiYk1OZlQyK2FNenZTUT09OjovMAshOdw20ZaMWEUtm/W5', 'N2J5c05zVHlyR1EydTBlRFNxMzB5dz09OjovSVFNN7KWY/3qvrb6t4Vv', 'VWRueVNTZTBmK3I1VE9jcFR2cys3dz09OjrRkpdhkIwrVqWQEEH4Slr7', 'aHpIa1ZhVHNYZy9LVmNZRndNZElMZz09Ojp0zK/GqQbrHjH0M2QOFKCY', 'UnVYSldHWW5xRHJmOTRYVWwxWi90dz09Ojo155foGh2/OdsuKXuKPx9J', 'UHVmTkVTM3E3c0MzV3d0TWJqbUpCZmZEckRxUE1MUSs5bm8wMEdFK2dyL1daOXU3TXhpTnk0aWRValo0YjlsYzo6ZxzIOJdeuuyB/aZM/+5XYw==', 'COWORKER', 'NON', '95582205', '83346794', 'ET0001', 'O', 'STlHT', 'RjMvRklITHhUcUsyYlJBcnFzazg4dz09Ojq8NdZmSmeYne7HAU', 'eFYzU', '2018-06-15', '', '', '', 'ZkdZaGZvOVJlSENMSEhYS0NNL1NGZz09OjqFzrwzVHkXTgku+uHjQmZf', '2018-09-03 16:25:02', '0000-00-00 00:00:00', NULL, NULL),
(14, 'martial.dubois28@outlook.fr', 'f4ba30d2d1284fa2a5cfed649adfe87ed85a47692b29883118a19c525968ee6b8ae50423a45e68e455f7fec353bb2ae7a78ad5a05316973621a49c639a01da5e', 0, '', '', '2019-05-28 10:38:32', 'ZU9nVGQ3QTl5T3hZYXRXdTJLelY0UT09OjoytySS+K4ghDgsMdwJe7TO', 'SjFBRjJ2MVJoZ3VXa29qUDEyRGZQUT09OjoG701bokQtwFD2ukQAxYl0', NULL, NULL, 'dE4xM2VyeldwUVo1enY1N2RyWDRndz09OjoUHXkJsJJ3cb5pJ0X49ZQ3', 'ekliTXFudHZhNTRIM3VNOUg5SDlLdz09OjreA6wTSG8RguZj/9upwNbS', 'QkpVYWppU1JMUXhmYTloRk8ySU5Ndz09Ojp7sIgxdhd46z7yk7+NG9vI', 'VFBqb2RnY251Ky92SkxpWm01TVNPZz09Ojpgru80ppi5gssQ3MgKzPfW', 'YjV2K1J4WE5hZ2NVNURDVndzZFlMUT09OjroQJ0kp8qYYgrDpmbC/bIs', 'eUVya211VjMzOEVta05mRHRqbDA2VWFBWHE4VlRidm02ZGtTd3M2NjJ0UT06Ou55mfr4abTwyIgwhmk7yDU=', '', 'NON', '58084865', '33360535', '', 'O', 'WmkzM', 'MEVBSmxnRThXYkJhcGxKNkRhM3p4UT09OjqO80HzmcwQRC0fxR', 'Sy9BL', '2018-06-15', '', '', '', 'K0RMeG1wa2QwU3lZZ1g2emZIM05XZz09OjoYYfApU0sS1c1Dtjrxu5hW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(15, 'mathilde@transversaldesign.fr', '5f7ecdd8c6412134009f9c9a83759f7095101f0a15cd5187ba37c526f38c3ddedb94820b51b4b8815e136a09646791bcc96332fe2529a54941b86a3590262108', 0, '', '', '2019-05-28 10:38:32', 'UzhtNjB5bUJYd1JhcEFqKzY2SHdIZz09OjrSbPj50n5U1xwXjQrwmkeW', 'd1gySGVPZ092bzAvMzdmR2tYdkRBQT09OjqabCJYx5A2YH3gdC1gaMgN', NULL, NULL, 'dXlUVG1YTEQxYUxkSDN5RVZ0RTcvdmMwSmYxMnpzbWdXakwyMlFIY1N1Yz06OjkuEBJNxEmgIqrlg1NPrLk=', 'WGF4K3VldDlSbEFPYTlKeXA2Y0VXZz09Ojr/slouOKEBkU+Vlda5Rxnu', 'VTI0aTZ6SVhLd3N6akRYcWFDLys0UT09Ojru7Q02tP6J/E5l/dhilWYL', 'Z0c1OWlyQmt0Q3lPUi9paW9zcjFWZz09OjoTuhrN+ADu4vk6YRPsQCAZ', 'c2Q4SklUTldIUE8ySTdFek9uYlJFUT09Ojrx1h9641Gr3lZJjjhwFmXF', 'd3VBTTNQS0QveVdieEg3Q2huTHZMcmNmMWNkcmIrdXh2ZXNlOXdua3JzRT06Os94ACPodEHXPioVUpswbyo=', 'COWORKER', 'NON', '21384991', '1652394', 'ET0001', 'O', 'QWU5Q', 'ODB4anZDRTFVUTVNTDJwY0w2TEtmdz09OjqM90+pxNDd+S8LWs', 'NmsxN', '2018-06-15', '', '', '', 'RStHV1NvTXgyakFtaVlWRW5FTmlKZz09OjrxpXTPsUWxqFazZDC7/Atn', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(16, 'nbernier@centre-britannique.org', 'a28abae30be27a0305b87c9f8dee1432924acdfaf8cf3d5dac66270660812091b877d7f8b5f41e1ab5ff0c6e453881f6be8c596fc367c5a1c680e36cd82488fe', 0, '', '', '2019-05-28 10:38:32', 'UEpLc25UT3V0NmJtcDZ3aUROVldrQT09OjpkoyBPrzCoq41WMTelD12n', 'Mkx0ckV0b1JIVUF4SEpxUjh6TDhBUT09Ojpv8XfGx5ksMtzY7BI28zkK', NULL, NULL, 'Vk1XMlNKTzFZOGgrTVVOQ1d2YjdMT2NPWHd2VmFHZWlFQ3duOW9lNGRwTT06OkbkhFGxHVUW8bGc9mpfZGc=', 'ckFvd3ZMS2MvNFlLQ3Z3MWRacUNmQT09OjrRsZ3v3F9h6kI/WdVoq2fV', 'K2tUbytaKzUyR0pXekpkeXJKNU9Odz09OjpsJH4lhM3WPefzFSz9eCg0', 'UUQ1MGR6MkYrbVBTOU5YQzQxZEh1UT09Ojp9I533Ovx0ql2utMDRaPgF', 'TVdweGVvRUdwRDNyUHRnN2ZtRXlwQT09OjrCtoTYowpMIvp/NLh0R0eK', 'Szhpb1BPaVZYSGtzdy81NGFNYWhwY3FoT2RUYzR5RUNhRHlERGJLbnp4dz06OreISJ7UiL3aWDcsik9yAbc=', 'COWORKER', 'NON', '2843025', '24847455', 'ET0001', 'O', 'Y1hKU', 'QmtrWGFrdVpSSVlFc1dCYWxjRUYrUT09OjocU4s/EytOv/WWPG', 'VUtiM', '2018-06-15', '', '', '', 'SkxTUVJKYTZYTW1VbzhDb29Ybk5aUT09OjrPFb8tOuCFnxr6O5+iFxkv', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(17, 'perm1', '3a98873e9348234cf6c9af3a1c67aca1199d8d2837c2268eb83b28441e3c468e2ad6d5998c951a07bf85277453fbf83f48034b9c20cb85c6314ed0acff4600e0', 0, '', '', '2019-05-28 10:38:32', 'dTlWOWZPZ21oZWNEUDc1SFV3RGx3QT09OjomwLEaYuvSZEhCPWMwJJZI', 'RjdFa1IwK0gvOGNTTmdERUVVM2VpUT09OjrSfPQ82MblnyMqSv24FPfI', NULL, NULL, 'VDBRc1ovU00wWEJXRUl0bmFJL05RZz09OjrMcqr1Zghsqzy4OsH57UEc', 'c2hXTjZxbDd6c1JMMGJrdjhkNytpdz09OjovQhmBVTrONh50o427ns18', 'Ui92SjU1eUVPaGdDSjdwY1hlN3ovZz09Ojp1QGiV7/KDhSv/RTolH0Z7', 'RTZMQU91czZxa011aHNndkpwazNmQT09OjrrN5k6C4oqZgETDrDSGvPs', 'VE4vOW5RRUZqdS9qOUNnTkFiSCtkUT09OjrOdhnodkZFj47lCUM3Zl7g', 'QWZOYUlhT29uRjlwMCt3S2g3UmovUGQ0UUpJNjloUk1mQW1VMFM2MlhLVT06OpwzHThRdymKKqGvTMa+fbk=', 'COWORKER', 'NON', '2524643', '87781290', 'ET0001', 'O', 'c0tib', 'YUhQWEpZTjJJZXZYMWc3WmFkeWNIUT09OjqmIx7Difg9x5mMqM', 'Mll6b', '2018-06-15', '', 'ET0002', '', 'dlErUEtwU2RzMUxIWlNNSVM2SWNMQT09Ojqit+6C2bL2vsBgDqt84SHO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(18, 'test1', '3a98873e9348234cf6c9af3a1c67aca1199d8d2837c2268eb83b28441e3c468e2ad6d5998c951a07bf85277453fbf83f48034b9c20cb85c6314ed0acff4600e0', 0, '', '', '2019-05-28 10:38:32', 'Wjc5MUl5bnkyQjEzRm1OZ3JFb0tJUT09OjpzpuKo9zPwGF2+91HzWm09', 'QUE1S3lTOFFYRWxISDUxam5WUnp4UT09OjqgIRwAZ1rNb3sFvAm+2o4K', NULL, NULL, 'RHUwRjExQTNpZUs1NmR2WmJYL0tuQT09OjqLR7uginLtscp05+hbWR+c', 'a3BhVTJxcmdsSjdyZlhTUFMvaVJMbUsyeHdNNzAwc1BQUVU0c2MzUWE5UT06Og/D8bcX27PF4jHqLqh3oGE=', 'ZlB3ZTU4RXhCa3Y5SXlHR3hPVVlVQT09Ojo03p3IXjypo3bKJ61xyjVL', 'OXJRRmJaUTVLYm5ONEliR1c4am9mdz09Ojoh7XRVKJI0S9j6yPDx/vbq', 'QWZBRFp0bDdMejl1a3d0MGVLbXY4Zz09OjqUIJyRqohCdEoinaR4bqDL', 'UThwNmV2REFwcHJqMFIyS0VFaHlwakp1VHpLRTIxMFNvNGNZd3pjeUlYZz06OsJy6ZpZISCOcXfaOjt5f/M=', 'COWORKER', 'NON', '2524643', '87781290', 'ET0001', 'O', 'TkdYR', 'QkE5SzlOWW9SWGg0VHduKzZPU0poQT09Ojr7HbyQh0++4Q0B3A', 'M2Fob', '2018-06-15', '', '', '', 'Wk1IUWpYWW4vT3VETnRmVThwWG1MUT09OjpQzJ6mmeJuDB14vAkHCPFX', '2018-09-12 23:26:58', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateursav`
--

CREATE TABLE `utilisateursav` (
  `UT_IDUSER` int(10) NOT NULL,
  `UT_LOGIN` varchar(250) NOT NULL DEFAULT '',
  `UT_PASSWORD` varchar(250) NOT NULL,
  `UT_NOM` varchar(250) NOT NULL,
  `UT_PRENOM` varchar(250) NOT NULL,
  `UT_ADRESSE1` varchar(250) NOT NULL,
  `UT_ADRESSE2` varchar(250) NOT NULL,
  `UT_VILLE` varchar(250) NOT NULL,
  `UT_CODEPOSTAL` varchar(250) NOT NULL,
  `UT_PAYS` varchar(250) NOT NULL,
  `UT_EMAIL` varchar(250) NOT NULL,
  `UT_STATUT` varchar(20) NOT NULL,
  `UT_ID1` varchar(20) NOT NULL,
  `UT_ID2` varchar(20) NOT NULL,
  `UT_ETABLISSEMENT` varchar(6) NOT NULL,
  `UT_VALIDE` varchar(1) NOT NULL DEFAULT 'N',
  `UT_INITIALE` varchar(5) NOT NULL,
  `UT_IDMODIF` varchar(50) NOT NULL,
  `UT_CIVILITE` varchar(5) NOT NULL,
  `UT_IDDATEDEMANDE` date NOT NULL,
  `UT_TIERSGESTION` varchar(50) NOT NULL,
  `UT_ADMINSITE` varchar(6) NOT NULL,
  `UT_VERIF` varchar(20) NOT NULL,
  `UT_TEL` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `voucher`
--

CREATE TABLE `voucher` (
  `BA_NUMERO` int(6) NOT NULL,
  `BA_LOGIN` varchar(20) NOT NULL,
  `BA_ETABLISSEMENT` varchar(10) NOT NULL,
  `BA_LIBELLE` varchar(30) NOT NULL,
  `BA_VALEUR` decimal(5,0) NOT NULL,
  `BA_DATECREATION` date NOT NULL,
  `BA_DATEREPRISE` date NOT NULL,
  `BA_VALIDE` varchar(3) NOT NULL,
  `BA_REFREPRISE` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `voucher`
--

INSERT INTO `voucher` (`BA_NUMERO`, `BA_LOGIN`, `BA_ETABLISSEMENT`, `BA_LIBELLE`, `BA_VALEUR`, `BA_DATECREATION`, `BA_DATEREPRISE`, `BA_VALIDE`, `BA_REFREPRISE`) VALUES
(7, '28-LV', 'ET0001', 'Promo adhÃ©sion', '5', '2018-08-20', '1900-01-01', 'OUI', ''),
(8, 'jr.menu@gmail.com', 'ET0001', 'Promo adhÃ©sion', '5', '2018-08-20', '1900-01-01', 'OUI', '');

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

CREATE TABLE `zone` (
  `EM_EMPLACEMENT` int(7) NOT NULL,
  `EM_ETABLISSEMENT` varchar(6) NOT NULL,
  `EM_LIBELLE` varchar(50) NOT NULL,
  `EM_NBRRESAMAX` int(2) NOT NULL,
  `EM_TYPEZONE` varchar(50) NOT NULL,
  `EM_NBRPLACESMAX` int(2) NOT NULL,
  `EM_PRIXPARZONE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `EM_PRIXPARPLACE` decimal(6,2) NOT NULL DEFAULT '0.00',
  `EM_HEUREDEBUT` time NOT NULL,
  `EM_HEUREFIN` time NOT NULL,
  `EM_ARTZONE` varchar(20) NOT NULL COMMENT 'ARTICLE DE L''EMPLACEMENT',
  `EM_ARTPLACE` varchar(20) NOT NULL COMMENT 'ARTICLE DE LA PLACE - NULL SI UNE SEULE PLACE',
  `EM_DEMIJOURNEE` varchar(3) DEFAULT NULL,
  `EM_ARTJOURNEE` varchar(30) DEFAULT NULL,
  `EM_ARTMATIN` varchar(30) DEFAULT NULL,
  `EM_ARTAPM` varchar(30) DEFAULT NULL,
  `EM_ARTPERSONNE` varchar(30) DEFAULT NULL,
  `EM_BLOQUE` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `zone`
--

INSERT INTO `zone` (`EM_EMPLACEMENT`, `EM_ETABLISSEMENT`, `EM_LIBELLE`, `EM_NBRRESAMAX`, `EM_TYPEZONE`, `EM_NBRPLACESMAX`, `EM_PRIXPARZONE`, `EM_PRIXPARPLACE`, `EM_HEUREDEBUT`, `EM_HEUREFIN`, `EM_ARTZONE`, `EM_ARTPLACE`, `EM_DEMIJOURNEE`, `EM_ARTJOURNEE`, `EM_ARTMATIN`, `EM_ARTAPM`, `EM_ARTPERSONNE`, `EM_BLOQUE`) VALUES
(1, 'ET0001', 'Zone Coworking', 5, 'COWORKING', 1, '5.00', '0.00', '00:00:00', '19:00:00', 'ARTCOWORK', '', '', '', '', '', '', 'NON'),
(2, 'ET0001', 'Salle rÃ©union matin', 1, 'SALLE REUNION', 12, '5.00', '1.00', '00:00:00', '19:00:00', 'SALLEMATIN', 'PLACEREUNION', '', '', '', '', '', 'NON'),
(3, 'ET0001', 'Salle rÃ©union aprÃ¨s-midi', 1, 'SALLE REUNION', 12, '5.00', '1.00', '00:00:00', '19:00:00', 'SALLEAPM', 'PLACEREUNION', '', '', '', '', '', 'NON'),
(4, 'ET0001', 'Salle rÃ©union soir', 1, 'SALLE REUNION', 12, '5.00', '1.00', '00:00:00', '19:00:00', 'SALLESOIR', 'PLACEREUNION', '', '', '', '', '', 'NON'),
(5, 'ET0002', 'Zone Coworking', 5, 'COWORKING', 1, '0.00', '0.00', '00:00:00', '00:00:00', '', '', '', '', '', '', '', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD UNIQUE KEY `AR_ARTICLENO` (`AR_ARTICLENO`);

--
-- Index pour la table `choixcode`
--
ALTER TABLE `choixcode`
  ADD PRIMARY KEY (`CC_ID`);

--
-- Index pour la table `compteur`
--
ALTER TABLE `compteur`
  ADD PRIMARY KEY (`CT_NUMERO`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`DO_NUMERO`,`DO_CODE`);

--
-- Index pour la table `emplacement`
--
ALTER TABLE `emplacement`
  ADD PRIMARY KEY (`EM_ETABLISSEMENT`,`EM_EMPLACEMENT`);

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`ET_ETABLISSEMENT`);

--
-- Index pour la table `gestionemail`
--
ALTER TABLE `gestionemail`
  ADD PRIMARY KEY (`GE_NUMERO`);

--
-- Index pour la table `ligne`
--
ALTER TABLE `ligne`
  ADD PRIMARY KEY (`GL_IDLIGNE`);

--
-- Index pour la table `ligneattente`
--
ALTER TABLE `ligneattente`
  ADD PRIMARY KEY (`GLA_IDLIGNE`);

--
-- Index pour la table `lignecompl`
--
ALTER TABLE `lignecompl`
  ADD PRIMARY KEY (`GL2_NUMID`);

--
-- Index pour la table `modepaie`
--
ALTER TABLE `modepaie`
  ADD PRIMARY KEY (`MO_NUMERO`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`RE_NUMRESA`),
  ADD KEY `PL_USER` (`RE_USER`),
  ADD KEY `PL_USER_2` (`RE_USER`);

--
-- Index pour la table `piece`
--
ALTER TABLE `piece`
  ADD PRIMARY KEY (`GP_NUMERO`);

--
-- Index pour la table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`PR_NUMERO`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`RE_NUMRESA`),
  ADD KEY `PL_USER` (`RE_USER`),
  ADD KEY `PL_USER_2` (`RE_USER`);

--
-- Index pour la table `tiers`
--
ALTER TABLE `tiers`
  ADD PRIMARY KEY (`T_NUMERO`);

--
-- Index pour la table `tiersetab`
--
ALTER TABLE `tiersetab`
  ADD PRIMARY KEY (`TE_NUMERO`),
  ADD KEY `TE_NUMERO` (`TE_NUMERO`),
  ADD KEY `TE_LOGIN` (`TE_LOGIN`),
  ADD KEY `TE_ETABLISSEMENT` (`TE_ETABLISSEMENT`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`UT_LOGIN`),
  ADD KEY `UT_IDUSER` (`UT_IDUSER`);

--
-- Index pour la table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`BA_NUMERO`);

--
-- Index pour la table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`EM_EMPLACEMENT`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `AR_ARTICLENO` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `choixcode`
--
ALTER TABLE `choixcode`
  MODIFY `CC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `compteur`
--
ALTER TABLE `compteur`
  MODIFY `CT_NUMERO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
  MODIFY `DO_NUMERO` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `gestionemail`
--
ALTER TABLE `gestionemail`
  MODIFY `GE_NUMERO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `ligne`
--
ALTER TABLE `ligne`
  MODIFY `GL_IDLIGNE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT pour la table `ligneattente`
--
ALTER TABLE `ligneattente`
  MODIFY `GLA_IDLIGNE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `lignecompl`
--
ALTER TABLE `lignecompl`
  MODIFY `GL2_NUMID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT pour la table `modepaie`
--
ALTER TABLE `modepaie`
  MODIFY `MO_NUMERO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `RE_NUMRESA` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `piece`
--
ALTER TABLE `piece`
  MODIFY `GP_NUMERO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT pour la table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `PR_NUMERO` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `RE_NUMRESA` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=545;

--
-- AUTO_INCREMENT pour la table `tiers`
--
ALTER TABLE `tiers`
  MODIFY `T_NUMERO` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tiersetab`
--
ALTER TABLE `tiersetab`
  MODIFY `TE_NUMERO` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `UT_IDUSER` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT pour la table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `BA_NUMERO` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `zone`
--
ALTER TABLE `zone`
  MODIFY `EM_EMPLACEMENT` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
