﻿<?php
/**
 * managezone.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
    // Les informations de connexion sont bonnes, on affiche le contenu protégé
	if((isset($_GET['ACTION'])))
	{
	echo 'oko';
	?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
			<script language="javascript">
		
			$(document).ready(function(){

				
			 $("select").change(function(){
				 
			 var x = $(this).attr('id');
			 alert(x);
			 var idchange = x.substring(14,x.length);
			 $('#paramuser_011_'+idchange).val('OUI');
			});
			});
			
		
				
					function myclosewindow()
			{
			window.close();
			window.opener.location.href="managezone.php"
			}
			</script>
			<!--<link rel="stylesheet" href="lib/file.css">-->
			</head>
			<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:50px;">
			
			<?php
			include ("include/fonction_admresa.php"); 
			
			if ($_GET['ACTION'] == 'manage')
			{
				modif_place();
			}
			//insert_utilsateur();
			
			
			//insert_appel($_POST['Nom_Client'], $_POST['DateCall'], $_POST['HeureCall'], $_SESSION['login'], $_POST['TitreCall'], $_POST['commentaires'], $_POST['Type_Call'], $_POST['NewCategorieCall']);
	  		
			?>
			
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	
	}
	else
	{
		//echo $_POST['BoutonValider'];
		//echo $_POST['IDModif'];
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>

			
			<link rel="stylesheet" href="lib/file.css">
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js" /></script>
			<script type="text/javascript">


			function veriflibzone(id)
			{
				var zoneorg = $('#'+id+'_libzoneorg').text();
				var zone = $('#LIBELLE_'+id).val();
				if (zone != zoneorg)
				{
					for (i = 0; i < 30; i++)
					{
						if ($('#'+i+'_infolibzone').text() == zone)
						{
							alert ('Une zone portant le nom '+zone+' existe déjà !');
							$('#LIBELLE_'+id).val(zoneorg);
						}
					}
				}
			}
			
			function changeart1(id)
			{
				var article = $("select[name='ART1_"+id+"'] > option:selected").val();
				$('#HEUREDEBUT_'+id).val($('#'+article+'_HeureDebut').text());
				$('#AFFHEUREDEBUT_'+id).val($('#'+article+'_HeureDebut').text());
				$('#HEUREFIN_'+id).val($('#'+article+'_HeureFin').text());
				$('#AFFHEUREFIN_'+id).val($('#'+article+'_HeureFin').text());
				$('#PRIX_'+id).val($('#'+article+'_PrixArticle').text());
				$('#ARTZONE_'+id).val(article);
			}
			
			function changezone(id)
			{
				var article = $("select[name='TYPEZONE_"+id+"'] > option:selected").val();
				if (article == 'COWORKING')
				{
					var index = ((id) * 100) + 5;
					$('#modifzone_' + index).hide();
					var index = ((id) * 100) + 6;
					$('#modifzone_' + index).hide();
					$('#PRIXPARPERSONNE_'+id).val(0);
					$('#NBRPLACE_'+id).val(1);
					$('#ARTPLACE_'+id).val('');
				}
				else
				{
					var index = ((id) * 100) + 5;
					$('#modifzone_' + index).show();
					var index = ((id) * 100) + 6;
					$('#modifzone_' + index).show();
				}
			}
			
			function changeart2(id)
			{
				var article = $("select[name='ART2_"+id+"'] > option:selected").val();
				$('#PRIXPARPERSONNE_'+id).val($('#'+article+'_PrixArticle').text());
				$('#ARTPLACE_'+id).val(article);
			}
			
			
			function afficher(id)
			{
				var j;
				for (j = 0; j < 30; j++)
				{
					var index = (id) + j;
					$('#modifzone_' + index).show();
				}
				var j;
				for (j = 0; j < 30; j++)
				{
					var index = (j) * 100;
					$('#modifzone_' + index).hide();
					if (index != id)
					{
						for (k = 31; k < 37; k++)
						{
							var sousindex = index + k;
							$('#modifzone_' + sousindex).hide();
						}
					}
					var index = ((j) * 100) + 15;
					$('#modifzone_' + index).hide();
				}
				if (id == 0)
				{
					$('#IDModif').val('100');
				}
				else
				{
					var j = (id) / 100;
					$('#IDModif').val(j);
				}
			};
			
			function cacher(id)
			{
				var j;
				for (j = 0; j < 30; j++)
				{
					var index = (id) + j;
					$('#modifzone_' + index).hide();
				}
				var j;
				for (j = 0; j < 30; j++)
				{
					var index = (j) * 100;
					$('#modifzone_' + index).show();
					
					if (index != id)
					{
						for (k = 31; k < 37; k++)
						{
							var sousindex = index + k;
							$('#modifzone_' + sousindex).show();
						}
					}		
					var index = ((j) * 100) + 15;
					$('#modifzone_' + index).show();
					
				}
				
				var j = (id) / 100;
				$('#IDModif').val('');
				
			};
			
			


			</script>
			</head>
			<body >
			<?php
			include ("include/fonction_admresa.php"); 
			admentete_page("Gestion des zones disponible");
			//barre_menu(); 
			if (!empty($_POST['IDModif']))
			{
				if ($_POST['IDModif'] == 100)
				{
					insert_zone();
				}
				else
				{
					update_zone();
				}
			}
			affiche_zone();

			//footer(); 
			?>
			
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
    <?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur
    
    header('Location: index.php');


}
?>