<?php

// Afficher les erreurs à l'écran
require ("../scripts/constantes.php");
require ("../scripts/fonctions.php");
require('fpdf.php');

define('EURO', chr(128));

$joursemaine = array('dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi');


$sql = "SELECT *, date_format(TE_DATEVALIDATION,'%d/%m/%Y') as DATEVALIDATION,
			 date_format(TE_DEBUTADHESION,'%d/%m/%Y') as DATEDEBUTADH, date_format(TE_DATEFINADHESION,'%d/%m/%Y') as DATEFINADH,
			 case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK
			 FROM TIERSETAB
			 LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
			 LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
			 WHERE TE_LOGIN = '".$_GET['adherant']."' AND TE_ETABLISSEMENT = '".$_GET['etablissement']."'";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
	{
		
		if (($data['TE_STATUT'] == 'VALIDE') && ($data['ADHESIONOK'] == 'OUI' ))
		{
			$statut = 'OUI';
		}
		else
		{
			$statut = 'NON';
		}

		$nometab = $data['ET_LIBELLE'];
		$adresseetab  = $data['ET_ADRESSE1'];
		$villeetab = $data['ET_CODEPOSTAL'] .' ' .$data['ET_VILLE'];
		$NomClient = decrypt2($data['UT_CIVILITE'],$data['UT_ID2']) .' ' . decrypt2($data['UT_NOM'],$data['UT_ID2']) .' ' . decrypt2($data['UT_PRENOM'],$data['UT_ID2']) ;
		$EmailClient = decrypt2($data['UT_EMAIL'],$data['UT_ID2']);
		$emailetab  = $data['ET_EMAIL'];
		$webetab  = $data['ET_SITEWEB'];
		$contactadh = decrypt2($data['UT_CONTACTNOM'],$data['UT_ID2']) .' ' . decrypt2($data['UT_CONTACTPRENOM'],$data['UT_ID2']) ;
		$Adresseadh = decrypt2($data['UT_ADRESSE1'],$data['UT_ID2']);
		$Villeadh = decrypt2($data['UT_CODEPOSTAL'],$data['UT_ID2']) .' ' .decrypt2($data['UT_VILLE'],$data['UT_ID2']);
		if (!empty($data['TE_VALIDEUR']))
		{
			$valideur = decrypt2($data['TE_VALIDEUR'],$data['TE_ID']);
		}
		$DateValidation = $data['DATEVALIDATION'];
		$DateAdhesion = 'Du ' .$data['DATEDEBUTADH'] . ' au ' .$data['DATEFINADH'];
		$typeadherant = $data['UT_TYPE'];
                                             
		$napetab = $data['ET_NAP'];
		$CodeCient = $data['TE_LOGIN'];
		$logo = "../img/".$data['ET_IMAGENOM'];
		if ((!isset($data['TE_TEXTCOCHE1'])) || ($statut == 'NON'))
		{
			$textecoche1 = $data['ET_ADHESIONCOCHE1'];
			$ValidCoche1 = "NON";
		}
		else
		{
			$textecoche1 = $data['TE_TEXTCOCHE1'];
			$ValidCoche1 = $data['TE_COCHE1'];
		}
		
		if ((!isset($data['TE_TEXTCOCHE2'])) || ($statut == 'NON'))
		{
			$textecoche2 = $data['ET_ADHESIONCOCHE2'];
			$ValidCoche2 = "NON";
		}
		else
		{
			$textecoche2 = $data['TE_TEXTCOCHE2'];
			$ValidCoche2 = $data['TE_COCHE2'];
		}
		if ((!isset($data['TE_TEXTRGPD'])) || ($statut == 'NON'))
		{
			$textergpd = $data['ET_ADHESIONRGPD1'];
		}
		else
		{
			$textergpd = $data['TE_TEXTRGPD'];
		}



	}




class PDF extends FPDF
{

	function Header()
	{
		global $nometab;
		global $adresseetab;
		global $villeetab;
		global $NomClient;
		global $EmailClient;
		global $emailetab;
		global $webetab;
		global $contactadh;
		global $Adresseadh;
		global $Villeadh;
		global $typeadherant;
		global $valideur;
		global $DateValidation;
		global $DateAdhesion;
		global $DateFinAd;
		global $ValidCoche1;
		global $ValidCoche2;
		global $napetab;
		global $CodeCient;
		global $logo;
		global $textecoche1;
		global $textecoche2;
		global $textergpd;
		global $statut;

		$this->Image($logo,10,6,80);
		$this->Rect(100,10,90,20);
		$this->SetFont('Arial','B',10);
		$this->SetX(-105);
		$this->Cell(0,10,$nometab,0,1,'L');
		$this->SetX(-105);
		$this->Cell(0,0,$adresseetab,0,1,'L');
		$this->SetX(-105);
		$this->Cell(0,10,$villeetab,0,1,'L');
		$this->SetFont('Arial','B',15);
		//$this->SetX(-80);
		$this->Ln(3);
		$this->Cell(0,10,"BULLETIN D'ADHESION",0,1,'C');
		$this->SetFont('Arial','B',10);
		if ($statut == 'NON')
		{
			$this->Cell(0,5,"ADHESION EN ATTENTE DE VALIDATION",0,1,'C');
		}

		$this->Ln(2);
		$this->SetFont('Arial','',8);
		$this->Cell(0,10,utf8_decode("Nom Prénom ou dénomination de l'adhérant : "),0,0,'L');
		$this->SetFont('Arial','I',8);
		$this->SetX(-120);
		$this->Cell(0,10,$NomClient,0,1,'L');
		$this->SetFont('Arial','',8);
		$this->Cell(0,0,utf8_decode("Email de l'adhérant : "),0,0,'L');
		$this->SetFont('Arial','I',8);
		$this->SetX(-165);
		$this->Cell(0,0,$EmailClient,0,1,'L');
		$this->SetFont('Arial','',8);
		if ($statut == 'OUI')
		{
			$this->Cell(0,10,utf8_decode("Période d'adhésion : "),0,0,'L');
			$this->SetFont('Arial','I',8);
			$this->SetX(-165);
			$this->Cell(0,10,$DateAdhesion,0,1,'L');
		}
		else
		{
			$this->Cell(0,10,utf8_decode("Période d'adhésion : "),0,1,'L');
		}
		$this->SetFont('Arial','',8);
		$this->Cell(0,0,utf8_decode("Agissant en qualité de : "),0,0,'L');
		$this->SetFont('Arial','I',8);
		$this->SetX(-160);
		$this->Cell(0,0,'Personne ' .$typeadherant,0,1,'L');
		$this->SetFont('Arial','',8);
		$this->Cell(0,10,utf8_decode("Adresse : "),0,0,'L');
		$this->SetFont('Arial','I',8);
		$this->SetX(-180);
		$this->Cell(0,10,$Adresseadh .' ' .$Villeadh,0,1,'L');
		$this->Rect(10,$this->GetY()+4,4,2);
		if ($ValidCoche1 == 'OUI')
		{
			$this->Cell(0,10,"x",0,0,'L');
		}
		$this->SetFont('Arial','I',8);
		$this->SetX(-195);
		$this->MultiCell(0,4,utf8_decode($textecoche1),0,"L");
		$this->Rect(10,$this->GetY()+4,4,2);
		if ($ValidCoche2 == 'OUI')
		{
			$this->Cell(0,10,"x",0,0,'L');
		}
		$this->SetFont('Arial','I',8);
		$this->SetX(-195);
		$this->MultiCell(0,4,utf8_decode($textecoche2),0,"L");
		$this->Ln(1);
		$this->MultiCell(0,4,utf8_decode($textergpd),0,"L");
		if ($statut == 'OUI')
		{
			$this->Ln(3);
			$this->Cell(0,10,utf8_decode("Adhésion validée le : ") .$DateValidation .'  par : ' .utf8_decode($valideur),0,0,'L');
			
		}
		
		// Saut de 'L'igne

	}


}

$pdf = new PDF('L','mm','A5');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,80);

$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(224,235,255);
$numligne = 0;
$libligne = '';
$nbrligneprint = 1;




//for ($i = 1; $i <= 50; $i++) {
//    $pdf->Cell(0,6,'test',1,1,'L',false);
//}
$pdf->Output();

?>
