<?php
/**
 * list_resa.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: install_mysql.php,v 1.9 2009-10-09 07:55:48 Gestion Coworking Exp $
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

require('include/fpdf.php');


define('EURO', chr(128));


session_start ();


$months = array("janvier", "fevrier", "mars", "avril", "mai", "juin",
			"juillet", "aout", "septembre", "octobre", "novembre", "decembre");
$joursemaine = array('dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi');			
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "SELECT * FROM `ETABLISSEMENT` WHERE ET_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

$req = $conn->query($sql) or die('Erreur SQL !<br>');
while($data = mysqli_fetch_array($req))
	{
		$nometab = $data['ET_LIBELLE'];
		$adresseetab  = $data['ET_ADRESSE1'];
		$villeetab = $data['ET_CODEPOSTAL'] .' ' .$data['ET_VILLE'];
		//$DateFacture = 'Le Quesnoy, le ' .$joursemaine[$data['NUMJOUR']] .' ' .$data['JOUR'] .' ' .$months[$data['Mois']-1] .' ' .$data['YEARPIECE'];
		

	}


class PDF extends FPDF
{

function Header()
{
	global $nometab;
	global $adresseetab;
	global $villeetab;
	//global $NomClient;
	//global $EmailClient;
	//global $NomFacture;
	//global $AdresseFacture;
	//global $VilleFacture;
	//global $RefFacture;
	//global $DateFacture;
	$this->Image('img/logo-la-verriere.png',10,6,80);
    $this->SetFont('Arial','B',15);
	$this->SetX(-80);
    $this->Cell(0,10,utf8_decode('Détail des saisie et réservation'),0,1,'L');
	$this->Ln(10);
	$this->Cell(0,10,$nometab,0,1,'L');
	$this->SetFont('Arial','B',10);
	$this->Cell(0,8,$adresseetab,0,1,'L');
	$this->SetFont('Arial','B',10);
	$this->Cell(0,8,$villeetab,0,1,'L');
	$this->SetX(-80);
	// Saut de ligne
    
}



function Footer()
{
	global $TotalHT;
	global $TotalQteFact;
	$this->SetY(-55);
	//$this->Ln(5);
	//$this->SetX(-90);
	
//	$this->SetFillColor(0,204,255);
//	$this->SetTextColor(255);
//	$this->SetDrawColor(0,0,0);
//	$this->SetLineWidth(.3);
//	$this->SetFont('Arial','B',10);
//	$this->Cell(50,6,'Code Article',1,0,'L',true);
//	$this->Cell(30,6,'Designation',1,0,'L',true);
	
//	$this->SetFillColor(224,235,255);
//  $this->SetTextColor(0);
//  $this->SetFont('Arial','',10);
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro de page
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,60);

$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(224,235,255);
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
if (isset($_GET['mois']) )
{
	if ($_GET['facture'] == 'OUI')
	{
	$sql = "select RE_USER,DATE_FORMAT(RE_DATE, '%d/%m/%Y') as DATE, RE_LIBELLEEMPLACEMENT, RE_NBRPLACE, RE_ZONE, RE_ZONELIBELLE, RE_FACTURE from RESERVATION 
			WHERE RE_USER = '" .$_GET['user']. "' AND RE_ANNEE = " .$_GET['an']." AND RE_MOIS = ".$_GET['mois']." AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'OUI' AND RE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."' 
			ORDER BY RE_USER, RE_ZONE, RE_FACTURE, RE_DATE";
	}
	else
	{
		if (isset($_GET['user']))
		{
			$sql = "select RE_USER,DATE_FORMAT(RE_DATE, '%d/%m/%Y') as DATE, RE_LIBELLEEMPLACEMENT, RE_NBRPLACE, RE_ZONE, RE_ZONELIBELLE, RE_FACTURE from RESERVATION 
					WHERE RE_USER = '" .$_GET['user']. "' AND RE_ANNEE = " .$_GET['an']." AND RE_MOIS = ".$_GET['mois']." AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'  
					ORDER BY RE_USER, RE_ZONE, RE_FACTURE, RE_DATE";	
		}
		else
		{
			$sql = "select RE_USER,DATE_FORMAT(RE_DATE, '%d/%m/%Y') as DATE, RE_LIBELLEEMPLACEMENT, RE_NBRPLACE, RE_ZONE, RE_ZONELIBELLE, RE_FACTURE from RESERVATION 
					WHERE RE_ANNEE = " .$_GET['an']." AND RE_MOIS = ".$_GET['mois']." AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."' 
					ORDER BY RE_USER, RE_ZONE, RE_FACTURE, RE_DATE";	
		}
	}
}
else
{
	if ($_GET['facture'] == 'OUI')
	{
	$sql = "select RE_USER,DATE_FORMAT(RE_DATE, '%d/%m/%Y') as DATE, RE_LIBELLEEMPLACEMENT, RE_NBRPLACE, RE_ZONE, RE_ZONELIBELLE, RE_FACTURE from RESERVATION 
			WHERE RE_USER = '" .$_GET['user']. "' AND RE_ANNEE = " .$_GET['an']." AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'OUI' AND RE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."' 
			ORDER BY RE_USER, RE_ZONE, RE_FACTURE, RE_DATE";
	}
	else
	{
	$sql = "select RE_USER,DATE_FORMAT(RE_DATE, '%d/%m/%Y') as DATE, RE_LIBELLEEMPLACEMENT, RE_NBRPLACE, RE_ZONE, RE_ZONELIBELLE, RE_FACTURE from RESERVATION 
			WHERE RE_USER = '" .$_GET['user']. "' AND RE_ANNEE = " .$_GET['an']." AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'NON' AND RE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."' 
			ORDER BY RE_USER, RE_ZONE, RE_FACTURE, RE_DATE";	
	}
}

$numpage=0;
$newnumpage=1;
//$zone='';
//$usergrp='';
$req = $conn->query($sql) or die('Erreur SQL !<br>');
while($data = mysqli_fetch_array($req))
	{
		if ($numpage != $newnumpage)
		{
			//echo $numpage;
			//echo $pdf->gety() .' - ';
			$usergrp = '';
			$zone = '';
			//$pdf->Cell(30,6,$data['RE_FACTURE'] .$numpage .$pdf->PageNo(),1,1,'C',false);
		}
		$newnumpage = $pdf->PageNo();
		if ($data['RE_USER'] != $usergrp)
		{
			$pdf->Ln(5);
			$pdf->SetFillColor(27,158,111);
			$pdf->SetTextColor(255);
			$pdf->SetDrawColor(0,0,0);
			$pdf->SetLineWidth(.3);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(180,6,utf8_decode('UTILISATEUR ' .$data['RE_USER']),1,1,'C',true);
			$pdf->SetTextColor(0);
			$usergrp = $data['RE_USER'];
			$zone='';
		}
		if ($data['RE_ZONE'] != $zone) 
		{
			$pdf->Ln(5);
			$pdf->SetFillColor(27,158,111);
			$pdf->SetTextColor(255);
			$pdf->SetDrawColor(0,0,0);
			$pdf->SetLineWidth(.3);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(180,6,utf8_decode('ZONE ' .$data['RE_ZONELIBELLE']),1,1,'C',true);
			$pdf->Cell(40,6,'Date',1,0,'C',true);
			$pdf->Cell(70,6,'Designation',1,0,'C',true);
			$pdf->Cell(40,6,'Nbre place','TLR',0,'C',true);
			$pdf->Cell(30,6,utf8_decode('Cloturé'),1,1,'C',true);
			$pdf->SetTextColor(0);
			//$pdf->Rect(10,101,110,132);
			$zone = $data['RE_ZONE'];
		}
		
		$pdf->Cell(40,6,$data['DATE'],1,0,'C',false);
		$pdf->Cell(70,6,utf8_decode($data['RE_LIBELLEEMPLACEMENT']),1,0,'C',false);
		$pdf->Cell(40,6,$data['RE_NBRPLACE'],1,0,'C',false);
		$pdf->Cell(30,6,$data['RE_FACTURE'] ,1,1,'C',false);
		$numpage = $pdf->PageNo();
	}

//$pdf->Cell(30,6,GetPageHeight(),1,1,'C',false);
//for ($i = 1; $i <= 50; $i++) {
//    $pdf->Cell(0,6,'test',1,1,'L',false);
//}
$pdf->Output();
?>
